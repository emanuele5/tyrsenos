When I was little in 2008, my first PC used Windows XP.
It was a simpler time.

This is a project born of hubris and nostalgia.
To prove myself, and prove that a modern lightweight operating system is still possible.

(that same PC dual-boots Sunflower and Linux Mint BTW)
