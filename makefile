export ROOTDIR=$(PWD)/rootdir
export CC=/usr/local/cross/bin/x86_64-elf-sunflower-gcc -O3 -Wall
export OBJCOPY=objcopy

kernel:
	cd src && $(MAKE)

user:
	mkdir -p $(ROOTDIR)/boot/prog/bin
	mkdir -p $(ROOTDIR)/boot/prog/lib
	cd usrsrc && $(MAKE)

all: kernel user

image: all
	cp src/sunflower.bin rootdir/boot
	qemu-img create sunflower.img 64M
	mkfs.minix sunflower.img>/dev/null
	mkdir -p loop
	sudo mount -t minix -o loop sunflower.img loop
	cp -r rootdir/boot/* loop
	sudo umount loop
	dd if=src/boot/boot.bin of=sunflower.img conv=notrunc

test: image
	qemu-system-x86_64 -s -drive id=disk,file=sunflower.img

clean:
	(cd src;make clean)
	(cd usrsrc;make clean)

.PHONY: clean test image user kernel
