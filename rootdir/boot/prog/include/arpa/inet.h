#pragma once

#include <netinet/in.h>

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
	
	#define htons(x) __builtin_bswap16(x)
	#define htonl(x) __builtin_bswap32(x)
	#define ntohs(x) __builtin_bswap16(x)
	#define ntohl(x) __builtin_bswap32(x)
	
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__

	#define htons(x) (x)
	#define htonl(x) (x)
	#define ntohs(x) (x)
	#define ntohl(x) (x)

#endif

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

char* inet_ntoa(struct in_addr in);
const char* inet_ntop(int af,const void* restrict src,char* restrict dst,socklen_t size);

#ifdef  __cplusplus
}
#endif
