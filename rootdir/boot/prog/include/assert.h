#pragma once

#include <stdio.h>
#include <stdlib.h>

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

#ifndef NDEBUG
	extern void __assert(const char* file,int line,const char * func,const char* failedexpr);
	#define assert(expr) ((expr) ? (void)0 : __assert(__FILE__,__LINE__,__FUNCTION__,#expr))
#else
	#define assert(expr) ((void)0)
#endif

#ifdef  __cplusplus
}
#endif
