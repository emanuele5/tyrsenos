#pragma once

#ifdef  __cplusplus
extern "C"
{
#endif

int isalpha(int c);
int isblank(int c);
int isalnum(int c);
int iscntrl(int c);
int isdigit(int c);
int isgraph(int c);
int islower(int c);
int isprint(int c);
int ispunct(int c);
int isspace(int c);
int isupper(int c);
int isxdigit(int c);

int toupper(int c);
int tolower(int c);

int isascii(int c);
int toascii(int c);

// obsolescent macros
#define _tolower(c) ((c)+'a'-'A')
#define _toupper(c) ((c)+'A'-'a')

#ifdef  __cplusplus
}
#endif
