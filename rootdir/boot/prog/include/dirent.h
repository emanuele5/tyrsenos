/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <limits.h>
#include <sys/types.h>
	
#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

typedef struct
{
	int fd;
}DIR;

struct dirent
{
	ino_t d_ino;
	off_t d_off;
	uint8_t d_namelen;
	char d_name[NAME_MAX+1];
};

DIR* opendir(const char* dirname);
DIR* fdopendir(int fd);
int dirfd(DIR* dirp);
struct dirent *readdir(DIR* dirp);
int readdir_r(DIR* restrict dirp,struct dirent* restrict entry,struct dirent** restrict result);
void seekdir(DIR* dirp,long pos);
long telldir(DIR* dirp);
void rewinddir(DIR* dirp);
int closedir(DIR* dirp);

#ifdef  __cplusplus
}
#endif
