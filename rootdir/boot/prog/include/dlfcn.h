#pragma once

#define RTLD_LAZY	(1)
#define RTLD_NOW	(2)
#define RTLD_GLOBAL	(3)
#define RTLD_LOCAL	(4)

#ifdef _GNU_SOURCE
#define RTLD_DEFAULT	((void*)0)
#endif

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

int dlclose(void*);
char* dlerror(void);
void* dlopen(const char*,int);
void* dlsym(void* restrict,const char* restrict);

#ifdef  __cplusplus
}
#endif
