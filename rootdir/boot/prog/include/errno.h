#pragma once

#define ENOSYS			1
#define ENOENT			2
#define EFAULT			3
#define EACCES			4
#define EBADF			5
#define ENXIO			6
#define EINVAL			7
#define ENOEXEC			8
#define ENOTDIR			9
#define EOVERFLOW		10
#define EINTR			11
#define EWOULDBLOCK		12
#define EAGAIN			EWOULDBLOCK
#define EEXIST			13
#define ERANGE			14
#define ENOMEM			15
#define EPERM			16
#define ESRCH			17
#define ENOTTY			18
#define EISDIR			19
#define ELOOP			20
#define ENAMETOOLONG	21
#define ENOSPC			22
#define EPROTONOSUPPORT	23
#define ENODEV			24
#define EPIPE			25
#define EDOM			26
#define E2BIG			27
#define EXDEV			28
#define EBUSY			29
#define ENOTEMPTY		30
#define EFBIG			31
#define ESPIPE			32
#define EIO				33
#define ECHILD			34
#define ENOBUFS			35
#define ENOTSOCK		36
#define EROFS			37
#define EMFILE			38
#define ENFILE			39
#define EDEADLK			40
#define ENETUNREACH		41
#define ENETDOWN		42
#define EMLINK			43
#define ETIMEDOUT		44
#define EPROTOTYPE		45
#define EMSGSIZE		46
#define EILSEQ			47
#define ENOLCK			48
#define ENOMSG			49
#define ENOTCONN		50
#define EINPROGRESS		51
#define ENETRESET		52
#define EHOSTUNREACH	53
#define ECONNRESET		54
#define ECONNREFUSED	55
#define EALREADY		56
#define ECONNABORTED	57
#define EADDRINUSE		58
#define EDESTADDRREQ	59
#define EAFNOSUPPORT	60
#define EADDRNOTAVAIL	61
#define EISCONN			62
#define ENOTSUP			63
#define ENOPROTOOPT 	64
#define EOPNOTSUPP		ENOTSUP

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

extern int* const __errno_location;
#define errno (*__errno_location)

#ifdef  __cplusplus
}
#endif
