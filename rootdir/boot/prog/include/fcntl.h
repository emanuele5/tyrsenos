#pragma once

#include <sys/types.h>

// acces flags for open and fnctl
#define O_ACCMODE	7
#define O_EXEC		4
#define O_RDWR		3
#define O_WRONLY 	2
#define O_RDONLY	1
#define O_SEARCH	1 // search = read for directories

// state flags for open and fcntl
#define O_APPEND	8
#define O_NONBLOCK	16

// creation flags for open and fcntl
#define O_DIRECTORY	32
#define O_NOCTTY	64
#define O_CREAT		128
#define O_TRUNC		256
#define O_EXCL		512
#define O_SYNC		1024
#define O_CLOEXEC	2048

// file descriptor of cwd for *at functions
#define AT_FDCWD	-1

// other flags
#define AT_EMPTY_PATH		1
#define AT_SYMLINK_NOFOLLOW	2
#define AT_SYMLINK_FOLLOW	4
#define AT_REMOVEDIR		8

// fcntl commands
#define F_GETFL		1
#define F_SETFL		2
#define F_DUPFD		3
#define	F_GETFD		4
#define F_SETFD		5

#define FD_CLOEXEC	1

int openat(int dirfd, const char *pathname, int flags, ...);
int open(const char *pathname, int flags, ...);
int creat(const char *pathname, mode_t mode);

int fcntl(int fd, int cmd, ...);
