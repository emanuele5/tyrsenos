#pragma once

#include <stddef.h>

// flags
#define GLOB_APPEND		1
#define GLOB_DOOFFS		2
#define GLOB_ERR		4
#define GLOB_MARK		8
#define GLOB_NOCHECK	16
#define GLOB_NOESCAPE	32
#define GLOB_NOSORT		64

// return values
#define GLOB_NOMATCH	-1
#define GLOB_ABORTED	-2
#define GLOB_ESCAPED	-3
#define GLOB_NOSPACE	-4

typedef struct
{
	size_t	gl_pathc;
	char**	gl_pathv;
	size_t	gl_offs;
}glob_t;

int glob(const char* restrict pattern,int flags,int (*errfunc)(const char* epath,int eerrno),glob_t* restrict pglob);
void globfree(glob_t* pglob);
