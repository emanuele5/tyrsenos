#pragma once

#include <stdint.h>

#define PRIuMAX "lu"
#define PRIXMAX "lX"

#define PRId64 "ld"
#define PRIu64 "lu"
#define PRIo64 "lo"
#define PRIx64 "lx"

#define PRId32 "d"
#define PRIu32 "u"
#define PRIo32 "o"
#define PRIx32 "x"

#define PRId16 "d"

#define SCNu64 "lu"
#define SCNx64 "lx"
