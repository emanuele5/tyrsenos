#pragma once

#define NAME_MAX	(255)	// max directory entry length
#define OPEN_MAX	(512)	// max open file for a process
#define PAGE_SIZE	(4096)
#define PAGESIZE	PAGE_SIZE
#define PATH_MAX	(4096)	// max path length
#define PIPE_BUF	(4096)	// max atomic transaction on pipes

#define SSIZE_MAX	(INT64_MAX)

#define LONG_BIT	(sizeof(long)*CHAR_BIT)

// minimum values (defined by the POSIX standard
#define _POSIX_CHILD_MAX	(25)
#define _POSIX_LINK_MAX		(8)
#define _POSIX_MAX_CANON	(255)
#define _POSIX_MAX_INPUT	(255)
#define _POSIX_NAME_MAX		(14)
#define _POSIX_OPEN_MAX		(20)
#define _POSIX_PATH_MAX		(256)
#define _POSIX_PIPE_BUF		(512)
#define _POSIX_SSIZE_MAX	(32767)

#define MB_LEN_MAX	(4)
