#pragma once

#define NAN			__builtin_nanf("")
#define INFINITY	__builtin_inff()

#define HUGE_VALF	INFINITY
#define HUGE_VAL	((double)INFINITY)
#define HUGE_VALL	((long double)INFINITY)

#define FP_NAN       0
#define FP_INFINITE  1
#define FP_ZERO      2
#define FP_SUBNORMAL 3
#define FP_NORMAL    4

#ifdef  __cplusplus
extern "C"
{
#endif

typedef float float_t;
typedef double double_t;

float fabsf(float x);
double fabs(double x);
long double fabsl(long double x);

double exp(double x);
float expf(float x);
long double expl(long double x);

double ldexp(double x,int exp);

double scalbn(double x,int n);

double log(double x);
float logf(float x);
long double logl(long double x);

double log10(double x);

double sqrt(double x);
float sqrf(float x);
long double sqrl(long double x);

double sin(double x);

double cos(double x);

double tan(double x);

double asin(double x);

double acos(double x);

double atan(double x);

double atan2(double y,double x);

double sinh(double x);

double cosh(double x);

double tanh(double x);

double pow(double x,double y);

double floor(double x);

double ceil(double x);

double fmod(double x,double y);

double modf(double x,double *iptr);

double frexp(double x,int *e);

#ifdef  __cplusplus
}
#endif
