#pragma once

#include <stdint.h>

#define ETHER_ADDR_LEN 6

#define ETHERTYPE_IP	0x0800
#define ETHERTYPE_ARP	0x0806
#define ETHERTYPE_IPV6	0x86dd

struct ether_addr
{
	uint8_t ether_addr_octet[ETHER_ADDR_LEN];
};

struct ether_header
{
	uint8_t ether_dhost[ETHER_ADDR_LEN];
	uint8_t ether_shost[ETHER_ADDR_LEN];
	uint16_t ether_type;
};

struct ether_footer
{
	uint32_t ether_crc;
};
