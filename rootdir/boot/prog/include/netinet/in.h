#pragma once

#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>

#define INET_ADDRSTRLEN	16
#define INET6_ADDRSTRLEN 46

#ifdef  __cplusplus
extern "C"
{
#endif

typedef uint16_t in_port_t;
typedef uint32_t in_addr_t;

struct in_addr
{
	in_addr_t s_addr;
};

struct in6_addr
{
	uint8_t s6_addr[16];
};

struct sockaddr_in
{
	sa_family_t sin_family;
	in_port_t sin_port;
	struct in_addr sin_addr;
};

struct sockaddr_in6
{
	sa_family_t sin6_family;
	in_port_t sin6_port;
	uint32_t sin6_flowinfo;
	struct in6_addr sin6_addr;
	uint32_t sin6_scope_id;
};

#define INADDR_ANY			((in_addr_t)0x00000000)
#define INADDR_BROADCAST	((in_addr_t)0xffffffff)

#ifdef  __cplusplus
}
#endif
