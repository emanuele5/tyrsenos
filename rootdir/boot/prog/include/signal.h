#pragma once

#include <sys/types.h>
#include <time.h>

typedef int sig_atomic_t;
typedef uint32_t sigset_t;
typedef void (*sighandler_t)(int);

#define SIG_DFL	((sighandler_t)0)
#define SIG_IGN	((sighandler_t)1)

#define SIG_SETMASK	0
#define SIG_BLOCK	1
#define SIG_UNBLOCK	2

#define SA_RESTART		1
#define SA_RESETHAND	2

// siginfo_t.si_code values for SIGFPE
#define FPE_FLTDIV	1
#define FPE_INTDIV	2

// sigevent.sigev_notify values
#define SIGEV_NONE		0
#define SIGEV_SIGNAL	1
#define SIGEV_THREAD	2

// signal numbers
#define SIGABRT		1
#define SIGALRM		2
#define SIGBUS		3
#define SIGCHLD		4
#define SIGCONT		5
#define SIGFPE		6
#define SIGHUP		7
#define SIGILL		8
#define SIGINT		9
#define SIGKILL		10
#define SIGPIPE		11
#define SIGQUIT		12
#define SIGSEGV		13
#define SIGSTOP		14
#define SIGTERM		15
#define SIGTSTP		16
#define SIGTTIN		17
#define SIGTTOU		18
#define SIGUSR1		19
#define SIGUSR2		20
#define SIGPOLL		21
#define SIGPROF		22
#define SIGSYS		23
#define SIGTRAP		24
#define SIGURG		25
#define SIGVTALRM	26
#define SIGXCPU		27
#define SIGXFSZ		28

#define NSIG	28

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

union sigval
{
	int sival_int;
	void* sival_ptr;
};

typedef struct
{
	int si_signo;
	int si_code;
	int si_errno;
	pid_t si_pid;
	uid_t si_uid;
	union
	{
		void *si_addr;
		int si_status;
		union sigval si_value;
	};
}siginfo_t;

struct sigaction
{
	union
	{
		void (*sa_handler)(int);
		void (*sa_sigaction)(int,siginfo_t*,void*);
	};
	sigset_t sa_mask;
	int sa_flags;
};

struct sigevent
{
	int sigev_notify;
	int sigev_signo;
	union sigval sigev_value;
	void(*sigev_notify_function)(union sigval);
	void* sigev_notify_attributes; // should be pthread_attr_t*, but threads are not implemented yet
};

int sigemptyset(sigset_t *set);
int sigfillset(sigset_t *set);
int sigaddset(sigset_t *set, int signum);
int sigdelset(sigset_t *set, int signum);
int sigismember(const sigset_t *set, int signum);

int sigprocmask(int how,const sigset_t* restrict set,sigset_t* restrict oldset);
int sigwaitinfo(const sigset_t* restrict set,siginfo_t* restrict info);
int sigwait(const sigset_t* restrict set,int* restrict sig);
int sigpending(sigset_t* set);
int sigsuspend(const sigset_t *sigmask);

int sigaction(int sig,const struct sigaction* sa,struct sigaction* old);
int signal(int sig,sighandler_t func);
int kill(pid_t pid,int signum);
int raise(int signum);

#ifdef  __cplusplus
}
#endif
