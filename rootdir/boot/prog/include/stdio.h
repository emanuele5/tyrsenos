#pragma once

#include <limits.h>
#include <stddef.h>
#include <stdarg.h>
#include <sys/types.h>

#define EOF				(-1)
#define BUFSIZ			PAGE_SIZE
#define FILENAME_MAX	PATH_MAX
#define P_tmpdir		"/tmp"
#define L_tmpnam		(16)
#define TMP_MAX			(10000)

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2

#define _IOFBF	0
#define _IOLBF	1
#define _IONBF	2

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

typedef off_t fpos_t;

struct _IO_FILE;
typedef struct _IO_FILE FILE;

extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;

// file access
FILE* fopen(const char* restrict pathname,const char* restrict mode);
FILE* fdopen(int fd,const char* mode);
FILE* freopen(const char* restrict pathname,const char* restrict mode,FILE* restrict stream);
int fclose(FILE* stream);
int fflush(FILE* stream);
void setbuf(FILE* stream,char* restrict buf);
int setvbuf(FILE* stream,char* restrict buf,int type,size_t size);
int fileno(FILE* stream);

// direct io
size_t fread(void* restrict ptr,size_t size,size_t n,FILE* restrict stream);
size_t fwrite(const void* restrict ptr,size_t size,size_t n,FILE* restrict stream);

// unformatted io
int fgetc(FILE* stream);
int getc(FILE* stream);
int getc_unlocked(FILE* stream);
char* fgets(char* restrict s,int n,FILE* restrict stream);
int fputc(int c,FILE* stream);
int putc(int c,FILE* stream);
int putc_unlocked(int c,FILE* stream);
int fputs(const char* restrict s,FILE* restrict stream);
int getchar(void);
int putchar(int c);
int puts(const char* s);
int ungetc(int c,FILE* stream);

// formatted io
int scanf(const char* restrict format,...);
int fscanf(FILE* restrict stream, const char* restrict format,...);
int sscanf(const char* restrict s,const char* restrict format,...);
int vscanf(const char* restrict format,va_list ap);
int vfscanf(FILE* restrict stream, const char* restrict format,va_list ap);
int vsscanf(const char* restrict s,const char* restrict format,va_list ap);
int printf(const char* restrict format,...);
int fprintf(FILE* restrict stream, const char* restrict format, ...);
int sprintf(char* restrict s,const char* restrict format,...);
int snprintf(char* restrict s,size_t n,const char* restrict format,...);
int vprintf(const char* restrict format,va_list ap);
int vfprintf(FILE* restrict stream,const char* restrict format,va_list ap);
int vsprintf(char* restrict s,const char* restrict format,va_list ap);
int vsnprintf(char* restrict s,size_t n,const char* restrict format,va_list ap);

// file positioning
int fseek(FILE* stream,long offset,int whence);
int fseeko(FILE* stream,off_t offset,int whence);
int fgetpos(FILE* restrict stream,fpos_t* restrict pos);
int fsetpos(FILE* stream,const fpos_t* pos);
long ftell(FILE* stream);
off_t ftello(FILE* stream);
void rewind(FILE* stream);

// error handling
void clearerr(FILE* stream);
int feof(FILE* stream);
int ferror(FILE* stream);
void perror(const char* s);

// operations on files
int remove(const char* filename);
int rename(const char* oldpath,const char* newpath);
FILE* tmpfile(void);
char* tmpnam(char* s);

// getline extension
ssize_t getdelim(char** restrict lineptr,size_t* restrict n,int delimiter,FILE* restrict stream);
ssize_t getline(char** restrict lineptr,size_t* restrict n,FILE* restrict stream);

// pipe extesnion
FILE* popen(const char* cmd,const char* mode);
int pclose(FILE* stream);

#ifdef  __cplusplus
}
#endif
