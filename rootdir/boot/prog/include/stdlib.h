#pragma once

#include <limits.h>
#include <stddef.h>

#define MB_CUR_MAX	(1)

#define RAND_MAX	INT_MAX

#define EXIT_FAILURE	(-1)
#define EXIT_SUCCESS	(0)

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

typedef struct
{
	int quot;
	int rem;
}div_t;

typedef struct
{
	long  quot;
	long rem;
}ldiv_t;

typedef struct
{
	long long  quot;
	long long rem;
}lldiv_t;

int abs(int i);
long labs(long i);
long long llabs(long long i);

div_t div(int numerator,int denominator);
ldiv_t ldiv(long numerator,long denominator);
lldiv_t lldiv(long long numerator,long long denominator);

int atoi(const char* str);
long atol(const char* str);
long long atoll(const char* str);
double atof(const char* str);

long strtol(const char* restrict nptr,char** restrict endptr,int base);
long long strtoll(const char* restrict nptr,char** restrict endptr,int base);
unsigned long strtoul(const char* restrict nptr,char** restrict endptr,int base);
unsigned long long strtoull(const char* restrict nptr,char** restrict endptr,int base);
float strtof(const char* nptr,char** endptr);
double strtod(const char* restrict nptr,char** restrict endptr);
long double strtold(const char* nptr,char** endptr);

int mblen(const char* s,size_t n);
int mbtowc(wchar_t* restrict pwc,const char* restrict s,size_t n);
int wctomb(char* s,wchar_t wch);
size_t mbstowcs(wchar_t* restrict wcs,const char* restrict s,size_t n);
size_t wcstombs(char* restrict s,const wchar_t* restrict wcs,size_t n);

void qsort(void* base,size_t nmemb,size_t size,int(*compar)(const void*,const void*));
void* bsearch(const void* key,const void* base,size_t nmemb,size_t size,int(*compar)(const void*,const void*));

void srand(unsigned seed);
int rand(void);

void* malloc(size_t size);
void* calloc(size_t nmemb,size_t size);
void* realloc(void* ptr,size_t size);
void free(void *ptr);

char* getenv(const char* name);
int putenv(char* string);
int setenv(const char* name,const char* envval,int overwrite);

__attribute__((noreturn)) void abort(void);
__attribute__((noreturn)) void exit(int status);
__attribute__((noreturn)) void _Exit(int status);
int atexit(void(*func)(void));

int system(const char* command);

#ifdef  __cplusplus
}
#endif
