#pragma once

#include <stddef.h>

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

void* memset(void* s, int c,size_t n);
void* memmove(void* dest,const void* src,size_t n);
void* memcpy(void* restrict dest,const void* restrict src,size_t n);
int memcmp(const void* s1,const void* s2,size_t n);
void* memchr(const void* s,int c,size_t n);

char* strcat(char* restrict dest,const char* restrict src);
char* strncat(char* restrict dest,const char* restrict src,size_t n);

char* strcpy(char* restrict dest,const char* restrict src);
char* strncpy(char* restrict dest,const char* restrict src,size_t n);
char* stpcpy(char* restrict dest,const char* restrict src);
char* stpncpy(char* restrict dest,const char* restrict src,size_t n);

int strcmp(const char* s1,const char* s2);
int strncmp(const char* s1,const char* s2,size_t n);

size_t strlen(const char* s);
size_t strnlen(const char* s,size_t n);

char* strtok(char *restrict s,const char *restrict sep);
char* strchr(const char* s,int c);
char* strrchr(const char* s,int c);
char* strpbrk(const char* s1,const char* s2);
size_t strspn(const char* s,const char* accept);
size_t strcspn(const char* s,const char* reject);
char* strstr(const char* s1,const char* s2);

char* strdup(const char* s);
char* strndup(const char* s,size_t n);

int strcoll(const char* s1,const char* s2);
size_t strxfrm(char* restrict dest,const char* restrict src,size_t n);
char* strsignal(int sig);
char* strerror(int errnum);

#ifdef _GNU_SOURCE
char* strchrnul(const char* s,int c);
#endif

#ifdef  __cplusplus
}
#endif
