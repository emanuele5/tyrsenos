#pragma once

#include <sys/types.h>

// ioctl request numbers for framebuffer devices
#define FRAMEBUFFER_GET_INFO	(0x000f0000)
	
struct framebuffer_info
{
	size_t fb_size;		// framebuffer size (bytes)
	size_t line_size;	// line size (bytes)
	unsigned x_res;		// x resolution (pixels)
	unsigned y_res;		// y resolution (pixels)
	unsigned bpp;		// bytes per pixel
};
