#pragma once

#include <sys/types.h>

#define INPUT_TYPE_KEYPRESS		0x1
#define INPUT_TYPE_KEYRELEASE	0x2
#define INPUT_TYPE_MOVXREL		0x3
#define INPUT_TYPE_MOVYREL		0x4
#define INPUT_TYPE_CLICK		0x5

#define INPUT_CLICK_LEFT	0x1
#define INPUT_CLICK_RIGHT	0x2
#define INPUT_CLICK_MIDDLE	0x3

#define INPUT_KEY_INVALID	0x00
#define INPUT_KEY_A			0x04
#define INPUT_KEY_B			0x05
#define INPUT_KEY_C			0x06
#define INPUT_KEY_D			0x07
#define INPUT_KEY_E			0x08
#define INPUT_KEY_F			0x09
#define INPUT_KEY_G			0x0a
#define INPUT_KEY_H			0x0b
#define INPUT_KEY_I			0x0c
#define INPUT_KEY_J			0x0d
#define INPUT_KEY_K			0x0e
#define INPUT_KEY_L			0x0f
#define INPUT_KEY_M			0x10
#define INPUT_KEY_N			0x11
#define INPUT_KEY_O			0x12
#define INPUT_KEY_P			0x13
#define INPUT_KEY_Q			0x14
#define INPUT_KEY_R			0x15
#define INPUT_KEY_S			0x16
#define INPUT_KEY_T			0x17
#define INPUT_KEY_U			0x18
#define INPUT_KEY_V			0x19
#define INPUT_KEY_W			0x1a
#define INPUT_KEY_X			0x1b
#define INPUT_KEY_Y			0x1c
#define INPUT_KEY_Z			0x1d
#define INPUT_KEY_1			0x1e
#define INPUT_KEY_2			0x1f
#define INPUT_KEY_3			0x20
#define INPUT_KEY_4			0x21
#define INPUT_KEY_5			0x22
#define INPUT_KEY_6			0x23
#define INPUT_KEY_7			0x24
#define INPUT_KEY_8			0x25
#define INPUT_KEY_9			0x26
#define INPUT_KEY_0			0x27
#define INPUT_KEY_ENTER		0x28
#define INPUT_KEY_ESCAPE	0x29
#define INPUT_KEY_BACKSPACE	0x2a
#define INPUT_KEY_TAB		0x2b
#define INPUT_KEY_SPACE		0x2c
#define INPUT_KEY_MINUS		0x2d
#define INPUT_KEY_EQUAL		0x2e
#define INPUT_KEY_LEFTBRACE	0x2f
#define INPUT_KEY_RIGHTBRACE 0x30
#define INPUT_KEY_BACKSLASH	0x31
#define INPUT_KEY_SEMICOLON	0x33
#define INPUT_KEY_APOSTROPHE 0x34
#define INPUT_KEY_TILDE		0x35
#define INPUT_KEY_COMMA		0x36
#define INPUT_KEY_DOT		0x37
#define INPUT_KEY_SLASH		0x38
#define INPUT_KEY_CAPSLOCK	0x39
#define INPUT_KEY_F1		0x3a
#define INPUT_KEY_F2		0x3b
#define INPUT_KEY_F3		0x3c
#define INPUT_KEY_F4		0x3d
#define INPUT_KEY_F5		0x3e
#define INPUT_KEY_F6		0x3f
#define INPUT_KEY_F7 		0x40
#define INPUT_KEY_F8		0x41
#define INPUT_KEY_F9		0x42
#define INPUT_KEY_F10 		0x43
#define INPUT_KEY_F11		0x44
#define INPUT_KEY_F12		0x45
#define INPUT_KEY_PRINTSCREEN 0x46
#define INPUT_KEY_SCROLLLOCK 0x47
#define INPUT_KEY_PAUSE		0x48
#define INPUT_KEY_INSERT	0x49
#define INPUT_KEY_HOME		0x4a
#define INPUT_KEY_PAGEUP	0x4b
#define INPUT_KEY_DEL		0x4c
#define INPUT_KEY_END		0x4d
#define INPUT_KEY_PAGEDOWN	0x4e
#define INPUT_KEY_RIGHTARROW 0x4f
#define INPUT_KEY_LEFTARROW	0x50
#define INPUT_KEY_DOWNARROW	0x51
#define INPUT_KEY_UPARROW	0x52

#define INPUT_KEY_LEFTCTRL	0xe0
#define INPUT_KEY_LEFTSHIFT	0xe1
#define INPUT_KEY_LEFTALT	0xe2
#define INPUT_KEY_RIGHTCTRL	0xe4
#define INPUT_KEY_RIGHTSHIFT 0xe5
#define INPUT_KEY_RIGHTALT	0xe6

struct input_event
{
	clock_t time;
	uint32_t type;
	int32_t data;
};
