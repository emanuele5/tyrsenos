#pragma once

#include <sys/types.h>

struct procinfo
{
	pid_t	pid;
	pid_t	ppid;
	uid_t	uid;
	uid_t	euid;
	gid_t	gid;
	gid_t	egid;

	pid_t	next;
};
