#pragma once

#include <stdint.h>

#define SYS_OPEN		1
#define SYS_CLOSE		2
#define SYS_READ		3
#define SYS_WRITE		4
#define SYS_EXEC		5
#define SYS_STAT		6
#define SYS_FORK		7
#define SYS_MMAP		8
#define SYS_GETPID		9
#define SYS_GETPROCINFO	10
#define SYS_SETUID		11
#define SYS_SETGID		12
#define SYS_CHDIR		13
#define SYS_SEEK		14
#define SYS_READDIR		15
#define SYS_FCNTL		16
#define SYS_DUP			17
#define SYS_IOCTL		18
#define SYS_EXIT		19
#define SYS_WAITCHILD	20
#define SYS_SIGMASK		21
#define SYS_SIGSEND		22
#define SYS_SIGWAIT		23
#define SYS_SIGPENDING	24
#define SYS_SIGACTION	25
#define SYS_GETTIME		26
#define SYS_CHOWN		27
#define SYS_PIPE		28
#define SYS_SETTIME		29
#define SYS_PREAD		30
#define SYS_PWRITE		31
#define SYS_REBOOT		32
#define SYS_ACCESS		33
#define SYS_SYNC		34

static inline uint64_t syscall0(uint64_t n)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n):"memory");
	return ret;
}

static inline uint64_t syscall1(uint64_t n,uint64_t arg1)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n),"b"(arg1):"memory");
	return ret;
}

static inline uint64_t syscall2(uint64_t n,uint64_t arg1,uint64_t arg2)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n),"b"(arg1),"c"(arg2):"memory");
	return ret;
}

static inline uint64_t syscall3(uint64_t n,uint64_t arg1,uint64_t arg2,uint64_t arg3)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n),"b"(arg1),"c"(arg2),"d"(arg3):"memory");
	return ret;
}

static inline uint64_t syscall4(uint64_t n,uint64_t arg1,uint64_t arg2,uint64_t arg3,uint64_t arg4)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n),"b"(arg1),"c"(arg2),"d"(arg3),"S"(arg4):"memory");
	return ret;
}

static inline uint64_t syscall5(uint64_t n,uint64_t arg1,uint64_t arg2,uint64_t arg3,uint64_t arg4,uint64_t arg5)
{
	uint64_t ret;
	asm volatile("int $0x30":"=a"(ret):"a"(n),"b"(arg1),"c"(arg2),"d"(arg3),"S"(arg4),"D"(arg5):"memory");
	return ret;
}
