#pragma once

// ioctrl request numbers for tty devices

#define TCSETATTR	0x00060000
#define TCGETATTR	0x00060001
