#pragma once

#include <sys/types.h>

#define PROT_NONE	0
#define PROT_EXEC	1
#define PROT_READ	2
#define PROT_WRITE	4

#define	MAP_FIXED		8
#define MAP_SHARED		16
#define MAP_PRIVATE		32
#define MAP_ANONYMOUS	64
	
#define MS_SYNC			1
#define MS_ASYNC		2
#define MS_INVALIDATE	4

#define MAP_FAILED	((void*)-1)

void* mmap(void* addr,size_t length,int prot,int flags,int fd,off_t offset);
int msync(void* addr,size_t length,int flags);
