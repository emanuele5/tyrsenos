#pragma once

#define RLIM_INFINITY	(0ull)

// resource IDs
#define RLIMIT_FSIZE	(0)

typedef uint64_t rlim_t;

struct rlimit
{
	rlim_t rlim_cur;
	rlim_t rlim_max;
};

int getrlimit(int resource,struct rlimit* rlp);
int setrlimit(int resource,const struct rlimit* rlp);
