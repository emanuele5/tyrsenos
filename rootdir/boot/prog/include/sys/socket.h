#pragma once

#include <stddef.h>
#include <stdint.h>

// socket domains
#define AF_UNSPEC	0
#define AF_UNIX		1
#define AF_INET		2
#define AF_INET6	3

// socket types
#define SOCK_STREAM	1
#define SOCK_DGRAM	2
#define SOCK_RAW	3

#ifdef  __cplusplus
extern "C"
{
#endif

typedef size_t socklen_t;

typedef uint8_t sa_family_t;

struct sockaddr
{
	sa_family_t sa_family;
	char sa_data[];
};

struct msghdr
{
	void*			msg_name;
	socklen_t		msg_namelen;
	void*			msg_iov;	// should be struct iovec*, when it will be implemented
	int				msg_iovlen;
	void*			msg_control;
	socklen_t		msg_controllen;
	int				msg_flags;
};

struct cmsghdr
{
	socklen_t cmsg_len;
	int cmsg_level;
	int cmsg_type;
};

#ifdef  __cplusplus
}
#endif
