#pragma once

#include <sys/types.h>

static inline dev_t makedev(unsigned maj,unsigned min)
{
	return (maj<<16)+min;
}

static inline unsigned major(dev_t dev)
{
	return (dev>>16)&0xffff;
}

static inline unsigned minor(dev_t dev)
{
	return dev&0xffff;
}
