#pragma once

#include <stdint.h>
#include <stddef.h>

typedef int64_t		blkcnt_t;
typedef int32_t		blksize_t;
typedef uint64_t	clock_t;
typedef unsigned	clockid_t;
typedef uint16_t	dev_t;
typedef uint16_t	gid_t;
typedef int32_t		id_t;
typedef uint32_t	ino_t;
typedef uint16_t	mode_t;
typedef uint16_t	nlink_t;
typedef int64_t		off_t;
typedef int32_t		pid_t;
typedef void*		pthread_t; // threads not implemented, but this won't break ABI when we will
typedef int64_t		ssize_t;
typedef int64_t		suseconds_t;
typedef uint64_t	time_t;
typedef unsigned	timer_t;
typedef uint16_t	uid_t;
