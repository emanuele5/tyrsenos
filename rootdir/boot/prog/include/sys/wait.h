#pragma once

#include <sys/types.h>

#define WNOHANG	1
#define WUNTRACED	2

#define WEXITSTATUS(status) (status&0xff)
#define WSTOPSIG(status)	((status>>8)&0x7f)
#define WTERMSIG(status)	((status>>8)&0x7f)
#define WIFEXITED(status)	(WTERMSIG(status)==0)
#define WIFSIGNALED(status)	(WTERMSIG(status)!=0)
#define WIFSTOPPED(status)	((status>>8)&0x80)
#define WIFCONTINUED(status) (WIFSTOPPED(status)==0)

pid_t wait(int* wstatus);
pid_t waitpid(pid_t pid,int* wstatus,int options);
