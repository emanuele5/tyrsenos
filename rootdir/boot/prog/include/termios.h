/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <stdint.h>

// c_iflag flags
#define BRKINT	(1<<0)
#define ICRNL	(1<<1)
#define IGNBRK	(1<<2)
#define IGNCR	(1<<3)
#define IGNPAR	(1<<4)
#define INLCR	(1<<5)
#define INPCK	(1<<6)
#define ISTRIP	(1<<7)
#define IXANY	(1<<8)
#define IXOFF	(1<<9)
#define IXON	(1<<10)
#define PARMRK	(1<<11)

// c_oflag flags
#define OPOST	(1<<0)
#define ONLCR	(1<<1)
#define OCRNL	(1<<2)
#define ONOCR	(1<<3)
#define ONLRET	(1<<4)
#define OFDEL	(1<<5)
#define OFILL	(1<<6)
#define NLDLY	(1<<7)
	#define NL0	(0<<7)
	#define	NL1	(1<<7)
#define CRDLY	(3<<8)
	#define CR0	(0<<8)
	#define CR1 (1<<8)
	#define CR2 (2<<8)
	#define CR3 (3<<8)
#define TABDLY	(3<<10)
	#define TAB0	(0<<10)
	#define TAB1	(1<<10)
	#define TAB2	(2<<10)
	#define TAB3	(3<<10)
#define BSDLY	(1<<12)
	#define BS0	(0<<12)
	#define BS1	(1<<12)
#define VTDLY	(1<<13)
	#define VT0	(0<<13)
	#define VT1	(1<<13)
#define FFDLY	(1<<14)
	#define FF0	(0<<14)
	#define FF1	(1<<14)

// c_cflag flags
#define CSIZE	(3<<0)
	#define CS5	(0<<0)
	#define CS6	(1<<0)
	#define CS7	(2<<0)
	#define CS8	(3<<0)
#define CSTOPB	(1<<2)
#define CREAD	(1<<3)
#define PARENB	(1<<4)
#define PARODD	(1<<5)
#define HUPCL	(1<<6)
#define CLOCAL	(1<<7)

// c_lflag flags
#define ECHO	(1<<0)
#define ECHOE	(1<<1)
#define ECHOK	(1<<2)
#define ECHONL	(1<<3)
#define ICANON	(1<<4)
#define IEXTEN	(1<<5)
#define ISIG	(1<<6)
#define NOFLSH	(1<<7)
#define TOSTOP	(1<<8)

// speed values
#define B0		(0)
#define B50		(1)
#define B75		(2)
#define B110	(3)
#define B134	(4)
#define B150	(5)
#define B200	(6)
#define B300	(7)
#define B600	(8)
#define B1200	(9)
#define B1800	(10)
#define B2400	(11)
#define B4800	(12)
#define B9600	(13)

#define NCCS	11
// c_cc indexes
#define VEOF	0
#define VEOL	1
#define VERASE	2
#define VINTR	3
#define VKILL	4
#define VMIN	5
#define VQUIT	6
#define VSTART	7
#define VSTOP	8
#define VSUSP	9
#define VTIME	10

// constants for tcsetattr
#define TCSANOW			(1)
#define TCSADRAIN		(2)
#define TCSAFLUSH		(3)

// constants for tcflush
#define TCIFLUSH	(1)
#define TCOFLUSH	(2)
#define TCIOFLUSH	(3)

typedef uint16_t tcflag_t;
typedef uint16_t speed_t;
typedef unsigned char cc_t;

#ifdef  __cplusplus
extern "C"
{
#endif

struct termios
{
	tcflag_t c_iflag;
	tcflag_t c_oflag;
	tcflag_t c_cflag;
	tcflag_t c_lflag;
	speed_t c_ispeed;
	speed_t c_ospeed;
	cc_t c_cc[NCCS];
};

int tcsetattr(int fd,int opt,const struct termios* tc_ptr);
int tcgetattr(int fd,struct termios* tc_ptr);
int cfsetispeed(struct termios* tc_ptr,speed_t speed);
int cfsetospeed(struct termios* tc_ptr,speed_t speed);
speed_t cfgetispeed(const struct termios *tc_ptr);
speed_t cfgetospeed(const struct termios *tc_ptr);

#ifdef  __cplusplus
}
#endif
