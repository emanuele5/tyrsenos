#pragma once

#include <sys/types.h>

#define CLOCKS_PER_SEC (1000000)

#define CLOCK_REALTIME	(0)
#define CLOCK_MONOTONIC	(1)

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

struct tm
{
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};

struct timespec
{
	time_t tv_sec;
	long tv_nsec;
};

struct itimerspec
{
	struct timespec it_interval;
	struct timespec it_value;
};

extern int daylight;
extern long timezone;
extern char* tzname[];

// time manipulation
double difftime(time_t t1,time_t t2);
time_t time(time_t* t_ptr);
clock_t clock(void);

// posix clock functions
int clock_gettime(clockid_t clockid,struct timespec* ts);
int clock_settime(clockid_t clockid,const struct timespec* ts);

// format conversions
char* asctime(const struct tm* tm_ptr);
char* asctime_r(const struct tm* restrict tm_ptr,char* restrict buf);
char* ctime(const time_t* t_ptr);
size_t strftime(char* restrict s,size_t maxsize,const char* restrict format,const struct tm* restrict tm_ptr);
struct tm* gmtime(const time_t* t_ptr);
struct tm* localtime(const time_t* t_ptr);
time_t mktime(struct tm* tm_ptr);

#ifdef  __cplusplus
}
#endif
