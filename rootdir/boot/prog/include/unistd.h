#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

// standard streams file descriptors
#define STDIN_FILENO	0
#define STDOUT_FILENO	1
#define STDERR_FILENO	2

// access types for access
#define X_OK	4
#define W_OK	2
#define R_OK	1
#define F_OK	0

// names for confstr
#define _CS_PATH			1
// names for pathconf
#define _PC_PATH_MAX		1
// names for sysconf
#define _SC_LOGIN_NAME_MAX	0
#define _SC_CLK_TCK			1

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

extern char* optarg;
extern int optind,opterr,optopt;

int getopt(int argc, char * const argv[], const char *optstring);

/* unimplemented */
pid_t getpgrp(void);
pid_t getpgid(pid_t pid);
pid_t getsid(pid_t pid);
pid_t tcgetpgrp(int fd);
int setuid(uid_t uid);
int setgid(gid_t gid);
int setpgrp(void);
int setpgid(pid_t pid,pid_t pgid);
int tcsetpgrp(int fd,pid_t pgrp);

pid_t getpid(void);
pid_t getppid(void);
uid_t getuid(void);
gid_t getgid(void);
uid_t geteuid(void);
gid_t getegid(void);

pid_t fork(void);
__attribute__((noreturn)) void _exit(int status);

int execv(const char* path,char *const argv[]);
int execve(const char* path,char *const argv[],char *const envp[]);
int fexecve(int fd,char *const argv[],char *const envp[]);
int execvp(const char *path,char *const argv[]);
#ifdef _GNU_SOURCE
int execvpe(const char *file,char *const argv[],char *const envp[]);
#endif

int access(const char* pathname,int mode);
int faccessat(int fd,const char* pathname,int mode,int flags);

int chown(const char* path,uid_t uid,gid_t gid);
int lchown(const char* path,uid_t uid,gid_t gid);
int fchown(int fd,uid_t uid,gid_t gid);
int fchownat(int fd,const char* path,uid_t uid,gid_t gid,int flags);

int chdir(const char* path);
int fchdir(int fd);

int truncate(const char* path,off_t size);
int ftruncate(int fd,off_t size);
	
int link(const char* path1,const char* path2);
int linkat(int fd1,const char* path1,int fd2,const char* path2,int flags);

int symlink(const char* path1,const char* path2);

int unlink(const char* path);

int dup(int oldfd);
int dup2(int oldfd,int newfd);
int pipe(int fd[2]);

ssize_t read(int fd,void *buf,size_t count);
ssize_t pread(int fd,void *buf,size_t count,off_t pos);
ssize_t write(int fd,const void *buf,size_t count);
ssize_t pwrite(int fd,const void *buf,size_t count,off_t pos);

off_t lseek(int fd, off_t offset, int whence);
int close(int fd);
int fsync(int fd);
int sync(void);

int isatty(int fd);
int tcgetpgrp(int fd);
int tcsetpgrp(int fd,pid_t pgid);

size_t confstr(int name,char* buf,size_t len);
long pathconf(const char* path,int name);
long sysconf(int name);
	
char* getcwd(char* buf,size_t size);

#ifdef  __cplusplus
}
#endif
