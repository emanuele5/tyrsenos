#pragma once

#include <stddef.h>

#ifdef  __cplusplus
#define restrict __restrict__
extern "C"
{
#endif

typedef int wctype_t;
typedef long wint_t;
typedef struct
{
	wint_t wch;
	unsigned short count;
	unsigned short length;
}mbstate_t;

#define WEOF ((wint_t)-1)

wint_t btowc(int ch);
int wctob(wint_t ch);

size_t mbrtowc(wchar_t* restrict pwc,const char* restrict s,size_t n,mbstate_t* restrict state);
size_t wcrtomb(char* restrict s,wchar_t wch,mbstate_t* restrict state);

size_t mbsnrtowcs(wchar_t* restrict dst,const char** restrict src_ptr,size_t src_len,size_t dst_len,mbstate_t* restrict state);
size_t mbsrtowcs(wchar_t* restrict dest,const char** restrict src_ptr,size_t n,mbstate_t* restrict state);
size_t wcsrtombs(char* restrict dest,const wchar_t** restrict src_ptr,size_t n,mbstate_t* restrict state);

// wide char string manipulation

wchar_t* wcscpy(wchar_t* restrict dest,const wchar_t* restrict src);
wchar_t* wcsncpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n);
wchar_t* wcpcpy(wchar_t* restrict dest,const wchar_t* restrict src);
wchar_t* wcpncpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n);
wchar_t* wcscat(wchar_t* restrict dest,const wchar_t* restrict src);
wchar_t* wcsdup(const wchar_t* wcs);

// wide char string examination

int wcscmp(const wchar_t* s1,const wchar_t* s2);
int wcsncmp(const wchar_t* s1,const wchar_t* s2,size_t n);
size_t wcslen(const wchar_t* wcs);
size_t wcsnlen(const wchar_t* wcs,size_t n);
wchar_t* wcschr(const wchar_t* wcs,wchar_t wc);
wchar_t* wcsrchr(const wchar_t* s,wchar_t c);
size_t wcsspn(const wchar_t* s,const wchar_t* accept);
size_t wcscspn(const wchar_t* s,const wchar_t* reject);
wchar_t* wcsstr(const wchar_t* s1,const wchar_t* s2);

// wide char array functions

wchar_t* wmemcpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n);
int wmemcmp(const wchar_t* ws1,const wchar_t* ws2,size_t n);
wchar_t* wmemchr(const wchar_t* ws,wchar_t wc,size_t n);
wchar_t* wmemset(wchar_t* ws,wchar_t wc,size_t n);

// wide char ctype functions

wctype_t wctype(const char* property);
int iswctype(wint_t wch,wctype_t charclass);

int iswupper(wint_t wch);
int iswlower(wint_t wch);
int iswcntrl(wint_t wch);
int iswdigit(wint_t wch);
int iswpunct(wint_t wch);
int iswalpha(wint_t wch);
int iswspace(wint_t wch);
int iswalnum(wint_t wch);

#ifdef  __cplusplus
}
#endif
