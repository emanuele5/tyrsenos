; DavLo DAValli LOader
; Copyright (C) 2014-2024 Emanuele Davalli
; Rilasciato sotto licenza GPL v3+
;
; Cerca nella root di una partizione minix il file nella variabile kern_filename
; se non lo trova aspetta un input dell'utente per riavviare
; se lo trova lo carica, imposta la modalità a 64 bit, abilita la linea a20
; il bootloader imposta una gdt con segmenti piatti per codice kernel e dati kernel e una idt vuota
; gli interrupt sono disabilitati
; il kernel è caricato a 0xffff8000000010000 virtuale, 0x00010000 fisico
; lo stack è impostato a 0xffff8000000090000 virtuale, 0x00090000 fisico
; il primo gigabyte è mappato a se stesso e a se stesso+0xffff800000000000
; le tabelle delle pagine sono a 0x90000-0x92fff fisico
; il codice del bootloader risiede a 0x7c00 e occupa 1kb
; i dati del bootloader risiedono nella pagina che parte da 0x8000

bits 16

	jmp 07c0h:bootloader_start

bootloader_start:
	cli
	cld
	mov ax,07C0h
	mov ds,ax
	mov es,ax

	xor ax,ax
	mov ss,ax
	mov sp,7c00h

	mov [boot_dev],dl ;save the boot device
	mov byte[boot_dev_ext],0

	mov ah,41h
	mov bx,55aah
	int 13h ;controlla che il disco supporti le estensioni di int 13h
	jc .get_geom_info
	cmp bx,0aa55h
	jne .get_geom_info
	inc byte[boot_dev_ext]

.get_geom_info:
	push es
	mov ah,8
	int 13h ;search information on boot disk geometry
	pop es
	inc dh
	and cl,3fh
	mov [boot_dev_hpc],dh
	mov [boot_dev_spt],cl

	xor ax,ax
	xor bx,bx
	call disk_read_block ;carica la seconda parte del bootloader

read_superblock:
	inc ax
	mov bx,buffer
	call disk_read_block ;legge il superblock

	cmp word[bx+10h],138fh
	mov si,nofserror
	jne error ;se non lo trova da errore

	mov ax,[bx+4] ;numero di blocchi riservati ai descrittori di gruppo
	add [first_inode_block],ax
	mov ax,[bx+6] ;numero di blocchi riservati ai blocchi gdt
	add [first_inode_block],ax

	mov ax,[first_inode_block] ;legge gli inode
	call disk_read_block
	mov ax,[bx+14] ;cerca il blocco con le directory entry
	call disk_read_block
	add bx,32

search_direntry:
	cmp bx,1024+buffer ;abbiamo superato la fine del blocco?
	mov si,nokernerror ;se si allora il file non esiste
	ja error
	add bx,32
	mov di,kern_filename
	lea si,[bx+2]
	mov cx,kern_filename_len
	repe cmpsb ;confronta i nomi
	jne search_direntry

	mov cx,[bx] ;mette in cx il numero dell'inode associato al file
	dec cx
	mov ax,cx
	shr ax,5
	add ax,[first_inode_block] ;calcola il blocco di inode necessario
	mov bx,buffer
	call disk_read_block
	mov si,buffer
	shl cx,5
	and cx,3ffh ;cx contiene ora l'offset dell'inode nel buffer
	add si,cx
	mov eax,[si+4] ;salva la grandezza del file
	mov [kfsize],eax
	add si,14 ;ora si punta ai blocchi da caricare
	xor ebx,ebx
	mov cx,7 ;i blocchi da caricare sono 7
	mov dx,1000h
	mov es,dx

load_block:
	lodsw ;load block number
	call disk_read_block ;load a file block

	add ebx,400h
	cmp ebx,[kfsize] ;check for complete file loading
	jae get_memory_map

	test bx,bx ;check for 16bit overflow
	jnz .nooverflow

	add dx,1000h
	mov es,dx

	.nooverflow:
	loop load_block

	;load in buffer the indirect blocks numbers
	push es
	push bx
	mov bx,buffer
	mov ax,07c0h
	mov es,ax
	lodsw
	call disk_read_block
	mov si,bx
	mov cx,512
	pop bx
	pop es
	jmp load_block

disk_read_block:
	pusha
	movzx eax,ax
	shl eax,1
	add eax,[part_start]
	cmp byte[boot_dev_ext],1
	je .readext

	;chs read
	xor dx,dx
	mov cl,[boot_dev_spt]
	movzx cx,cl
	div cx ; divide linear sector by sectors-per-head
	inc dx ; sector offset start from 1
	mov cl,dl ; and goes in cl

	div byte[boot_dev_hpc] ; divide head number by heads-per-cilynder
	mov dh,ah ; head number goes in dh
	mov ch,al ; cilynder number goes in ch
	mov dl,[boot_dev]
	mov ax,202h
	int 13h
	jc .error

	.end:
	mov ax,0e2eh
	int 10h
	popa
	ret

	.readext:
	mov [read_ext_packet+8],eax
	mov [read_ext_packet+6],es
	mov [read_ext_packet+4],bx
	mov si,read_ext_packet
	mov dl,[boot_dev]
	mov ah,42h
	int 13h
	jnc .end

	.error:
	mov si,diskioerror

error:
	call print
	xor ax,ax
	int 16h
	int 19h

print:
	mov ah,0eh
	lodsb
	or al,al
	jz .fi
	int 10h
	jmp print
	.fi:
	ret

nofserror db "Unknown filesystem",0
nokernerror db "Kernel not found",0
diskioerror db "Disk i/o error",0

kern_filename	db "sunflower.bin",0
kern_filename_len equ $-kern_filename

;settore di inizio della partizione di avvio
part_start dd 0

read_ext_packet:
	db 16
	db 0
	dw 2
	times 12 db 0

times 510-($-$$) db 0
dw 0AA55h

;=========================
;secondo settore del disco
;=========================

get_memory_map:
	mov ax,0e0ah
	int 10h
	mov ax,0e0dh
	int 10h

	mov ax,2401h
	int 15h ;this should enable the a20 line
	in al,92h
	or al,2
	out 92h,al ;this just in case the BIOS doesn't support the function above

	;the kernel has been loaded, now read the BIOS memory map
	mov word[bootinfo.memorymap_len],0
	mov ax,07c0h
	mov es,ax
	mov di,memorymap
	mov dword[bootinfo.memorymap],memorymap+7c00h ;save the memory map address
	xor ebx,ebx
	mov edx,534d4150h
	.do_int15:
		mov eax,0e820h
		mov ecx,24
		int 15h
		jc get_vesa_info
		cmp eax,534d4150h
		jne get_vesa_info
		inc word[bootinfo.memorymap_len]
		test ebx,ebx
		jz get_vesa_info
		add di,24
	jmp .do_int15

get_vesa_info:
	; check for edid capabilities
	mov ax,4f15h
	mov bl,0
	int 10h
	mov di,buffer
	cmp ax,004fh
	jne .default_mode

	; read edid
	mov ax,4f15h
	mov bl,1
	xor cx,cx
	xor dx,dx
	int 10h

	mov bh,[buffer+3ah]
	shr bh,4
	mov bl,[buffer+38h]
	mov ch,[buffer+3dh]
	shr ch,4
	mov cl,[buffer+3bh]
	jmp .save_mode

	.retry_default_mode:
	pop ds
	.default_mode:
	; use safe resolution 640x480
	mov bx,640
	mov cx,480

	.save_mode:
	mov [bootinfo.display_xres],bx
	mov [bootinfo.display_yres],cx

	; read controller information with mode list
	mov ax,4f00h
	int 10h
	push ds
	mov si,[buffer+0eh]
	mov ds,[buffer+10h]
	mov di,buffer+256

	; search for the mode
	.loop:
		lodsw
		cmp ax,0ffffh
		je .retry_default_mode ; use 640x480 if native resolution isn't supported
		push ax
		push cx
		mov cx,ax
		mov ax,4f01h
		int 10h
		pop cx
		pop ax
		cmp bx,[es:buffer+256+12h]
		jne .loop
		cmp cx,[es:buffer+256+14h]
		jne .loop
		cmp byte[es:buffer+256+19h],32
		jne .loop

	; set the mode
	mov ebx,[es:buffer+256+28h]
	mov [es:bootinfo.vesa_fb],ebx
	mov bx,ax
	mov ax,4f02h
	int 10h
	pop ds

	mov ax,9000h
	mov es,ax
	xor di,di
	xor eax,eax
	mov ecx,1000h
	rep stosd ;zeroes a 16 kb buffer at 9000:0000
	
	;create the page map level 4
	xor di,di
	mov eax,91003h
	mov [es:di],eax ;points to the page directory pointer table
	mov [es:di+800h],eax ;copy address space to high address
	
	;create the page directory pointer table
	add di,1000h
	mov eax,92003h
	mov [es:di],eax ; points to the page directory

	;create the page directory
	add di,1000h
	mov eax,83h
	.pd_loop:
		mov [es:di],eax
		add eax,200000h
		add di,8
		cmp eax,200000h*512
	jb .pd_loop ;populate the page directory

	mov al,0ffh
	out 0a1h,al
	out 21h,al ;disable all irqs

	lidt [idtinfo] ;load temporary idt
	lgdt [gdtinfo] ;load temporary gdt
	
	mov eax,90000h
	mov cr3,eax ;cr3 points to the pml4
	
	mov eax,10100000b
	mov cr4,eax ;set PAE and PGE bits
	
	mov ecx,0c0000080h
	rdmsr
	or eax,100h
	wrmsr ;set LME enabled bit

	mov eax,cr0
	or eax,80000001h
	mov cr0,eax ;enable protection and pagination

	jmp dword 8:lmode1+7c00h

bits 64

lmode1:
	mov eax,10h
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax
	mov ss,ax
	mov rsp,0ffff800000090000h
	mov rax,0ffff800000010000h
	mov rdi,bootinfo+7c00h+0ffff800000000000h
	call rax ;execute the kernel!
	jmp $

align 16

idtinfo: ;idtr dummy
	dw 0
	dd 0

align 16

gdtinfo:
	dw  gdtlength
	dd  gdt_table+7c00h

align 16

gdt_table:
	.null: dq 0 ;null segment
	.flat_code: ;code segment
		dq 0x00209a0000000000
	.flat_data: ;data segment
		dq 0x0000920000000000

	gdtlength equ $-gdt_table

first_inode_block dw 2

times 1024-($-$$) db 0

section .bss

buffer resb 1024
memorymap resb 1024

kfsize resd 1
boot_dev_spt resb 1		; sectors per track of boot device
boot_dev_hpc resb 1	; heads of boot device
boot_dev resb 1			; boot device bios code
boot_dev_ext resb 1 	; boot device supports int 13h extended functions

bootinfo:
	.memorymap resq 1		; mappa della memoria fornita dal bios
	.memorymap_len resw 1	; lunghezza della mappa della memoria fornita dal bios
	.display_xres resw 1
	.display_yres resw 1
	.vesa_fb resd 1				; indirizzo del framebuffer
