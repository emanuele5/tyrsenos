/*
 * Copyright 2021-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <drivers/ahci.h>
#include <drivers/ata.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>

//
//	Frame Information Structure (FIS) descriptions
//

enum fis_type
{
	FIS_TYPE_REGH2D		=0x27,
	FIS_TYPE_REGD2H		=0x34,
	FIS_TYPE_DATA		=0x46,
	FIS_TYPE_PIOSETUP	=0x5f,
};

struct ahci_fis_regh2d
{
	uint8_t type;

	uint8_t pmport:4;
	uint8_t res0:3;	// reserved
	uint8_t c:1;

	// registers
	uint8_t cmd;
	uint8_t feature0;
	uint8_t lba0;
	uint8_t lba1;
	uint8_t lba2;
	uint8_t device;
	uint8_t lba3;
	uint8_t lba4;
	uint8_t lba5;
	uint8_t feature1;
	uint8_t count0;
	uint8_t count1;
	uint8_t icc;
	uint8_t control;

	uint8_t res1[4]; // reserved
};

struct ahci_fis_regd2h
{
	uint8_t type;

	uint8_t pmport:4;
	uint8_t res0:2;	// reserved
	uint8_t intr:1; // interrupt
	uint8_t res1:1; // reserved

	// registers
	uint8_t status;
	uint8_t error;
	uint8_t lba0;
	uint8_t lba1;
	uint8_t lba2;
	uint8_t device;
	uint8_t lba3;
	uint8_t lba4;
	uint8_t lba5;
	uint8_t res2; // reserved
	uint8_t count0;
	uint8_t count1;

	uint8_t res3[6]; // reserved
};

struct ahci_fis_piosetup
{
	uint8_t type;
	uint8_t pmport:4;
	uint8_t res0:1;  // reserved
	uint8_t d: 1; // transfer direction 1=read
	uint8_t intr: 1; // interrupt
	uint8_t res1: 1; // reserved

	// registers
	uint8_t status;
	uint8_t error;
	uint8_t lba0;
	uint8_t lba1;
	uint8_t lba2;
	uint8_t device;
	uint8_t lba3;
	uint8_t lba4;
	uint8_t lba5;
	uint8_t res2; // reserved
	uint8_t count0;
	uint8_t count1;
	uint8_t res3; // reserved
	uint8_t estatus;
	uint16_t tcount; // transfer count

	uint8_t res4[2]; // reserved
};

//
// AHCI memory structures
//

struct ahci_cmd_header
{
	uint8_t cmdfis_len:5; // in 32-bits words
	uint8_t atapi:1;
	uint8_t write:1;
	uint8_t prefetchable:1;
	uint8_t reset:1;
	uint8_t bist:1;
	uint8_t clearbusy:1;
	uint8_t res0:1; // reserved
	uint8_t pmport:4;

	uint16_t prdt_len;
	volatile uint32_t prd_bcount;
	uint32_t ctb_addr;
	uint32_t ctb_addr_ext;

	uint32_t res1[4]; // reserved
};

struct ahci_received_fis
{
	uint8_t dmasetup[28];
	uint32_t pad0;

	struct ahci_fis_piosetup piosetup;
	uint32_t pad1[3];

	struct ahci_fis_regd2h regd2h;
	uint32_t pad2;

	uint8_t sdbfis[8];
	uint8_t ufis[64];
	uint32_t pad3[24];
}__attribute__((packed));

struct ahci_cmd_table
{
	struct ahci_fis_regh2d regh2d;
	uint32_t padding[11];
	uint32_t atapicmd[4];
	uint32_t reserved[12];
	struct
	{
		uint32_t data_addr;
		uint32_t data_addr_ext;
		uint32_t res0; // reserved
		uint32_t byte_count:22;
		uint32_t res1:9; // reserved
		uint32_t intr:1;
	}prdt[];
};

//
// AHCI controller interface
//

struct ahci_controller
{
	uint32_t cap;
	uint32_t ghc;
	uint32_t is;
	uint32_t pi;
	uint32_t vs;
	uint32_t ccc_ctl;
	uint32_t ccc_pts;
	uint32_t em_loc;
	uint32_t em_ctl;
	uint32_t cap2;
	uint32_t bohc;

	uint32_t reserved[29];
	uint32_t vendor_specific[24];

	struct
	{
		uint32_t clb;
		uint32_t clbu;
		uint32_t fb;
		uint32_t fbu;
		uint32_t is;
		uint32_t ie;
		uint32_t cmd;
		uint32_t res0; // reserved
		uint32_t tfd;
		uint32_t sig;
		uint32_t ssts;
		uint32_t sctl;
		uint32_t serr;
		uint32_t sact;
		uint32_t ci;
		uint32_t sntf;
		uint32_t fbs;
		uint32_t res1[11];
		uint32_t vendor_specific[4];
	}ports[32];
};

struct ahci_device
{
	volatile struct ahci_controller* controller;
	volatile struct ahci_cmd_header* command_list;
	volatile struct ahci_received_fis* received_fis;
	volatile struct ahci_cmd_table* cmd_table_start;
	uint8_t port;
};

static int ahci_send_cmd(void* dev,const struct ata_taskfile* tf);

static const struct ata_primitives ahci_primitives=
{
	.send_cmd=ahci_send_cmd,
};

static void ahci_stop_port(volatile struct ahci_controller* c,unsigned port)
{
	c->ports[port].cmd&=~0x0011;
	while(c->ports[port].cmd&0x00c0);
}

static void ahci_start_port(volatile struct ahci_controller* c,unsigned port)
{
	while(c->ports[port].cmd&0x0080);
	c->ports[port].cmd|=0x0011;
}

static int ahci_send_cmd(void* dev,const struct ata_taskfile* tf)
{
	const struct ahci_device* d=dev;
	volatile struct ahci_controller* c=d->controller;
	uint8_t p=d->port;

	// number of prdt entries needed
	unsigned prdt_num=0;
	if(tf->flags&TASKFILE_DATA) prdt_num=(tf->count+7)/8;

	// wait for device not busy
	while(c->ports[p].tfd&ATA_STATUS_BUSY);

	// set command header
	volatile struct ahci_cmd_header* cmd_hdr=d->command_list;
	cmd_hdr[0].cmdfis_len=5; // size of host-to-device fis in 4B words
	cmd_hdr[0].atapi=1&&(tf->flags&TASKFILE_ATAPI);
	cmd_hdr[0].write=1&&(tf->flags&TASKFILE_WRITE);
	cmd_hdr[0].prefetchable=1;
	cmd_hdr[0].reset=0;
	cmd_hdr[0].bist=0;
	cmd_hdr[0].clearbusy=0;
	cmd_hdr[0].pmport=0;
	cmd_hdr[0].prdt_len=prdt_num;

	// set command fis
	volatile struct ahci_cmd_table* cmd_fis=d->cmd_table_start;
	cmd_fis[0].regh2d.type=FIS_TYPE_REGH2D;
	cmd_fis[0].regh2d.pmport=0;
	cmd_fis[0].regh2d.c=1;
	cmd_fis[0].regh2d.cmd=tf->cmd;
	cmd_fis[0].regh2d.feature0=tf->feature;
	if(tf->flags&TASKFILE_ADDRESS)
	{
		cmd_fis[0].regh2d.lba0=tf->lba[0];
		cmd_fis[0].regh2d.lba1=tf->lba[1];
		cmd_fis[0].regh2d.lba2=tf->lba[2];
		cmd_fis[0].regh2d.count0=tf->count;
	}
	cmd_fis[0].regh2d.device=tf->device;
	cmd_fis[0].regh2d.icc=0;
	cmd_fis[0].regh2d.control=0;

	if(tf->flags&TASKFILE_DATA)
	{
		uint8_t* buf=(uint8_t*)tf->data;
		unsigned rem=tf->count*512;
		// set the prdt entries, 1 every 4kb
		for(unsigned i=0;i<prdt_num;i++)
		{
			// calculate byte count for this entry
			unsigned byte_count=PAGE_SIZE;
			if(rem<PAGE_SIZE) byte_count=rem;

			physaddr phys_buf=virtmem_get_physaddr(buf);
			cmd_fis[0].prdt[i].data_addr=phys_buf&0xffffffff;
			cmd_fis[0].prdt[i].data_addr_ext=(phys_buf>>32)&0xffffffff;
			cmd_fis[0].prdt[i].byte_count=byte_count-1;
			cmd_fis[0].prdt[i].intr=0;

			buf+=PAGE_SIZE;
			rem-=byte_count;
		}
	}

	// send command
	c->ports[p].ci=1;
	// wait for command completion
	while(c->ports[p].ci&1);

	// check for errors
	if(c->ports[p].tfd&ATA_STATUS_ERR) return c->ports[p].serr;
	else return 0;
}

void pci_ahci_init(const struct pci_device* pcidev)
{
	physaddr abar=pci_dev_get_bar5(pcidev);
	volatile struct ahci_controller* c=virtmem_map_kernel_range(abar,0x1100,PAGE_WT|PAGE_WRITE);
	klog("ahci: found PCI AHCI controller\n\tiomem: %lx",abar);

	// does the device support BIOS handoff?
	if(c->cap2&1)
	{
		c->bohc|=2; // request ownership
		while(c->bohc&1); // wait for bios to release
	}

	// disable interrupts
	c->ghc&=~2;
	// set AHCI mode (shouldn't be needed)
	c->ghc|=0x80000000;

	// check ports
	uint32_t ports=c->pi;
	unsigned portsnum=c->cap&0x1f;
	for(unsigned i=0;i<portsnum;i++)
	{
		if(ports&(1<<i))
		{
			// device connected to port, check type
			uint32_t devtype=c->ports[i].sig;
			if(devtype!=0x00000101) continue; // we only support ATA

			// stop port
			ahci_stop_port(c,i);

			// prepare memory area
			/* NOTE: we allocate raw pages due to the high alignemnt requirement for
				the command table (1kiB) and the received FIS (256B), which are not supported
				by kmalloc */
			uint8_t* port_mem_virt=virtmem_get_heap_pages(PAGE_SIZE);
			memset(port_mem_virt,0,PAGE_SIZE);
			
			// create the device struct
			struct ahci_device* dev=kmalloc(sizeof(struct ahci_device));
			dev->controller=c;
			dev->command_list=(void*)(port_mem_virt);
			dev->received_fis=(void*)(port_mem_virt+1024);
			dev->cmd_table_start=(void*)(port_mem_virt+1280);
			dev->port=i;
			
			// now set the physical addresses for use by the device
			physaddr port_mem_phys=virtmem_get_physaddr(port_mem_virt);
			c->ports[i].clb=port_mem_phys&0xffffffff;
			c->ports[i].clbu=(port_mem_phys>>32)&0xffffffff;
			port_mem_phys+=1024;
			c->ports[i].fb=port_mem_phys&0xffffffff;
			c->ports[i].fbu=(port_mem_phys>>32)&0xffffffff;
			port_mem_phys+=256;
			dev->command_list[0].ctb_addr=port_mem_phys&0xffffffff;
			dev->command_list[0].ctb_addr_ext=(port_mem_phys>>32)&0xffffffff;

			// now we can restart the port
			ahci_start_port(c,i);

			// register the device in the ata subsytem
			if(!ata_register_device(dev,&ahci_primitives))
			{
				virtmem_release_heap_pages(port_mem_virt,PAGE_SIZE);
				kfree(dev);
			}
		}
	}
}
