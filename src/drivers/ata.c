/*
 * Copyright 2021-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <drivers/ata.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <vfs/blockdevice.h>
#include <vfs/descriptor.h>
#include <vfs/generic.h>
#include <vfs/ramfs.h>

#include <errno.h>

enum ata_idbuf_offsets
{
	ATA_ID_MODEL		=27,
	ATA_ID_CAPABILITIES	=49,
	ATA_ID_SECTORS28	=60,
	ATA_ID_COMMANDSETS	=82,
	ATA_ID_SECTORS48	=100,
};

struct ata_device
{
	void* data;
	const struct ata_primitives* primitives;
	uint32_t cmd_sets;
	uint16_t capabilities;
	char model[40];
};

static void generate_taskfile_lba(const struct ata_device* dev,struct ata_taskfile* tf,uint64_t start,uint16_t count)
{
	tf->flags=TASKFILE_ADDRESS|TASKFILE_DATA;
	tf->count=count;
	if(dev->capabilities&(1<<9))
	{
		// device supports lba
		tf->lba[0]=start&0xff;
		tf->lba[1]=(start>>8)&0xff;
		tf->lba[2]=(start>>16)&0xff;
		tf->device=0x40|((start>>24)&0xf);
	}
	else
	{
		// device only supports chs
		unsigned sector=(start%63)+1;
		unsigned cylinder=(start+1-sector)/(16*63);
		unsigned head=(start+1-sector)%(16*63)/63;
		tf->lba[0]=sector;
		tf->lba[1]=cylinder&0xff;
		tf->lba[2]=(cylinder>>8)&0xff;
		tf->device=head;
	}
}

static int do_rw(const struct ata_device* dev,void* buf,uint64_t sec,uint16_t count,bool write)
{
	struct ata_taskfile tf;
	generate_taskfile_lba(dev,&tf,sec,count);
	tf.data=buf;
	if(write)
	{
		tf.flags|=TASKFILE_WRITE;
		tf.cmd=ATA_WRITE_PIO;
	}
	else tf.cmd=ATA_READ_PIO;

	dev->primitives->send_cmd(dev->data,&tf);

	if(write)
	{
		tf.flags=0;
		tf.cmd=ATA_CACHE_FLUSH;
		dev->primitives->send_cmd(dev->data,&tf);
	}
	return 0;
}

static ssize_t ata_read(struct vfs_inode* dev,off_t pos,void* buf,size_t count,int flags)
{
	if(count%512 || pos%512) return -EIO;
	do_rw(dev->inode_data,buf,pos/512,count/512,false);
	return count;
}

static ssize_t ata_write(struct vfs_inode* dev,off_t pos,const void* buf,size_t count,int flags)
{
	if(count%512 || pos%512) return -EIO;
	do_rw(dev->inode_data,(void*)buf,pos/512,count/512,true);
	return count;
}

static int ata_free(struct vfs_inode* dev)
{
	kfree(dev->inode_data);
	return 0;
}

static const struct inode_functions ata_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=ata_free,
	.read=ata_read,
	.write=ata_write,
	.ioctl=generic_ioctl_enotty,
};

int ata_register_device(void* dev_data,const struct ata_primitives* primitives)
{
	static const dev_t ATA_DEVNUM=0x300;
	static unsigned ata_count=0;
	// send ATA_IDENTIFY command
	uint16_t buf[256];
	const struct ata_taskfile id_tf=
	{
		.flags=TASKFILE_DATA,
		.data=buf,
		.count=1,
		.cmd=ATA_IDENTIFY,
	};
	if(primitives->send_cmd(dev_data,&id_tf)!=0) return 0;

	// create ata_device struct
	struct ata_device* atadev=kmalloc_type(struct ata_device);
	atadev->data=dev_data;
	atadev->primitives=primitives;
	atadev->cmd_sets=buf[ATA_ID_COMMANDSETS]|buf[ATA_ID_COMMANDSETS+1]<<16;
	atadev->capabilities=buf[ATA_ID_CAPABILITIES];
	// copy the device name fixing endianness
	for(unsigned i=0;i<20;i++)
	{
		atadev->model[i*2]=buf[ATA_ID_MODEL+i]>>8;
		atadev->model[i*2+1]=buf[ATA_ID_MODEL+i]&0xff;
	}

	off_t size;
	// disk size is in different places based on lba-48 support
	if(atadev->cmd_sets&(1<<26))
	{
		size=buf[ATA_ID_SECTORS48];
		size|=buf[ATA_ID_SECTORS48+1]<<16;
	}
	else
	{
		size=buf[ATA_ID_SECTORS28];
		size|=buf[ATA_ID_SECTORS28+1]<<16;
	}
	size*=512;
	
	klog("ata: found device %40s\n\tsize %lu MiB",atadev->model,size/1048576);
	blockdevice_register(size,ATA_DEVNUM|ata_count++,atadev,&ata_functions);
	return 1;
}
