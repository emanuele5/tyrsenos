/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <kernel/time.h>
#include <lib/kprintf.h>

enum cmos_port
{
	CMOS_PORT_SELECT	=0x70,
	CMOS_PORT_DATA		=0x71,
};

enum cmos_register
{
	CMOS_REG_SECONDS	=0x00,
	CMOS_REG_MINUTES	=0x02,
	CMOS_REG_HOURS		=0x04,
	CMOS_REG_DAY		=0x07,
	CMOS_REG_MONTH		=0x08,
	CMOS_REG_YEAR		=0x09,
	CMOS_REG_STATUSA	=0x0a,
	CMOS_REG_STATUSB	=0x0b,
	CMOS_REG_CENTURY	=0x32,
};

static inline uint8_t cmos_read(enum cmos_register reg)
{
	outportb(CMOS_PORT_SELECT,reg);
	return inportb(CMOS_PORT_DATA);
}

static inline void cmos_write(enum cmos_register reg,uint8_t data)
{
	outportb(CMOS_PORT_SELECT,reg);
	outportb(CMOS_PORT_DATA,data);
}

static inline int cmos_update_in_progress(void)
{
	return (cmos_read(CMOS_REG_STATUSA)&0x80)!=0;
}

static unsigned bcd_to_int(uint8_t bcd)
{
	return (bcd>>4)*10+(bcd&0xf);
}

void cmos_init(void)
{
	unsigned seconds,minutes,hours,day,month,year;
	// wait until update has finished
	if(cmos_update_in_progress()) kdelay(20000000);
	while(1)
	{
		seconds=bcd_to_int(cmos_read(CMOS_REG_SECONDS));
		minutes=bcd_to_int(cmos_read(CMOS_REG_MINUTES));
		hours=bcd_to_int(cmos_read(CMOS_REG_HOURS));
		day=bcd_to_int(cmos_read(CMOS_REG_DAY));
		month=bcd_to_int(cmos_read(CMOS_REG_MONTH));
		year=bcd_to_int(cmos_read(CMOS_REG_YEAR));
		year+=bcd_to_int(cmos_read(CMOS_REG_CENTURY))*100;
		if(cmos_update_in_progress()) kdelay(20000000);
		else break;
	}

	klog("rtc: boot time %04d-%02d-%02dT%02d:%02d:%02d UTC",year,month,day,hours,minutes,seconds);

	// convert to unix epoch
	if(month<2) // start the year with march
	{
		month+=12;
		year--;
	}
	month-=2;
	uint64_t days=year*365-719499;	// days since march 1971
	days+=year/4-year/100+year/400;	// count leap years
	days+=367*month/12;				// days in the year
	time_t rtc_time=((days*24+hours)*60+minutes)*60+seconds;
	realtime.tv_sec=rtc_time;
	realtime.tv_nsec=0;
}
