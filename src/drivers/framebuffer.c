/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */
 
#include <drivers/framebuffer.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/virtual.h>
#include <vfs/console.h>
#include <vfs/generic.h>
#include <vfs/ramfs.h>

#include "font.h"

#include <errno.h>
#include <sunflower/framebuffer.h>

// FRAMEBUFFER FUNCTIONS

struct
{
	uint32_t* addr;
	uint16_t x_res;
	uint16_t y_res;
}fb_data;

static void framebuffer_fill(uint16_t startline,uint16_t endline,uint32_t colour)
{
	uint32_t* pos=fb_data.addr+startline*fb_data.x_res;
	size_t count=(endline-startline)*fb_data.x_res;
	for(size_t i=0;i<count;i++) pos[i]=colour;
}

static ssize_t framebuffer_read(struct vfs_inode* dev,off_t pos,void* buf,size_t count,int flags)
{
	if(pos>dev->size) return -ENOSPC;

	size_t maxreadcount=dev->size-pos;
	if(count>maxreadcount) count=maxreadcount;

	memcpy(buf,(uint8_t*)fb_data.addr+pos,count);
	return count;
}

static ssize_t framebuffer_write(struct vfs_inode* dev,off_t pos,const void* buf,size_t count,int flags)
{
	if(pos>=dev->size) return -ENOSPC;

	size_t maxwritecount=dev->size-pos;
	if(count>maxwritecount) count=maxwritecount;

	memcpy((uint8_t*)fb_data.addr+pos,buf,count);
	return count;
}

static int framebuffer_ioctl(struct vfs_inode* dev,unsigned cmd,unsigned long data)
{
	struct framebuffer_info* user_ptr;
	switch(cmd)
	{
	case FRAMEBUFFER_GET_INFO:
		// TODO: check pointer
		user_ptr=(void*)data;
		user_ptr->fb_size=dev->size;
		user_ptr->line_size=fb_data.x_res*4;
		user_ptr->x_res=fb_data.x_res;
		user_ptr->y_res=fb_data.y_res;
		user_ptr->bpp=4;
		return 0;
	default:
		return -EINVAL;
	}
}

static const struct inode_functions framebuffer_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=generic_free,
	.read=framebuffer_read,
	.write=framebuffer_write,
	.ioctl=framebuffer_ioctl,
};

//
// FB CONSOLE FUNCTIONS
//

static const uint8_t DEFAULT_FG=0x0b;
static const uint8_t DEFAULT_BG=0x00;

static const uint32_t fbcon_colors[16]=
{
	0x00000000, // black
	0x00de382b, // dark red
	0x0039b54a, // dark green
	0x00ffc706, // orange
	0x00006fb8, // dark blue
	0x00762671, // magenta
	0x002cb5e9, // dark cyan
	0x00cccccc, // light gray
	0x00808080, // gray
	0x00ff0000, // red
	0x0000ff00, // green
	0x00ffff00, // yellow
	0x000000ff, // blue
	0x00ff00ff, // magenta
	0x0000ffff, // cyan
	0x00ffffff, // white
};

enum fbcon_status {STATUS_NORMAL,STATUS_ESCAPE,STATUS_CSI,STATUS_GETPAR,STATUS_GOTPAR};

struct
{
	uint32_t bg_color;
	uint32_t fg_color;
	unsigned rows;
	unsigned cols;
	unsigned cur_row;
	unsigned cur_col;
	enum fbcon_status status;
	unsigned par[4];
	unsigned par_index;
}fbcon_data;

static void fbcon_putchar(unsigned char ch)
{
	if(ch>127 || ch<32) return;
	ch-=32;

	uint32_t* pos=fb_data.addr+(fbcon_data.cur_row*fb_data.x_res*FONT_HEIGHT)+(fbcon_data.cur_col*FONT_WIDTH);
	for(unsigned i=0;i<FONT_HEIGHT;i++)
	{
		for(unsigned j=0;j<FONT_WIDTH;j++)
		{
			if(font0[ch*FONT_HEIGHT+i]&1<<(FONT_WIDTH-1-j)) pos[j]=fbcon_data.fg_color;
			else pos[j]=fbcon_data.bg_color;
		}
		pos+=fb_data.x_res;
	}
}

static void fbcon_scroll_up(void)
{
	for(uint16_t i=FONT_HEIGHT;i<fb_data.y_res;i++)
	{
		memcpy(fb_data.addr+(i-FONT_HEIGHT)*fb_data.x_res,fb_data.addr+i*fb_data.x_res,fb_data.x_res*4);
	}
	framebuffer_fill(fb_data.y_res-FONT_HEIGHT,fb_data.y_res,fbcon_data.bg_color);
}

static void fbcon_lf(void)
{
	fbcon_data.cur_row++;
	if(fbcon_data.cur_row==fbcon_data.rows)
	{
		fbcon_data.cur_row--;
		fbcon_scroll_up();
	}
}

static void fbcon_cr(void)
{
	fbcon_data.cur_col=0;
}

static void fbcon_doA(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	if(fbcon_data.cur_row<n) fbcon_data.cur_row=0;
	else fbcon_data.cur_row-=n;
}

static void fbcon_doB(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	fbcon_data.cur_row+=n;
	if(fbcon_data.cur_row>=fbcon_data.rows) fbcon_data.cur_row=fbcon_data.rows-1;
}

static void fbcon_doC(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	fbcon_data.cur_col+=n;
	if(fbcon_data.cur_col>=fbcon_data.cols) fbcon_data.cur_col=fbcon_data.cols-1;
}

static void fbcon_doD(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	if(fbcon_data.cur_col<n) fbcon_data.cur_col=0;
	else fbcon_data.cur_col-=n;
}

static void fbcon_dod(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	fbcon_data.cur_row=n;
	if(fbcon_data.cur_row>=fbcon_data.rows) fbcon_data.cur_row=fbcon_data.rows-1;
}

static void fbcon_doG(void)
{
	unsigned n=fbcon_data.par[0];
	if(n==0) n=1;

	fbcon_data.cur_col=n;
	if(fbcon_data.cur_col>=fbcon_data.cols) fbcon_data.cur_col=fbcon_data.cols-1;
}

static void fbcon_doH(void)
{
	unsigned r=fbcon_data.par[0];
	unsigned c=fbcon_data.par[1];
	if(r>0) r--;
	if(c>0) c--;

	fbcon_data.cur_row=r;
	fbcon_data.cur_col=c;
	if(fbcon_data.cur_row>=fbcon_data.rows) fbcon_data.cur_row=fbcon_data.rows-1;
	if(fbcon_data.cur_col>=fbcon_data.cols) fbcon_data.cur_col=fbcon_data.cols-1;
}

static void fbcon_doJ(void)
{
	unsigned p=fbcon_data.par[0];
	if(p==0) framebuffer_fill(fbcon_data.cur_row*8,fb_data.y_res,fbcon_data.bg_color);
}

static void fbcon_dom(void)
{
	for(unsigned i=0;i<=fbcon_data.par_index;i++)
	{
		unsigned p=fbcon_data.par[i];
		uint32_t tmp;
		switch(p)
		{
		case 0:
			fbcon_data.fg_color=fbcon_colors[DEFAULT_FG];
			fbcon_data.bg_color=fbcon_colors[DEFAULT_BG];
			break;
		case 7:;
			tmp=fbcon_data.bg_color;
			fbcon_data.bg_color=fbcon_data.fg_color;
			fbcon_data.fg_color=tmp;
			break;
		case 10:
			break;
		case 30:
		case 31:
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:
			fbcon_data.fg_color=fbcon_colors[p-30];
			break;
		case 39:
			fbcon_data.fg_color=fbcon_colors[DEFAULT_FG];
			break;
		case 40:
		case 41:
		case 42:
		case 43:
		case 44:
		case 45:
		case 46:
		case 47:
			fbcon_data.bg_color=fbcon_colors[p-40];
			break;
		case 49:
			fbcon_data.bg_color=fbcon_colors[DEFAULT_BG];
			break;
		}
	}
}

ssize_t fbcon_write(void* device,const void* data,size_t len)
{
	const char* str=data;
	for(size_t i=0;i<len;i++)
	{
		unsigned char ch=str[i];

		switch(fbcon_data.status)
		{
		case STATUS_NORMAL:
			switch(ch)
			{
			// common escape characters
			case '\b':
				if(fbcon_data.cur_col!=0) fbcon_data.cur_col--;
				break;
			case '\t':
				fbcon_data.cur_col+=4-(fbcon_data.cur_col%4);
				if(fbcon_data.cur_col>=fbcon_data.cols)
				{
					fbcon_cr();
					fbcon_lf();
				}
				break;
			case '\n':
				fbcon_lf();
			case '\r':
				fbcon_cr();
				break;
			case '\e':
				fbcon_data.status=STATUS_ESCAPE;
				break;
			default:
				// generic character
				fbcon_putchar(ch);
				fbcon_data.cur_col++;
				// handle end of line
				if(fbcon_data.cur_col>=fbcon_data.cols)
				{
					fbcon_cr();
					fbcon_lf();
				}
				break;
			}
			break;

		case STATUS_ESCAPE:
			if(ch=='[') fbcon_data.status=STATUS_CSI;
			break;

		case STATUS_CSI:
			fbcon_data.par_index=0;
			fbcon_data.status=STATUS_GETPAR;
			memset(fbcon_data.par,0,4*sizeof(unsigned));

		case STATUS_GETPAR:
			// parse CSI parameters
			if(ch>='0'&&ch<='9')
			{
				fbcon_data.par[fbcon_data.par_index]*=10;
				fbcon_data.par[fbcon_data.par_index]+=ch-'0';
				break;
			}
			else if(ch==';')
			{
				fbcon_data.par_index++;
				break;
			}
			fbcon_data.status=STATUS_GOTPAR;

		case STATUS_GOTPAR:
			// parse CSI command
			switch(ch)
			{
			case 'A':
				fbcon_doA();
				break;
			case 'B':
				fbcon_doB();
				break;
			case 'C':
				fbcon_doC();
				break;
			case 'D':
				fbcon_doD();
				break;
			case 'd':
				fbcon_dod();
				break;
			case 'G':
				fbcon_doG();
				break;
			case 'H':
				fbcon_doH();
				break;
			case 'J':
				fbcon_doJ();
				break;
			case 'm':
				fbcon_dom();
				break;
			}
			fbcon_data.status=STATUS_NORMAL;
			break;
		}
	}
	return len;
}

void early_framebuffer_init(const struct bootinfo* info)
{
	// init framebuffer
	fb_data.x_res=info->display_xres;
	fb_data.y_res=info->display_yres;
	size_t size=fb_data.x_res*fb_data.y_res*4;
	fb_data.addr=virtmem_map_kernel_range(info->vesa_fb,size,PAGE_WT);
	
	// init the fb console
	fbcon_data.bg_color=fbcon_colors[DEFAULT_BG];
	fbcon_data.fg_color=fbcon_colors[DEFAULT_FG];
	fbcon_data.rows=fb_data.y_res/FONT_HEIGHT;
	fbcon_data.cols=fb_data.x_res/FONT_WIDTH;
	fbcon_data.status=STATUS_NORMAL;
	
	framebuffer_fill(0,fb_data.y_res,fbcon_colors[DEFAULT_BG]);
	console_register_output(fbcon_write,NULL);
	klog("framebuffer0: boot framebuffer %dx%dx%d",fb_data.x_res,fb_data.y_res,32);
}

void framebuffer_init(void)
{
	size_t size=fb_data.x_res*fb_data.y_res*4;
	ramfs_register_device("framebuffer0",S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP,size,0x0f00,&fb_data,&framebuffer_functions);

	tty_register_output(fbcon_write,NULL);
}
