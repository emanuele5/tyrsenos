/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <drivers/ata.h>
#include <drivers/ide.h>
#include <lib/kprintf.h>
#include <memory/kmalloc.h>
#include <kernel/time.h>

// registri delle porte principali
enum ide_register
{
	IDE_REG_DATA		=0x00,
	IDE_REG_ERROR		=0x01,
	IDE_REG_FEATURES	=0x01,
	IDE_REG_SECCOUNT0	=0x02,
	IDE_REG_LBA0		=0x03,
	IDE_REG_LBA1		=0x04,
	IDE_REG_LBA2		=0x05,
	IDE_REG_HDDEVSEL	=0x06,
	IDE_REG_COMMAND		=0x07,
	IDE_REG_STATUS		=0x07,
	// extended registers
	IDE_REG_SECCOUNT1	=0x02,
	IDE_REG_LBA3		=0x03,
	IDE_REG_LBA4		=0x04,
	IDE_REG_LBA5		=0x05,
	// registri delle porte di controllo
	IDE_REG_CONTROL		=0x0c,
	IDE_REG_ALTSTATUS	=0x0c,
	IDE_REG_DEVADDR		=0x0d,
};

struct ide_channel
{
	uint16_t cmd;
	uint16_t ctrl;
	uint16_t dma_ctrl;
	uint8_t irq;
	uint8_t irq_disable;
};

struct ide_device
{
	struct ide_channel* channel;
	uint8_t drive;
};

static int ide_send_cmd(void* dev,const struct ata_taskfile* tf);

static const struct ata_primitives ide_primitives=
{
	.send_cmd=ide_send_cmd,
};

static void ide_write(const struct ide_channel* c,enum ide_register reg,uint8_t data)
{
	if(reg<0xc)
	{
		// command registers
		return outportb(c->cmd+reg,data);
	}
	else if(reg<0xe)
	{
		// control registers
		return outportb(c->ctrl+reg-0xa,data);
	}
}

static uint8_t ide_read(const struct ide_channel* c,enum ide_register reg)
{
	if(reg<0xc)
	{
		// command registers
		return inportb(c->cmd+reg);
	}
	else if(reg<0xe)
	{
		// control registers
		return inportb(c->ctrl+reg-0xa);
	}
	else return 0xff;
}

static inline uint16_t ide_read_data16(const struct ide_channel* c)
{
	return inportw(c->cmd);
}

static inline void ide_write_data16(const struct ide_channel* c,uint16_t data)
{
	outportw(c->cmd,data);
}

static inline void ide_wait_not_busy(const struct ide_channel* c)
{
	while(ide_read(c,IDE_REG_STATUS) & (ATA_STATUS_BUSY|ATA_STATUS_DRQ));
}

static inline void ide_wait_not_busy_timeout(const struct ide_channel* c,unsigned timeout)
{
	while(ide_read(c,IDE_REG_STATUS) & (ATA_STATUS_BUSY|ATA_STATUS_DRQ) && timeout--);
}

static int ide_send_cmd(void* dev,const struct ata_taskfile* tf)
{
	const struct ide_device* d=dev;

	// wait for drive not busy
	ide_wait_not_busy(d->channel);

	// select drive
	uint8_t devsel=0xa0|tf->device|(d->drive<<4);
	ide_write(d->channel,IDE_REG_HDDEVSEL,devsel);

	// set registers
	ide_wait_not_busy(d->channel);
	ide_write(d->channel,IDE_REG_FEATURES,tf->feature);
	if(tf->flags&TASKFILE_ADDRESS)
	{
		ide_write(d->channel,IDE_REG_SECCOUNT0,tf->count);
		ide_write(d->channel,IDE_REG_LBA0,tf->lba[0]);
		ide_write(d->channel,IDE_REG_LBA1,tf->lba[1]);
		ide_write(d->channel,IDE_REG_LBA2,tf->lba[2]);
	}

	// send command
	ide_write(d->channel,IDE_REG_COMMAND,tf->cmd);
	// check for errors
	if(ide_read(d->channel,IDE_REG_STATUS)&ATA_STATUS_ERR) return ide_read(d->channel,IDE_REG_ERROR);

	// do data transfer if necessary
	if(tf->flags&TASKFILE_DATA)
	{
		uint16_t* buf=(uint16_t*)tf->data;
		for(size_t i=0;i<tf->count;i++)
		{
			// before transferring each sector, wait for drive not busy
			while(ide_read(d->channel,IDE_REG_STATUS)&ATA_STATUS_BUSY);
			for(int j=0;j<256;j++)
			{
				if(tf->flags&TASKFILE_WRITE) ide_write_data16(d->channel,*buf++);
				else *buf++=ide_read_data16(d->channel);
			}
		}
	}
	return 0;
}

static int ide_init_channel(struct ide_channel* c)
{
	// number of devices detected on the channel
	unsigned disk_found=0;

	ide_write(c,IDE_REG_CONTROL,2);

	for(int d=0;d<2;d++)
	{
		// NOTE: the waiting parts don't follow any logic, but it works on all the machines I tried
		// select drive
		ide_wait_not_busy_timeout(c,10000);
		ide_write(c,IDE_REG_HDDEVSEL,0xa0|(d<<4));
		ide_wait_not_busy_timeout(c,10000);

		// check for drive present
		uint8_t status=ide_read(c,IDE_REG_STATUS);
		if(!(status&ATA_STATUS_DRDY) || (status&ATA_STATUS_ERR)) continue;

		// call the ata subsystem to initialize it
		struct ide_device* dev=kmalloc(sizeof(struct ide_device));
		dev->channel=c;
		dev->drive=d;
		if(!ata_register_device(dev,&ide_primitives)) kfree(dev);
		else disk_found++;
	}
	return disk_found;
}

void pci_ide_init(const struct pci_device* dev)
{
	struct ide_channel* ch1=kmalloc(sizeof(struct ide_channel));
	struct ide_channel* ch2=kmalloc(sizeof(struct ide_channel));
	uint8_t prog_if=pci_dev_get_progif(dev);
	if(prog_if&1)
	{
		// first channel in pci mode
		ch1->cmd=pci_dev_get_bar0(dev)&0xfffc;
		ch1->ctrl=pci_dev_get_bar1(dev)&0xfffc;
	}
	else
	{
		ch1->cmd=0x1f0;
		ch1->ctrl=0x3f6;
	}
	if(prog_if&4)
	{
		// second channel in pci mode
		ch2->cmd=pci_dev_get_bar2(dev)&0xfffc;
		ch2->ctrl=pci_dev_get_bar3(dev)&0xfffc;
	}
	else
	{
		ch2->cmd=0x170;
		ch2->ctrl=0x376;
	}
	if(prog_if&0x80)
	{
		// supports pci bus-master
		uint16_t base=pci_dev_get_bar4(dev)&0xfffc;
		ch1->dma_ctrl=base;
		ch2->dma_ctrl=base+8;
	}
	else
	{
		ch1->dma_ctrl=0;
		ch2->dma_ctrl=0;
	}
	ch1->irq_disable=2;
	ch2->irq_disable=2;
	klog("ide: found PCI IDE controller\n\tchannel 1 io: %x %x\n\tchannel 2 io: %x %x",ch1->cmd,ch1->ctrl,ch2->cmd,ch2->ctrl);
	if(!ide_init_channel(ch1)) kfree(ch1);
	if(!ide_init_channel(ch2)) kfree(ch2);
}
