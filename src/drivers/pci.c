/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <drivers/ahci.h>
#include <drivers/ide.h>
#include <drivers/pci.h>
#include <drivers/pic.h>
#include <drivers/rtl8139.h>
#include <kernel/interrupts.h>
#include <lib/kprintf.h>
#include <memory/kmalloc.h>

enum pci_class
{
	PCI_CLASS_IDE		=0x0101,
	PCI_CLASS_SATA		=0x0106,
	PCI_CLASS_ETHERNET	=0x0200,
	PCI_CLASS_VGA		=0x0300,
	PCI_CLASS_ISA		=0x0601,
	PCI_CLASS_PCIBRIDGE	=0x0604,
	PCI_CLASS_USB		=0x0c03,
};

enum pci_progif
{
	PCI_PROGIF_USB_UHCI=0x00,
};

enum pci_vendor
{
	PCI_VENDOR_INTEL=0x8086,
};

enum pci_status
{
	PCI_STATUS_INTERRUPT=8,
};

static void pci_bus_check(uint8_t bus);

static void pci_load_driver(const struct pci_device* dev)
{
	uint16_t vendorID=pci_dev_get_vendor(dev);
	uint16_t deviceID=pci_dev_get_device(dev);
	uint16_t devclass=pci_dev_get_class(dev);
	//uint8_t progif=pci_dev_check_progif(dev);
	klog("pci %02x:%02x.%x: vendor=%04x device=%04x class=%04x",dev->bus,dev->slot,dev->func,vendorID,deviceID,devclass);

	struct pci_device* devcopy=kmalloc(sizeof(struct pci_device));
	devcopy->bus=dev->bus;
	devcopy->slot=dev->slot;
	devcopy->func=dev->func;
	// load the driver
	switch(devclass)
	{
	case PCI_CLASS_IDE:
		pci_ide_init(devcopy);
		break;

	case PCI_CLASS_SATA:
		pci_ahci_init(devcopy);
		break;
		
	case PCI_CLASS_ETHERNET:
		if(vendorID==0x10ec && deviceID==0x8139) rtl8139_init(devcopy);
		else kfree(devcopy);
		break;

	case PCI_CLASS_PCIBRIDGE:
		pci_bus_check(pci_dev_get_secbus(devcopy));
		kfree(devcopy);
		break;

	default:
		kfree(devcopy);
		break;
	}
}

static void pci_dev_check(uint8_t bus,uint8_t dev)
{
	struct pci_device cur_dev={bus,dev,0};
	uint16_t vendorID=pci_dev_get_vendor(&cur_dev);
	if(vendorID==0xffff) return;
	pci_load_driver(&cur_dev);

	// check for multifunction device
	if(pci_dev_get_header(&cur_dev)&0x80)
	{
		for(cur_dev.func=1;cur_dev.func<8;cur_dev.func++)
		{
			vendorID=pci_dev_get_vendor(&cur_dev);
			if(vendorID==0xffff) continue;
			pci_load_driver(&cur_dev);
		}
	}
}

static void pci_bus_check(uint8_t bus)
{
	for(uint8_t device=0;device<32;device++)
	{
		pci_dev_check(bus,device);
	}
}

void pci_init(void* pcie_addr)
{
	pci_bus_check(0);
}

//
//	PCI interrupt handling functions
//

struct pci_interrupt_handler
{
	const struct pci_device* device;
	void* data;
	void(*function)(void*);
};

struct pci_interrupt
{
	size_t n_handlers;
	struct pci_interrupt_handler* handlers;
};

static struct pci_interrupt pci_interrupts[15];

static void pci_interrupt_dispatcher(struct user_context* ctx)
{
	unsigned intnum=ctx->int_num-IRQ_START;
	
	const struct pci_interrupt* intr=pci_interrupts+intnum;
	for(size_t i=0;i<intr->n_handlers;i++)
	{
		const struct pci_interrupt_handler* handler=(intr->handlers)+i;
		uint16_t status=pci_dev_get_status(handler->device);
		if(status&PCI_STATUS_INTERRUPT)
		{
			handler->function(handler->data);
		}
	}
	pic_ack_irq(intnum);
}

void pci_register_interrupt_handler(const struct pci_device* dev,void* private_data,void(function)(void*))
{
	uint8_t intnum=pci_dev_get_int_line(dev);
	if(intnum==0xff) return; // device doesn't use an interrupt
	
	struct pci_interrupt* intr=pci_interrupts+intnum;
	intr->handlers=krealloc_array(intr->handlers,struct pci_interrupt_handler,intr->n_handlers+1);
	
	struct pci_interrupt_handler* handler=(intr->handlers)+intr->n_handlers;
	handler->device=dev;
	handler->data=private_data;
	handler->function=function;
	
	if(intr->n_handlers==0)
	{
		interrupt_set(intnum+IRQ_START,pci_interrupt_dispatcher);
		pic_enable_irq(intnum);
	}
	intr->n_handlers++;
}
