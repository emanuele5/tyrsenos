/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <drivers/pic.h>

enum pic_ports
{
	PIC_MASTER_CMD	=0x20,
	PIC_MASTER_DATA	=0x21,
	PIC_SLAVE_CMD	=0xa0,
	PIC_SLAVE_DATA	=0xa1,
};

static const uint8_t PIC_EOI=0x20;

void pic_init(void)
{
	// send PIC initialization command
	outportb(PIC_MASTER_CMD,0x11);
	outportb(PIC_SLAVE_CMD,0x11);
	/* now the PICs wait for 3 initializazion bytes
	 * 1) irq offset in the interrupts vector
	 * 2) master-slave linking
	 * 3) other environment informations */
	outportb(PIC_MASTER_DATA,IRQ_START);
	outportb(PIC_SLAVE_DATA,IRQ_START+8);
	outportb(PIC_MASTER_DATA,4);
	outportb(PIC_SLAVE_DATA,2);
	outportb(PIC_MASTER_DATA,1);
	outportb(PIC_SLAVE_DATA,1);
	// mask all IRQs (except IRQ 2 which is needed by the master to communicate with the slave)
	outportb(PIC_MASTER_DATA,0xfb);
	outportb(PIC_SLAVE_DATA,0xff);
	sti();
}

void pic_ack_irq(int n)
{
	if(n>=8) outportb(PIC_SLAVE_CMD,PIC_EOI);
	outportb(PIC_MASTER_CMD,PIC_EOI);
}

void pic_disable_irq(int n)
{
	uint16_t port;
	if(n<8) port=PIC_MASTER_DATA;
	else
	{
		port=PIC_SLAVE_DATA;
		n-=8;
	}
	uint8_t mask=inportb(port)|(1<<n);
	outportb(port,mask);
}

void pic_enable_irq(int n)
{
	uint16_t port;
	if(n<8) port=PIC_MASTER_DATA;
	else
	{
		port=PIC_SLAVE_DATA;
		n-=8;
	}
	uint8_t mask=inportb(port)&~(1<<n);
	outportb(port,mask);
}
