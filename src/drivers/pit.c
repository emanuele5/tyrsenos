/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <drivers/pic.h>
#include <drivers/pit.h>
#include <kernel/interrupts.h>
#include <kernel/time.h>

static const uint64_t HZ=1193182;
static const uint16_t DEFAULT_RELOAD=(HZ*20000000u/1000000000u);

enum pit_ports
{
	PIT_CHANNEL0	=0x40,
	PIT_CHANNEL1	=0x41,
	PIT_CHANNEL2	=0x42,
	PIT_CMD			=0x43,
};

static uint16_t cur_reload=0;

static inline uint16_t nanos_to_reload(unsigned long nanos)
{
	return HZ*nanos/1000000000ull;
}

static inline unsigned long reload_to_nanos(uint16_t reload)
{
	return reload*1000000000ull/HZ;
}

static void pit_set(uint16_t value)
{
	if(value==0) value=2;
	outportb(PIT_CMD,0x34);
	outportb(PIT_CHANNEL0,value&0xff);
	outportb(PIT_CHANNEL0,value>>8);
	cur_reload=value;
}

void pit_handler(struct user_context*)
{
	// restore old value if we arrive from a oneshot
	uint16_t old_reload=cur_reload;
	if(cur_reload!=DEFAULT_RELOAD) pit_set(DEFAULT_RELOAD);
	pic_ack_irq(0);

	ktick(reload_to_nanos(old_reload));
}

void pit_alarm(clock_t nanos)
{
	pit_set(nanos_to_reload(nanos));
}

void pit_init()
{
	pit_set(DEFAULT_RELOAD);
	interrupt_set(IRQ_START+0,pit_handler);
	pic_enable_irq(0);
}
