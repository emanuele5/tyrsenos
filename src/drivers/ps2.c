/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <drivers/pic.h>
#include <vfs/console.h>
#include <vfs/inputdevice.h>
#include <kernel/interrupts.h>
#include <lib/kprintf.h>

#include <sunflower/input.h>

enum ps2_io
{
	PS2_IO_DATA	=0x60,
	PS2_IO_CMD	=0x64,
};

enum ps2_port
{
	PS2_PORT1,
	PS2_PORT2,
};

enum ps2_command
{
	PS2_READ_CFG		=0x20,
	PS2_WRITE_CFG		=0x60,
	PS2_PORT2_DISABLE	=0xa7,
	PS2_PORT2_ENABLE	=0xa8,
	PS2_PORT2_TEST		=0xa9,
	PS2_SELF_TEST		=0xaa,
	PS2_PORT1_TEST		=0xab,
	PS2_PORT1_DISABLE	=0xad,
	PS2_PORT1_ENABLE	=0xae,
	PS2_PORT2_SEND		=0xd4,

	PS2_IDENTIFY		=0xf2,
	PS2_SET_SAMPLE_RATE	=0xf3,
	PS2_ENABLE_SCAN		=0xf4,
	PS2_DISABLE_SCAN	=0xf5,
	PS2_ACK				=0xfa,
	PS2_RESET			=0xff,
};

static bool isdualport=true;
static struct tty_data* keyboard_tty;
static struct inputdevice_data* mouse_dev;
static struct inputdevice_data* keyboard_dev;

static void ps2_keyboard_interrupt(struct user_context*);
static void ps2_init_keyboard(enum ps2_port port);
static void ps2_mouse_interrupt(struct user_context*);
static void ps2_init_mouse(enum ps2_port port);

//
// PS/2 helper functions
//

static inline void ps2_flush(void)
{
	while(inportb(PS2_IO_CMD)&1) inportb(PS2_IO_DATA);
}

static inline void ps2_send_wait(void)
{
	while(inportb(PS2_IO_CMD)&2);
}

static inline void ps2_recv_wait(void)
{
	while(!(inportb(PS2_IO_CMD)&1));
}

static inline void ps2_cmd(enum ps2_command cmd)
{
	ps2_send_wait();
	outportb(PS2_IO_CMD,cmd);
}

static void ps2_send(uint8_t data,enum ps2_port port)
{
	if(port==PS2_PORT2)
	{
		ps2_send_wait();
		outportb(PS2_IO_CMD,PS2_PORT2_SEND);
	}
	ps2_send_wait();
	outportb(PS2_IO_DATA,data);
}

static inline uint8_t ps2_recv()
{
	ps2_recv_wait();
	return inportb(PS2_IO_DATA);
}

static inline void ps2_write_config(uint8_t cfg)
{
	ps2_cmd(PS2_WRITE_CFG);
	ps2_send(cfg,0);
}

static inline uint8_t ps2_read_config()
{
	ps2_cmd(PS2_READ_CFG);
	return ps2_recv();
}

// questa funzione abilita una porta, la controlla, identifica il dispositivo
// e chiama la funzione per inizializzarlo per poi disabilitarla
static void check_port(enum ps2_port port)
{
	enum ps2_command test_cmd=PS2_PORT1_TEST;
	enum ps2_command enable_cmd=PS2_PORT1_ENABLE;
	if(port==PS2_PORT2)
	{
		test_cmd=PS2_PORT2_TEST;
		enable_cmd=PS2_PORT2_ENABLE;
	}
	ps2_flush();
	// check the port
	ps2_cmd(test_cmd);
	uint8_t check=ps2_recv();
	if(check!=0) return;

	//identify device
	ps2_cmd(enable_cmd);
	ps2_send(PS2_DISABLE_SCAN,port);
	ps2_recv();
	ps2_send(PS2_IDENTIFY,port);
	ps2_recv();
	uint8_t id_byte=ps2_recv();

	const char* id_string="";
	switch(id_byte)
	{
	case 0:
		id_string="Standard PS/2 mouse";
		ps2_init_mouse(port);
		break;
	case 3:
		id_string="Mouse with scroll wheel";
		ps2_init_mouse(port);
		break;
	case 4:
		id_string="Mouse with 5 buttons";
		ps2_init_mouse(port);
		break;
	case 0xab:
		id_byte=ps2_recv();
		switch(id_byte)
		{
		case 0x41:
		case 0xc1:
			id_string="Controller translated MF2 keyboard";
			break;
		case 0x83:
			id_string="MF2 keyboard";
			break;
		}
		ps2_init_keyboard(port);
		break;
	}
	klog("ps2(%d): %s",port,id_string);
}

void ps2_init(void)
{
	// disable devices
	ps2_cmd(PS2_PORT2_DISABLE);
	ps2_cmd(PS2_PORT1_DISABLE);
	ps2_flush();

	// disable IRQs and translation in the controller configuration byte
	uint8_t cfg_byte=ps2_read_config();
	cfg_byte&=0b00111100;
	ps2_write_config(cfg_byte);

	// execute controller self-test
	ps2_cmd(PS2_SELF_TEST);
	uint8_t check=ps2_recv();
	if(check!=0x55) return;

	/* il controller potrebbe essersi resettato a questo punto,
	 * reload configuration byte*/
	ps2_write_config(cfg_byte);

	// verifichiamo che sia un controller doppio abilitando la 2^ porta
	ps2_cmd(PS2_PORT2_ENABLE);
	cfg_byte=ps2_read_config();
	if((cfg_byte&(1<<5))==0) ps2_cmd(PS2_PORT2_DISABLE);
	else isdualport=false;

	// check first port
	check_port(PS2_PORT1);
	// check second port
	if(isdualport) check_port(PS2_PORT2);

	ps2_flush();
	cfg_byte=ps2_read_config();
	// abilita la scansione e imposta gli interrupt
	ps2_send(PS2_ENABLE_SCAN,PS2_PORT1);
	ps2_recv();
	interrupt_set(IRQ_START+1,ps2_keyboard_interrupt);
	pic_enable_irq(1);
	if(isdualport)
	{
		ps2_send(PS2_ENABLE_SCAN,PS2_PORT2);
		ps2_recv();
		interrupt_set(IRQ_START+12,ps2_mouse_interrupt);
		pic_enable_irq(12);
	}
	// enable irq
	cfg_byte|=3;
	ps2_write_config(cfg_byte);
	ps2_flush();
	// reenable ports
	ps2_cmd(PS2_PORT2_ENABLE);
	ps2_cmd(PS2_PORT1_ENABLE);
}

//
// keyboard functions
//

// scancode-2 to keycode conversion table
static const uint8_t keytable[256]=
{
	0x00,0x42,0x00,0x3e,0x3c,0x3a,0x3b,0x45,0x00,0x43,0x41,0x3f,0x3d,0x2b,0x35,0x00,
	0x00,0xe2,0xe1,0x00,0xe0,0x14,0x1e,0x00,0x00,0x00,0x1d,0x16,0x04,0x1a,0x1f,0x00,
	0x00,0x06,0x1b,0x07,0x08,0x21,0x20,0x00,0x00,0x2c,0x19,0x09,0x17,0x15,0x22,0x00,
	0x00,0x11,0x05,0x0b,0x0a,0x1c,0x23,0x00,0x00,0x00,0x10,0x0d,0x18,0x24,0x25,0x00,
	0x00,0x36,0x0e,0x0c,0x12,0x27,0x26,0x00,0x00,0x37,0x38,0x0f,0x33,0x13,0x2d,0x00,
	0x00,0x00,0x34,0x00,0x2f,0x2e,0x00,0x00,0x39,0xe5,0x28,0x30,0x00,0x31,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x2a,0x00,0x00,0x1e,0x00,0x21,0x24,0x00,0x00,0x00,
	0x27,0x37,0x1f,0x22,0x23,0x25,0x29,0x00,0x44,0x00,0x20,0x2d,0x00,0x26,0x47,0x00,
	// extended scancodes and F7 (which is 0x83 for some reason)
	0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0xe6,0x00,0x00,0xe4,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x4d,0x00,0x50,0x4a,0x00,0x00,0x00,
	0x49,0x4c,0x51,0x00,0x4f,0x52,0x00,0x00,0x00,0x00,0x4e,0x00,0x00,0x4b,0x00,0x00,
};

// keycode to ASCII conversion table
static const char asciitable[52]=
{
	'a','b','c','d','e','f','g','h','i','j','k','l',
	'm','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2',
	'3','4','5','6','7','8','9','0','\n','\e','\b','\t',' ','-','=','[',
	']','\\',';','\'','~',',','.','/',
};
static const char asciitable_shifted[52]=
{
	'A','B','C','D','E','F','G','H','I','J','K','L',
	'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','@',
	'#','$','%','^','&','*','(',')','\n','\e','\b','\t',' ','_','+','{',
	'}','|',':','"','`','<','>','?',
};

enum keyboard_status
{
	STATUS_RELEASE	=1,
	STATUS_SHIFT	=2,
	STATUS_CAPSLOCK	=4,
	STATUS_EXTENDED	=8,
	STATUS_CONTROL	=16,
};

void ps2_keyboard_interrupt(struct user_context*)
{
	static enum keyboard_status status=0;

	uint8_t scancode=inportb(PS2_IO_DATA);
	pic_ack_irq(1);

	if(scancode==0xf0) // handle release prefix
	{
		status|=STATUS_RELEASE;
		return;
	}
	else if(scancode==0xe0) // handle extended prefix
	{
		status|=STATUS_EXTENDED;
		return;
	}

	// convert scancode to keycode
	if(status&STATUS_EXTENDED)
	{
		scancode+=128;
		status&=~STATUS_EXTENDED;
	}
	uint8_t keycode=keytable[scancode];

	if(status&STATUS_RELEASE)
	{
		if(keycode==INPUT_KEY_RIGHTSHIFT || keycode==INPUT_KEY_LEFTSHIFT) status&=~STATUS_SHIFT;
		else if(keycode==INPUT_KEY_RIGHTCTRL || keycode==INPUT_KEY_LEFTCTRL) status&=~STATUS_CONTROL;
		status&=~STATUS_RELEASE;
		inputdevice_putdata(keyboard_dev,INPUT_TYPE_KEYRELEASE,keycode);
	}
	else
	{
		if(keycode==INPUT_KEY_RIGHTSHIFT || keycode==INPUT_KEY_LEFTSHIFT) status|=STATUS_SHIFT;
		else if(keycode==INPUT_KEY_RIGHTCTRL || keycode==INPUT_KEY_LEFTCTRL) status|=STATUS_CONTROL;
		else if(keycode==INPUT_KEY_CAPSLOCK) status^=STATUS_CAPSLOCK;
		else if(keycode!=0 && keycode<56)
		{
			char ch;
			if(status&STATUS_CONTROL)
			{
				ch=asciitable[keycode-4]-'a'+1;
			}
			else
			{
				// get normal ascii character to send to tty
				if(status&STATUS_SHIFT) ch=asciitable_shifted[keycode-4];
				else ch=asciitable[keycode-4];
				if(status&STATUS_CAPSLOCK && keycode<30) ch^=0x20; // this is genius
			}
			tty_put(keyboard_tty,ch);
		}
		inputdevice_putdata(keyboard_dev,INPUT_TYPE_KEYPRESS,keycode);
	}
}

void ps2_init_keyboard(enum ps2_port port)
{
	// reset device
	ps2_send(PS2_RESET,port);
	ps2_recv();
	uint8_t test=ps2_recv();
	if(test!=0xaa) return;

	// set scancode set 2
	ps2_send(0xf0,port);
	ps2_send(2,port);
	ps2_recv();

	keyboard_tty=tty_register_input();
	keyboard_dev=inputdevice_create();
}

//
// mouse functions
//

static void ps2_mouse_interrupt(struct user_context*)
{
	static uint8_t packet[3];
	static int i=0;

	packet[i]=inportb(PS2_IO_DATA);
	pic_ack_irq(12);

	// bit 3 of the first byte should always be 1
	if((i==0)&&(packet[0]&8)==0)
	{
		klog("ps2: lost mouse sync");
		return;
	}

	// after three byte the packet is complete
	if(i==2)
	{
		i=0;
		if(packet[0]&1) inputdevice_putdata(mouse_dev,INPUT_TYPE_CLICK,INPUT_CLICK_LEFT);
		if(packet[0]&2) inputdevice_putdata(mouse_dev,INPUT_TYPE_CLICK,INPUT_CLICK_RIGHT);
		if(packet[0]&4) inputdevice_putdata(mouse_dev,INPUT_TYPE_CLICK,INPUT_CLICK_MIDDLE);

		int relx=packet[1]-((packet[0]<<4)&0x100);
		int rely=packet[2]-((packet[0]<<3)&0x100);
		inputdevice_putdata(mouse_dev,INPUT_TYPE_MOVXREL,relx);
		inputdevice_putdata(mouse_dev,INPUT_TYPE_MOVYREL,rely);
	}
	else i++;
}

static void ps2_init_mouse(enum ps2_port port)
{
	// reset device
	ps2_send(PS2_RESET,port);
	ps2_recv();
	uint8_t test=ps2_recv();
	if(test!=0xaa) return;

	// set sample rate
	ps2_send(PS2_SET_SAMPLE_RATE,port);
	ps2_send(10,port);
	ps2_recv();

	mouse_dev=inputdevice_create();
}
