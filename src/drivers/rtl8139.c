/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <drivers/pci.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>
#include <net/core.h>
#include <net/ethernet.h>

enum rtl8139_register
{
	IDR0	=0x00,
	IDR1	=0x01,
	IDR2	=0x02,
	IDR3	=0x03,
	IDR4	=0x04,
	IDR5	=0x05,
	TSD0	=0x10,
	TSD1	=0x14,
	TSD2	=0x18,
	TSD3	=0x1c,
	TSAD0	=0x20,
	TSAD1	=0x24,
	TSAD2	=0x28,
	TSAD3	=0x2c,
	RBSTART	=0x30,
	CR		=0x37,
	CAPR	=0x38,
	IMR		=0x3c,
	ISR		=0x3e,
	RCR		=0x44,
	CONFIG1	=0x52,
};

enum rtl8139_interrupt
{
	ROK		=0x0001,
	RER		=0x0002,
	TOK		=0x0004,
	TER		=0x0008,
	RXOVW	=0x0010,
	PUN		=0x0020,
	LinkChg	=0x0020,
	FOVW	=0x0040,
	LenChg	=0x2000,
	TimeOut	=0x4000,
	SERR	=0x8000,
};

struct rtl8139_data
{
	uint16_t iobase;
	uint8_t* rxbuf;
	uint32_t rx_current;
	uint8_t* txarea;
	uint8_t* txbufs[4];
	unsigned tx_current;
};

static const unsigned RXBUF_SIZE=8192;
static const unsigned RXBUF_AREA_SIZE=RXBUF_SIZE+16+2048;
static const unsigned TXBUF_SIZE=1792;
static const unsigned TXBUF_NUM=4;
static const unsigned TXBUF_AREA_SIZE=TXBUF_SIZE*TXBUF_NUM;

static inline void rtl8139_out8(const struct rtl8139_data* devdata,enum rtl8139_register reg,uint8_t data)
{
	outportb(devdata->iobase+reg,data);
}

static inline void rtl8139_out16(const struct rtl8139_data* devdata,enum rtl8139_register reg,uint16_t data)
{
	outportw(devdata->iobase+reg,data);
}

static inline void rtl8139_out32(const struct rtl8139_data* devdata,enum rtl8139_register reg,uint32_t data)
{
	outportd(devdata->iobase+reg,data);
}

static inline uint8_t rtl8139_in8(const struct rtl8139_data* devdata,enum rtl8139_register reg)
{
	return inportb(devdata->iobase+reg);
}

static inline uint16_t rtl8139_in16(const struct rtl8139_data* devdata,enum rtl8139_register reg)
{
	return inportw(devdata->iobase+reg);
}

void rtl8139_handler(void* ifaceptr)
{
	struct network_interface* interface=ifaceptr;
	struct rtl8139_data* devdata=interface->private_data;
	uint16_t status=rtl8139_in16(devdata,ISR);
	// acknowledge interrupts
	rtl8139_out16(devdata,ISR,TOK|ROK);

	if(status&ROK)
	{
		uint32_t rx_current=devdata->rx_current;
		uint32_t rx_offset=rx_current%RXBUF_SIZE;

		uint32_t rx_status=*(uint32_t*)(devdata->rxbuf+rx_offset);
		uint16_t rx_size=rx_status>>16;
	
		// create packet struct and copy the packet there
		struct network_packet* packet=kmalloc_type(struct network_packet);
		packet->data=kmalloc(rx_size);
		packet->offset=0;
		packet->size=rx_size;
		packet->interface=interface;
		memcpy(packet->data,devdata->rxbuf+rx_offset+4,rx_size);

		// update rx buffer pointer
		rx_current=(rx_current+rx_size+4+3)&~3;
		devdata->rx_current=rx_current;
		
		// send the packet to the ethernet layer
		ethernet_packet_handle(packet);
	}
	else if(status&TOK)
	{
		// TODO
	}
}

void rtl8139_transmit(struct network_interface* interface,struct network_packet* packet)
{
	struct rtl8139_data* devdata=interface->private_data;
	unsigned index=devdata->tx_current%4;
	
	kprintf("output %d\n",index);

	unsigned size=packet->size;
	memcpy(devdata->txbufs[index],packet->data+packet->offset,packet->size);
	rtl8139_out32(devdata,TSD0+index*4,size);
	
	devdata->tx_current++;
}

void rtl8139_init(struct pci_device* pcidev)
{
	// allocate data structures
	struct rtl8139_data* devdata=kmalloc_type(struct rtl8139_data);
	devdata->iobase=pci_dev_get_bar0(pcidev)&0xfffc;
	klog("rtl8139: iobase %04x",devdata->iobase);

	struct network_interface* interface=kmalloc_type(struct network_interface);
	interface->private_data=devdata;
	// copy mac address
	for(unsigned i=0;i<6;i++)
	{
		interface->ether_address.ether_addr_octet[i]=rtl8139_in8(devdata,IDR0+i);
	}
	klog("rtl8139: mac address %02x:%02x:%02x:%02x:%02x:%02x",
		interface->ether_address.ether_addr_octet[0],interface->ether_address.ether_addr_octet[1],
		interface->ether_address.ether_addr_octet[2],interface->ether_address.ether_addr_octet[3],
		interface->ether_address.ether_addr_octet[4],interface->ether_address.ether_addr_octet[5]);
	
	// allocate buffers
	devdata->rxbuf=virtmem_get_heap_pages(RXBUF_AREA_SIZE);
	devdata->txarea=virtmem_get_heap_pages(TXBUF_AREA_SIZE);
	// TODO: check physical addresses are 32-bit
	for(unsigned i=0;i<4;i++)
	{
		devdata->txbufs[i]=devdata->txarea+TXBUF_SIZE*i;
	}
	devdata->rx_current=0;
	devdata->tx_current=0;

	// power on
	rtl8139_out8(devdata,CONFIG1,0);
	// software reset
	rtl8139_out8(devdata,CR,0x10);
	while((rtl8139_in8(devdata,CR)&0x10)!=0);
	// set rx and tx buffers
	rtl8139_out32(devdata,RBSTART,virtmem_get_physaddr(devdata->rxbuf));
	physaddr txbuf_phys=virtmem_get_physaddr(devdata->txarea);
	for(unsigned i=0;i<4;i++)
	{
		rtl8139_out32(devdata,TSAD0+i*4,txbuf_phys+TXBUF_SIZE*i);
	}

	// register pci interrupt
	pci_register_interrupt_handler(pcidev,interface,rtl8139_handler);
	// enable dma
	pci_dev_set_command(pcidev,pci_dev_get_command(pcidev)|4);
	// set interrupt mask to allow Transmit OK and Receive OK interrupts
	rtl8139_out16(devdata,IMR,TOK|ROK);
	// configure receive buffer, accept all packets and enable delayed wrap
	rtl8139_out32(devdata,RCR,0x8f);
	// set Transmitter Enabled and Receiver Enabled
	rtl8139_out8(devdata,CR,0x0c);
}
