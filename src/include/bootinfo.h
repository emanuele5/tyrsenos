#pragma once

	#include <stdint.h>

	#define MEM_USABLE	1
	#define MEM_RESERVED	2

	struct bios_mm
	{
		uint64_t b_addr;
		uint64_t len;
		uint32_t type;
		uint32_t acpi_attr;
	};

	struct bootinfo
	{
		uint64_t mem_map;
		uint16_t mem_map_size;
		uint16_t display_xres;
		uint16_t display_yres;
		uint32_t vesa_fb;
		uint8_t vga_video_mode;
	}__attribute__((packed));
