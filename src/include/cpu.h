#pragma once

#include <stdint.h>

static inline void cli(void)
{
	asm volatile("cli");
}

static inline void sti(void)
{
	asm volatile("sti");
}

static inline void halt(void)
{
	asm volatile("hlt");
}

static inline void outportb(uint16_t port,uint8_t val)
{
	asm volatile("outb %0,%1"::"a"(val),"d" (port));
}

static inline void outportw(uint16_t port,uint16_t val)
{
	asm volatile("outw %0,%1"::"a"(val),"d" (port));
}

static inline void outportd(uint16_t port,uint32_t val)
{
	asm volatile("outl %0,%1"::"a"(val),"d" (port));
}

static inline uint8_t inportb(uint16_t port)
{
	uint8_t ret;
	asm volatile ("inb %1,%0":"=a" (ret):"d"(port));
	return ret;
}

static inline uint16_t inportw(uint16_t port)
{
	uint16_t ret;
	asm volatile ("inw %1,%0":"=a" (ret):"d"(port));
	return ret;
}

static inline uint32_t inportd(uint16_t port)
{
	uint32_t ret;
	asm volatile ("inl %1,%0":"=a" (ret):"d"(port));
	return ret;
}

static inline uint64_t rdcr0(void)
{
	uint64_t ret;
	asm volatile ("movq %%cr0,%0":"=r" (ret):);
	return ret;
}

static inline void wrcr0(uint64_t val)
{
	asm volatile ("movq %0,%%cr0"::"r" (val));
}

static inline uint64_t rdcr2(void)
{
	uint64_t ret;
	asm volatile ("mov %%cr2,%0":"=r" (ret):);
	return ret;
}

static inline void wrcr3(uint64_t val)
{
	asm volatile ("mov %0,%%cr3"::"r"(val):"memory");
}

static inline uint64_t rdcr4(void)
{
	uint64_t ret;
	asm volatile ("mov %%cr4,%0":"=r" (ret):);
	return ret;
}

static inline void wrcr4(uint64_t val)
{
	asm volatile ("mov %0,%%cr4"::"r" (val));
}

static inline void invlpg(uintptr_t addr)
{
	asm volatile ("invlpg (%0)"::"r"(addr):"memory");
}

void cpu_init(void);
