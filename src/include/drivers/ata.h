/*
 * Copyright 2021-2023 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <stdint.h>

enum ata_status_register
{
	ATA_STATUS_BUSY=0x80,
	ATA_STATUS_DRDY=0x40,
	ATA_STATUS_DRFL=0x20,
	ATA_STATUS_DRSC=0x10,
	ATA_STATUS_DRQ =0x08,
	ATA_STATUS_CORR=0x04,
	ATA_STATUS_IDX =0x02,
	ATA_STATUS_ERR =0x01,
};

enum ata_command
{
	ATA_READ_PIO		=0x20,
	ATA_READ_PIO_EXT	=0x24,
	ATA_WRITE_PIO		=0x30,
	ATA_WRITE_PIO_EXT	=0x34,
	ATA_PACKET_IDENTIFY	=0xa1,
	ATA_CACHE_FLUSH		=0xe7,
	ATA_CACHE_FLUSH_EXT	=0xea,
	ATA_IDENTIFY		=0xec,
};

enum ata_taskfile_flags
{
	TASKFILE_ADDRESS	=0x01,
	TASKFILE_EXTENDED	=0x02,
	TASKFILE_DATA		=0x04,
	TASKFILE_WRITE		=0x08,
	TASKFILE_ATAPI		=0x10,
};

struct ata_taskfile
{
	enum ata_taskfile_flags flags;
	void* data;			// valid only if flags&TASKFILE_DATA
	uint8_t lba[6];		// valid only if flags&TASKFILE_ADDRESS
	uint16_t count;		// valid only if flags&TASKFILE_DATA
	uint16_t feature;
	uint8_t device;
	enum ata_command cmd;
};

struct ata_primitives
{
	int(*send_cmd)(void*,const struct ata_taskfile*);
};

int ata_register_device(void* dev_data,const struct ata_primitives* primitives);
