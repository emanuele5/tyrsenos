#pragma once

#include <bootinfo.h>

void early_framebuffer_init(const struct bootinfo* info);
void framebuffer_init(void);
