/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <drivers/pci.h>

void pci_ide_init(const struct pci_device* dev);
