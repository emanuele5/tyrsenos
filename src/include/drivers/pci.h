/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <stdint.h>
#include <cpu.h>

struct pci_device
{
	uint8_t bus;
	uint8_t slot;
	uint8_t func;
};

static const uint16_t PCI_CFG_ADDR_PORT=0xcf8;
static const uint16_t PCI_CFG_DATA_PORT=0xcfc;

void pci_init(void* pcie_addr);
void pci_register_interrupt_handler(const struct pci_device* dev,void* private_data,void(function)(void*));

// generic functions for accessing the PCI configuration space

static inline uint32_t pci_cfg_read32(const struct pci_device* dev,uint8_t off)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	return inportd(PCI_CFG_DATA_PORT);
}

static inline uint16_t pci_cfg_read16(const struct pci_device* dev,uint8_t off)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	return inportw(PCI_CFG_DATA_PORT+(off&2));
}

static inline uint8_t pci_cfg_read8(const struct pci_device* dev,uint8_t off)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	return inportb(PCI_CFG_DATA_PORT+(off&3));
}

static inline void pci_cfg_write32(const struct pci_device* dev,uint8_t off,uint32_t val)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	outportd(PCI_CFG_DATA_PORT,val);
}

static inline void pci_cfg_write16(const struct pci_device* dev,uint8_t off,uint16_t val)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	outportw(PCI_CFG_DATA_PORT+(off&2),val);
}

static inline void pci_cfg_write8(const struct pci_device* dev,uint8_t off,uint8_t val)
{
	uint32_t addr=(dev->bus<<16)|(dev->slot<<11)|(dev->func<<8)|(off&0xfc)|0x80000000;
	outportd(PCI_CFG_ADDR_PORT,addr);
	outportb(PCI_CFG_DATA_PORT+(off&3),val);
}

// functions for accessing PCI configuration registers

static inline uint16_t pci_dev_get_vendor(const struct pci_device* dev)
{
	return pci_cfg_read16(dev,0);
}

static inline uint16_t pci_dev_get_device(const struct pci_device* dev)
{
	return pci_cfg_read16(dev,2);
}

static inline uint16_t pci_dev_get_command(const struct pci_device* dev)
{
	return pci_cfg_read16(dev,4);
}

static inline void pci_dev_set_command(const struct pci_device* dev,uint16_t val)
{
	pci_cfg_write16(dev,4,val);
}

static inline uint16_t pci_dev_get_status(const struct pci_device* dev)
{
	return pci_cfg_read16(dev,6);
}

static inline void pci_dev_set_status(const struct pci_device* dev,uint16_t val)
{
	pci_cfg_write16(dev,6,val);
}

static inline uint8_t pci_dev_get_progif(const struct pci_device* dev)
{
	return pci_cfg_read8(dev,9);
}

static inline uint16_t pci_dev_get_class(const struct pci_device* dev)
{
	return pci_cfg_read16(dev,0xa);
}

static inline uint8_t pci_dev_get_header(const struct pci_device* dev)
{
	return pci_cfg_read8(dev,0xe);
}

static inline uint32_t pci_dev_get_bar0(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x10);
}

static inline uint32_t pci_dev_get_bar1(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x14);
}

static inline uint32_t pci_dev_get_bar2(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x18);
}

static inline uint8_t pci_dev_get_secbus(const struct pci_device* dev)
{
	return pci_cfg_read8(dev,0x19);
}

static inline uint32_t pci_dev_get_bar3(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x1c);
}

static inline uint32_t pci_dev_get_bar4(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x20);
}

static inline uint32_t pci_dev_get_bar5(const struct pci_device* dev)
{
	return pci_cfg_read32(dev,0x24);
}

static inline uint8_t pci_dev_get_int_line(const struct pci_device* dev)
{
	return pci_cfg_read8(dev,0x3c);
}
