#pragma once

#include <stdint.h>

static const uint8_t IRQ_START=0x20;

void pic_init(void);

void pic_ack_irq(int n);
void pic_enable_irq(int n);
void pic_disable_irq(int n);
