#pragma once

#include <kernel/interrupts.h>

void syscall_handler(struct user_context* context);
