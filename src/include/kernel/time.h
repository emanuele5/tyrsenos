#pragma once

#include <time.h>

extern struct timespec realtime;
extern struct timespec monotonictime;

int sys_clock_gettime(clockid_t clockid,struct timespec* time);
int sys_clock_settime(clockid_t clockid,const struct timespec* time);

void kdelay(long nanos);
void ktick(long nanos);

static inline clock_t timespec_to_micros(struct timespec ts)
{
	return ts.tv_nsec/1000 + ts.tv_sec*CLOCKS_PER_SEC;
}
