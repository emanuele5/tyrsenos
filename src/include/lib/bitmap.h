#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

static inline void bitmap_set(uint8_t* bitmap,size_t pos)
{
	bitmap[pos/8]|=1<<pos%8;
}

static inline void bitmap_unset(uint8_t* bitmap,size_t pos)
{
	bitmap[pos/8]&=~(1<<pos%8);
}

static inline bool bitmap_isset(const uint8_t* bitmap,size_t pos)
{
	return bitmap[pos/8]&(1<<pos%8);
}

static inline bool bitmap_isunset(const uint8_t* bitmap,size_t pos)
{
	return !bitmap_isset(bitmap,pos);
}
