/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <stdarg.h>
#include <stddef.h>

int kprintf(const char* format,...) __attribute__((format(printf,1,2)));
int ksnprintf(char* restrict s,size_t n,const char* restrict format,...) __attribute__((format(printf,3,4)));

void klog(const char* message,...) __attribute__((format(printf,1,2)));
__attribute__((noreturn)) void kpanic(const char* message,...) __attribute__((format(printf,1,2)));
