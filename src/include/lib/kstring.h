/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <memory/kmalloc.h>

void memset(void* s,char c,size_t n);
void* memcpy(void* restrict dest,const void* restrict src,size_t n);
int memcmp(const void* restrict s1,const void* restrict s2,size_t n);
size_t kmemchr(const void* s,char c,size_t n);
size_t kmemrchr(const void* s,char c,size_t n);

int strcmp(const char* s1,const char* s2);
char* strcpy(char* restrict dest,const char* restrict src);
char* strncpy(char* restrict dest,const char* restrict src,size_t n);
char* stpcpy(char* restrict dest,const char* restrict src);
size_t strlen(const char* s);

// kstring: pascal-style dynamically allocated owning string

typedef struct
{
	char* data;
	size_t size;
}kstring;

static inline void kstring_free(kstring s)
{
	kfree(s.data);
}

// kstringview: pascal-style non-owning non-modifiable string,

typedef struct
{
	const char* data;
	size_t size;
}kstringview;

#define kstringview_from_literal(x) (kstringview){.data=x,.size=sizeof(x)-1}

static inline kstringview kstringview_from_cstring(const char* s)
{
	return (kstringview){s,strlen(s)};
}

static inline int kstringview_compare(kstringview s1,kstringview s2)
{
	if(s1.size>s2.size) return 1;
	else if(s1.size<s2.size) return -1;
	else return memcmp(s1.data,s2.data,s1.size);
}

static inline kstringview kstringview_from_kstring(kstring s)
{
	return (kstringview){s.data,s.size};
}

static inline size_t kstringview_search(kstringview s,char c)
{
	return kmemchr(s.data,c,s.size);
}

static inline size_t kstringview_rsearch(kstringview s,char c)
{
	return kmemrchr(s.data,c,s.size);
}

static inline kstringview kstringview_prefix(kstringview s,size_t end)
{
	return (kstringview){s.data,end};
}

static inline kstringview kstringview_suffix(kstringview s,size_t start)
{
	return (kstringview){s.data+start,s.size-start};
}

static inline kstring kstringview_concat(kstringview s1,kstringview s2)
{
	size_t size=s1.size+s2.size;
	char* data=kmalloc(size);
	memcpy(data,s1.data,s1.size);
	memcpy(data+s1.size,s2.data,s2.size);
	return (kstring){data,size};
}
