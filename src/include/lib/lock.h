#pragma once

#include <stdatomic.h>
#include <stddef.h>
#include <stdbool.h>

// scheduling lock

struct process;

struct lock
{
	struct process* val;
};

static inline void lock_create(struct lock* l)
{
	l->val=NULL;
}

static inline bool lock_isfree(const struct lock* l)
{
	return l->val==NULL;
}

static inline bool lock_isbusy(const struct lock* l)
{
	return l->val!=NULL;
}

static inline void lock_release(struct lock* l)
{
	l->val=NULL;
}

void lock_waitacquire(struct lock* l);

// spinlock

typedef atomic_flag spinlock;

static inline void spinlock_create(spinlock* sl)
{
	atomic_flag_clear(sl);
}

static inline void spinlock_acquire(spinlock* sl)
{
    while(atomic_flag_test_and_set_explicit(sl,memory_order_acquire))
    {
        __builtin_ia32_pause();
    }
}

static inline void spinlock_release(spinlock* sl)
{
    atomic_flag_clear_explicit(sl,memory_order_release);
}
