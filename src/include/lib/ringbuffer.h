/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#pragma once

#include <memory/virtual.h>
#include <stddef.h>
#include <stdint.h>

#define RINGBUF_SIZE PAGE_SIZE

struct ringbuffer
{
	uint8_t*	data;
	size_t		pos;
	size_t		count;
};

static inline void ringbuffer_create(struct ringbuffer* rb)
{
	rb->data=virtmem_get_heap_pages(RINGBUF_SIZE);
	rb->pos=0;
	rb->count=0;
}

int ringbuffer_read(struct ringbuffer* rb,void* data,size_t count);
int ringbuffer_write(struct ringbuffer* rb,const void* data,size_t count);

static inline void ringbuffer_destroy(struct ringbuffer* rb)
{
	virtmem_release_heap_pages(rb->data,RINGBUF_SIZE);
}

static inline size_t ringbuffer_get_available(const struct ringbuffer* rb)
{
	return RINGBUF_SIZE-rb->count;
}

static inline void ringbuffer_pop(struct ringbuffer* rb,size_t count)
{
	if(rb->count>=count) rb->count-=count;
	else rb->count=0;
}
