#pragma once

#include <stddef.h>

#define kmalloc_type(T) ((T*)kmalloc(sizeof(T)))
#define kmalloc_array(T,N) ((T*)kmalloc(sizeof(T)*(N)))
#define krealloc_array(ptr,T,N) ((T*)krealloc(ptr,sizeof(T)*(N)))

__attribute__((alloc_size(1))) __attribute__((malloc)) void* kmalloc(size_t size);
void kfree(void* ptr);
__attribute__((alloc_size(2))) void* krealloc(void* ptr,size_t newsize);
