#pragma once

#include <memory/physical.h>
#include <scheduler/process.h>
#include <vfs/inode.h>

struct mmap_area
{
	uint8_t* address;
	size_t length;
	int flags;
	physaddr phys;
	// for backed areas
	struct vfs_inode* backing;
	off_t offset;
};

void area_erase_all(void);
void area_copy_all(const struct process* parent,struct process* child);
void* area_map(uint8_t* addr,size_t len,int flags,struct vfs_inode* inode,off_t off);
void* sys_mmap(void* addr,size_t len,int flags,int fd,off_t off);
