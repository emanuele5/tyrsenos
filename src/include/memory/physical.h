#pragma once

#include <bootinfo.h>
#include <stddef.h>
#include <limits.h>

#define PAGE_SIZE_HUGE	(PAGE_SIZE*512)

#define PKSTACK	(0x8f000)

typedef uint64_t physaddr;

void physmem_init(const struct bios_mm* map,size_t map_size);
void physmem_reclaim_bootmem(void);

physaddr physmem_get(size_t size);
void physmem_release(physaddr start,size_t size);
