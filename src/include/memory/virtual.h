#pragma once

#include <bootinfo.h>
#include <memory/physical.h>

#include <stdbool.h>

enum page_flags
{
	PAGE_PRESENT	=(1<<0),		// added automatically by all mapping functions
	PAGE_WRITE		=(1<<1),
	PAGE_USER		=(1<<2),		// added automatically by virtmem_map_user_range
	PAGE_WT			=(1<<3),
	PAGE_NOCACHE	=(1<<4),
	PAGE_ACCESSED	=(1<<5),
	PAGE_DIRTY		=(1<<6),
	PAGE_BIG		=(1<<7),
	PAGE_GLOBAL		=(1<<8),		// added automatically by virtmem_map_kernel_range
	PAGE_XD			=(1ull<<63),
};

static const uintptr_t KERN_MEMORY_BASE=0xffff800000000000;
static const uintptr_t VKSTACK=0x8f000+KERN_MEMORY_BASE;

// here because needed in mmap.c
static inline void* kernel_address(physaddr phys)
{
	return (void*)(phys+KERN_MEMORY_BASE);
}

// memory mapping initialization
void virtmem_init(const struct bios_mm* map,size_t map_size);

// copy memory mapping, used for fork
physaddr virtmem_create_address_space(void);

// translate virtual to physical address
physaddr virtmem_get_physaddr(const void* virt);

//
// address checking functions
//
static inline bool virtmem_is_kernel(const void* ptr)
{
	return (uintptr_t)ptr>=0x8000000000000000;
}

static inline bool virtmem_is_user(const void* ptr)
{
	return (uintptr_t)ptr<0x8000000000000000;
}

bool virtmem_range_is_free(const void* virt,size_t size);
bool virtmem_range_is_user_readable(const void* p,size_t size);
bool virtmem_range_is_user_writeable(const void* p,size_t size);

//
// memory mapping functions
//
void* virtmem_map_kernel_range(physaddr phys,size_t len,enum page_flags flags);
void virtmem_map_user_range(physaddr cr3,void* virt,physaddr phys,size_t len,enum page_flags flags);
void virtmem_unmap_user_range(physaddr cr3,void* virt,size_t len);

void* virtmem_get_heap_pages(size_t size);
void virtmem_release_heap_pages(void* virt,size_t size);
