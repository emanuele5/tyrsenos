#pragma once

#include <stddef.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>

struct network_interface
{
	struct in_addr inet_address;
	struct ether_addr ether_address;
	void* private_data;
};

struct network_packet
{
	uint8_t* data;
	size_t offset;
	size_t size;
	struct network_interface* interface;
};
