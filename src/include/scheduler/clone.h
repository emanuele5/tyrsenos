#pragma once

#include <kernel/interrupts.h>
#include <scheduler/process.h>

typedef void(*kernel_return_function)(void);

struct process* process_clone(struct process* parent,kernel_return_function retfunc);
struct user_context* process_get_user_context(const struct process* p);

pid_t sys_fork(void);
