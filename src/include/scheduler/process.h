#pragma once

#include <lib/lock.h>
#include <memory/physical.h>
#include <memory/mmap.h>
#include <vfs/inode.h>

#include <signal.h>

#define PROC_RUNNING	0
#define PROC_RUNNABLE	1
#define PROC_WAITING	2
#define PROC_ZOMBIE		3
#define PROC_WAITSIG	4

struct process
{
	// important pointers, DO NOT MOVE, they are hardcoded in the task-switching routines
	physaddr cr3;
	void* kernel_stack; // page of kernel stack
	void* sp0;		// top of kernel stack
	int status;
	
	// process ids
	pid_t pid;
	uid_t uid;
	gid_t gid;
	uid_t euid;
	gid_t egid;
	
	// signals
	sighandler_t handlers[NSIG];
	sigset_t sigmask;
	sigset_t pending;
	siginfo_t last_siginfo;
	
	struct lock* waiting_on;
	
	struct vfs_inode* cwd;
	
	size_t file_count;
	struct file_descriptor** files;
	size_t area_count;
	struct mmap_area** areas;
	
	// pid linked list
	struct process* prev;
	struct process* next;
	// genealogy
	struct process* parent;
	struct process* firstchild;
	// siblings linked list
	struct process* prevsibling;
	struct process* nextsibling;
};

extern struct process idle_process;
extern struct process* cur_process;

void process_init(void);

void process_yieldto(struct process* nextproc,int curstatus);
void process_yield(int curstatus);
void process_exit(int status);

void sys_exit(int status);
int sys_waitchild(pid_t pid,int* status,int flags);
