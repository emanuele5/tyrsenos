#pragma once

#include <sunflower/procinfo.h>

struct process* process_find(pid_t pid);

pid_t sys_getpid(void);
int sys_getprocinfo(pid_t pid,struct procinfo* info);
int sys_setuid(uid_t uid);
int sys_setgid(gid_t gid);
