#pragma once

#include <scheduler/process.h>

#include <signal.h>

// SIG_DFL has no meaning as a pointer, in that case it's reused to indicate termination
#define SIG_TRM	(SIG_DFL)

extern sighandler_t default_sighandlers[NSIG+1];

int process_send_signal(struct process* p,int signum,int status);
void process_handle_signals(void);

int sys_sigaction(int signum,const struct sigaction* sa,struct sigaction* old_sa);
int sys_sigmask(int how,const sigset_t* set,sigset_t* oldset);
int sys_sigsend(pid_t pid,int signum);
int sys_sigpending(sigset_t* set);
int sys_sigwait(const sigset_t* restrict set,siginfo_t* restrict info);
