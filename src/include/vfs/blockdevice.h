#pragma once

#include <vfs/inode.h>

struct vfs_inode* blockdevice_register(off_t size,dev_t ndev,void* data,const struct inode_functions* funcs);
