#pragma once

#include <lib/lock.h>
#include <lib/ringbuffer.h>
#include <vfs/inode.h>

#include <termios.h>

extern struct vfs_inode console_inode;

void console_register_output(ssize_t(*func)(void*,const void*,size_t),void* data);
ssize_t console_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags);

struct tty_data
{
	struct termios termios;

	bool hasinput;
	struct ringbuffer rb;
	struct lock waiting;
	size_t waiting_count;

	void* write_data;
	ssize_t(*write_function)(void*,const void*,size_t);
};

struct tty_data* tty_register_input(void);
void tty_register_output(ssize_t(*func)(void*,const void*,size_t),void* dev);

void tty_put(struct tty_data* tty,char ch);
