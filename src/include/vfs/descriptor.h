#pragma once

#include <vfs/inode.h>

#include <dirent.h>
#include <stdbool.h>

struct file_descriptor
{
	struct vfs_inode* inode;
	off_t pos;
	int flags;
	bool cloexec;
};

struct file_descriptor* descriptor_get(int fd);
int descriptor_open(struct vfs_inode* inode,int flags,bool cloexec);
struct file_descriptor* descriptor_copy(const struct file_descriptor* oldf);

int sys_open(int dirfd,const char* pathname,int flags,mode_t mode);
int sys_close(int fd);
int sys_ioctl(int fd,unsigned cmd,unsigned long data);
int sys_fcntl(int fd,int cmd,int arg);
int sys_dup(int fd,int newfd);

ssize_t sys_read(int fd,void* buf,size_t count);
ssize_t sys_pread(int fd,void* buf,size_t count,off_t pos);
ssize_t sys_write(int fd,const void* buf,size_t count);
ssize_t sys_pwrite(int fd,const void* buf,size_t count,off_t pos);
off_t sys_seek(int fd,off_t offset,int whence);
int sys_readdir(int fd,struct dirent* buf);
