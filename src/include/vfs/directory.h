#pragma once

#include <limits.h>
#include <sys/types.h>
#include <vfs/inode.h>

struct directory_entry
{
	unsigned index;
	struct vfs_inode* child;
	char name[NAME_MAX];
	size_t namelen;

	// linked list of directories
	struct directory_entry* next;
	struct directory_entry* prev;
	// lru cache (to be implemented
	struct directory_entry* lru_next;
	struct directory_entry* lru_prev;
};

/*	create a struct directory_entry and insert it in the direntry cache
	does not otherwise update the directory inode */
struct directory_entry* directory_entry_create(struct vfs_inode* dir,const char* name,unsigned index,struct vfs_inode* child);

int directory_get_entry(struct vfs_inode* dir,unsigned index,struct directory_entry** result);

// initialize an inode as a directory object: initialize direntry cache and object operations
void directory_inode_init(struct vfs_inode* inode);
