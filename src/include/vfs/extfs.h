#pragma once

#include <vfs/inode.h>

struct mounted_filesystem* extfs_mount(struct vfs_inode* device,char* name);
int extfs_read_inode(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result);
