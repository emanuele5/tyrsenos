#pragma once

#include <vfs/inode.h>

int generic_free(struct vfs_inode*);
int generic_write_inode(struct vfs_inode*,bool);

ssize_t generic_read_enxio(struct vfs_inode*,off_t,void*,size_t,int);
ssize_t generic_write_enxio(struct vfs_inode*,off_t,const void*,size_t,int);
ssize_t generic_write_rofs(struct vfs_inode*,off_t,const void*,size_t,int);
int generic_ioctl_enotty(struct vfs_inode*,unsigned,unsigned long);

void generic_openclose(struct vfs_inode*,int);
int generic_sync(struct vfs_inode*);
