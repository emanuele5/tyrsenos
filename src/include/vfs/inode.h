#pragma once

#include <lib/kstring.h>

#include <stdbool.h>
#include <sys/stat.h>

struct vfs_inode;
struct mounted_filesystem;
struct directory_entry;
struct cached_page;
struct pipe_data;

struct filesystem_driver
{
	/*
	 * 
	 * name: filesystem_driver::mount
	 * mount a filesystem on the given device
	 * @device: inode of the underlying device
	 * @return: NULL on failure, a new struct mounted_filesystem on success
	 * 
	 */
	struct mounted_filesystem*(*mount)(struct vfs_inode*,char*);
	/*
	 * 
	 * name: filesystem_driver::unmount
	 * unmount a filesystem, writing all its metadata on device and deallocating it from ram
	 * @fs: the filesystem to unmount
	 * @return: >=0 on success, <0 on failure
	 * 
	 */
	int(*unmount)(struct mounted_filesystem*);
	/*
	 * 
	 * name: filesystem_driver::sync
	 * write all the filesystem metadata on device
	 * @fs: the filesystem to unmount
	 * @return: >=0 on success, <0 on failure
	 * 
	 */
	int(*sync)(struct mounted_filesystem*);
	/*
	 * 
	 * name: filesystem_driver::read_inode
	 * read an inode from device
	 * @fs: the filesystem from which to read
	 * @num: the inode number
	 * @result: new inode read from device
	 * @return: >=0 on success, <0 on failure
	 * 
	 */
	int(*read_inode)(struct mounted_filesystem*,ino_t,struct vfs_inode**);
	// name of the filesystem driver
	const char name[16];
};

struct mounted_filesystem
{
	// initialized by driver
	struct vfs_inode* root;
	void* fs_data;
	// initialized by vfs
	struct vfs_inode* device;
	const struct filesystem_driver* driver;
	
	// linked list of inode
	// struct vfs_inode* inodes;
	// linked list of mounted filesystems
	struct mounted_filesystem* prev;
	struct mounted_filesystem* next;
};

extern struct mounted_filesystem* mounted_fs_list;

// per filesystem/device functions
struct inode_functions
{
	//
	//	COMMON INODE FUNCTIONS
	//
	/*
	* 
	* write_inode: write an inode to device or device cache
	* @inode: the inode to write
	* @sync: true if the inode should be synced to device, otherwise only to device cache
	* @return: <0 for error, 0 for successful write
	* 
	*/
	int(*write_inode)(struct vfs_inode*,bool);
	/*
	* 
	* free_inode: frees inode specific memory
	* @inode: the inode to free
	* @return: <0 for error, 0 for successful freeing
	* 
	*/
	int(*free_inode)(struct vfs_inode*);
	int(*remove_inode)(struct vfs_inode*);

	//
	//	FILE FUNCTIONS
	//
	ssize_t(*read)(struct vfs_inode*,off_t,void*,size_t,int);
	ssize_t(*write)(struct vfs_inode*,off_t,const void*,size_t,int);
	off_t(*resize)(struct vfs_inode*,off_t);
	/*
	* 
	* ioctl: whichever action on an inode don't fit in the other abstractions
	* @inode: the inode on which to act
	* @cmd: the command to execute
	* @data: data relative to the command
	* @return: <0 on error, >=0 on success
	* 
	*/
	int(*ioctl)(struct vfs_inode*,unsigned,unsigned long);
	
	//
	// DIRECTORY FUNCTIONS
	//

	/*
	* 
	* readdir: reads a directory entry from the underlying device or cache
	* @dir: the directory inode
	* @result: the directory entry to fill, the index field indicates which entry to read
	* @return: <0 for error, 0 for EOF, >0 for successfull read
	* 
	*/
	int(*readdir)(struct vfs_inode*,struct directory_entry*);
	int(*link)(struct vfs_inode*,struct directory_entry*);
	int(*unlink)(struct vfs_inode*,struct directory_entry*);
	int(*create)(struct vfs_inode*,const char*,mode_t,struct vfs_inode**);
};

// per object type functions
struct object_functions
{
	void (*open)(struct vfs_inode*,int);
	void (*close)(struct vfs_inode*,int);
	ssize_t(*read)(struct vfs_inode*,off_t,void*,size_t,int);
	ssize_t(*write)(struct vfs_inode*,off_t,const void*,size_t,int);
	int(*sync)(struct vfs_inode*);
	void(*free_cache)(struct vfs_inode*);
};

struct vfs_inode
{
	// inode public data, needed for stat
	// dev and num are unique identifiers for every inode ever
	dev_t	dev;
	ino_t	num;
	mode_t	mode;
	uid_t	uid;
	gid_t	gid;
	off_t	size;
	nlink_t	links;
	dev_t	ndev;
	struct timespec atime;
	struct timespec mtime;
	struct timespec ctime;
	
	struct mounted_filesystem* fs;
	// filesystem-specific inode data
	void* inode_data;
	// filesystem-specific methods
	const struct inode_functions* inode_funcs;
	
	// per object type data and io functions, initialized by inode_cache_insert
	union
	{
		struct directory_entry** directory_cache;
		struct cached_page** page_cache;
		struct pipe_data* pipe_data;
	};
	const struct object_functions* obj_funcs;
	
	// inode cache data
	bool dirty;
	uint64_t refcount;
	struct vfs_inode* prev;
	struct vfs_inode* next;
	
	struct vfs_inode* fs_prev;
	struct vfs_inode* fs_next;
	
	// LRU list, unimplemented
	struct vfs_inode* lru_prev;
	struct vfs_inode* lru_next;
};

/* Mark inode as used once (by the caller),
 * automatically initialize the per object-type structures
 * and insert it in the cache.
 * Should not be used by normal filesystem drivers.*/
void inode_cache_insert(struct vfs_inode* inode);

/* Search an inode into the cache.
 * If it's not in the cache try to get it from the filesystem using
 * filesystem_driver::read_inode() */
int inode_get(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result);

/*	creates a new inode on device*/
int inode_create(struct vfs_inode* dir,const char* name,mode_t mode,struct vfs_inode** result);

off_t inode_resize(struct vfs_inode* inode,off_t newsize);

static inline void inode_acquire(struct vfs_inode* inode)
{
	inode->refcount++;
}
void inode_release(struct vfs_inode* inode);
int inode_sync(struct vfs_inode* inode);

bool inode_check_permissions(const struct vfs_inode* inode,int openflags);

// syscall implementations
int sys_sync(void);
int sys_access(int fd,const char* pathname,int mode,int flags);
int sys_chdir(int fd,const char* pathname,int flags);
int sys_stat(int fd,const char* pathname,struct stat* buf,int flags);
int sys_chown(int fd,const char* pathname,uid_t owner,gid_t group,int flags);

// lookup functions

#define LOOKUP_STOPATPARENT	1

int lookup(struct vfs_inode* dir,kstringview name,struct vfs_inode** result);
int lookup_path(int dirfd,kstringview pathname,struct vfs_inode** result,int flags);
