#pragma once

#include <lib/lock.h>
#include <lib/ringbuffer.h>

struct inputdevice_data
{
	struct ringbuffer rb;
	struct lock waiting;
	size_t waiting_count;
};

struct inputdevice_data* inputdevice_create(void);
void inputdevice_putdata(struct inputdevice_data* device,uint32_t type,uint32_t data);
