#pragma once

#include <vfs/inode.h>

struct mounted_filesystem* minixfs_mount(struct vfs_inode* device,char* name);
int minixfs_syncfs(struct mounted_filesystem* fs);
int minixfs_read_inode(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result);
