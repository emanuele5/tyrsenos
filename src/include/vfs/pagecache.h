#pragma once

#include <vfs/inode.h>

void uncached_inode_init(struct vfs_inode* inode);
void cached_inode_init(struct vfs_inode* inode);
