#pragma once

#include <lib/lock.h>
#include <lib/ringbuffer.h>
#include <vfs/inode.h>

struct pipe_data
{
	struct ringbuffer ringbuf;
	struct lock wait_read;
	struct lock wait_write;
	uint32_t read_count;
	uint32_t write_count;
};

void pipe_inode_init(struct vfs_inode* inode);

int sys_pipe(int fd[2]);
