#pragma once

#include <vfs/inode.h>

static const dev_t RAMFS_DEVNUM=0x0000;

static const ino_t ROOT_NUM		=0;
static const ino_t DEV_NUM		=1;
static const ino_t TMP_NUM		=2;
static const ino_t NULL_NUM		=3;
static const ino_t TTY_NUM		=4;
static const ino_t CONSOLE_NUM	=5;

extern struct vfs_inode root_inode;

void ramfs_init(void);

// the only function that can add childs to /dev
struct vfs_inode* ramfs_register_device(const char* name,mode_t mode,off_t size,dev_t ndev,void* data,const struct inode_functions* funcs);

// the only function that can add childs to /
void ramfs_mount(struct vfs_inode* fs_root,const char* name);
