/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <lib/kprintf.h>

#include <cpuid.h>

void cpu_init(void)
{
	uint32_t dummy;
	char vendor_string[13];
	vendor_string[12]=0;
	
	// get vendor string
	__get_cpuid(0,&dummy,(uint32_t*)vendor_string,(uint32_t*)vendor_string+1,(uint32_t*)vendor_string+2);
	
	// get processor type and features
	uint32_t fms,features;
	__get_cpuid(1,&fms,&dummy,&dummy,&dummy);
	uint32_t family=(fms>>8)&0xf;
	uint32_t model=(fms>>4)&0xf;
	uint32_t stepping=fms&0xf;
	
	// get processor name
	char processor_name[49];
	processor_name[48]=0;
	__get_cpuid(0x80000002,(uint32_t*)processor_name,(uint32_t*)processor_name+1,(uint32_t*)processor_name+2,(uint32_t*)processor_name+3);
	__get_cpuid(0x80000003,(uint32_t*)processor_name+4,(uint32_t*)processor_name+5,(uint32_t*)processor_name+6,(uint32_t*)processor_name+7);
	__get_cpuid(0x80000004,(uint32_t*)processor_name+8,(uint32_t*)processor_name+9,(uint32_t*)processor_name+10,(uint32_t*)processor_name+11);
	
	klog("cpuid: %s %s family=0x%02x, model=0x%02x, stepping=0x%02x",vendor_string,processor_name,family,model,stepping);
	
	// this is a 64bit kernel: assume sse and sse2 and enable it
	// clear CRO.EM and set CR0.MP
	uint64_t cr0=rdcr0();
	cr0|=(1<<1);
	cr0&=~(1<<2);
	wrcr0(cr0);
	// set CR4.OSFXSR and CR4.OSXMMEXCPT
	uint64_t cr4=rdcr4();
	cr4|=(3<<9);
	wrcr4(cr4);
}
