bits 64

extern call_interrupt_handler

%macro INT_NOERR 1
global int%1_entry
int%1_entry:
	push qword 0
	push qword %1
	jmp commmon_int_entry
%endmacro

%macro INT_ERR 1
global int%1_entry
int%1_entry:
	push qword %1
	jmp commmon_int_entry
%endmacro

; processor interrupts/exceptions
INT_NOERR	0
INT_NOERR	1
INT_NOERR	2
INT_NOERR	3
INT_NOERR	4
INT_NOERR	5
INT_NOERR	6
INT_NOERR	7
INT_ERR		8
INT_NOERR	9
INT_ERR		10
INT_ERR		11
INT_ERR		12
INT_ERR		13
INT_ERR		14
INT_NOERR	16
INT_ERR		17
INT_NOERR	18
INT_NOERR	19
INT_NOERR	20
INT_ERR		21
INT_NOERR	28
INT_ERR		29
INT_ERR		30
; external interrutps/IRQs
INT_NOERR	32
INT_NOERR	33
INT_NOERR	35
INT_NOERR	36
INT_NOERR	37
INT_NOERR	38
INT_NOERR	39
INT_NOERR	40
INT_NOERR	41
INT_NOERR	42
INT_NOERR	43
INT_NOERR	44
INT_NOERR	45
INT_NOERR	46
INT_NOERR	47
INT_NOERR	48

commmon_int_entry:
	; save user registers
	push rax
	push rbx
	push rcx
	push rdx
	push rsi
	push rdi
	push rbp
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	cld
	mov rdi,rsp ; put stack pointer as first argument
	call call_interrupt_handler

global return_to_userspace
return_to_userspace:
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rbp
	pop rdi
	pop rsi
	pop rdx
	pop rcx
	pop rbx
	pop rax
	add rsp,16 ; clean error code and interrupt number
	iretq
