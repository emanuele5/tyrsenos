/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <kernel/interrupts.h>
#include <kernel/syscall.h>
#include <lib/kprintf.h>
#include <memory/virtual.h>
#include <scheduler/signal.h>

struct idt_entry
{
	uint16_t offset1;
	uint16_t selector;
	uint8_t ist;
	uint8_t attrib;
	uint16_t offset2;
	uint32_t offset3;
	uint32_t reserved;
}__attribute__((packed));

struct gdt_entry
{
	uint16_t limit;
	uint16_t base1;
	uint8_t base2;
	uint8_t access;
	uint8_t flags;
	uint8_t base3;
}__attribute__((packed));

struct tss
{
	uint32_t res0;
	uint64_t rsp0;
	uint64_t rsp1;
	uint64_t rsp2;
	uint64_t res1;
	uint64_t ist1;
	uint64_t ist2;
	uint64_t ist3;
	uint64_t ist4;
	uint64_t ist5;
	uint64_t ist6;
	uint64_t ist7;
	uint64_t res2;
	uint16_t res3;
	uint16_t iopb;
}__attribute__((packed));

// generic pointer to IDT and GDT
struct dtr
{
	uint16_t limit;
	void* addr;
}__attribute__((packed));

// interrupt entry points
extern void int0_entry(void);
extern void int1_entry(void);
extern void int2_entry(void);
extern void int3_entry(void);
extern void int4_entry(void);
extern void int5_entry(void);
extern void int6_entry(void);
extern void int7_entry(void);
extern void int8_entry(void);
extern void int9_entry(void);
extern void int10_entry(void);
extern void int11_entry(void);
extern void int12_entry(void);
extern void int13_entry(void);
extern void int14_entry(void);
extern void int16_entry(void);
extern void int17_entry(void);
extern void int18_entry(void);
extern void int19_entry(void);
extern void int20_entry(void);
extern void int21_entry(void);
extern void int28_entry(void);
extern void int29_entry(void);
extern void int30_entry(void);
extern void int32_entry(void);
extern void int33_entry(void);
extern void int35_entry(void);
extern void int36_entry(void);
extern void int37_entry(void);
extern void int38_entry(void);
extern void int39_entry(void);
extern void int40_entry(void);
extern void int41_entry(void);
extern void int42_entry(void);
extern void int43_entry(void);
extern void int44_entry(void);
extern void int45_entry(void);
extern void int46_entry(void);
extern void int47_entry(void);
extern void int48_entry(void);

#define INTERRUPT_NUM	0x49

static const unsigned IRQ_START=0x20;

// tss and tss pointer
struct tss glob_tss=
{
	.rsp0=VKSTACK+PAGE_SIZE,
	.iopb=sizeof(struct tss),
};
static const uintptr_t tssp=((uintptr_t)&glob_tss);

// gdt and gdt register
static struct gdt_entry gdt[7]=
{
	{0,0,0,0,0,0}, // null segment
	{0xffff,0,0,0x9a,0xaf,0}, // kernel code
	{0xffff,0,0,0x92,0xcf,0}, // kernel data
	{0xffff,0,0,0xfa,0xab,0}, // user code
	{0xffff,0,0,0xf2,0xcb,0}, // user data
	{sizeof(struct tss),0,0,0x89,0x20,0}, // tss high
	{0x8000,0xffff,0,0,0,0},
};
static const struct dtr gdtr={sizeof(gdt),gdt};

// idt and gdt register
static struct idt_entry idt[INTERRUPT_NUM];
static const struct dtr idtr={sizeof(idt),idt};

static void idt_entry_set(unsigned n,void(handler)(),unsigned attr)
{
	uintptr_t addr=(uintptr_t)handler;
	idt[n].offset1=addr&0xffff;
	idt[n].selector=8;
	idt[n].ist=0;
	idt[n].attrib=attr;
	idt[n].offset2=(addr>>16)&0xffff;
	idt[n].offset3=(addr>>32)&0xffffffff;
	idt[n].reserved=0;
}

// interrupt handling

static void do_de(struct user_context* ctx)
{
	klog("Division Error\nIP=%lx",ctx->rip);
	if(ctx->cs==0x08) kpanic("Division Error in kernel space");
	else process_send_signal(cur_process,SIGFPE,0);
}

static void do_ud(struct user_context* ctx)
{
	klog("Invalid opcode\nIP=%lx",ctx->rip);
	if(ctx->cs==0x08) kpanic("Invalid opcode in kernel space");
	else process_send_signal(cur_process,SIGILL,0);
}

static void do_gp(struct user_context* ctx)
{
	klog("General Protection Fault\nIP=%lx Error code=%lx",ctx->rip,ctx->error_code);
	if(ctx->cs==0x08) kpanic("General Protection Fault in kernel space");
	else process_send_signal(cur_process,SIGSEGV,0);
}

static void do_pf(struct user_context* ctx)
{
	uintptr_t addr=rdcr2();
	klog("Page Fault accessing CR2=%lx from IP=%lx error=%ld",addr,ctx->rip,ctx->error_code);
	if(ctx->cs==0x08) kpanic("Page Fault in kernel space");
	else process_send_signal(cur_process,SIGBUS,0);
}

static interrupt_function handlers[INTERRUPT_NUM]=
{
	[0]=do_de,
	[6]=do_ud,
	[13]=do_gp,
	[14]=do_pf,
	[48]=syscall_handler,
};

void call_interrupt_handler(struct user_context* context)
{
	if(handlers[context->int_num]==NULL)
	{
		klog("unhandled interrupt %ld",context->int_num);
		return;
	}
	handlers[context->int_num](context);
	process_handle_signals();
}

void interrupt_set(unsigned num,interrupt_function handler)
{
	if(num<IRQ_START || num>=INTERRUPT_NUM) return;
	handlers[num]=handler;
}

void interrupts_init(void)
{	
	idt_entry_set(0,int0_entry,0x8f);
	idt_entry_set(1,int1_entry,0x8f);
	idt_entry_set(2,int2_entry,0x8e);
	idt_entry_set(3,int3_entry,0x8f);
	idt_entry_set(4,int4_entry,0x8f);
	idt_entry_set(5,int5_entry,0x8f);
	idt_entry_set(6,int6_entry,0x8f);
	idt_entry_set(7,int7_entry,0x8f);
	idt_entry_set(8,int8_entry,0x8f);
	idt_entry_set(9,int9_entry,0x8f);
	idt_entry_set(10,int10_entry,0x8f);
	idt_entry_set(11,int11_entry,0x8f);
	idt_entry_set(12,int12_entry,0x8f);
	idt_entry_set(13,int13_entry,0x8f);
	idt_entry_set(14,int14_entry,0x8f);
	idt_entry_set(16,int16_entry,0x8f);
	idt_entry_set(17,int17_entry,0x8f);
	idt_entry_set(18,int18_entry,0x8f);
	idt_entry_set(19,int19_entry,0x8f);
	idt_entry_set(20,int20_entry,0x8f);
	idt_entry_set(21,int21_entry,0x8f);
	idt_entry_set(28,int28_entry,0x8f);
	idt_entry_set(29,int29_entry,0x8f);
	idt_entry_set(30,int30_entry,0x8f);
	idt_entry_set(32,int32_entry,0x8e);
	idt_entry_set(33,int33_entry,0x8e);
	idt_entry_set(35,int35_entry,0x8e);
	idt_entry_set(36,int36_entry,0x8e);
	idt_entry_set(37,int37_entry,0x8e);
	idt_entry_set(38,int38_entry,0x8e);
	idt_entry_set(39,int39_entry,0x8e);
	idt_entry_set(40,int40_entry,0x8e);
	idt_entry_set(41,int41_entry,0x8e);
	idt_entry_set(42,int42_entry,0x8e);
	idt_entry_set(43,int43_entry,0x8e);
	idt_entry_set(44,int44_entry,0x8e);
	idt_entry_set(45,int45_entry,0x8e);
	idt_entry_set(46,int46_entry,0x8e);
	idt_entry_set(47,int47_entry,0x8e);
	idt_entry_set(48,int48_entry,0xee);
	// set the tss segment in the gdt
	gdt[5].base1=tssp&0xffff;
	gdt[5].base2=tssp>>16&0xff;
	gdt[5].base3=tssp>>24;

	asm volatile("lidt %0"::"m"(idtr));
	asm volatile("lgdt %0"::"m"(gdtr));
	asm volatile("movw $0x28,%%ax\n\
		ltr %%ax":::"rax");
}
