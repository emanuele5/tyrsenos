/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <scheduler/process.h>
#include <cpu.h>

#include <errno.h>

int sys_reboot(unsigned magic,unsigned cmd)
{
	if(magic!=0x01031999) return -EINVAL;
	if(cur_process->uid!=0) return -EPERM;
	// this actually halts and does not reboot. for now
	klog("shutdown: system halted");
	while(1) halt();
}
