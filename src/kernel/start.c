/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <bootinfo.h>
#include <cpu.h>
#include <drivers/cmos.h>
#include <drivers/framebuffer.h>
#include <drivers/pci.h>
#include <drivers/pic.h>
#include <drivers/pit.h>
#include <drivers/ps2.h>
#include <kernel/interrupts.h>
#include <memory/physical.h>
#include <memory/virtual.h>
#include <scheduler/process.h>
#include <vfs/ramfs.h>

__attribute__((noreturn)) void idle_main(void)
{
	while(1)
	{
		//TODO: we could do many things here, like writing back caches and such
		halt();
	}
}

__attribute__((section(".init"))) void start(const struct bootinfo* info)
{
	// init gdt and idt
	interrupts_init();
	// init memory
	physmem_init((struct bios_mm*)info->mem_map,info->mem_map_size);
	virtmem_init((struct bios_mm*)info->mem_map,info->mem_map_size);
	early_framebuffer_init(info);
	
	cpu_init();

	pic_init();
	// init basic vfs
	ramfs_init();

	framebuffer_init();
	pit_init();
	cmos_init();
	ps2_init();
	pci_init(NULL);
	physmem_reclaim_bootmem();
	process_init();
	idle_main();
}
