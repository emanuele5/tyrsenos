/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <kernel/interrupts.h>
#include <kernel/reboot.h>
#include <kernel/time.h>
#include <lib/kprintf.h>
#include <memory/mmap.h>
#include <scheduler/clone.h>
#include <scheduler/elfloader.h>
#include <scheduler/procinfo.h>
#include <scheduler/signal.h>
#include <vfs/descriptor.h>
#include <vfs/inode.h>
#include <vfs/pipe.h>

#include <errno.h>

static void do_sys_open(struct user_context* ctx)
{
	ctx->rax=sys_open(ctx->rbx,(const char*)ctx->rcx,ctx->rdx,ctx->rsi);
}

static void do_sys_close(struct user_context* ctx)
{
	ctx->rax=sys_close(ctx->rbx);
}

static void do_sys_read(struct user_context* ctx)
{
	ctx->rax=sys_read(ctx->rbx,(void*)ctx->rcx,ctx->rdx);
}

static void do_sys_write(struct user_context* ctx)
{
	ctx->rax=sys_write(ctx->rbx,(const void*)ctx->rcx,ctx->rdx);
}

static void do_sys_execve(struct user_context* ctx)
{
	ctx->rax=sys_execve(ctx->rbx,(const char*)ctx->rcx,(const char**)ctx->rdx,(const char**)ctx->rsi,ctx->rdi);
}

static void do_sys_stat(struct user_context* ctx)
{
	ctx->rax=sys_stat(ctx->rbx,(const char*)ctx->rcx,(struct stat*)ctx->rdx,ctx->rsi);
}

static void do_sys_fork(struct user_context* ctx)
{
	ctx->rax=sys_fork();
}

static void do_sys_mmap(struct user_context* ctx)
{
	ctx->rax=(uintptr_t)sys_mmap((void*)ctx->rbx,ctx->rcx,ctx->rdx,ctx->rsi,ctx->rdi);
}

static void do_sys_getpid(struct user_context* ctx)
{
	ctx->rax=sys_getpid();
}

static void do_sys_getprocinfo(struct user_context* ctx)
{
	ctx->rax=sys_getprocinfo(ctx->rbx,(struct procinfo*)ctx->rcx);
}

static void do_sys_setuid(struct user_context* ctx)
{
	ctx->rax=sys_setuid(ctx->rbx);
}

static void do_sys_setgid(struct user_context* ctx)
{
	ctx->rax=sys_setgid(ctx->rbx);
}

static void do_sys_chdir(struct user_context* ctx)
{
	ctx->rax=sys_chdir(ctx->rbx,(const char*)ctx->rcx,ctx->rdx);
}

static void do_sys_seek(struct user_context* ctx)
{
	ctx->rax=sys_seek(ctx->rbx,ctx->rcx,ctx->rdx);
}

static void do_sys_readdir(struct user_context* ctx)
{
	ctx->rax=sys_readdir(ctx->rbx,(struct dirent*)ctx->rcx);
}

static void do_sys_fcntl(struct user_context* ctx)
{
	ctx->rax=sys_fcntl(ctx->rbx,ctx->rcx,ctx->rdx);
}

static void do_sys_dup(struct user_context* ctx)
{
	ctx->rax=sys_dup(ctx->rbx,ctx->rcx);
}

static void do_sys_ioctl(struct user_context* ctx)
{
	ctx->rax=sys_ioctl(ctx->rbx,ctx->rcx,ctx->rdx);
}

static void do_sys_exit(struct user_context* ctx)
{
	sys_exit(ctx->rbx);
}

static void do_sys_waitchild(struct user_context* ctx)
{
	ctx->rax=sys_waitchild(ctx->rbx,(int*)ctx->rcx,ctx->rdx);
}

static void do_sys_sigmask(struct user_context* ctx)
{
	ctx->rax=sys_sigmask(ctx->rbx,(const sigset_t*)ctx->rcx,(sigset_t*)ctx->rdx);
}

static void do_sys_sigsend(struct user_context* ctx)
{
	ctx->rax=sys_sigsend(ctx->rbx,ctx->rcx);
}

static void do_sys_sigwait(struct user_context* ctx)
{
	ctx->rax=sys_sigwait((const sigset_t*)ctx->rbx,(siginfo_t*)ctx->rcx);
}

static void do_sys_sigpending(struct user_context* ctx)
{
	ctx->rax=sys_sigpending((sigset_t*)ctx->rbx);
}

static void do_sys_sigaction(struct user_context* ctx)
{
	ctx->rax=sys_sigaction(ctx->rbx,(const struct sigaction*)ctx->rcx,(struct sigaction*)ctx->rdx);
}

static void do_sys_clock_gettime(struct user_context* ctx)
{
	ctx->rax=sys_clock_gettime(ctx->rbx,(struct timespec*)ctx->rcx);
}

static void do_sys_chown(struct user_context* ctx)
{
	ctx->rax=sys_chown(ctx->rbx,(const char*)ctx->rcx,ctx->rdx,ctx->rsi,ctx->rdi);
}

static void do_sys_pipe(struct user_context* ctx)
{
	ctx->rax=sys_pipe((int*)ctx->rbx);
}

static void do_sys_clock_settime(struct user_context* ctx)
{
	ctx->rax=sys_clock_settime(ctx->rbx,(const struct timespec*)ctx->rcx);
}

static void do_sys_pread(struct user_context* ctx)
{
	ctx->rax=sys_pread(ctx->rbx,(void*)ctx->rcx,ctx->rdx,ctx->rsi);
}

static void do_sys_pwrite(struct user_context* ctx)
{
	ctx->rax=sys_pwrite(ctx->rbx,(const void*)ctx->rcx,ctx->rdx,ctx->rsi);
}

static void do_sys_reboot(struct user_context* ctx)
{
	ctx->rax=sys_reboot(ctx->rbx,ctx->rcx);
}

static void do_sys_access(struct user_context* ctx)
{
	ctx->rax=sys_access(ctx->rbx,(const char*)ctx->rcx,ctx->rdx,ctx->rsi);
}

static void do_sys_sync(struct user_context* ctx)
{
	ctx->rax=sys_sync();
}

static const interrupt_function syscall_table[]=
{
	do_sys_open,
	do_sys_close,
	do_sys_read,
	do_sys_write,
	do_sys_execve,
	do_sys_stat,
	do_sys_fork,
	do_sys_mmap,
	do_sys_getpid,
	do_sys_getprocinfo,
	do_sys_setuid,
	do_sys_setgid,
	do_sys_chdir,
	do_sys_seek,
	do_sys_readdir,
	do_sys_fcntl,
	do_sys_dup,
	do_sys_ioctl,
	do_sys_exit,
	do_sys_waitchild,
	do_sys_sigmask,
	do_sys_sigsend,
	do_sys_sigwait,
	do_sys_sigpending,
	do_sys_sigaction,
	do_sys_clock_gettime,
	do_sys_chown,
	do_sys_pipe,
	do_sys_clock_settime,
	do_sys_pread,
	do_sys_pwrite,
	do_sys_reboot,
	do_sys_access,
	do_sys_sync,
};

static const unsigned SYSCALL_NUM=sizeof(syscall_table)/sizeof(interrupt_function);

void syscall_handler(struct user_context* ctx)
{
	unsigned num=ctx->rax;
	//klog("pid: %d syscall %d: %lx %lx %lx",cur_process->pid,num,ctx->rbx,ctx->rcx,ctx->rdx);
	if(num==0 || num>SYSCALL_NUM) ctx->rax=-ENOSYS;
	else syscall_table[num-1](ctx);
	//klog("pid: %d syscall %d: %lx %lx %lx=%lx",cur_process->pid,num,ctx->rbx,ctx->rcx,ctx->rdx,ctx->rax);
}
