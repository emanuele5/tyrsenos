/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <drivers/pit.h>
#include <kernel/time.h>
#include <memory/virtual.h>
#include <scheduler/process.h>

#include <errno.h>

struct kclock
{
	struct timespec* time;
	int(*settime)(struct timespec);
};

//
// realtime clock implementation
//

struct timespec realtime={};

static int clock_realtime_settime(struct timespec time)
{
	realtime=time;
	return 0;
}

static const struct kclock clock_realtime=
{
	.time=&realtime,
	.settime=clock_realtime_settime,
};

//
// monotonic clock implementation
//

struct timespec monotonictime={};

static int clock_monotonic_settime(struct timespec time)
{
	return -EINVAL;
}

static const struct kclock clock_monotonic=
{
	.time=&monotonictime,
	.settime=clock_monotonic_settime,
};

//
// clock functions implementations
//

static const struct kclock* clocks[]=
{
	[CLOCK_REALTIME]=&clock_realtime,
	[CLOCK_MONOTONIC]=&clock_monotonic,
};

static const clockid_t nclocks=sizeof(clocks)/sizeof(*clocks);

static const struct kclock* clockid_to_kclock(clockid_t id)
{
	if(id>=nclocks) return NULL;
	return clocks[id];
}

int sys_clock_gettime(clockid_t clockid,struct timespec* time)
{
	// check clock exists
	const struct kclock* clk=clockid_to_kclock(clockid);
	if(clk==NULL) return -EINVAL;
	// check user pointer
	if(!virtmem_range_is_user_writeable(time,sizeof(struct timespec))) return -EFAULT;
	
	*time=*clk->time;
	return 0;
}

int sys_clock_settime(clockid_t clockid,const struct timespec* time)
{
	// check clock exists
	const struct kclock* clk=clockid_to_kclock(clockid);
	if(clk==NULL) return -EINVAL;
	// check user pointer
	if(!virtmem_range_is_user_readable(time,sizeof(struct timespec))) return -EFAULT;
	
	// check timespec is valid
	if(time->tv_nsec<0 || time->tv_nsec>1000000000) return -EINVAL;
	
	return clk->settime(*time);
}

//
// timer functions implementation
//

struct ktimer
{
	const struct kclock* clock;
};

static struct ktimer* timerid_to_ktimer(timer_t timerid)
{
	return NULL;
};

int sys_timer_create(clockid_t clockid,struct sigevent* event)
{
	// check clock exists
	const struct kclock* clk=clockid_to_kclock(clockid);
	if(clk==NULL) return -EINVAL;
	
	// TODO: check user address
	switch(event->sigev_notify)
	{
	case SIGEV_NONE:
		break;
	// all the others are unsupported
	// TODO: at least sending a signal should be easy?
	case SIGEV_SIGNAL:
	case SIGEV_THREAD:
	default:
		return -EINVAL;
	}
	
	return -ENOSYS;
}

int sys_timer_settime(timer_t timerid,int flags,const struct itimerspec* newvalue,struct itimerspec* oldvalue)
{
	struct ktimer* timer=timerid_to_ktimer(timerid);
	if(timer==NULL) return -EINVAL;
	
	return -ENOSYS;
}

int sys_timer_gettime(timer_t timerid,struct itimerspec* value)
{
	struct ktimer* timer=timerid_to_ktimer(timerid);
	if(timer==NULL) return -EINVAL;
	
	return -ENOSYS;
}

//
// kernel timekeeping functions
//

static void timespec_update(struct timespec* ts,long nanos)
{
	long nsec=ts->tv_nsec+nanos;
	// nanoseconds normalization
	if(nsec>1000000000)
	{
		ts->tv_sec+=nsec/1000000000;
		nsec%=1000000000;
	}
	ts->tv_nsec=nsec;
}

static int timespec_compare(const struct timespec* t1,const struct timespec* t2)
{
	if(t1->tv_sec!=t2->tv_sec) return t1->tv_sec-t2->tv_sec;
	return t1->tv_nsec-t2->tv_nsec;
}

void kdelay(long nanos)
{
	struct timespec target=monotonictime;
	timespec_update(&target,nanos);
	pit_alarm(nanos);
	while(timespec_compare(&target,&monotonictime)>0) halt();
}

void ktick(long nanos)
{
	for(clockid_t i=0;i<nclocks;i++)
	{
		const struct kclock* clk=clockid_to_kclock(i);
		if(clk) timespec_update(clk->time,nanos);
	}
	process_yield(PROC_RUNNABLE);
}
