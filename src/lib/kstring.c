#include <stddef.h>

void memset(void* s,char c,size_t n)
{
	char* dest=s;
	for(size_t i=0;i<n;i++) dest[i]=c;
}

void* memcpy(void* restrict dest,const void* restrict src,size_t n)
{
	const char* s=src;
	char* d=dest;
	for(size_t i=0;i<n;i++)
	{
		d[i]=s[i];
	}
	return dest;
}

int memcmp(const void* restrict s1,const void* restrict s2,size_t n)
{
	const char* str1=s1;
	const char* str2=s2;
	for(size_t i=0;i<n;i++)
	{
		if(str1[i]!=str2[i]) return str1[i]-str2[i];
	}
	return 0;
}

size_t kmemchr(const void* s,char c,size_t n)
{
	const char* str=s;
	for(size_t i=0;i<n;i++)
	{
		if(str[i]==c) return i;
	}
	return ~0u;
}

size_t kmemrchr(const void* s,char c,size_t n)
{
	const char* str=s;
	for(size_t i=n-1;i<=n;i--)
	{
		if(str[i]==c) return i;
	}
	return ~0u;
}

int strcmp(const char* s1,const char* s2)
{
	for(size_t i=0;;i++)
	{
		if(s1[i]!=s2[i]) return s1[i]-s2[i];
		if(s1[i]==0) break;
	}
	return 0;
}

char* strcpy(char* restrict dest,const char* restrict src)
{
	char* d=dest;
	while((*dest++ = *src++));
	return d;
}

char* stpcpy(char* restrict dest,const char* restrict src)
{
	while((*dest++ = *src++));
	return dest;
}

char* strncpy(char* restrict dest,const char* restrict src,size_t n)
{
	char* d=dest;
	while(n && (*dest++ = *src++)) n--;
	return d;
}

size_t strlen(const char* s)
{
	size_t i=0;
	while(*s++) i++;
	return i;
}
