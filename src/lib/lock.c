#include <lib/lock.h>
#include <scheduler/process.h>

void lock_waitacquire(struct lock* l)
{
	cur_process->waiting_on=l;
	while(l->val!=NULL) process_yield(PROC_WAITING);
	l->val=cur_process;
}
