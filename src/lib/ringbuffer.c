/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/ringbuffer.h>

int ringbuffer_read(struct ringbuffer* rb,void* data,size_t count)
{
	if(count>rb->count) count=rb->count;
	uint8_t* d=(uint8_t*)data;

	// TODO: check if this can be optimized to memcpy
	size_t i=0;
	while(i<count)
	{
		d[i++]=rb->data[rb->pos%RINGBUF_SIZE];
		rb->pos++;
	}
	rb->count-=count;
	return count;
}

int ringbuffer_write(struct ringbuffer* rb,const void* data,size_t count)
{
	if(count>RINGBUF_SIZE) count=RINGBUF_SIZE;
	const uint8_t* d=(const uint8_t*)data;

	// TODO: check if this can be optimized to memcpy
	size_t i=0;
	while(i<count)
	{
		rb->data[(rb->pos+rb->count)%RINGBUF_SIZE]=d[i++];
		if(rb->count>=RINGBUF_SIZE) rb->pos++;
		else rb->count++;
	}
	return count;
}
