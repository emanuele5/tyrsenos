/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>

/*
	NOTES ON ALIGNMENT
	4-byte allocations are 4-byte aligned
	8-byte allocations are 8-byte aligned
	16-byte allocations are 16-byte aligned
	32-byte allocations are 32-byte aligned
	64-byte allocations are 64-byte aligned
	128-byte allocations are 128-byte aligned
	248-byte allocations and bigger are 8-byte aligned
*/

#define KMALLOC_TYPES 10

struct kmalloc_page_type
{
	unsigned size;
	unsigned elements;
	unsigned bmp_size;
};

struct kmalloc_header
{
	struct kmalloc_header* prev;
	struct kmalloc_header* next;
	uint8_t type;
	uint8_t bitmap[];
}__attribute__((packed));

static const struct kmalloc_page_type types[KMALLOC_TYPES]=
{
	{4,989,127},
	{8,502,63},
	{16,252,47},
	{32,126,47},
	{64,63,47},
	{128,31,111},
	{248,16,111},
	{504,8,47},
	{1016,4,15},
	{2032,2,15},
};

static struct kmalloc_header* firsts[KMALLOC_TYPES];

static size_t object_offset(unsigned type,unsigned n)
{
	return sizeof(struct kmalloc_header)+types[type].bmp_size+types[type].size*n;
}

void* kmalloc(size_t size)
{
	if(size==0 || size>PAGE_SIZE) return NULL;
	
	// TODO: this is a hack, we need a better way to kmalloc >=PAGE_SIZE
	if(size>2032) return virtmem_get_heap_pages(PAGE_SIZE);

	unsigned type;
	for(type=0;size>types[type].size;type++);
	
	for(struct kmalloc_header* page=firsts[type];page!=NULL;page=page->next)
	{
		for(unsigned i=0;i<types[type].elements;i++)
		{
			if((page->bitmap[i/8]&(1<<i%8))==0)
			{
				page->bitmap[i/8]|=(1<<i%8);
				return (uint8_t*)page+object_offset(type,i);
			}
		}
	}
	// no space in the already allocated pages, create a new one
	struct kmalloc_header* page=virtmem_get_heap_pages(PAGE_SIZE);
	page->next=firsts[type];
	page->prev=NULL;
	page->type=type;
	memset(page->bitmap,0,types[type].bmp_size);
	page->bitmap[0]=1;
	if(page->next!=NULL) page->next->prev=page;
	firsts[type]=page;
	return (uint8_t*)page+object_offset(type,0);
}

void kfree(void* ptr)
{
	if(ptr==NULL) return;

	struct kmalloc_header* page=(struct kmalloc_header*)((uintptr_t)ptr&~0xfff);
	size_t offset=(uintptr_t)ptr&0xfff;
	if(offset==0)
	{
		// also TODO: see corresponding note on kmalloc
		virtmem_release_heap_pages(ptr,PAGE_SIZE);
		return;
	}
	unsigned type=page->type;
	
	size_t header_size=sizeof(struct kmalloc_header)+types[type].bmp_size;
	unsigned index=(offset-header_size)/types[type].size;
	page->bitmap[index/8]&=~(1<<index%8);
	
	// check if the page is completely empty
	for(unsigned i=0;i<types[type].bmp_size;i++)
	{
		if(page->bitmap[i]!=0) return;
	}
	// if the page is empty deallocate it completely
	if(page->next!=NULL) page->next->prev=page->prev;
	if(page->prev!=NULL) page->prev->next=page->next;
	else firsts[type]=page->next;
	virtmem_release_heap_pages(page,PAGE_SIZE);
}

void* krealloc(void* ptr,size_t size)
{
	if(ptr==NULL) return kmalloc(size);

	const struct kmalloc_header* page=(struct kmalloc_header*)((uintptr_t)ptr&~0xfff);
	unsigned type=page->type;

	// if the new size nicely fits in the current page, don't do anything
	if((size<=types[type].size)&&(size>types[type-1].size)) return ptr;
	else
	{
		// otherwise we make a new allocation, and copy in it the smaller of the sizes
		void* newptr=kmalloc(size);
		if(size>types[type].size) size=types[type].size;
		memcpy(newptr,ptr,size);
		kfree(ptr);
		return newptr;
	}
}
