/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <memory/virtual.h>
#include <memory/mmap.h>
#include <scheduler/process.h>
#include <vfs/descriptor.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/mman.h>

/* this is a ugly hack that relies on contiguous physical ang virtual pages
 * TODO: switch to a per-page allocation with refcounted pages, as to allow
 * - allow non-contiguous physical pages
 * - copy-on-write
 * - lazy loading
 * - shared pages
 */

void area_erase(struct mmap_area* area)
{
	virtmem_unmap_user_range(cur_process->cr3,area->address,area->length);
	physmem_release(area->phys,area->length);
	kfree(area);
}

void area_erase_all(void)
{
	size_t area_count=cur_process->area_count;
	for(size_t i=0;i<area_count;i++)
	{
		area_erase(cur_process->areas[i]);
	}
	cur_process->area_count=0;
	kfree(cur_process->areas);
	cur_process->areas=NULL;
}

void area_copy_all(const struct process* parent,struct process* child)
{
	size_t area_count=parent->area_count;
	child->area_count=area_count;
	child->areas=kmalloc_array(struct mmap_area*,area_count);
	for(size_t i=0;i<area_count;i++)
	{
		struct mmap_area* oldarea=parent->areas[i];
		if(oldarea!=NULL)
		{
			struct mmap_area* newarea=kmalloc_type(struct mmap_area);			
			newarea->address=oldarea->address;
			newarea->length=oldarea->length,
			newarea->flags=oldarea->flags;
			newarea->phys=physmem_get(newarea->length);
			newarea->backing=oldarea->backing;
			if(newarea->backing) inode_acquire(newarea->backing);
			newarea->offset=oldarea->offset;
			
			enum page_flags vm_flags=0;
			if(newarea->flags&PROT_WRITE) vm_flags|=PAGE_WRITE;
			virtmem_map_user_range(child->cr3,newarea->address,newarea->phys,newarea->length,vm_flags);
			memcpy(kernel_address(newarea->phys),kernel_address(oldarea->phys),newarea->length);
			
			child->areas[i]=newarea;
		}
		else child->areas[i]=NULL;
	}
}

void* area_map(uint8_t* addr,size_t len,int flags,struct vfs_inode* inode,off_t off)
{
	if(flags&MAP_SHARED) return (void*)-EINVAL; // MAP_SHARED not supported yet
	
	if(len==0) return (void*)-EINVAL;
	if(!(flags&MAP_PRIVATE) && !(flags&MAP_SHARED)) return (void*)-EINVAL;
	
	// round offset and length
	off-=off%PAGE_SIZE;
	len=(len+(PAGE_SIZE-1))&~(PAGE_SIZE-1);
	
	size_t page_offset=(uintptr_t)addr%PAGE_SIZE;
	if(flags&MAP_FIXED) // user wants ONLY this address, so check it very thoroughly
	{
		if(page_offset || addr==NULL) return (void*)-EINVAL;		// must be page-aligned
		if(addr==NULL) return (void*)-EINVAL;						// can't be null
		if(virtmem_is_kernel(addr)||virtmem_is_kernel(addr+len)) return (void*)-ENOMEM;	// can't be in kernel memory
		if(!virtmem_range_is_free(addr,len)) return (void*)-ENOMEM;	// must be free
	}
	else
	{
		addr-=page_offset;	// align address

		if((addr==NULL)||virtmem_is_kernel(addr)||virtmem_is_kernel(addr+len)||!virtmem_range_is_free(addr,len))
		{
			// if given address is not valid, search for a suitable hole in the address space
			// TODO: this is basically O(n²), but will need an helper in the vmm to become O(n)
			bool found=false;
			addr=(uint8_t*)PAGE_SIZE;
			while(virtmem_is_user(addr+len))
			{
				if(virtmem_range_is_free(addr,len))
				{
					found=true;
					break;
				}
				addr+=PAGE_SIZE;
			}
			if(found==false) return (void*)-ENOMEM;
		}
	}
	
	// do the actual mapping
	enum page_flags vm_flags=0;
	if(flags&PROT_WRITE) vm_flags|=PAGE_WRITE;
	physaddr phys=physmem_get(len);
	virtmem_map_user_range(cur_process->cr3,addr,phys,len,vm_flags);
	if(inode)
	{
		inode->obj_funcs->read(inode,off,addr,len,0);
	}
	else memset(addr,0,len);
	
	// create area struct
	struct mmap_area* newarea=kmalloc_type(struct mmap_area);
	newarea->address=addr;
	newarea->length=len;
	newarea->flags=flags;
	newarea->phys=phys;
	newarea->backing=inode;
	if(inode) inode_acquire(inode);
	newarea->offset=off;

	// insert area in the process struct
	int area_index=cur_process->area_count++;
	cur_process->areas=krealloc_array(cur_process->areas,struct mmap_area*,cur_process->area_count);
	cur_process->areas[area_index]=newarea;
	return addr;
}

void* sys_mmap(void* addr,size_t len,int flags,int fd,off_t off)
{
	struct vfs_inode* inode=NULL;
	if(!(flags&MAP_ANONYMOUS))
	{
		struct file_descriptor* f=descriptor_get(fd);
		if(!f) return (void*)-EBADF;
		
		if(!(f->flags&O_RDONLY)) return (void*)-EACCES;
		if(flags&MAP_SHARED && flags&PROT_WRITE && !(f->flags&O_WRONLY)) return (void*)-EACCES;
		
		inode=f->inode;
	}
	return area_map(addr,len,flags,inode,off);
}
