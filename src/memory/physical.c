/*
 * Copyright 2019-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/bitmap.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/physical.h>
#include <memory/virtual.h>

static const physaddr PKSTART		=0x10000;
static const physaddr PBOOTINFO		=0x8000;
static const physaddr PBOOTPTABLES	=0x90000;

extern uint8_t kend;

static size_t physmem_bitmap_len;
static uint8_t* physmem_bitmap;
static uint64_t physmem_freepages=0;

static void use_pages(physaddr start,size_t count)
{
	for(size_t page=start/PAGE_SIZE;page<start/PAGE_SIZE+count;page++)
	{
		bitmap_set(physmem_bitmap,page);
	}
	physmem_freepages-=count;
}

static void release_pages(physaddr start,size_t count)
{
	for(size_t page=start/PAGE_SIZE;page<start/PAGE_SIZE+count;page++)
	{
		bitmap_unset(physmem_bitmap,page);
	}
	physmem_freepages+=count;
}

static inline size_t page_count(size_t size)
{
	return (size+PAGE_SIZE-1)/PAGE_SIZE;
}

void physmem_init(const struct bios_mm* map,size_t map_size)
{
	physaddr usable_mem_end=0;
	// find the end of usable memory
	for(unsigned i=0;i<map_size;i++)
	{
		if(map[i].type==MEM_USABLE)
		{
			physaddr area_end=map[i].b_addr+map[i].len;
			if(area_end>usable_mem_end) usable_mem_end=area_end;
		}
	}
	
	physmem_bitmap_len=(usable_mem_end+32767)/32768;
	physmem_bitmap=&kend;
	// mark all memory occupied
	memset(physmem_bitmap,0xff,physmem_bitmap_len);
	// mark free all usable memory
	for(unsigned i=0;i<map_size;i++)
	{
		if(map[i].type==MEM_USABLE)
		{
			release_pages(map[i].b_addr,map[i].len/PAGE_SIZE);
		}
	}
	// mark occupied all kernel
	use_pages(PKSTART,page_count((uintptr_t)(physmem_bitmap+physmem_bitmap_len)-KERN_MEMORY_BASE-PKSTART));
	// mark occupied the kernel stack
	use_pages(PKSTACK,1);
	// mark occupied the bootinfo
	use_pages(PBOOTINFO,1);
	// mark occupied the provisional page tables
	use_pages(PBOOTPTABLES,3);
}

void physmem_reclaim_bootmem(void)
{
	release_pages(PBOOTINFO,1);
	release_pages(PBOOTPTABLES,3);
}

physaddr physmem_get(size_t size)
{
	size_t pages=page_count(size);
	if(physmem_freepages<pages)
	{
		//TODO: try to empty caches
		kpanic("Out of memory");
	}
	size_t count=0;
	for(size_t i=0;i<physmem_bitmap_len*8;i++)
	{
		if(bitmap_isset(physmem_bitmap,i)) count=0;
		else
		{
			count++;
			if(count==pages)
			{
				physaddr result=(i-pages+1)*PAGE_SIZE;
				use_pages(result,pages);
				return result;
			}
		}
	}
	__builtin_unreachable();
}

void physmem_release(physaddr start,size_t size)
{
	size_t pages=page_count(size);
	release_pages(start,pages);
}
