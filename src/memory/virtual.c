/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <cpu.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/virtual.h>
#include <scheduler/process.h>

static const physaddr PAGE_ADDRESS_MASK	=0x000ffffffffff000;
static const physaddr PAGE_FLAGS_MASK	=0xfff0000000000fff;

static physaddr create_table(void)
{
	physaddr page=physmem_get(PAGE_SIZE);
	memset(kernel_address(page),0,PAGE_SIZE);
	return page;
}

static physaddr* get_table(const physaddr* parent,unsigned index)
{
	physaddr table=parent[index];
	if(!(table&PAGE_PRESENT)) return NULL;

	table&=PAGE_ADDRESS_MASK;
	return kernel_address(table);
}

static physaddr* get_or_create_table(physaddr* parent,unsigned index,enum page_flags flags)
{
	physaddr table=parent[index];
	if(!(table&PAGE_PRESENT))
	{
		// create table if it doesn't exist
		table=create_table()|PAGE_PRESENT|PAGE_WRITE;
		if(flags&PAGE_USER) table|=PAGE_USER;
		parent[index]=table;
	}
	table&=PAGE_ADDRESS_MASK; 
	return kernel_address(table);
}

void virtmem_init(const struct bios_mm* mem_map,size_t map_size)
{
	//get a page to be our first pml4
	cur_process->cr3=create_table();

	for(unsigned i=0;i<map_size;i++)
	{
		if(mem_map[i].type==MEM_USABLE)
		{
			virtmem_map_kernel_range(mem_map[i].b_addr,mem_map[i].len,0);
		}
	}
	wrcr3(cur_process->cr3);
}

physaddr virtmem_create_address_space(void)
{
	const physaddr* oldbase=kernel_address(cur_process->cr3);
	physaddr result=physmem_get(PAGE_SIZE);
	physaddr* newbase=kernel_address(result);
	memcpy(newbase+256,oldbase+256,PAGE_SIZE/2);
	memset(newbase,0,PAGE_SIZE/2);
	return result;
}

static physaddr get_page_entry(uintptr_t virt)
{
	const physaddr* base=kernel_address(cur_process->cr3);
	// get the page directory page table
	size_t pml4_index=(virt>>39)&0x1ff;
	const physaddr* pdpt=get_table(base,pml4_index);
	if(pdpt==NULL) return 0;
	// get the page directory
	size_t pdpt_index=(virt>>30)&0x1ff;
	const physaddr* pd=get_table(pdpt,pdpt_index);
	if(pd==NULL) return 0;
	// get the page table
	size_t pd_index=(virt>>21)&0x1ff;
	const physaddr* pt=get_table(pd,pd_index);
	if(pt==NULL) return 0;
	// get the page entry
	size_t pt_index=(virt>>12)&0x1ff;
	return pt[pt_index];
}

physaddr virtmem_get_physaddr(const void* virt)
{
	if(virtmem_is_kernel(virt)) return (uintptr_t)virt-KERN_MEMORY_BASE;
	else return get_page_entry((uintptr_t)virt)&PAGE_ADDRESS_MASK;
}

static enum page_flags virtmem_get_flags(uintptr_t virt)
{
	return get_page_entry(virt)&PAGE_FLAGS_MASK;
}

static bool virtmem_is_present(const void* virt)
{
	return virtmem_get_flags((uintptr_t)virt)&PAGE_PRESENT;
}

static inline bool virtmem_is_writeable(const void* virt)
{
	enum page_flags flags=virtmem_get_flags((uintptr_t)virt);
	if(flags&PAGE_PRESENT && flags& PAGE_WRITE) return true;
	else return false;
}

bool virtmem_range_is_free(const void* virt,size_t size)
{
	uintptr_t start=(uintptr_t)virt;
	for(uintptr_t i=0;i<size;i+=PAGE_SIZE)
	{
		if(virtmem_is_present((void*)(start+i))) return false;
	}
	return true;
}

bool virtmem_range_is_user_readable(const void* p,size_t size)
{
	const uint8_t* byteptr=p;
	// check overflow
	if(((uint8_t*)-1l)-size < byteptr) return false;

	if(virtmem_is_kernel(byteptr)) return false;
	if(virtmem_is_kernel(byteptr+size)) return false;

	for(size_t i=0;i<size;i+=PAGE_SIZE)
	{
		if(!virtmem_is_present(byteptr+i)) return false;
	}
	return true;
}

bool virtmem_range_is_user_writeable(const void* p,size_t size)
{
	const uint8_t* byteptr=p;
	// check overflow
	if(((uint8_t*)-1l)-size < byteptr) return false;

	if(virtmem_is_kernel(byteptr)) return false;
	if(virtmem_is_kernel(byteptr+size)) return false;

	for(size_t i=0;i<size;i+=PAGE_SIZE)
	{
		if(!virtmem_is_writeable(byteptr+i)) return false;
	}
	return true;
}

static void virtmem_map_page4k(physaddr cr3,uintptr_t virt,physaddr phys,enum page_flags flags)
{
	physaddr* base=kernel_address(cr3);
	// get the page directory page table
	size_t pml4_index=(virt>>39)&0x1ff;
	physaddr* pdpt=get_or_create_table(base,pml4_index,flags);
	// get the page directory
	size_t pdpt_index=(virt>>30)&0x1ff;
	physaddr* pd=get_or_create_table(pdpt,pdpt_index,flags);
	// get the page table
	size_t pd_index=(virt>>21)&0x1ff;
	physaddr* pt=get_or_create_table(pd,pd_index,flags);
	// set the page
	size_t pt_index=(virt>>12)&0x1ff;
	pt[pt_index]=phys|flags|PAGE_PRESENT;
	invlpg(virt);
}

static void virtmem_unmap_page4k(physaddr cr3,uintptr_t virt)
{
	const physaddr* base=kernel_address(cur_process->cr3);
	// get the page directory page table
	size_t pml4_index=(virt>>39)&0x1ff;
	const physaddr* pdpt=get_table(base,pml4_index);
	if(pdpt==NULL) return;
	// get the page directory
	size_t pdpt_index=(virt>>30)&0x1ff;
	const physaddr* pd=get_table(pdpt,pdpt_index);
	if(pd==NULL) return;
	// get the page table
	size_t pd_index=(virt>>21)&0x1ff;
	physaddr* pt=get_table(pd,pd_index);
	if(pt==NULL) return;
	// remove the page entry
	size_t pt_index=(virt>>12)&0x1ff;
	pt[pt_index]=0;
	invlpg(virt);
}

static void virtmem_map_range(physaddr cr3,void* virt,physaddr phys,size_t len,enum page_flags flags)
{
	uintptr_t v=(uintptr_t)virt;
	while(len>=PAGE_SIZE)
	{
		virtmem_map_page4k(cr3,v,phys,flags);
		phys+=PAGE_SIZE;
		v+=PAGE_SIZE;
		len-=PAGE_SIZE;
	}
}

static void virtmem_unmap_range(physaddr cr3,void* virt,size_t len)
{
	uintptr_t v=(uintptr_t)virt;
	while(len>=PAGE_SIZE)
	{
		virtmem_unmap_page4k(cr3,v);
		v+=PAGE_SIZE;
		len-=PAGE_SIZE;
	}
}

void* virtmem_map_kernel_range(physaddr phys,size_t len,enum page_flags flags)
{
	void* virt=kernel_address(phys);
	virtmem_map_range(cur_process->cr3,virt,phys,len,flags|PAGE_WRITE|PAGE_GLOBAL);
	return virt;
}

void virtmem_map_user_range(physaddr cr3,void* virt,physaddr phys,size_t len,enum page_flags flags)
{
	virtmem_map_range(cr3,virt,phys,len,flags|PAGE_USER);
}

void virtmem_unmap_user_range(physaddr cr3,void* virt,size_t len)
{
	virtmem_unmap_range(cr3,virt,len);
}

void* virtmem_get_heap_pages(size_t size)
{
	physaddr phys=physmem_get(size);
	return kernel_address(phys);
}

void virtmem_release_heap_pages(void* virt,size_t size)
{
	physaddr phys=(uintptr_t)virt-KERN_MEMORY_BASE;
	physmem_release(phys,size);
}
