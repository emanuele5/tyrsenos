/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <net/ethernet.h>

#include <netinet/in.h>
#include <netinet/if_ether.h>

enum arp_op_type
{
	ARP_OP_REQUEST	=1,
	ARP_OP_REPLY	=2,
};

struct arp_packet
{
	uint16_t hardware_type;
	uint16_t protocol_type;
	uint8_t hardware_length;
	uint8_t protocol_length;
	uint16_t operation;
	// realistically, arp is used only to associate MAC and IPv4 addresses
	struct ether_addr sender_mac;
	in_addr_t sender_ip;
	struct ether_addr target_mac;
	in_addr_t target_ip;
}__attribute__((packed));

void arp_handle(struct network_packet* packet)
{
	struct arp_packet* data=(struct arp_packet*)(packet->data+packet->offset);

	uint16_t hardware_type=ntohs(data->hardware_type);
	uint16_t protocol_type=ntohs(data->protocol_type);
	uint8_t hardware_length=data->hardware_length;
	uint8_t protocol_length=data->protocol_length;
	uint16_t operation=ntohs(data->operation);
	// we only support MAC for hardware and IP for protocol
	// drop unsopported or invalid packets
	if(hardware_type!=1 || hardware_length!=6) return;
	if(protocol_type!=ETHERTYPE_IP || protocol_length!=4) return;
	if(operation!=ARP_OP_REQUEST && operation!=ARP_OP_REPLY) return;
	
	// TODO
	return;
}
