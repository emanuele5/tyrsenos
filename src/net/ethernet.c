/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <net/arp.h>
#include <net/ethernet.h>

#include <arpa/inet.h>
#include <netinet/if_ether.h>

void ethernet_packet_handle(struct network_packet* packet)
{
	struct ether_header* header=(struct ether_header*)(packet->data+packet->offset);
	uint16_t type=ntohs(header->ether_type);
	
	packet->offset+=sizeof(struct ether_header);
	packet->size-=sizeof(struct ether_header)+sizeof(struct ether_footer);

	switch(type)
	{
	case ETHERTYPE_IP:
		break;
	case ETHERTYPE_ARP:
		arp_handle(packet);
		break;
	case ETHERTYPE_IPV6:
		break;
	default:
		klog("ethernet: unknown packet type %04x",type);
		break;
	};
}
