/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <vfs/descriptor.h>
#include <vfs/inode.h>

#include <errno.h>
#include <sys/socket.h>

static ssize_t socket_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	return -ENOSYS;
}

static ssize_t socket_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	return -ENOSYS;
}

static const struct object_functions socket_functions=
{
	.read=socket_read,
	.write=socket_write,
};

int sys_socket(int domain,int type,int protocol)
{
	switch(domain)
	{
	case AF_UNIX:
	case AF_INET:
	default:
		return -EAFNOSUPPORT;
	}
}

int sys_connect(int sockfd,const struct sockaddr* addr,socklen_t addrlen)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;
	
	return -ENOSYS;
}

int sys_bind(int sockfd,const struct sockaddr* addr,socklen_t addrlen)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;

	return -ENOSYS;
}

int sys_listen(int sockfd,int backlog)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;

	return -ENOSYS;
}

int sys_accept(int sockfd,struct sockaddr* restrict addr,socklen_t* restrict addrlen)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;

	return -ENOSYS;
}

ssize_t sys_sendmsg(int sockfd,const struct msghdr* msg,int flags)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;

	return -ENOSYS;
}

ssize_t sys_recvmsg(int sockfd,struct msghdr* msg,int flags)
{
	struct file_descriptor* f=descriptor_get(sockfd);
	if(f==NULL) return -EBADF;

	struct vfs_inode* socket=f->inode;
	if(!S_ISSOCK(socket->mode)) return -ENOTSOCK;

	return -ENOSYS;
}
