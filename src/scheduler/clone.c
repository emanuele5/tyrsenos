/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <memory/virtual.h>
#include <memory/kmalloc.h>
#include <scheduler/clone.h>
#include <vfs/descriptor.h>

struct kernel_context
{
	uint64_t r15;
	uint64_t r14;
	uint64_t r13;
	uint64_t r12;
	uint64_t rbp;
	uint64_t rbx;
	uint64_t rflags;
	uint64_t rip;
	uint64_t retrip;
};

static pid_t last_pid=0;

__attribute__((noreturn)) extern void return_to_userspace(void);

struct user_context* process_get_user_context(const struct process* p)
{
	void* stack_top=(uint8_t*)p->kernel_stack+PAGE_SIZE;
	return (struct user_context*)stack_top-1;
}

struct process* process_clone(struct process* parent,kernel_return_function retfunc)
{
	struct process* child=kmalloc(sizeof(struct process));

	child->cr3=virtmem_create_address_space();
	// allocate a kernel stack
	child->kernel_stack=virtmem_get_heap_pages(PAGE_SIZE);
	// set stack top
	child->sp0=(uint8_t*)child->kernel_stack+PAGE_SIZE-sizeof(struct user_context)-sizeof(struct kernel_context);
	// inject return function into child call stack
	struct kernel_context* kctx=child->sp0;
	kctx->retrip=(uintptr_t)return_to_userspace;
	kctx->rip=(uintptr_t)retfunc;
	kctx->rflags=0;
	
	child->status=PROC_WAITING; // waiting so it's not runnable until fully complete

	// copy ids
	child->pid=++last_pid;
	child->uid=parent->uid;
	child->gid=parent->gid;
	child->euid=parent->euid;
	child->egid=parent->egid;
	// copy signals data
	memcpy(child->handlers,parent->handlers,sizeof(sighandler_t)*NSIG);
	child->sigmask=parent->sigmask;
	child->pending=parent->pending;
	child->last_siginfo=parent->last_siginfo;
	
	child->waiting_on=NULL; // assume parent process isn't waiting (how would it fork otherwise?)
	
	child->cwd=parent->cwd;
	inode_acquire(child->cwd); // cwd cannot be NULL, so avoid checking
	
	child->file_count=parent->file_count;
	child->files=kmalloc_array(struct file_descriptor*,child->file_count);
	// copy open files table
	for(unsigned i=0;i<child->file_count;i++)
	{
		if(parent->files[i]!=NULL) child->files[i]=descriptor_copy(parent->files[i]);
		else child->files[i]=NULL;
	}
	// copy memory areas
	area_copy_all(parent,child);
	// copy registers
	struct user_context* c_ctx=process_get_user_context(child);
	const struct user_context* p_ctx=process_get_user_context(parent);
	memcpy(c_ctx,p_ctx,sizeof(struct user_context));
	c_ctx->rax=0;
	// update genealogy
	child->parent=parent;
	child->firstchild=NULL;
	child->prevsibling=NULL;
	child->nextsibling=parent->firstchild;
	parent->firstchild=child;
	if(child->nextsibling) child->nextsibling->prevsibling=child;
	
	struct process* p=cur_process;
	while(p->next) p=p->next;
	p->next=child;
	child->prev=p;
	child->next=NULL;
	
	child->status=PROC_RUNNABLE;
	
	return child;
}

void return_to_forked_child(void)
{
}

pid_t sys_fork(void)
{
	const struct process* child=process_clone(cur_process,return_to_forked_child);
	return child->pid;
}
