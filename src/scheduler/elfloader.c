/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <kernel/interrupts.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/mmap.h>
#include <scheduler/clone.h>
#include <scheduler/elfloader.h>
#include <scheduler/process.h>
#include <scheduler/signal.h>
#include <vfs/descriptor.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>

struct elf_header
{
	char ident[16];		// see enum elf_ident_offses
	uint16_t type;
	uint16_t machine;	// must be ox3e (EM_AMD64)
	uint32_t version;	// should always be 1
	uint64_t entry;		// program entry point
	uint64_t phoff;		// program header table offset
	uint64_t shoff;		// section header table offset
	uint32_t flags;		// unused
	uint16_t ehsize;	// header size
	uint16_t phentsize;	// program header entry size
	uint16_t phnum;		// program header entry count
	uint16_t shentsize;	// section header entry size
	uint16_t shnum;		// section header enytu count
	uint16_t shstrndx;
};

enum elf_ident_offsets
{
	EI_MAG0			=0, // 0x7f
	EI_MAG1			=1, // 'E'
	EI_MAG2			=2, // 'L'
	EI_MAG3			=3, // 'F'
	EI_CLASS		=4, // must be 2 (ELFCLASS64)
	EI_DATA			=5, // must be 1 (ELFDATA2LSB)
	EI_VERSION		=6, // should always be 1
	EI_ABI			=7, // should always be 0 (System V ABI)
	EI_ABIVERSION	=8, // we have no use for it as of now
	EI_PAD			=9,	// start of padding, ignored
	EI_NIDENT		=16,
};

struct elf_shdr
{
	uint32_t name;
	uint32_t type;
	uint64_t flags;
	uint64_t addr;
	uint64_t offset;
	uint64_t size;
	uint32_t link;
	uint32_t info;
	uint64_t addralign;
	uint64_t entsize;
};

enum elf_shdr_types
{
	SHT_RELA=4,
};

struct elf_sym
{
	uint32_t name;
	unsigned char info;
	unsigned char other;
	uint16_t shndx;
	uint64_t value;
	uint64_t size;
}__attribute__((packed));

struct elf_rela
{
	uint64_t offset;
	uint64_t info;
	int64_t addend;
};

enum elf_rel_type
{
	X86_64_COPY		=5,
	X86_64_GLOB_DAT	=6,
	X86_64_JUMP_SLOT=7,
	X86_64_RELATIVE	=8,
};

struct elf_phdr
{
	uint32_t type;
	uint32_t flags;
	uint64_t offset;	// offset in the file
	uint64_t vaddr;		// virtual address of the segment
	uint64_t paddr;		// physical address of the segment (unused by us)
	uint64_t filesz;	// size of segment on file
	uint64_t memsz;		// size of segment in memory
	uint64_t align;		// alignment, must be 0 or a power of 2
};

enum elf_phdr_types
{
	PT_NULL		=0,
	PT_LOAD		=1,
	PT_DYNAMIC	=2,
	PT_INTERP	=3,
	PT_NOTE		=4,
	PT_SHLIB	=5,
	PT_PHDR		=6,
	PT_TLS		=7,
};

enum elf_phdr_flags
{
	PF_X=1,
	PF_W=2,
	PF_R=4,
};

struct elf_dyn
{
	int64_t tag;
	union
	{
		uint64_t val;
		uint64_t ptr;
	};
};

enum elf_dyn_tags
{
	DT_NULL		=0,
	DT_NEEDED	=1,
	DT_HASH		=4,
	DT_STRTAB	=5,
	DT_SYMTAB	=6,
	DT_STRSZ	=10,
};

struct symdef
{
	const struct elf_object* object;
	const struct elf_sym* sym;
};

// kernel structure to keep track of the elf structures during loading
struct elf_object
{
	struct vfs_inode*		inode;
	struct elf_header		header;
	struct elf_phdr* 		phdr_table;
	struct elf_shdr* 		shdr_table;

	//
	// valid only for executables with a dyn section, all pointers null otherwise
	//

	const char*				dyn_str;	// points to object data
	const struct elf_sym*	dyn_sym;	// points to object data
	size_t					dyn_symsz;

	// dependencies
	unsigned				depcount;
	struct elf_object**		deps;
	size_t*					depnames;
	// relocation base
	uintptr_t				base;
};

static const kstringview libdir=kstringview_from_literal("/boot/prog/lib/");

// checks elf header and initializes kernel structures
static int elf_load_metadata(struct elf_object* program)
{
	// initialize structure
	program->phdr_table=NULL;
	program->shdr_table=NULL;
	program->depcount=0;
	program->depnames=NULL;
	program->deps=NULL;
	// read header
	struct vfs_inode* exec_inode=program->inode;
	int err=exec_inode->obj_funcs->read(exec_inode,0,&program->header,sizeof(struct elf_header),0);
	if(err<0) return err;

	// check header is valid
	if(memcmp(program->header.ident,"\x7f""ELF",4)!=0) return -ENOEXEC;
	if(program->header.ident[EI_CLASS]!=2) return -ENOEXEC;
	if(program->header.ident[EI_DATA]!=1) return -ENOEXEC;
	if(program->header.ident[EI_VERSION]!=1) return -ENOEXEC;
	if(program->header.ident[EI_ABI]!=0) return -ENOEXEC;
	if(program->header.machine!=0x3e) return -ENOEXEC;
	if(program->header.version!=1) return -ENOEXEC;
	
	// read program header table
	size_t phdr_size=program->header.phentsize*program->header.phnum;
	program->phdr_table=kmalloc(phdr_size);
	err=exec_inode->obj_funcs->read(exec_inode,program->header.phoff,program->phdr_table,phdr_size,0);
	if(err<0) return err;
	// read section header table
	size_t shdr_size=program->header.shentsize*program->header.shnum;
	program->shdr_table=kmalloc(shdr_size);
	err=exec_inode->obj_funcs->read(exec_inode,program->header.shoff,program->shdr_table,shdr_size,0);
	if(err<0) return err;

	return 0;
}

static void elf_deallocate_metadata(const struct elf_object* program)
{
	if(program->phdr_table) kfree(program->phdr_table);
	if(program->shdr_table) kfree(program->shdr_table);
	if(program->depnames) kfree(program->depnames);
	if(program->deps) kfree(program->deps);
}

static int elf_load_image(struct elf_object* p)
{
	uintptr_t image_end=0;
	const struct elf_dyn* dyn=NULL;
	
	const struct elf_phdr* phdr_table=p->phdr_table;
	for(unsigned i=0;i<p->header.phnum;i++)
	{
		// load section, load it
		if(phdr_table[i].type==PT_LOAD)
		{
			int load_flags=MAP_FIXED|MAP_PRIVATE;
			if(phdr_table[i].flags&PF_R) load_flags|=PROT_READ;
			if(phdr_table[i].flags&PF_W) load_flags|=PROT_WRITE;
			if(phdr_table[i].flags&PF_X) load_flags|=PROT_EXEC;
			
			unsigned page_offset=(phdr_table[i].vaddr + p->base)%PAGE_SIZE;
	
			off_t f_start=phdr_table[i].offset-page_offset;
			uintptr_t m_start=phdr_table[i].vaddr + p->base - page_offset;
			size_t f_size=phdr_table[i].filesz+page_offset;
			size_t m_size=phdr_table[i].memsz+page_offset;
			area_map((void*)m_start,m_size,load_flags,p->inode,f_start);
			memset((void*)(m_start+f_size),0,m_size-f_size);
			
			// update end of image
			uintptr_t m_end=m_start+m_size;
			if(m_end>image_end) image_end=m_end;
		}
		else if(phdr_table[i].type==PT_DYNAMIC)
		{
			// save the address of the dynamic section, it will be loaded by a LOAD section
			dyn=(void*)(phdr_table[i].vaddr+p->base);
		}
	}
	// parse dynamic section if presents
	if(dyn)
	{
		for(unsigned i=0;dyn[i].tag!=DT_NULL;i++)
		{
			switch(dyn[i].tag)
			{
			case DT_NEEDED:
				p->depnames=krealloc_array(p->depnames,size_t,p->depcount+1);
				p->depnames[p->depcount]=dyn[i].ptr+p->base;
				p->depcount++;
				break;
			case DT_HASH:
				p->dyn_symsz=((uint32_t*)(dyn[i].ptr+p->base))[1];
				break;
			case DT_STRTAB:
				p->dyn_str=(void*)(dyn[i].ptr+p->base);
				break;
			case DT_SYMTAB:
				p->dyn_sym=(void*)(dyn[i].ptr+p->base);
				break;
			}
		}
	}
	// return end address rounded to next page
	return (image_end+PAGE_SIZE-1)&~(PAGE_SIZE-1);
}

static struct symdef search_symbol(const struct elf_object* p,const char* name,bool copy_reloc)
{
	if(!copy_reloc) for(size_t i=0;i<p->dyn_symsz;i++)
	{
		const struct elf_sym* sym=p->dyn_sym+i;
		if(sym->shndx==0) continue;
		if(sym->value==0) continue;

		const char* sym_name=p->dyn_str+sym->name;
		if(strcmp(name,sym_name)==0) return (struct symdef){p,sym};
	}
	// recursive search, depth first (should be breadth first)
	for(size_t i=0;i<p->depcount;i++)
	{
		const struct elf_object* dep=p->deps[i];
		if(dep==NULL) continue;
		struct symdef def=search_symbol(dep,name,false);
		if(def.sym!=NULL) return def;
	}
	return (struct symdef){NULL,NULL};
}

static void elf_relocate(const struct elf_object* p,const struct elf_object* root)
{
	// search the section with the relocation tables
	const struct elf_shdr* shdr_table=p->shdr_table;
	for(unsigned i=0;i<p->header.shnum;i++)
	{	
		if(shdr_table[i].type==SHT_RELA)
		{
			// found it! now iterate on every element
			const struct elf_rela* rela=(void*)(shdr_table[i].addr + p->base);
			size_t relacount=shdr_table[i].size/sizeof(struct elf_rela);
			for(size_t j=0;j<relacount;j++)
			{
				uint32_t reltype=(uint32_t)rela[j].info;
				uint64_t* reloff=(void*)(rela[j].offset + p->base);
				int64_t reladdend=rela[j].addend;
				uint64_t symval=0;

				struct symdef def={0};
				// R_X86_64_RELATIVE doesn't need a symbol, otherwise we need to search for it
				if(reltype!=X86_64_RELATIVE)
				{
					// take the index of the symbol name
					size_t symindex=rela[j].info>>32;
					const char* symname=p->dyn_str + p->dyn_sym[symindex].name;
					
					if(reltype==X86_64_COPY) def=search_symbol(root,symname,true);
					else def=search_symbol(root,symname,false);
					if(def.sym==NULL)
					{
						klog("exec: pid %d: missing symbol %s",cur_process->pid,symname);
						continue;
					}
					symval=def.sym->value + def.object->base;
				}
				switch(reltype)
				{
				case X86_64_COPY:
					memcpy(reloff,(void*)symval,def.sym->size);
					break;
				case X86_64_GLOB_DAT:
				case X86_64_JUMP_SLOT:
					*reloff=symval+reladdend;
					break;
				case X86_64_RELATIVE:
					*reloff=p->base+reladdend;
					break;
				default:
					klog("exec: unknown relocation type %d for %lx",reltype,rela[j].info);
					break;
				}
			}
		}
	}
}

static int do_elf_load(struct vfs_inode* inode,const char** user_argv,const char** user_envp)
{
	if(!inode_check_permissions(inode,O_EXEC)) return -EACCES;
	
	struct elf_object program;
	program.inode=inode;
	program.base=0;
	int err=elf_load_metadata(&program);
	if(err<0) return err;
	
	// now we know the file is a valid executable, we save all we need from it before erasing
	size_t str_size=0;
	// copy arguments
	size_t argc=0;
	char** k_argv=NULL;
	if(user_argv) for(;user_argv[argc];argc++)
	{
		k_argv=krealloc_array(k_argv,char*,argc+1);
		size_t len=strlen(user_argv[argc])+1;
		k_argv[argc]=kmalloc_array(char,len);
		memcpy(k_argv[argc],user_argv[argc],len);
		str_size+=len;
	}
	// copy environment
	size_t envc=0;
	char** k_envp=NULL;
	if(user_envp) for(;user_envp[envc];envc++)
	{
		k_envp=krealloc_array(k_envp,char*,envc+1);
		size_t len=strlen(user_envp[envc])+1;
		k_envp[envc]=kmalloc_array(char,len);
		memcpy(k_envp[envc],user_envp[envc],len);
		str_size+=len;
	}
	
	// now we can erase the current memory map and load the new executable image
	area_erase_all();
	elf_load_image(&program);
	
	// allocate stack
	area_map((void*)0x3f0000,0x10000,PROT_WRITE|MAP_FIXED|MAP_ANONYMOUS|MAP_PRIVATE,NULL,0);
	
	if(program.depcount)
	{
		uintptr_t libbase=0x40000000;
		
		program.deps=kmalloc_array(struct elf_object*,program.depcount);
		for(size_t i=0;i<program.depcount;i++)
		{
			kstringview libname=kstringview_from_cstring(program.dyn_str+program.depnames[i]);
			kstring libpath=kstringview_concat(libdir,libname);
			
			// search the library
			struct vfs_inode* libfile;
			err=lookup_path(AT_FDCWD,kstringview_from_kstring(libpath),&libfile,0);
			if(err!=0)
			{
				klog("exec: pid %d: missing library %s",cur_process->pid,libpath.data);
				kstring_free(libpath);
				program.deps[i]=NULL;
				continue;
			}
			kstring_free(libpath);
			
			// load the library
			struct elf_object* lib=kmalloc_type(struct elf_object);
			lib->inode=libfile,
			lib->base=libbase;
			elf_load_metadata(lib);
			libbase=elf_load_image(lib);
			program.deps[i]=lib;
		}
		
		// link each library
		for(size_t i=0;i<program.depcount;i++)
		{
			const struct elf_object* lib=program.deps[i];
			if(lib!=NULL) elf_relocate(lib,&program);
		}
		elf_relocate(&program,&program);
		// clean libraries
		for(size_t i=0;i<program.depcount;i++)
		{
			struct elf_object* lib=program.deps[i];
			if(lib==NULL) continue;
			inode_release(lib->inode);
			elf_deallocate_metadata(lib);
			kfree(lib);
		}
	}
	
	// close CLOEXEC files
	for(size_t i=0;i<cur_process->file_count;i++)
	{
		if(cur_process->files[i] && cur_process->files[i]->cloexec) sys_close(i);
	}
	
	// reset signals
	memcpy(cur_process->handlers,default_sighandlers,sizeof(sighandler_t)*NSIG);
	cur_process->sigmask=0;
	cur_process->pending=0;
	
	// allocate a region for arguments and environment
	size_t data_area_size=(argc+envc+2)*sizeof(char*)+str_size;
	void* data_area=area_map(NULL,data_area_size,PROT_READ|PROT_WRITE|MAP_PRIVATE|MAP_ANONYMOUS,NULL,0);
	user_argv=data_area;
	user_envp=user_argv+argc+1;
	char* str_ptr=(void*)(user_envp+envc+1);
	// copy arguments
	for(size_t i=0;i<argc;i++)
	{
		user_argv[i]=str_ptr;
		str_ptr=stpcpy(str_ptr,k_argv[i])+1;
		kfree(k_argv[i]);
	}
	user_argv[argc]=NULL;
	kfree(k_argv);
	// copy environment
	for(size_t i=0;i<envc;i++)
	{
		user_envp[i]=str_ptr;
		str_ptr=stpcpy(str_ptr,k_envp[i])+1;
		kfree(k_envp[i]);
	}
	user_envp[envc]=NULL;
	kfree(k_envp);
	
	struct user_context* exec_context=process_get_user_context(cur_process);
	exec_context->rip=program.header.entry;
	exec_context->rsp=0x400000;
	exec_context->rflags=(1<<9);
	exec_context->rdi=argc;
	exec_context->rsi=(uintptr_t)user_argv;
	exec_context->rdx=envc;
	exec_context->rcx=(uintptr_t)user_envp;
	elf_deallocate_metadata(&program);
	return 0;
}

int sys_execve(int fd,const char* pathname,const char** argv,const char** envp,int flags)
{
	struct vfs_inode* exec_inode;
	if(flags&AT_EMPTY_PATH)
	{
		const struct file_descriptor* f=descriptor_get(fd);
		if(f==NULL) return -EBADF;
		if(!(f->flags&O_EXEC)) return -EBADF;
		exec_inode=f->inode;
		inode_acquire(exec_inode);
	}
	else
	{
		int err=lookup_path(fd,kstringview_from_cstring(pathname),&exec_inode,0);
		if(err<0) return err;
	}
	
	int err=do_elf_load(exec_inode,argv,envp);
	inode_release(exec_inode);
	return err;
}
