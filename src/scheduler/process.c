/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <memory/virtual.h>
#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <scheduler/clone.h>
#include <scheduler/elfloader.h>
#include <scheduler/process.h>
#include <scheduler/procinfo.h>
#include <scheduler/signal.h>
#include <vfs/descriptor.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>

struct process idle_process=
{
	.kernel_stack=(void*)(VKSTACK),
	.cwd=&root_inode,
};

struct process* cur_process=&idle_process;

static const kstringview init_filename=kstringview_from_literal("/boot/prog/bin/init");

void return_to_init(void)
{
	/* 	we can't directly use open or execve with a path, as it they that pathname is in valid user
		memory, so we manually do a lookup_path, allocate a file descriptor and then call execve */
	struct vfs_inode* init_inode;
	int err=lookup_path(AT_FDCWD,init_filename,&init_inode,0);
	if(err<0) kpanic("init program not found");
	int fd=descriptor_open(init_inode,O_EXEC,true); // set cloexec so that init can open stdin in its place
	
	struct user_context* context=process_get_user_context(cur_process);
	context->cs=0x1b;
	context->ss=0x23;

	err=sys_execve(fd,NULL,NULL,NULL,AT_EMPTY_PATH);
	if(err<0) kpanic("failed to execute init program");
}

void process_init(void)
{
	//create init process
	process_clone(cur_process,return_to_init);
}

extern void process_switch(struct process* next);

void process_yieldto(struct process* nextproc,int curstatus)
{
	cur_process->status=curstatus;
	process_switch(nextproc);
}

static bool can_run(const struct process* proc)
{
	if(proc->status==PROC_RUNNABLE) return true;
	if(proc->status==PROC_WAITING && lock_isfree(proc->waiting_on)) return true;
	return false;
}

void process_yield(int curstatus)
{
	struct process* p=cur_process;
	// round-robin scheduler
	while(p->next)
	{
		p=p->next;
		if(can_run(p))
		{
			process_yieldto(p,curstatus);
			return;
		}
	}

	p=idle_process.next;
	while(p)
	{
		if(can_run(p))
		{
			process_yieldto(p,curstatus);
			return;
		}
		if(p==cur_process) break;
		p=p->next;
	}

	process_yieldto(&idle_process,curstatus);
	return;
}

void process_exit(int status)
{
	// close all the files
	for(size_t i=0;i<cur_process->file_count;i++)
	{
		if(cur_process->files[i]!=NULL) sys_close(i);
	}
	kfree(cur_process->files);
	
	// clear user memory
	area_erase_all();
	// this shoudln't happen, init needs always be running
	if(cur_process->pid==1) kpanic("killing init");
	process_send_signal(cur_process->parent,SIGCHLD,0);
	process_yieldto(cur_process->parent,PROC_ZOMBIE);
	/* process_yieldto never returns here: when the parent calls sys_waitchild, the kernel calls
	 * process_deallocate() that removes the last data of this process */
	__builtin_unreachable();
}

void sys_exit(int status)
{
	// filter exit status to 1 byte
	process_exit(status&0xff);
}

static void process_deallocate(struct process* p)
{
	// remove from the pid list
	if(p->next) p->next->prev=p->prev;
	if(p->prev) p->prev->next=p->next;
	
	// remove from the sibling list
	if(p->nextsibling) p->nextsibling->prevsibling=p->prevsibling;
	if(p->prevsibling) p->prevsibling->nextsibling=p->nextsibling;
	else p->parent->firstchild=p->nextsibling;
	
	// children processes are adopted by pid 1
	struct process* init_process=idle_process.next;
	for(struct process* child=p->firstchild;child;child=child->nextsibling)
	{
		child->parent=init_process;
		child->nextsibling=init_process->firstchild;
		child->prevsibling=NULL;
		if(init_process->firstchild) init_process->firstchild->prevsibling=child;
		init_process->firstchild=child;
	}
	// deallocate stack and address space
	virtmem_release_heap_pages(p->kernel_stack,PAGE_SIZE);
	// virtmem_delete_address_space(p->cr3);
}

static struct process* get_first_zombie_child(void)
{
	for(struct process* p=cur_process->firstchild;p;p=p->nextsibling)
	{
		if(p->status==PROC_ZOMBIE) return p;
	}
	return NULL;
}

int sys_waitchild(pid_t pid,int* status,int flags)
{
	// TODO: check wstatus pointer is valid

	// we don't support process groups
	if(pid<-1) return -ECHILD;
	if(pid==0) return -ECHILD;

	if(cur_process->firstchild==NULL) return -ECHILD;
	
	struct process* child=NULL;
	if(pid>0)
	{
		child=process_find(pid);
		if(!child) return -ECHILD;
		if(child->parent!=cur_process) return -ECHILD;
	}
	
	while(1)
	{
		// check for child termination
		if(child && child->status==PROC_ZOMBIE) break;
		else
		{
			struct process* first_zombie=get_first_zombie_child();
			if(first_zombie)
			{
				child=first_zombie;
				break;
			}
		}
		process_yield(PROC_WAITSIG);
	}
	pid_t child_pid=child->pid;
	*status=cur_process->last_siginfo.si_status;
	process_deallocate(child);
	return child_pid;
}
