/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */
 
#include <scheduler/process.h>

#include <errno.h>
#include <sunflower/procinfo.h>

struct process* process_find(pid_t pid)
{
	if(pid==cur_process->pid) return cur_process;
	else if(pid>cur_process->pid)
	{
		struct process* p=cur_process->next;
		while(p)
		{
			if(pid==p->pid) return p;
			p=p->next;
		}
	}
	else
	{
		struct process* p=cur_process->prev;
		while(p)
		{
			if(pid==p->pid) return p;
			p=p->prev;
		}
	}
	return NULL;
}

pid_t sys_getpid(void)
{
	return cur_process->pid;
}

int sys_getprocinfo(pid_t pid,struct procinfo* info)
{
	const struct process* p;
	if(pid==0) p=cur_process;
	else
	{
		p=process_find(pid);
		if(p==NULL) return -ESRCH;
	}
	
	info->pid=p->pid;
	info->ppid=p->parent->pid;
	info->uid=p->uid;
	info->euid=p->euid;
	info->gid=p->gid;
	info->egid=p->egid;

	if(p->next!=NULL) info->next=p->next->pid;
	else info->next=0;
	return 0;
}

int sys_setuid(uid_t uid)
{
	if(cur_process->uid==0)
	{
		cur_process->uid=uid;
		cur_process->euid=uid;
		return 0;
	}
	else if(uid==cur_process->uid)
	{
		cur_process->euid=uid;
		return 0;
	}
	else return -EPERM;
}

int sys_setgid(gid_t gid)
{
	if(cur_process->gid==0)
	{
		cur_process->gid=gid;
		cur_process->egid=gid;
		return 0;
	}
	else if(gid==cur_process->gid)
	{
		cur_process->egid=gid;
		return 0;
	}
	else return -EPERM;
}
