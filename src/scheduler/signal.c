/*
 * Copyright 2020-2022,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <scheduler/procinfo.h>
#include <scheduler/signal.h>

#include <errno.h>

sighandler_t default_sighandlers[NSIG+1]=
{
	[SIGABRT]	=SIG_TRM,
	[SIGALRM]	=SIG_TRM,
	[SIGBUS]	=SIG_TRM,
	[SIGCHLD]	=SIG_IGN,
	[SIGCONT]	=SIG_IGN,
	[SIGFPE]	=SIG_TRM,
	[SIGHUP]	=SIG_TRM,
	[SIGILL]	=SIG_TRM,
	[SIGINT]	=SIG_TRM,
	[SIGKILL]	=SIG_TRM,
	[SIGPIPE]	=SIG_TRM,
	[SIGQUIT]	=SIG_TRM,
	[SIGSEGV]	=SIG_TRM,
	[SIGSTOP]	=SIG_IGN,
	[SIGTERM]	=SIG_TRM,
	[SIGTSTP]	=SIG_IGN,
	[SIGTTIN]	=SIG_IGN,
	[SIGTTOU]	=SIG_IGN,
	[SIGUSR1]	=SIG_TRM,
	[SIGUSR2]	=SIG_TRM,
	[SIGPOLL]	=SIG_TRM,
	[SIGPROF]	=SIG_TRM,
	[SIGSYS]	=SIG_TRM,
	[SIGTRAP]	=SIG_TRM,
	[SIGURG]	=SIG_IGN,
	[SIGVTALRM]=SIG_TRM,
	[SIGXCPU]	=SIG_TRM,
	[SIGXFSZ]	=SIG_TRM,
};

int sys_sigaction(int signum,const struct sigaction* sa,struct sigaction* old_sa)
{
	// TODO: check pointers are valid
	// TODO: return old sigaction in old_sa
	if(signum<1||signum>NSIG) return -EINVAL;
	
	if(sa==NULL) return 0;
	if(sa->sa_handler==SIG_DFL)
	{
		cur_process->handlers[signum]=default_sighandlers[signum];
		return 0;
	}
	if((signum==SIGABRT)||(signum==SIGBUS)||(signum==SIGKILL)||(signum==SIGSEGV)) return -EINVAL;
	cur_process->handlers[signum]=sa->sa_handler;
	return 0;
}

int sys_sigmask(int how,const sigset_t* set,sigset_t* oldset)
{
	if(oldset)
	{
		// TODO: check oldset is valid pointer
		*oldset=cur_process->sigmask;
	}
	if(set==NULL) return 0;
	// TODO: check set is valid pointer
	switch(how)
	{
	case SIG_SETMASK:
		cur_process->sigmask=*set;
		return 0;
	case SIG_BLOCK:
		cur_process->sigmask|=*set;
		return 0;
	case SIG_UNBLOCK:
		cur_process->sigmask&=~*set;
		return 0;
	default:
		return -EINVAL;
	}
}

// send a signal to a particular process
int process_send_signal(struct process* p,int signum,int status)
{
	p->last_siginfo.si_signo=signum;
	p->last_siginfo.si_pid=cur_process->pid;
	p->last_siginfo.si_uid=cur_process->uid;
	p->last_siginfo.si_status=status;
	p->pending|=((sigset_t)1<<signum);

	// if the process was waiting for a signal awaken it
	if(p->status==PROC_WAITSIG) p->status=PROC_RUNNABLE;
	return 0;
}

int sys_sigsend(pid_t pid,int signum)
{
	if(signum<1||signum>31) return -EINVAL;
	if((signum==SIGBUS)||(signum==SIGCHLD)||(signum==SIGSEGV)) return -EPERM;

	if(pid>0)
	{
		struct process* receiver=process_find(pid);
		if(receiver==NULL) return -ESRCH;
		return process_send_signal(receiver,signum,0);
	}
	return -EINVAL;
}

int sys_sigpending(sigset_t* set)
{
	// TODO: check pointer is valid
	*set=cur_process->pending;
	return 0;
}

int sys_sigwait(const sigset_t* restrict set,siginfo_t* restrict info)
{
	// TODO: check pointers are valid

	// sleep until signal pending
	if((*set&cur_process->pending)==0) process_yield(PROC_WAITSIG);

	sigset_t s=*set&cur_process->pending;
	for(unsigned i=0;i<NSIG;i++)
	{
		sigset_t n=1<<i;
		if(s&n)
		{
			cur_process->pending&=~n;
			if(info)
			{
				*info=cur_process->last_siginfo;
			}
			return i;
		}
	}
	return -EINTR;
}

// NOTE: this function runs last before returning to user-space,
// so if the process was running signal_wait it will already have cleared a pending signal
void process_handle_signals(void)
{
	// for each signal check if pending and if not blocked
	for(unsigned i=1;i<NSIG;i++)
	{
		sigset_t cursig=1<<i;
		if((cur_process->pending&cursig)&&!(cur_process->sigmask&cursig))
		{
			if(cur_process->handlers[i]==SIG_TRM)
			{
				klog("process %d killed by signal %d",cur_process->pid,i);
				// the signal causes the process to terminate
				process_exit((i<<8)|0xff);
			}
			// TODO: actually execute the signal
			// clean the signal
			cur_process->pending&=~cursig;
		}
	}
}
