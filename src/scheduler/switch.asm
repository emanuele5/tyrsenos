bits 64

extern glob_tss
extern cur_process

global process_switch
; rdi = pointer to next task to switch to
process_switch:
	pushf
	cli							;no interrupts allowed during the switch
	push rbx
	push rbp
	push r12
	push r13
	push r14
	push r15

	mov rax,[rdi+8]
	add rax,0x1000				;rax = next kernel stack bottom
	mov rdx,glob_tss+4
	mov [rdx],rax 				;load it in the tss
	
	mov rdx,cur_process
	mov rsi,[rdx]				;rsi = running process pointer
	mov [rsi+16],rsp			;save current rsp of running process
	mov rsp,[rdi+16]			;load new rsp from next process

	;we are now in next process context

	mov [rdx],rdi				;next process is now the running one
	mov dword[rdi+24],0			;set status PROC_RUNNING

	mov rax,[rdi]				;rax = next cr3
	mov cr3,rax 				;load next memory mapping
	
	pop r15
	pop r14
	pop r13
	pop r12
	pop rbp
	pop rbx
	popf
	ret
