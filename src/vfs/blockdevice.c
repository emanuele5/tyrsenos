/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <vfs/blockdevice.h>
#include <vfs/descriptor.h>
#include <vfs/extfs.h>
#include <vfs/minixfs.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <limits.h>
#include <sys/stat.h>

struct partition_entry
{
	uint8_t attr;
	uint8_t start_chs[3];
	uint8_t type;
	uint8_t end_chs[3];
	uint32_t start;
	uint32_t size;
}__attribute__((packed));

// all filesystems must be registered here
static const struct filesystem_driver filesystems[]=
{
	{minixfs_mount,NULL,minixfs_syncfs,minixfs_read_inode,"minix"},
	{extfs_mount,NULL,NULL,extfs_read_inode,"ext"},
};

static const unsigned FILESYSTEMS_NUM=sizeof(filesystems)/sizeof(struct filesystem_driver);

static void try_mount(struct vfs_inode* device,const char* devname)
{
	for(unsigned i=0;i<FILESYSTEMS_NUM;i++)
	{
		static char name[NAME_MAX];
		memcpy(name,devname,10);
		struct mounted_filesystem* fs=filesystems[i].mount(device,name);
		if(fs!=NULL)
		{
			fs->device=device;
			fs->driver=filesystems+i;
			if(mounted_fs_list!=NULL) mounted_fs_list->prev=fs;
			mounted_fs_list=fs;
			
			inode_cache_insert(fs->root);
			
			klog("blockdevice: mounted /dev/%s as %s on /%s",devname,filesystems[i].name,name);

			ramfs_mount(fs->root,name);
			return;
		}
	}
}

struct vfs_inode* blockdevice_register(off_t size,dev_t ndev,void* data,const struct inode_functions* funcs)
{
	static char disk_count=0;

	char name[10]="storage";
	name[7]=disk_count+'a';
	disk_count++;
	
	struct vfs_inode* device=ramfs_register_device(name,S_IFBLK|S_IWUSR|S_IRUSR|S_IWGRP|S_IRGRP,size,ndev,data,funcs);

	try_mount(device,name);
	
	/*struct partition_entry partition_table[4];
	struct file_descriptor f=
	{
		.inode=device,
		.pos=446,
	};
	int err=device->obj_funcs->read(&f,partition_table,sizeof(struct partition_entry[4]));
	if(err<446) return device;
	
	for(unsigned i=0;i<4;i++)
	{
		// TODO: handle partitions
	}*/
	return device;
}
