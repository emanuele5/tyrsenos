/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <scheduler/process.h>
#include <vfs/console.h>
#include <vfs/descriptor.h>
#include <vfs/generic.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sunflower/termios.h>

static const dev_t TTY_DEVNUM=0x0600;
static const dev_t CONSOLE_DEVNUM=0x0500;

//
// console
//

static const struct inode_functions console_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=generic_free,
	.read=generic_read_enxio,
	.write=console_write,
	.ioctl=generic_ioctl_enotty,
};

struct vfs_inode console_inode=
{
	.num=CONSOLE_NUM,
	.dev=RAMFS_DEVNUM,
	.ndev=CONSOLE_DEVNUM,
	.inode_funcs=&console_functions,
};

static void* con_data=NULL;
static ssize_t(*con_write_function)(void*,const void*,size_t)=NULL;

ssize_t console_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	if(con_write_function==NULL) return -ENXIO;
	else return con_write_function(con_data,buf,count);
}

void console_register_output(ssize_t(*func)(void*,const void*,size_t),void* data)
{
	con_data=data;
	con_write_function=func;
}

//
// tty
//

static ssize_t tty_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags);
static ssize_t tty_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags);
static int tty_ioctl(struct vfs_inode* inode,unsigned cmd,unsigned long data);
static int tty_free(struct vfs_inode* inode);

static const struct termios default_termios=
{
	.c_iflag=0,
	.c_oflag=0,
	.c_cflag=CS8|CREAD,
	.c_lflag=ECHO|ECHOE|ECHONL|ICANON,
	.c_cc=
	{
		[VEOF]=4,
		[VEOL]=0,
		[VERASE]='\b',
		[VINTR]=3,
		[VKILL]=21,
		[VMIN]=0,
		[VQUIT]=28,
		[VSTART]=17,
		[VSTOP]=19,
		[VSUSP]=26,
		[VTIME]=0,
	},
};

static const struct inode_functions tty_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=tty_free,
	.read=tty_read,
	.write=tty_write,
	.ioctl=tty_ioctl,
};

static unsigned tty_count=0;
static struct tty_data** ttys=NULL;

static struct tty_data* tty_create(void)
{
	// allocate and initialize tty data
	struct tty_data* tty=kmalloc(sizeof(struct tty_data));
	memcpy(&(tty->termios),&default_termios,sizeof(struct termios));
	tty->hasinput=false;
	ringbuffer_create(&tty->rb);
	lock_create(&tty->waiting);
	tty->waiting_count=0;
	tty->write_data=NULL;
	tty->write_function=NULL;
	// get the name for the new tty
	char name[6]="tty";
	ksnprintf(name+3,2,"%02d",tty_count);
	// register tty device
	dev_t ndev=TTY_DEVNUM|tty_count++;
	ramfs_register_device(name,S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,0,ndev,tty,&tty_functions);
	return tty;
}

static int tty_free(struct vfs_inode* inode)
{
	struct tty_data* tty=inode->inode_data;
	ringbuffer_destroy(&tty->rb);
	kfree(tty);
	return 0;
}

void tty_register_output(ssize_t(*func)(void*,const void*,size_t),void* data)
{
	unsigned i;
	for(i=0;i<tty_count;i++)
	{
		if(ttys[i]->write_function==NULL)
		{
			ttys[i]->write_function=func;
			ttys[i]->write_data=data;
			return;
		}
	}
	ttys=krealloc(ttys,(tty_count+1)*sizeof(struct tty_data*));
	ttys[i]=tty_create();
	ttys[i]->write_function=func;
	ttys[i]->write_data=data;
}

struct tty_data* tty_register_input(void)
{
	unsigned i;
	for(i=0;i<tty_count;i++)
	{
		if(!ttys[i]->hasinput)
		{
			ttys[i]->hasinput=true;
			return ttys[i];
		}
	}
	ttys=krealloc(ttys,(tty_count+1)*sizeof(struct tty_data*));
	ttys[i]=tty_create();
	ttys[i]->hasinput=1;
	return ttys[i];
}

// insert a character in the input buffer
void tty_put(struct tty_data* tty,char ch)
{
	// convert NL to CR if INLCR is set
	if(tty->termios.c_iflag&INLCR && ch=='\n') ch='\r';
	// ignore CR if IGNCR is set
	if(tty->termios.c_iflag&IGNCR && ch=='\r') return;
	// convert CR to NL if ICRNL is set
	if(tty->termios.c_iflag&ICRNL && ch=='\r') ch='\n';
	
	if(tty->termios.c_lflag&ICANON)
	{
		if(ch==tty->termios.c_cc[VEOF])
		{
			// end of reading for waiting process
			if(!lock_isfree(&tty->waiting))
			{
				process_yieldto(tty->waiting.val,PROC_RUNNABLE);
			}
			return;
		}
		else if(ch==tty->termios.c_cc[VERASE])
		{
			// handle backspace in canonic mode only if not at the beginning of the line
			if(tty->rb.count>0)
			{
				ringbuffer_pop(&tty->rb,1);
				if(tty->termios.c_lflag&ECHO)
				{
					tty->write_function(tty->write_data,&ch,1);
					if(tty->termios.c_lflag&ECHOE)
					{
						// error correcting backspace enabled
						tty->write_function(tty->write_data," ",1);
						tty->write_function(tty->write_data,&ch,1);
					}
				}
			}
			return;
		}
		else if((ch==tty->termios.c_cc[VEOL])||(ch=='\n'))
		{
			ringbuffer_write(&tty->rb,&ch,1);
			if(tty->termios.c_lflag&ECHONL) tty->write_function(tty->write_data,&ch,1);

			// a new line can be dispatched to a waiting process
			if(!lock_isfree(&tty->waiting))
			{
				process_yieldto(tty->waiting.val,PROC_RUNNABLE);
			}
			return;
		}
	}

	// normal characters handling
	ringbuffer_write(&tty->rb,&ch,1);
	if(tty->termios.c_lflag&ECHO) tty->write_function(tty->write_data,&ch,1);

	if(!(tty->termios.c_lflag&ICANON))
	{
		// non-canonic mode: awaken the process if enugh data waiting
		if(!lock_isfree(&tty->waiting) && (tty->rb.count >= tty->waiting_count))
		{
			process_yieldto(tty->waiting.val,PROC_RUNNABLE);
		}
	}
}

static ssize_t tty_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	struct tty_data* tty=inode->inode_data;
	lock_waitacquire(&tty->waiting);

	if(tty->termios.c_lflag&ICANON)
	{
		// canonic mode: we read in lines
		size_t line_size=0;
		for(size_t i=0;i<tty->rb.count;i++)
		{
			if(tty->rb.data[(tty->rb.pos+i)%4096]=='\n')
			{
				line_size=i+1;
				break;
			}
		}

		if(line_size==0)
		{
			// the line has not ended
			if(flags&O_NONBLOCK)
			{
				lock_release(&tty->waiting);
				return -EAGAIN;
			}
			else
			{
				// the process sleeps until tty_put receives a newline and awakens it
				process_yield(PROC_WAITING);
			}
		}
		else if(count>line_size) count=line_size;
	}
	else
	{
		// non-canonic mode: VMIN is the minimum read
		if(tty->rb.count < tty->termios.c_cc[VMIN])
		{
			tty->waiting_count=tty->termios.c_cc[VMIN];
			process_yield(PROC_WAITING);
		}
	}

	// copy to user buffer
	lock_release(&tty->waiting);
	return ringbuffer_read(&tty->rb,buf,count);
}

static ssize_t tty_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	struct tty_data* tty=inode->inode_data;
	if(tty->write_function==NULL) return -ENXIO;
	return tty->write_function(tty->write_data,buf,count);
}

static int tty_ioctl(struct vfs_inode* inode,unsigned cmd,unsigned long data)
{
	struct tty_data* tty=inode->inode_data;
	struct termios* user_termios;

	switch(cmd)
	{
	case TCGETATTR:
		user_termios=(void*)data;
		// TODO: check pointer is valid
		*user_termios=tty->termios;
		return 0;
	case TCSETATTR:
		user_termios=(void*)data;
		// TODO: check pointer is valid
		tty->termios=*user_termios;
		return 0;
	default:
		return -EINVAL;
	}
}
