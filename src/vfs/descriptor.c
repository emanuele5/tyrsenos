/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kstring.h>
#include <kernel/time.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>
#include <scheduler/process.h>
#include <vfs/descriptor.h>
#include <vfs/directory.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

//
//	MAIN VFS FUNCTIONS
//

struct file_descriptor* descriptor_get(int fd)
{
	if(fd<0 || fd>=cur_process->file_count) return NULL;
	else return cur_process->files[fd];
}

static struct file_descriptor* descriptor_create(struct vfs_inode* inode,int flags,bool cloexec)
{
	struct file_descriptor* f=kmalloc_type(struct file_descriptor);
	f->inode=inode;
	f->pos=0;
	f->flags=flags;
	f->cloexec=cloexec;
	// don't acquire the inode, assume it's already acquired by the caller
	inode->obj_funcs->open(inode,flags);
	return f;
}

int descriptor_open(struct vfs_inode* inode,int flags,bool cloexec)
{
	if(cur_process->file_count==OPEN_MAX) return -EMFILE;
	// search for free spot in the array
	for(int fd=0;fd<cur_process->file_count;fd++)
	{
		if(cur_process->files[fd]==NULL)
		{
			cur_process->files[fd]=descriptor_create(inode,flags,cloexec);
			return fd;
		}
	}
	// extend the array
	int fd=cur_process->file_count++;
	cur_process->files=krealloc_array(cur_process->files,struct file_descriptor*,cur_process->file_count);
	cur_process->files[fd]=descriptor_create(inode,flags,cloexec);
	return fd;
}

struct file_descriptor* descriptor_copy(const struct file_descriptor* oldf)
{
	struct file_descriptor* newf=kmalloc_type(struct file_descriptor);
	newf->inode=oldf->inode;
	newf->pos=oldf->pos;
	newf->flags=oldf->flags;
	newf->cloexec=oldf->cloexec;
	inode_acquire(newf->inode);
	newf->inode->obj_funcs->open(newf->inode,newf->flags);
	return newf;
}

int sys_open(int dirfd,const char* pathname,int flags,mode_t mode)
{
	kstringview path=kstringview_from_cstring(pathname);
	// get directory
	struct vfs_inode* dir;
	int err=lookup_path(dirfd,path,&dir,LOOKUP_STOPATPARENT);
	if(err!=0) return err;
	
	// extract the filename
	kstringview filename;
	size_t last_separator=kstringview_rsearch(path,'/');
	if(last_separator==~0u) filename=path;
	else filename=kstringview_suffix(path,last_separator+1);
	
	struct vfs_inode* result;
	err=lookup(dir,filename,&result);
	if(err==-ENOENT && flags&O_CREAT)
	{
		// file doesn't exits, but we can create it
		mode_t creatmode=(mode&0777)|S_IFREG;
		err=inode_create(dir,filename.data,creatmode,&result);
		if(err<0) goto error_release_dir;
	}
	else if(err==0)
	{
		// file exists
		if(flags&O_EXCL)
		{
			// and shoudln't
			err=-EEXIST;
			goto error_release_all;
		}
		if(S_ISDIR(result->mode))
		{
			// file is a directory, can't create or write it
			if(flags&O_WRONLY|| ((flags&O_CREAT)&&!(flags&O_DIRECTORY)) )
			{
				err=-EISDIR;
				goto error_release_all;
			}
		}
		else if(flags&O_DIRECTORY)
		{
			// user requested a directory but this ain't it
			err=-ENOTDIR;
			goto error_release_all;
		}
	}
	else goto error_release_dir;
	inode_release(dir);
	
	if(inode_check_permissions(result,flags)==0)
	{
		inode_release(result);
		return -EACCES;
	}
	// handle O_TRUNC
	if((flags&O_TRUNC) && (flags&O_WRONLY)) inode_resize(result,0);
	// handle O_CLOEXEC
	bool cloexec=(flags&O_CLOEXEC);

	int fd=descriptor_open(result,flags,cloexec);
	if(fd<0) inode_release(result);
	return fd;
	
	error_release_all:
	inode_release(result);
	error_release_dir:
	inode_release(dir);
	return err;
}

int sys_close(int fd)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	
	cur_process->files[fd]=NULL;
	f->inode->obj_funcs->close(f->inode,f->flags);
	inode_release(f->inode);
	kfree(f);
	return 0;
}

int sys_ioctl(int fd,unsigned cmd,unsigned long data)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	
	return f->inode->inode_funcs->ioctl(f->inode,cmd,data);
}

int sys_fcntl(int fd,int cmd,int arg)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	
	switch(cmd)
	{
	case F_DUPFD:
		for(int i=arg;i<cur_process->file_count;i++)
		{
			if(cur_process->files[i]==NULL) return sys_dup(fd,i);
		}
		return sys_dup(fd,cur_process->file_count);
		break;
	case F_GETFD:
		return f->cloexec;
	case F_SETFD:
		f->cloexec=arg;
		return 0;
	case F_GETFL:
		return f->flags;
	case F_SETFL:
	default:
		return -EINVAL;
	}
}

int sys_dup(int fd,int newfd)
{
	if(newfd>=OPEN_MAX) return -EBADF;
	const struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;

	// extend the array if necessary
	if(newfd>=cur_process->file_count)
	{
		int old_count=cur_process->file_count;
		cur_process->file_count=newfd+1;
		cur_process->files=krealloc_array(cur_process->files,struct file_descriptor*,cur_process->file_count);
		for(int i=old_count;i<cur_process->file_count;i++) cur_process->files[i]=NULL;
	}

	// close the old file if present
	if(cur_process->files[newfd]!=NULL)
	{
		sys_close(newfd);
	}

	// duplicate
	struct file_descriptor* newf=descriptor_copy(f);
	newf->cloexec=false;
	cur_process->files[newfd]=newf;
	return newfd;
}

static bool is_seekable(const struct file_descriptor* f)
{
	mode_t mode=f->inode->mode;
	return !(S_ISFIFO(mode)||S_ISSOCK(mode));
}

static ssize_t common_user_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	if(count==0) return 0;
	if(!virtmem_range_is_user_writeable(buf,count)) return -EFAULT;
	
	ssize_t ret=inode->obj_funcs->read(inode,pos,buf,count,flags);
	if(ret<0) return ret;
	
	// update inode metadata
	inode->atime=realtime;
	inode->dirty=true;
	return ret;
}

ssize_t sys_read(int fd,void* buf,size_t count)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	if(!(f->flags&O_RDONLY)) return -EBADF;
	
	
	ssize_t ret=common_user_read(f->inode,f->pos,buf,count,f->flags);	
	if(ret<0) return ret;
	f->pos+=ret;
	return ret;
}

ssize_t sys_pread(int fd,void* buf,size_t count,off_t pos)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	if(!(f->flags&O_RDONLY)) return -EBADF;
	
	if(!is_seekable(f)) return -ESPIPE;
	
	return common_user_read(f->inode,pos,buf,count,f->flags);
}

static ssize_t common_user_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	if(count==0) return 0;
	if(!virtmem_range_is_user_readable(buf,count)) return -EFAULT;
	
	ssize_t ret=inode->obj_funcs->write(inode,pos,buf,count,flags);
	if(ret<0) return ret;
	
	// update inode metadata
	inode->mtime=inode->ctime=realtime;
	inode->dirty=true;
	return ret;
}

ssize_t sys_write(int fd,const void* buf,size_t count)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	if(!(f->flags&O_WRONLY)) return -EBADF;
	
	// O_APPEND requires we move to the end of the file
	if(f->flags&O_APPEND) f->pos=f->inode->size;

	ssize_t ret=common_user_write(f->inode,f->pos,buf,count,f->flags);	
	if(ret<0) return ret;
	f->pos+=ret;
	return ret;
}

ssize_t sys_pwrite(int fd,const void* buf,size_t count,off_t pos)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	if(!(f->flags&O_WRONLY)) return -EBADF;
	
	if(!is_seekable(f)) return -ESPIPE;
	
	return common_user_write(f->inode,pos,buf,count,f->flags);
}

off_t sys_seek(int fd,off_t offset,int whence)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	
	// we cannot seek on fifos and sockets
	if(!is_seekable(f)) return -ESPIPE;
	
	switch(whence)
	{
	case SEEK_SET:
		if(offset<0) return -EINVAL;
		f->pos=offset;
		break;
	case SEEK_CUR:
		if(offset>(INT64_MAX-f->pos)) return -EOVERFLOW;
		if(-offset<f->pos) return -EINVAL;
		f->pos+=offset;
		break;
	case SEEK_END:
		if(-offset>(INT64_MAX-f->inode->size)) return -EOVERFLOW;
		if(offset>f->inode->size) return -EINVAL;
		f->pos=f->inode->size-offset;
		break;
	default:
		return -EINVAL;
	}
	return f->pos;
}

int sys_readdir(int fd,struct dirent* buf)
{
	struct file_descriptor* f=descriptor_get(fd);
	if(f==NULL) return -EBADF;
	
	struct vfs_inode* dir=f->inode;
	struct directory_entry* entry;
	
	int err=directory_get_entry(dir,f->pos,&entry);
	if(err<=0) return err;
	
	buf->d_ino=entry->child->num;
	buf->d_off=f->pos;
	buf->d_namelen=strlen(entry->name);
	strcpy(buf->d_name,entry->name);

	f->pos++;
	return 1;
}
