/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <vfs/directory.h>
#include <vfs/generic.h>
#include <errno.h>

#define DIRENTRY_CACHES_NUM	32

static struct directory_entry* directory_cache_search(struct vfs_inode* dir,unsigned index)
{
	struct directory_entry* cache_start=dir->directory_cache[index%DIRENTRY_CACHES_NUM];
	for(struct directory_entry* entry=cache_start;entry!=NULL;entry=entry->next)
	{
		if(entry->index==index) return entry;
	}
	return NULL;
}

static void directory_cache_insert(struct vfs_inode* dir,struct directory_entry* entry)
{
	struct directory_entry** caches=dir->directory_cache;
	unsigned n=entry->index%DIRENTRY_CACHES_NUM;
	entry->next=caches[n];
	entry->prev=NULL;
	if(caches[n]!=NULL) caches[n]->prev=entry;
	caches[n]=entry;
}


// directory operations

static ssize_t directory_write(struct vfs_inode*,off_t,const void*,size_t,int)
{
	return -EISDIR;
}

static ssize_t directory_read(struct vfs_inode*,off_t,void*,size_t,int)
{
	return -EISDIR;
}

int directory_get_entry(struct vfs_inode* dir,unsigned index,struct directory_entry** result)
{
	if(!S_ISDIR(dir->mode)) return -ENOTDIR;

	*result=directory_cache_search(dir,index);
	if(*result==NULL)
	{
		*result=kmalloc(sizeof(struct directory_entry));
		(*result)->index=index;
		int error=dir->inode_funcs->readdir(dir,*result);
		if(error>0) directory_cache_insert(dir,*result);
		else
		{
			// failed reading direntry
			kfree(*result);
			return error;
		}
	}
	return 1;
}

static void directory_cache_free(struct vfs_inode* dir)
{
	struct directory_entry** caches=dir->directory_cache;
	for(unsigned i=0;i<DIRENTRY_CACHES_NUM;i++)
	{
		struct directory_entry* entry=caches[i];
		while(entry)
		{
			struct directory_entry* next=entry->next;
			kfree(entry);
			entry=next;
		}
	}
	kfree(caches);
}

static const struct object_functions directory_functions=
{
	.open=generic_openclose,
	.close=generic_openclose,
	.write=directory_write,
	.read=directory_read,
	.free_cache=directory_cache_free,
	.sync=generic_sync,
};

struct directory_entry* directory_entry_create(struct vfs_inode* dir,const char* name,unsigned index,struct vfs_inode* child)
{
	struct directory_entry* entry=kmalloc(sizeof(struct directory_entry));
	entry->index=index;
	strcpy(entry->name,name);
	entry->child=child;
	inode_acquire(child);
	directory_cache_insert(dir,entry);
	return entry;
}

void directory_inode_init(struct vfs_inode* inode)
{
	size_t cache_size=sizeof(struct directory_entry*)*DIRENTRY_CACHES_NUM;
	inode->directory_cache=kmalloc(cache_size);
	memset(inode->directory_cache,0,cache_size);
	inode->obj_funcs=&directory_functions;
}
