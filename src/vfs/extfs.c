/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <vfs/inode.h>
#include <errno.h>

// on disk structures

enum ext_state
{
	STATUS_CLEAN	=1,
	STATUS_ERROR	=2,
	STATUS_ORPHANS	=4,
};

enum ext_error_handling
{
	ERROR_CONTINUE	=1,
	ERROR_REMOUNT_RO=2,
	ERROR_PANIC		=3,
};

enum ext_creator
{
	OS_LINUX	=0,
	OS_HURD		=1,
	OS_MASIX	=2,
	OS_FREEBSD	=3,
	OS_LITES	=4,
};

enum ext_opt_feature_flags
{
	COMPAT_DIR_PREALLOC		=0X001,
	COMPAT_IMAGIC_INODES	=0x002,
	COMPAT_HAS_JOURNAL		=0x004,
	COMPAT_EXT_ATTR			=0x008,
	COMPAT_RESIZE_INODE		=0x010,
	COMPAT_DIR_INDEX		=0x020,
	COMPAT_LAZY_BG			=0x040,
	COMPAT_EXCLUDE_INODE	=0x080,
	COMPAT_EXCLUDE_BITMAP	=0x100,
	COMPAT_SPARSE_SUPER2	=0x200,
	COMPAT_FAST_COMMIT		=0x400,
};

enum ext_req_feature_flags
{
	INCOMPAT_COMPRESSION	=0x00001,
	INCOMPAT_FILETYPE		=0x00002,
	INCOMPAT_RECOVERY		=0x00004,
	INCOMPAT_JOURNAL_DEV	=0x00008,
	INCOMPAT_META_BG		=0x00010,
	INCOMPAT_EXTENTS		=0x00040,
	INCOMPAT_64BIT			=0x00080,
	INCOMPAT_MMP			=0x00100,
	INCOMPAT_FLEX_BG		=0x00200,
	INCOMPAT_EA_INODE		=0x00400,
	INCOMPAT_DIRDATA		=0x01000,
	INCOMPAT_CSUM_SEED		=0x02000,
	INCOMPAT_LARGEDIR		=0x04000,
	INCOMPAT_INLINE_DATA	=0x08000,
	INCOMPAT_ENCRYPT		=0x10000,
};

enum ext_ro_feature_flags
{
	RO_COMPAT_SPARES_SUPER	=0x00001,
	RO_COMPAT_LARGE_FILE	=0x00002,
	RO_COMPAT_BTREE_DIR		=0x00004,
	RO_COMPAT_HUGE_FILE		=0x00008,
	RO_COMPAT_GDT_CSUM		=0x00010,
	RO_COMPAT_DIR_NLINK		=0x00020,
	RO_COMPAT_EXTRA_ISIZE	=0x00040,
	RO_COMPAT_HAS_SNAPSHOT	=0x00080,
	RO_COMPAT_QUOTA			=0x00100,
	RO_COMPAT_BIGALLOC		=0x00200,
	RO_COMPAT_METADATA_CSUM	=0x00400,
	RO_COMPAT_REPLICA		=0x00800,
	RO_COMPAT_READONLY		=0x01000,
	RO_COMPAT_PROJECT		=0x02000,
	RO_COMPAT_VERITY		=0x08000,
	RO_COMPAT_ORPHAN_PRES	=0x10000,
};

struct ext_superblock
{
	uint32_t inodes_count;
	uint32_t blocks_count_lo;
	uint32_t res_blocks_lo;
	uint32_t free_blocks_lo;
	uint32_t free_inodes;
	uint32_t first_data_block;
	uint32_t log_block_size;
	uint32_t log_cluster_size;
	uint32_t blocks_per_group;
	uint32_t clusters_per_group;
	uint32_t inodes_per_group;
	uint32_t mount_time;
	uint32_t write_time;
	uint16_t mnt_count;
	uint16_t max_mnt_count;
	uint16_t magic;
	uint16_t state;
	uint16_t errors;
	uint16_t minor_rev_level;
	uint32_t lastcheck;
	uint32_t checkinterval;
	uint32_t creator_os;
	uint32_t rev_level;
	uint16_t def_resuid;
	uint16_t def_resgid;
	// extended superblock fields (valid only if rev_level>=1)
	uint32_t first_ino;
	uint16_t inode_size;
	uint16_t block_group_nr;
	uint32_t feature_compat;
	uint32_t feature_incompat;
	uint32_t feature_ro_compat;
	uint8_t uuid[16];
	char volume_name[16];
	char last_mounted[64];
	uint32_t algorithm_usage_bitmap;
	uint8_t prealloc_blocks;
	uint8_t prealloc_dir_blocks;
	uint16_t reserved_gdt_blocks;
	uint8_t journal_uuid[16];
	uint32_t journal_inum;
	uint32_t journal_dev;
	uint32_t last_orphan;
	uint32_t hash_seed[4];
	uint8_t def_hash_version;
	uint8_t jnl_backup_type;
	uint16_t desc_size;
	uint32_t default_mount_opts;
	uint32_t first_meta_bg;
	uint32_t mkfs_time;
	uint32_t jnl_blocks[17];
	uint32_t blocks_count_hi;
	uint32_t res_blocks_hi;
	uint32_t free_blocks_hi;
	uint16_t min_extra_isize;
	uint16_t want_extra_isize;
	uint32_t flags;
	uint16_t raid_stride;
	uint16_t mmp_interval;
	uint64_t mmp_block;
	uint32_t raid_stripe_width;
	uint8_t log_groups_per_flex;
	uint8_t checksum_type;
	uint16_t reserved_pad;
	uint64_t kbytes_written;
	uint32_t snapshot_inum;
	uint32_t snapshot_id;
	uint64_t snapshot_res_blocks;
	uint32_t snapshot_list;
	uint32_t error_count;
	uint32_t first_error_time;
	uint32_t first_error_ino;
	uint64_t first_error_block;
	uint8_t first_error_func[32];
	uint32_t first_error_line;
	uint32_t last_error_time;
	uint32_t last_error_ino;
	uint32_t last_error_line;
	uint64_t last_error_block;
	uint8_t last_error_func[32];
	uint8_t mount_opts[64];
	uint32_t usr_quota_inum;
	uint32_t grp_quota_inum;
	uint32_t overhead_blocks;
	uint32_t backup_bgs[2];
	uint8_t encrypt_algos[4];
	uint8_t encrypt_pw_salt[16];
	uint32_t lpf_ino;
	uint32_t prj_quotas_inum;
	uint32_t checksum_seed;
	uint32_t reserved[98];
	uint32_t checksum;
}__attribute__((packed));

struct ext_blockgroup_descriptor
{
	uint32_t block_bitmap_lo;
	uint32_t inode_bitmap_lo;
	uint32_t inode_table_lo;
	uint16_t free_blocks_lo;
	uint16_t free_inodes_lo;
	uint16_t used_dirs_lo;
	uint16_t flags;
	uint32_t exclude_bitmap_lo;
	uint16_t block_bitmap_csum_lo;
	uint16_t inode_bitmap_csum_lo;
	uint16_t itable_unuses_lo;
	uint16_t checksum;
	// only valid if ext_superblock.desc_size>32
	uint32_t block_bitmap_hi;
	uint32_t inode_bitmap_hi;
	uint32_t inode_table_hi;
	uint16_t free_blocks_hi;
	uint16_t free_inodes_hi;
	uint16_t used_dirs_hi;
	uint16_t itable_unuses_hi;
	uint32_t exclude_bitmap_hi;
	uint16_t block_bitmap_csum_hi;
	uint16_t inode_bitmap_csum_hi;
	uint32_t reserved;
}__attribute__((packed));

struct ext_inode
{
	uint16_t mode;
	uint16_t uid;
	uint32_t size_lo;
	uint32_t atime;
	uint32_t ctime;
	uint32_t mtime;
	uint32_t dtime;
	uint16_t gid;
	uint16_t nlinks;
	uint32_t blocks_lo;
	uint32_t flags;
	uint32_t osd1;
	uint32_t block[15];
	uint32_t generation;
	uint32_t file_acl_lo;
	uint32_t size_hi;
	uint32_t faddr;
	uint16_t osd2[6];
	uint16_t extra_isize;
	uint16_t checksum_hi;
	uint32_t ctime_extra;
	uint32_t mtime_extra;
	uint32_t atime_extra;
	uint32_t crtime;
	uint32_t crtime_extra;
	uint32_t version_hi;
	uint32_t projid;
}__attribute__((packed));

struct ext_direntry
{
	uint32_t inode;
	uint16_t size;
	uint8_t name_len;
	uint8_t type;
	char name[];
}__attribute__((packed));

// virtual file-system structures

struct ext_fs_data
{
	unsigned block_size;
	uint64_t block_count;
	size_t block_group_count;
	struct ext_blockgroup_descriptor* bgdt;
	struct ext_superblock sb;
};

static int ext_readdir(struct vfs_inode* dir,struct directory_entry* result);

static const struct inode_functions ext_inode_functions=
{
	.readdir=ext_readdir,
};

static int ext_do_inode_read(struct vfs_inode* device,const struct ext_fs_data* fs_data,ino_t num,struct vfs_inode** result)
{
	// check inode number is valid
	if(num > fs_data->sb.inodes_count || num==0) return -EINVAL;
	num--;
	
	// get block group of the inode
	uint32_t bg_index=num/fs_data->sb.inodes_per_group;
	if(bg_index>=fs_data->block_group_count) return -EINVAL;
	const struct ext_blockgroup_descriptor* bg=fs_data->bgdt+bg_index;

	uint32_t index=num%fs_data->sb.inodes_per_group;
	uint32_t block=(index*fs_data->sb.inode_size)/fs_data->block_size;
	// TODO: check inode is present in the 
	off_t inode_table_start=(bg->inode_table_lo+((uint64_t)bg->inode_table_hi<<32))*fs_data->block_size;
	off_t inode_pos=inode_table_start+block*fs_data->block_size+index*fs_data->sb.inode_size;

	// read inode data from device
	struct ext_inode disk_ino;
	int err=device->obj_funcs->read(device,inode_pos,&disk_ino,fs_data->sb.inode_size,0);
	if(err<0) return err;

	*result=kmalloc_type(struct vfs_inode);
	// fill vfs inode structure
	(*result)->dev=device->ndev;
	(*result)->num=num+1;
	(*result)->mode=disk_ino.mode;
	(*result)->uid=disk_ino.uid;
	(*result)->gid=disk_ino.gid;
	off_t size=disk_ino.size_lo+((uint64_t)disk_ino.size_hi<<32);
	(*result)->size=size;
	(*result)->links=disk_ino.nlinks;
	(*result)->ndev=0;
	(*result)->atime=(struct timespec){disk_ino.atime,0};
	(*result)->mtime=(struct timespec){disk_ino.mtime,0};
	(*result)->ctime=(struct timespec){disk_ino.ctime,0};
	(*result)->inode_data=NULL;
	(*result)->inode_funcs=&ext_inode_functions;
	return err;
}

//
//	FILESYSTEM METHODS
//

struct mounted_filesystem* extfs_mount(struct vfs_inode* device,char* name)
{
	static const uint16_t EXT_MAGIC=0xef53;
	// read the superblock
	struct ext_superblock sb;
	int err=device->obj_funcs->read(device,1024,&sb,sizeof(struct ext_superblock),0);
	if(err<0) return NULL;
	// check superblock for magic number
	if(sb.magic!=EXT_MAGIC) return NULL;
	
	if(sb.rev_level<1) return NULL; // I have not seen any fs with version 0
	
	unsigned block_size=1024<<sb.log_block_size;
	uint64_t block_count=((uint64_t)sb.blocks_count_hi<<32)+sb.blocks_count_lo;
	// check number of block groups
	size_t bgcount=(sb.inodes_count+sb.inodes_per_group-1)/sb.inodes_per_group;
	size_t bgcount2=(block_count+sb.blocks_per_group-1)/sb.blocks_per_group;
	if(bgcount!=bgcount2) return NULL;

	// read block group descriptor table
	off_t bgdt_pos= (block_size==1024) ? 2048 : block_size;
	struct ext_blockgroup_descriptor* bgdt=kmalloc_array(struct ext_blockgroup_descriptor,bgcount);
	err=device->obj_funcs->read(device,bgdt_pos,bgdt,bgcount*sizeof(struct ext_blockgroup_descriptor),0);
	if(err<0) goto error_dealloc_bgdt;
	
	// initialize filesystem struct
	struct ext_fs_data* fs_data=kmalloc_type(struct ext_fs_data);
	fs_data->block_size=block_size;
	fs_data->block_count=block_count;
	fs_data->block_group_count=bgcount;
	fs_data->bgdt=bgdt;
	fs_data->sb=sb;
	
	// read root inode
	struct vfs_inode* root;
	err=ext_do_inode_read(device,fs_data,2,&root);
	if(err<0)
	{
		kfree(fs_data);
		goto error_dealloc_bgdt;
	}

	// initialize mount point
	struct mounted_filesystem* fs=kmalloc_type(struct mounted_filesystem);
	fs->root=root;
	fs->fs_data=fs_data;
	root->fs=fs;
	return fs;
	
	error_dealloc_bgdt:
	kfree(bgdt);
	return NULL;
}

int extfs_read_inode(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result)
{
	struct vfs_inode* device=fs->device;
	const struct ext_fs_data* fs_data=fs->fs_data;

	int err=ext_do_inode_read(device,fs_data,num,result);
	if(err<0) return err;
	(*result)->fs=fs;
	return err;
}

// directory functions

static int ext_readdir(struct vfs_inode* dir,struct directory_entry* result)
{
	return -ENOENT;
}
