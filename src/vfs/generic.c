#include <vfs/generic.h>

#include <errno.h>

int generic_free(struct vfs_inode*)
{
	return 0;
}

int generic_write_inode(struct vfs_inode*,bool)
{
	return 0;
}

//
//	FILE GENERIC FUNCTIONS
//

ssize_t generic_read_enxio(struct vfs_inode*,off_t,void*,size_t,int)
{
	return -ENXIO;
}

ssize_t generic_write_enxio(struct vfs_inode*,off_t,const void*,size_t,int)
{
	return -ENXIO;
}

int generic_ioctl_enotty(struct vfs_inode*,unsigned,unsigned long)
{
	return -ENOTTY;
}

//
//	OBJECT GENERIC FUNCTIONS
//

void generic_openclose(struct vfs_inode*,int)
{
}

int generic_sync(struct vfs_inode*)
{
	return 0;
}
