/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <kernel/time.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>
#include <scheduler/process.h>
#include <vfs/directory.h>
#include <vfs/inode.h>
#include <vfs/descriptor.h>
#include <vfs/pagecache.h>
#include <vfs/pipe.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <fcntl.h>

struct mounted_filesystem* mounted_fs_list=NULL;

#define INODE_CACHES_NUM 32

static struct vfs_inode* caches[INODE_CACHES_NUM];

static struct vfs_inode* inode_cache_search(const struct mounted_filesystem* fs,ino_t num)
{
	for(struct vfs_inode* ino=caches[num%INODE_CACHES_NUM];ino!=NULL;ino=ino->next)
	{
		if(ino->num==num && ino->fs==fs)
		{
			inode_acquire(ino);
			return ino;
		}
	}
	return NULL;
}

int inode_get(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result)
{
	*result=inode_cache_search(fs,num);
	if(*result) return 1;
	
	int err=fs->driver->read_inode(fs,num,result);
	if(err<0) return err;
	
	inode_cache_insert(*result);
	return err;
}

void inode_cache_insert(struct vfs_inode* inode)
{
	// inode is used once (by the caller)
	inode->refcount=1;
	// initialize per object-type structures
	if(S_ISDIR(inode->mode)) directory_inode_init(inode);
	else if(S_ISCHR(inode->mode)) uncached_inode_init(inode);
	else if(S_ISBLK(inode->mode) || S_ISREG(inode->mode)) cached_inode_init(inode);
	else if(S_ISFIFO(inode->mode)) pipe_inode_init(inode);
	else if(S_ISLNK(inode->mode)) klog("vfs: inode %p: symbolic links currently unsupported",inode);
	else kpanic("vfs: invalid mode on inode %p",inode);
	// insert into cache
	unsigned index=inode->num%INODE_CACHES_NUM;
	inode->next=caches[index];
	inode->prev=NULL;
	if(caches[index]!=NULL) caches[index]->prev=inode;
	caches[index]=inode;
}

void inode_release(struct vfs_inode* inode)
{
	if(inode->refcount==0) kpanic("releasing unused inode %p",inode);
	inode->refcount--;
	// if file is not referenced both in memory and fs, remove from the cache and implicitly from the fs
	if(inode->refcount==0 && inode->links==0)
	{
		// TODO
	}
}

int inode_create(struct vfs_inode* dir,const char* name,mode_t mode,struct vfs_inode** result)
{
	int err=dir->inode_funcs->create(dir,name,mode,result);
	if(err<0) return err;
	struct vfs_inode* child=*result;
	child->uid=cur_process->uid;
	child->gid=cur_process->gid;
	child->size=0;
	struct timespec creation_time=realtime;
	child->atime=creation_time;
	child->mtime=creation_time;
	child->ctime=creation_time;
	child->dirty=true; // inode just created and not written to device
	dir->dirty=true;
	return 0;
}

off_t inode_resize(struct vfs_inode* inode,off_t newsize)
{
	off_t result=inode->inode_funcs->resize(inode,newsize);
	if(result<0) return result;
	
	inode->size=result;
	return result;
}

bool inode_check_permissions(const struct vfs_inode* inode,int openflags)
{
	// exception for the root user, they do whatever they want
	if((cur_process->uid==0)&&!(openflags&O_EXEC)) return true;

	if(cur_process->uid==inode->uid)
	{
		if((openflags&O_RDONLY) && !(inode->mode&S_IRUSR)) return false;
		if((openflags&O_WRONLY) && !(inode->mode&S_IWUSR)) return false;
		if((openflags&O_EXEC) && !(inode->mode&S_IXUSR)) return false;
	}
	else if(cur_process->gid==inode->gid)
	{
		if((openflags&O_RDONLY)&& !(inode->mode&S_IRGRP)) return false;
		if((openflags&O_WRONLY)&& !(inode->mode&S_IWGRP)) return false;
		if((openflags&O_EXEC)&& !(inode->mode&S_IXGRP)) return false;
	}
	else
	{
		if((openflags&O_RDONLY)&& !(inode->mode&S_IROTH)) return false;
		if((openflags&O_WRONLY)&& !(inode->mode&S_IWOTH)) return false;
		if((openflags&O_EXEC)&& !(inode->mode&S_IXOTH)) return false;
	}
	return true;
}

int sys_sync(void)
{
	int err;
	for(unsigned i=0;i<INODE_CACHES_NUM;i++)
	{
		for(struct vfs_inode* inode=caches[i];inode!=NULL;inode=inode->next)
		{
			if(inode->dirty)
			{
				// sync inode metadata if dirty
				err=inode->inode_funcs->write_inode(inode,false);
				if(err<0) return err;
			}
			// sync inode data
			err=inode->obj_funcs->sync(inode);
			if(err<0) return err;
		}
	}
	for(struct mounted_filesystem* fs=mounted_fs_list;fs!=NULL;fs=fs->next)
	{
		// sync fs metadata
		err=fs->driver->sync(fs);
		if(err<0) return err;
		// sync underlying device
		err=fs->device->obj_funcs->sync(fs->device);
		if(err<0) return err;
	}
	return 0;
}

int sys_access(int fd,const char* pathname,int mode,int flags)
{
	struct vfs_inode* access_inode;
	int err=lookup_path(fd,kstringview_from_cstring(pathname),&access_inode,0);
	if(err<0) return err;
	
	bool status=inode_check_permissions(access_inode,mode);
	inode_release(access_inode);
	
	if(status==true) return 0;
	else return -EACCES;
}

int sys_chdir(int fd,const char* pathname,int flags)
{
	struct vfs_inode* newdir_inode;
	if(flags&AT_EMPTY_PATH)
	{
		const struct file_descriptor* f=descriptor_get(fd);
		if(f==NULL) return -EBADF;
		newdir_inode=f->inode;
		inode_acquire(newdir_inode);
	}
	else
	{
		int err=lookup_path(fd,kstringview_from_cstring(pathname),&newdir_inode,0);
		if(err<0) return err;
	}

	if(!S_ISDIR(newdir_inode->mode))
	{
		inode_release(newdir_inode);
		return -ENOTDIR;
	}
	if(!inode_check_permissions(newdir_inode,O_SEARCH))
	{
		inode_release(newdir_inode);
		return -EACCES;
	}
	
	struct vfs_inode* olddir_inode=cur_process->cwd;
	cur_process->cwd=newdir_inode;
	inode_release(olddir_inode);
	return 0;
}

int sys_stat(int fd,const char* pathname,struct stat* buf,int flags)
{
	struct vfs_inode* stat_inode;
	if(flags&AT_EMPTY_PATH)
	{
		const struct file_descriptor* f=descriptor_get(fd);
		if(f==NULL) return -EBADF;
		stat_inode=f->inode;
		inode_acquire(stat_inode);
	}
	else
	{
		int err=lookup_path(fd,kstringview_from_cstring(pathname),&stat_inode,0);
		if(err<0) return err;
	}
	if(!virtmem_range_is_user_writeable(buf,sizeof(struct stat))) return -EFAULT;
	buf->st_dev=stat_inode->dev;
	buf->st_ino=stat_inode->num;
	buf->st_mode=stat_inode->mode;
	buf->st_nlink=stat_inode->links;
	buf->st_uid=stat_inode->uid;
	buf->st_gid=stat_inode->gid;
	buf->st_rdev=stat_inode->ndev;
	buf->st_size=stat_inode->size;
	buf->st_atim=stat_inode->atime;
	buf->st_mtim=stat_inode->mtime;
	buf->st_ctim=stat_inode->ctime;
	buf->st_blksize=PAGE_SIZE;
	buf->st_blocks=(stat_inode->size+511)/512;
	inode_release(stat_inode);
	return 0;
}

int sys_chown(int fd,const char* pathname,uid_t owner,gid_t group,int flags)
{
	struct vfs_inode* chown_inode;
	if(flags&AT_EMPTY_PATH)
	{
		const struct file_descriptor* f=descriptor_get(fd);
		if(f==NULL) return -EBADF;
		chown_inode=f->inode;
		inode_acquire(chown_inode);
	}
	else
	{
		int err=lookup_path(fd,kstringview_from_cstring(pathname),&chown_inode,0);
		if(err<0) return err;
	}
	
	if(cur_process->euid!=chown_inode->uid && cur_process->uid!=0)
	{
		inode_release(chown_inode);
		return -EPERM;
	}
	
	uid_t  new_uid;
	if(owner==(uid_t)-1) new_uid=chown_inode->uid;
	else new_uid=owner;

	gid_t  new_gid;
	if(group==(gid_t)-1) new_gid=chown_inode->gid;
	else new_gid=group;
	
	chown_inode->uid=new_uid;
	chown_inode->gid=new_gid;
	
	chown_inode->dirty=true;
	inode_release(chown_inode);
	return 0;
}

// path search functions

int lookup(struct vfs_inode* dir,kstringview name,struct vfs_inode** result)
{
	for(unsigned i=0;;i++)
	{
		struct directory_entry* entry;
		int err=directory_get_entry(dir,i,&entry);
		if(err==0) return -ENOENT;
		if(err<0) return err;
		
		if(kstringview_compare(name,kstringview_from_cstring(entry->name))==0)
		{
			*result=entry->child;
			if(*result==NULL) return -ENOENT;
			inode_acquire(*result);
			return 0;
		}
	}
}

int lookup_path(int dirfd,kstringview pathname,struct vfs_inode** result,int flags)
{
	struct vfs_inode* dir;
	*result=NULL;

	if(pathname.size==0) return -ENOENT;
	if(pathname.data[0]=='/')
	{
		// absolute path
		dir=&root_inode;
		pathname=kstringview_suffix(pathname,1);
		if(pathname.size==0)
		{
			*result=&root_inode;
			return 0;
		}
	}
	else if(dirfd==AT_FDCWD) dir=cur_process->cwd; // relative path from cwd
	else
	{
		// relative path from dirfd
		struct file_descriptor* dirobj=descriptor_get(dirfd);
		if(dirobj==NULL) return -EBADF;
		dir=dirobj->inode;
	}

	// main cycle
	size_t component_end=kstringview_search(pathname,'/');
	while(component_end!=~0u)
	{
		// extract a name from the path
		kstringview component=kstringview_prefix(pathname,component_end);

		struct vfs_inode* child;
		int err=lookup(dir,component,&child);
		if(err!=0) return err;

		dir=child;
		pathname=kstringview_suffix(pathname,component_end+1);
		component_end=kstringview_search(pathname,'/');
	};
	
	if(flags&LOOKUP_STOPATPARENT)
	{
		// return the last subdirectory
		*result=dir;
		return 0;
	}
	int err=lookup(dir,pathname,result);
	return err;
}
