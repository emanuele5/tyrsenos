/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <kernel/time.h>
#include <memory/kmalloc.h>
#include <scheduler/process.h>
#include <vfs/descriptor.h>
#include <vfs/generic.h>
#include <vfs/inputdevice.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sunflower/input.h>

static ssize_t inputdevice_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags);
static int inputdevice_free(struct vfs_inode* inode);

static const struct inode_functions inputdevice_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=inputdevice_free,
	.read=inputdevice_read,
	.write=generic_write_enxio,
	.ioctl=generic_ioctl_enotty,
};

struct inputdevice_data* inputdevice_create(void)
{
	static const dev_t INPUT_DEVNUM=0x0e00;
	static unsigned input_count=0;
	// create data structure
	struct inputdevice_data* data=kmalloc(sizeof(struct inputdevice_data));
	ringbuffer_create(&data->rb);
	lock_create(&data->waiting);
	data->waiting_count=0;
	
	// calculate file name
	char name[8]="input";
	ksnprintf(name+5,2,"%02d",input_count);
	
	// register in vfs
	dev_t ndev=INPUT_DEVNUM|input_count++;
	ramfs_register_device(name,S_IFCHR|S_IRUSR|S_IRGRP|S_IROTH,0,ndev,data,&inputdevice_functions);
	
	return data;
}

static int inputdevice_free(struct vfs_inode* inode)
{
	struct inputdevice_data* data=inode->inode_data;
	ringbuffer_destroy(&data->rb);
	kfree(data);
	return 0;
}

void inputdevice_putdata(struct inputdevice_data* device,uint32_t type,uint32_t data)
{
	const struct input_event tmp=
	{
		.time=timespec_to_micros(monotonictime),
		.type=type,
		.data=data,
	};

	ringbuffer_write(&device->rb,&tmp,sizeof(struct input_event));

	if(!lock_isfree(&device->waiting) && (device->rb.count > device->waiting_count))
	{
		process_yieldto(device->waiting.val,PROC_RUNNABLE);
	}
}

static ssize_t inputdevice_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	struct inputdevice_data* data=inode->inode_data;
	lock_waitacquire(&data->waiting);

	if(!(flags&O_NONBLOCK) && data->rb.count<count)
	{
		data->waiting_count=count;
		process_yield(PROC_WAITING);
	}

	lock_release(&data->waiting);
	return ringbuffer_read(&data->rb,buf,count);
}
