/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/bitmap.h>
#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>
#include <vfs/directory.h>
#include <vfs/generic.h>
#include <vfs/minixfs.h>

#include <errno.h>
#include <fcntl.h>

// on disk structures

struct minix_superblock
{
	uint16_t n_inodes;
	uint16_t n_zones;
	uint16_t imap_blocks;
	uint16_t zmap_blocks;
	uint16_t first_data_zone;
	uint16_t log_zone_size;
	uint32_t max_size;
	uint16_t magic;
	uint16_t state;
}__attribute__((packed));

struct minix_inode
{
	uint16_t mode;
	uint16_t uid;
	uint32_t size;
	uint32_t time;
	uint8_t gid;
	uint8_t nlinks;
	uint16_t zones[9];
}__attribute__((packed));

struct minix_direntry
{
	uint16_t inode;
	char name[30];
}__attribute__((packed));

// virtual file system structures

struct minix_fs_data
{
	uint8_t* imap;
	uint8_t* zmap;
	struct minix_superblock sb;
};

struct minix_inode_data
{
	uint16_t zones[9];
};

static int minix_write_inode(struct vfs_inode* inode,bool sync);
static int minix_free_inode(struct vfs_inode* inode);
static ssize_t minix_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags);
static ssize_t minix_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags);
static off_t minix_resize(struct vfs_inode* inode,off_t newsize);
static int minix_readdir(struct vfs_inode* inode,struct directory_entry* result);
static int minix_create(struct vfs_inode* dir,const char* name,mode_t mode,struct vfs_inode** result);

static const unsigned MINIX_BLOCK_SIZE=1024;

static const struct inode_functions minix_inode_functions=
{
	.write_inode=minix_write_inode,
	.free_inode=minix_free_inode,
	.read=minix_read,
	.write=minix_write,
	.resize=minix_resize,
	.ioctl=generic_ioctl_enotty,
	.readdir=minix_readdir,
	.create=minix_create,
};

static int minix_do_inode_read(struct vfs_inode* device,const struct minix_fs_data* fs_data,ino_t num,struct vfs_inode** result)
{
	// check inode number is valid
	if(num > fs_data->sb.n_inodes || num==0) return -EINVAL;
	num--;
	// check for inode presence in the inode bitmap
	if(bitmap_isunset(fs_data->imap,num)) return -EINVAL;
	// calculate position on disk
	off_t pos=(2+fs_data->sb.imap_blocks+fs_data->sb.zmap_blocks)*1024+num*32;
	// read inode data from disk
	struct minix_inode disk_ino;
	int err=device->obj_funcs->read(device,pos,&disk_ino,sizeof(struct minix_inode),0);
	if(err<0) return err;
	
	// create data structure
	struct minix_inode_data* data=kmalloc_type(struct minix_inode_data);
	memcpy(data->zones,disk_ino.zones,sizeof(uint16_t[9]));
	// fill vfs_inode structure
	*result=kmalloc_type(struct vfs_inode);
	(*result)->dev=device->ndev;
	(*result)->num=num+1;
	(*result)->mode=disk_ino.mode;
	(*result)->uid=disk_ino.uid;
	(*result)->gid=disk_ino.gid;
	(*result)->size=disk_ino.size;
	(*result)->links=disk_ino.nlinks;
	(*result)->ndev=0;
	(*result)->atime=(struct timespec){disk_ino.time,0};
	(*result)->mtime=(struct timespec){disk_ino.time,0};
	(*result)->ctime=(struct timespec){disk_ino.time,0};
	(*result)->inode_data=data;
	(*result)->inode_funcs=&minix_inode_functions;
	return err;
}

static off_t disk_pos(struct vfs_inode* device,const struct minix_inode_data* ino_data,off_t pos)
{
	unsigned rem=pos%MINIX_BLOCK_SIZE;
	pos/=MINIX_BLOCK_SIZE;

	uint16_t block;
	if(pos<7)
	{
		block=ino_data->zones[pos];
		return block*MINIX_BLOCK_SIZE+rem;
	}
	
	pos-=7;
	if(pos<512)
	{
		off_t dpos=ino_data->zones[7]*MINIX_BLOCK_SIZE+pos*2;
		device->obj_funcs->read(device,dpos,&block,2,0);
		return block*MINIX_BLOCK_SIZE+rem;
	}

	pos-=512;
	uint16_t indirblock;

	off_t dpos=ino_data->zones[8]*MINIX_BLOCK_SIZE+pos/512*2;
	device->obj_funcs->read(device,dpos,&indirblock,2,0);
	dpos=indirblock*MINIX_BLOCK_SIZE+pos%512*2;
	device->obj_funcs->read(device,dpos,&block,2,0);
	return block*1024+rem;
}

static uint16_t allocate_block(const struct minix_fs_data* fs_data)
{
	for(uint16_t b=0;b<fs_data->sb.n_zones;b++)
	{
		if(bitmap_isunset(fs_data->zmap,b))
		{
			bitmap_set(fs_data->zmap,b);
			return b+fs_data->sb.first_data_zone;
		}
	}
	return 0;
}

static void free_block(const struct minix_fs_data* fs_data,uint16_t block)
{
	bitmap_unset(fs_data->zmap,block);
}

//
//	FILESYSTEM METHODS
//

struct mounted_filesystem* minixfs_mount(struct vfs_inode* device,char* name)
{
	static const uint16_t MINIX_MAGIC=0x138f;

	// read the superblock
	struct minix_superblock sb;
	int err=device->obj_funcs->read(device,1024,&sb,sizeof(struct minix_superblock),0);
	if(err<0) return NULL;
	// check superblock for magic number
	if(sb.magic!=MINIX_MAGIC) return NULL;

	uint16_t imap_size=sb.imap_blocks*1024;
	uint16_t zmap_size=sb.zmap_blocks*1024;
	uint8_t* imap=virtmem_get_heap_pages(imap_size);
	uint8_t* zmap=virtmem_get_heap_pages(zmap_size);
	// read inode and zone map
	if((err=device->obj_funcs->read(device,2048,imap,imap_size,0))<0) goto error_dealloc_maps;
	if((err=device->obj_funcs->read(device,2048+imap_size,zmap,zmap_size,0))<0) goto error_dealloc_maps;

	// initialize filesystem struct
	struct minix_fs_data* fs_data=kmalloc_type(struct minix_fs_data);
	fs_data->imap=imap;
	fs_data->zmap=zmap;
	memcpy(&fs_data->sb,&sb,sizeof(struct minix_superblock));
	// read root inode
	struct vfs_inode* root;
	err=minix_do_inode_read(device,fs_data,1,&root);
	if(err<0)
	{
		kfree(fs_data);
		goto error_dealloc_maps;
	}
	// initialize mount point
	struct mounted_filesystem* fs=kmalloc_type(struct mounted_filesystem);
	fs->root=root;
	fs->fs_data=fs_data;
	root->fs=fs;
	return fs;
	
	error_dealloc_maps:
	virtmem_release_heap_pages(imap,imap_size);
	virtmem_release_heap_pages(zmap,zmap_size);
	return NULL;
}

int minixfs_syncfs(struct mounted_filesystem* fs)
{
	const struct minix_fs_data* fs_data=fs->fs_data;
	struct vfs_inode* device=fs->device;
	
	uint16_t imap_size=fs_data->sb.imap_blocks*1024;
	uint16_t zmap_size=fs_data->sb.zmap_blocks*1024;
	// write back inode and zone maps
	int err=device->obj_funcs->write(device,2048,fs_data->imap,imap_size,0);
	if(err<0) return err;
	err=device->obj_funcs->write(device,2048+imap_size,fs_data->zmap,zmap_size,0);
	if(err<0) return err;
	return 0;
}

int minixfs_read_inode(struct mounted_filesystem* fs,ino_t num,struct vfs_inode** result)
{
	struct vfs_inode* device=fs->device;
	const struct minix_fs_data* fs_data=fs->fs_data;

	int err=minix_do_inode_read(device,fs_data,num,result);
	if(err<0) return err;
	(*result)->fs=fs;
	return err;
}

// common functions

static int minix_write_inode(struct vfs_inode* inode,bool sync)
{
	ino_t num=inode->num;
	const struct mounted_filesystem* fs=inode->fs;
	struct vfs_inode* device=fs->device;
	const struct minix_fs_data* fs_data=fs->fs_data;
	const struct minix_inode_data* data=inode->inode_data;
	// check inode number is valid
	if(num > fs_data->sb.n_inodes || num==0) return -EINVAL;
	num--;
	// check for inode presence in the inode bitmap
	if(bitmap_isunset(fs_data->imap,num)) return -EINVAL;
	// populate physical inode
	struct minix_inode ino;
	ino.mode=inode->mode;
	ino.uid=inode->uid;
	ino.size=inode->size;
	ino.time=inode->mtime.tv_sec;
	ino.gid=inode->gid;
	ino.nlinks=inode->links;
	memcpy(ino.zones,data->zones,sizeof(uint16_t[9]));
	// calculate position on disk
	off_t pos=(2+fs_data->sb.imap_blocks+fs_data->sb.zmap_blocks)*1024+num*32;
	// write inode data on disk
	int flags=sync ? O_SYNC : 0;
	int err=device->obj_funcs->write(device,pos,&ino,sizeof(struct minix_inode),flags);
	if(err<0) return err;
	else return 0;
}

static int minix_free_inode(struct vfs_inode* inode)
{
	kfree(inode->inode_data);
	return 0;
}

// file functions

static ssize_t minix_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	const struct minix_inode_data* file_data=inode->inode_data;
	const struct mounted_filesystem* fs=inode->fs;
	struct vfs_inode* device=fs->device;
	
	off_t rem=count;
	uint8_t* bufdata=buf;
	// handle unaligned start
	if(pos%MINIX_BLOCK_SIZE)
	{
		// chose minimum between remaining total and remaining until end of block
		unsigned len=MINIX_BLOCK_SIZE-pos%MINIX_BLOCK_SIZE;
		if(len>rem) len=rem;
		off_t dpos=disk_pos(device,file_data,pos);

		int err=device->obj_funcs->read(device,dpos,bufdata,len,0);
		if(err<0) return err;
		bufdata+=len;
		pos+=len;
		rem-=len;
	}
	while(rem)
	{
		// chose minimum between remaining and block size
		unsigned len=MINIX_BLOCK_SIZE;
		if(len>rem) len=rem;
		off_t dpos=disk_pos(device,file_data,pos);

		int err=device->obj_funcs->read(device,dpos,bufdata,len,0);
		if(err<0) return err;
		bufdata+=len;
		pos+=len;
		rem-=len;
	}
	return count;
}

static ssize_t minix_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	const struct minix_inode_data* file_data=inode->inode_data;
	const struct mounted_filesystem* fs=inode->fs;
	struct vfs_inode* device=fs->device;
	
	off_t rem=count;
	const uint8_t* bufdata=buf;
	// handle unaligned start
	if(pos%MINIX_BLOCK_SIZE)
	{
		// chose minimum between remaining total and remaining until end of block
		unsigned len=MINIX_BLOCK_SIZE-pos%MINIX_BLOCK_SIZE;
		if(len>rem) len=rem;
		off_t dpos=disk_pos(device,file_data,pos);

		int err=device->obj_funcs->write(device,dpos,bufdata,len,0);
		if(err<0) return err;
		bufdata+=len;
		pos+=len;
		rem-=len;
	}
	while(rem)
	{
		// chose minimum between remaining and block size
		unsigned len=MINIX_BLOCK_SIZE;
		if(len>rem) len=rem;
		off_t dpos=disk_pos(device,file_data,pos);

		int err=device->obj_funcs->write(device,dpos,bufdata,len,0);
		if(err<0) return err;
		bufdata+=len;
		pos+=len;
		rem-=len;
	}
	return count;
}

static off_t minix_resize(struct vfs_inode* inode,off_t newsize)
{
	struct minix_inode_data* inode_data=inode->inode_data;
	const struct mounted_filesystem* fs=inode->fs;
	struct vfs_inode* device=fs->device;

	uint16_t startblocks=(inode->size+MINIX_BLOCK_SIZE-1)/MINIX_BLOCK_SIZE;
	uint16_t endblocks=(newsize+MINIX_BLOCK_SIZE-1)/MINIX_BLOCK_SIZE;
	
	if(endblocks<startblocks)
	{
		// shrink
		for(uint16_t i=endblocks;i<startblocks;i++)
		{
			uint16_t block=disk_pos(device,inode_data,i);
			free_block(fs->fs_data,block);
		}
		// TODO: also free the blocks used for indirect and double indirect blocks
	}
	else if(endblocks>startblocks)
	{
		// grow
		for(uint16_t i=startblocks;i<endblocks;i++)
		{
			uint16_t block=allocate_block(fs->fs_data);
			// TODO: check block allocation was successful
			if(i<7)
			{
				inode_data->zones[i]=block;
				continue;
			}
			// TODO: indirect block allocation not implemented
			return -ENOSPC;
		}
	}
	return newsize;
}

// directory functions

static int create_direntry(struct vfs_inode* dir,const char* name,ino_t num)
{
	if(strlen(name)>30) return -ENAMETOOLONG;
	
	off_t pos=0;
	for(;pos<dir->size;pos+=sizeof(struct minix_direntry))
	{
		struct minix_direntry entry;
		int err=minix_read(dir,pos,&entry,sizeof(struct minix_direntry),0);
		// generic error: bail
		if(err<0) return err;
		// direntry is unused: we can use it
		if(entry.inode==0) break;
	}
	if(pos==dir->size)
	{
		// expand directory if necessary
		int err=minix_resize(dir,dir->size+sizeof(struct minix_direntry));
		if(err<0) return err;
		dir->size+=sizeof(struct minix_direntry);
	}
	// write direntry
	struct minix_direntry entry;
	entry.inode=num;
	strncpy(entry.name,name,30);
	return minix_write(dir,pos,&entry,sizeof(struct minix_direntry),0);
}

static int minix_readdir(struct vfs_inode* dir,struct directory_entry* result)
{
	struct mounted_filesystem* fs=dir->fs;
	// check entry is valid
	off_t pos=result->index*sizeof(struct minix_direntry);
	if(pos==dir->size) return 0;
	else if(pos>dir->size) return -ENOENT;

	// read direntry
	struct minix_direntry entry;
	int err=minix_read(dir,pos,&entry,sizeof(struct minix_direntry),0);
	if(err<=0) return err;
	
	err=inode_get(fs,entry.inode,&(result->child));
	if(err<0) return err;

	memcpy(result->name,entry.name,30);
	return err;
}

static int minix_create(struct vfs_inode* dir,const char* name,mode_t mode,struct vfs_inode** result)
{
	struct mounted_filesystem* fs=dir->fs;
	const struct minix_fs_data* fs_data=fs->fs_data;
	// search free inode in the inode map
	uint16_t num=0;
	for(uint16_t i=0;i<fs_data->sb.n_inodes;i++)
	{
		if(bitmap_isunset(fs_data->imap,i))
		{
			bitmap_set(fs_data->imap,i);
			num=i+1;
			break;
		}
	}
	if(num==0) return -ENOSPC;
	
	int err=create_direntry(dir,name,num);
	if(err<0) return err;
	
	struct minix_inode_data* data=kmalloc_type(struct minix_inode_data);
	memset(data->zones,0,sizeof(uint16_t[9]));
	
	*result=kmalloc_type(struct vfs_inode);
	(*result)->dev=fs->device->ndev;
	(*result)->num=num;
	(*result)->mode=mode;
	(*result)->inode_data=data;
	(*result)->fs=fs;
	(*result)->inode_funcs=&minix_inode_functions;
	return 0;
}
