/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kstring.h>
#include <memory/kmalloc.h>
#include <memory/virtual.h>
#include <vfs/generic.h>
#include <vfs/descriptor.h>

#include <errno.h>
#include <fcntl.h>

//
//	UNCACHED FUNCTIONS (for character devices)
//

static ssize_t uncached_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	return inode->inode_funcs->read(inode,pos,buf,count,flags);
}

static ssize_t uncached_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	return inode->inode_funcs->write(inode,pos,buf,count,flags);
}

static void uncached_free(struct vfs_inode* inode)
{
}

static const struct object_functions uncached_file_functions=
{
	.open=generic_openclose,
	.close=generic_openclose,
	.read=uncached_read,
	.write=uncached_write,
	.free_cache=uncached_free,
	.sync=generic_sync,
};

void uncached_inode_init(struct vfs_inode* inode)
{
	inode->obj_funcs=&uncached_file_functions;
}

//
//	PAGE CACHED FUNCTIONS (for block devices and regular files)
//

static const unsigned PAGE_CACHES_NUM=8;

// very ignorant implementation, will need to change it into a trie and also support mmap
struct cached_page
{
	off_t start;
	uint8_t* data;
	struct cached_page* next;
};

static struct cached_page* search_page(const struct vfs_inode* inode,off_t pos)
{
	unsigned i=(pos/PAGE_SIZE)%PAGE_CACHES_NUM;
	for(struct cached_page* page=inode->page_cache[i];page;page=page->next)
	{
		if(page->start==pos) return page;
	}
	return NULL;
}

static ssize_t create_page(struct vfs_inode* inode,off_t pos,struct cached_page** result)
{
	// allocate page
	struct cached_page* page=kmalloc(sizeof(struct cached_page));
	page->data=virtmem_get_heap_pages(PAGE_SIZE);
	// read from device
	ssize_t err=inode->inode_funcs->read(inode,pos,page->data,PAGE_SIZE,0);
	if(err<0)
	{
		// clear all upon fail
		virtmem_release_heap_pages(page->data,PAGE_SIZE);
		kfree(page);
		return err;
	}
	else if(err<PAGE_SIZE)
	{
		//TODO: memset to 0 the remaining space
	}
	// finish the structure
	page->start=pos;
	*result=page;
	// insert into the cache
	unsigned i=(pos/PAGE_SIZE)%PAGE_CACHES_NUM;
	page->next=inode->page_cache[i];
	inode->page_cache[i]=page;
	return 0;
}

static ssize_t get_page(struct vfs_inode* inode,off_t pos,struct cached_page** result)
{
	*result=search_page(inode,pos);
	if(*result==NULL) return create_page(inode,pos,result);
	else return 0;
}

static ssize_t cached_read(struct vfs_inode* inode,off_t pos,void* buf,size_t count,int flags)
{
	if(pos > inode->size) return -EOVERFLOW;
	if(pos+count > inode->size) count = inode->size - pos;
	
	size_t rem=count;
	uint8_t* bufdata=buf;
	unsigned offset=pos%PAGE_SIZE;
	
	if(offset&&rem)
	{
		unsigned len=PAGE_SIZE-offset;
		if(len>rem) len=rem;

		struct cached_page* page;
		int err=get_page(inode,pos-offset,&page);
		if(err<0) return err;

		memcpy(bufdata,page->data+offset,len);
		pos+=len;
		bufdata+=len;
		rem-=len;
	}
	while(rem)
	{
		unsigned len=PAGE_SIZE;
		if(len>rem) len=rem;

		struct cached_page* page;
		int err=get_page(inode,pos,&page);
		if(err<0) return err;

		memcpy(bufdata,page->data,len);
		pos+=len;
		bufdata+=len;
		rem-=len;
	}
	return count;
}

static ssize_t sync_page(struct vfs_inode* inode,struct cached_page* page)
{
	return inode->inode_funcs->write(inode,page->start,page->data,PAGE_SIZE,O_SYNC);
}

static ssize_t cached_write(struct vfs_inode* inode,off_t pos,const void* buf,size_t count,int flags)
{
	// TODO: try to resize

	if(pos+count > inode->size)
	{
		int err=inode_resize(inode,pos+count);
		if(err<0) return err;
		if(err<pos+count)
		{
			if(pos<inode->size) count=inode->size-pos;
			else return -EOVERFLOW;
		}
	}
	
	size_t rem=count;
	const uint8_t* bufdata=buf;
	unsigned offset=pos%PAGE_SIZE;
	
	if(offset&&rem)
	{
		unsigned len=PAGE_SIZE-offset;
		if(len>rem) len=rem;

		struct cached_page* page;
		int err=get_page(inode,pos-offset,&page);
		if(err<0) return err;

		memcpy(page->data+offset,bufdata,len);
		if(flags&O_SYNC) sync_page(inode,page);

		pos+=len;
		bufdata+=len;
		rem-=len;
	}
	while(rem)
	{
		unsigned len=PAGE_SIZE;
		if(len>rem) len=rem;

		struct cached_page* page;
		int err=get_page(inode,pos,&page);
		if(err<0) return err;

		memcpy(page->data,bufdata,len);
		if(flags&O_SYNC) sync_page(inode,page);

		pos+=len;
		bufdata+=len;
		rem-=len;
	}
	return count;
}

static int cached_sync(struct vfs_inode* inode)
{
	struct cached_page** caches=inode->page_cache;
	for(unsigned i=0;i<PAGE_CACHES_NUM;i++)
	{
		struct cached_page* page=caches[i];
		while(page)
		{
			struct cached_page* next=page->next;
			int err=sync_page(inode,page);
			if(err<0) return err;
			page=next;
		}
	}
	return 0;
}

static void cached_free(struct vfs_inode* inode)
{
	struct cached_page** caches=inode->page_cache;
	for(unsigned i=0;i<PAGE_CACHES_NUM;i++)
	{
		struct cached_page* page=caches[i];
		while(page)
		{
			struct cached_page* next=page->next;
			virtmem_release_heap_pages(page->data,PAGE_SIZE);
			kfree(page);
			page=next;
		}
	}
	kfree(caches);
}

static const struct object_functions cached_file_functions=
{
	.open=generic_openclose,
	.close=generic_openclose,
	.read=cached_read,
	.write=cached_write,
	.sync=cached_sync,
	.free_cache=cached_free,
};

void cached_inode_init(struct vfs_inode* inode)
{
	size_t cache_size=sizeof(struct cached_page*[PAGE_CACHES_NUM]);
	inode->page_cache=kmalloc(cache_size);
	memset(inode->page_cache,0,cache_size);
	inode->obj_funcs=&cached_file_functions;
}
