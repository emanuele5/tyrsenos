/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <kernel/time.h>
#include <memory/kmalloc.h>
#include <scheduler/process.h>
#include <scheduler/signal.h>
#include <vfs/descriptor.h>
#include <vfs/generic.h>
#include <vfs/pipe.h>

#include <errno.h>
#include <fcntl.h>

static void pipe_open(struct vfs_inode* inode,int mode)
{
	struct pipe_data* data=inode->pipe_data;
	if(mode&O_RDONLY) data->read_count++;
	if(mode&O_WRONLY) data->write_count++;
}

static void pipe_close(struct vfs_inode* inode,int mode)
{
	struct pipe_data* data=inode->pipe_data;
	if(mode&O_RDONLY)
	{
		data->read_count--;
		// if we are the last reader and there is a writer waiting, wake it up
		if(data->read_count==0 && lock_isbusy(&data->wait_write))
		{
			process_yieldto(data->wait_write.val,PROC_RUNNABLE);
		}
	}
	if(mode&O_WRONLY)
	{
		data->write_count--;
		// if we are the last writer and there is a reader waiting, wake it up
		if(data->write_count==0 && lock_isbusy(&data->wait_read))
		{
			process_yieldto(data->wait_read.val,PROC_RUNNABLE);
		}
	}
}

static ssize_t pipe_read(struct vfs_inode* inode,off_t,void* buf,size_t count,int flags)
{
	struct pipe_data* data=inode->pipe_data;
	lock_waitacquire(&data->wait_read);

	size_t rem=count;
	while(1)
	{
		size_t readable=data->ringbuf.count;
		if(readable>rem) readable=rem;
		rem-=ringbuffer_read(&data->ringbuf,buf,readable);

		// there is no one that can write more, this is all we can give
		if(data->write_count==0) break;
	
		if(rem==0) break;
		// buffer empty, switch to writing process or generally schedule something else
		if(lock_isbusy(&data->wait_write))
		{
			process_yieldto(data->wait_write.val,PROC_WAITING);
		}
		else process_yield(PROC_WAITING);
	}
	// no more to read, release the lock and eventually switch to writing process
	lock_release(&data->wait_read);
	if(lock_isbusy(&data->wait_write))
	{
		process_yieldto(data->wait_write.val,PROC_RUNNABLE);
	}
	return count-rem;
}

static ssize_t pipe_write(struct vfs_inode* inode,off_t,const void* buf,size_t count,int flags)
{
	struct pipe_data* data=inode->pipe_data;
	lock_waitacquire(&data->wait_write);

	size_t rem=count;
	while(1)
	{
		if(data->read_count==0)
		{
			// tried to write to a closed pipe
			lock_release(&data->wait_write);
			process_send_signal(cur_process,SIGPIPE,0);
			return -EPIPE;
		}

		size_t writeable=ringbuffer_get_available(&data->ringbuf);
		if(writeable>rem) writeable=rem;
		rem-=ringbuffer_write(&data->ringbuf,buf,writeable);

		if(rem==0)
		{
			// no more to write, release write lock and eventually switch to reading process
			lock_release(&data->wait_write);
			if(lock_isbusy(&data->wait_read))
			{
				process_yieldto(data->wait_read.val,PROC_RUNNABLE);
			}
			return count;
		}
		// buffer full, switch to reading process or generally schedule something else
		if(lock_isbusy(&data->wait_read))
		{
			process_yieldto(data->wait_read.val,PROC_WAITING);
		}
		else process_yield(PROC_WAITING);
	}
}

static void pipe_free(struct vfs_inode* inode)
{
	struct pipe_data* data=inode->pipe_data;
	ringbuffer_destroy(&data->ringbuf);
	kfree(data);
}

static const struct object_functions pipe_functions=
{
	.open=pipe_open,
	.close=pipe_close,
	.read=pipe_read,
	.write=pipe_write,
	.free_cache=pipe_free,
	.sync=generic_sync,
};

static ino_t pipefs_inonum=0;
static const dev_t PIPEFS_DEVNUM=0x0002;

void pipe_inode_init(struct vfs_inode* inode)
{
	struct pipe_data* data=kmalloc_type(struct pipe_data);
	ringbuffer_create(&data->ringbuf);
	lock_create(&data->wait_read);
	lock_create(&data->wait_write);
	data->read_count=0;
	data->write_count=0;
	inode->pipe_data=data;
	inode->obj_funcs=&pipe_functions;
}

static const struct inode_functions pipefs_functions=
{
	.write_inode=generic_write_inode,
	.free_inode=generic_free,
};

int sys_pipe(int fd[2])
{
	// TODO: check user pointer

	// create pipe inode and insert it into cache to finish initialization (in pipe_inode_init)
	struct vfs_inode* pipe_inode=kmalloc_type(struct vfs_inode);
	pipe_inode->num=pipefs_inonum++;
	pipe_inode->dev=PIPEFS_DEVNUM;
	pipe_inode->mode=S_IFIFO;
	pipe_inode->uid=cur_process->uid;
	pipe_inode->gid=cur_process->gid;
	pipe_inode->size=0;
	pipe_inode->atime=pipe_inode->ctime=pipe_inode->mtime=realtime;
	pipe_inode->links=0;
	pipe_inode->ndev=0;
	pipe_inode->inode_data=NULL;
	pipe_inode->inode_funcs=&pipefs_functions;
	inode_cache_insert(pipe_inode);
	
	// allocate file descriptors
	int read_fd=descriptor_open(pipe_inode,O_RDONLY,false);
	if(read_fd<0) goto error_alloc_read;
	int write_fd=descriptor_open(pipe_inode,O_WRONLY,false);
	if(write_fd<0) goto error_alloc_write;
	inode_acquire(pipe_inode); // descriptor_open will not do it for us
	
	fd[0]=read_fd;
	fd[1]=write_fd;
	return 0;
	
	error_alloc_write:
	sys_close(read_fd);

	error_alloc_read:
	inode_release(pipe_inode);
	return -EMFILE;
}
