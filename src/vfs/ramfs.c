/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <lib/kprintf.h>
#include <memory/kmalloc.h>
#include <vfs/console.h>
#include <vfs/directory.h>
#include <vfs/generic.h>
#include <vfs/ramfs.h>

#include <errno.h>
#include <sys/stat.h>

static int ramfs_readdir(struct vfs_inode* dir,struct directory_entry* entry);

static const struct inode_functions ramfs_rodir_functions=
{
	.write_inode=generic_write_inode,
	.readdir=ramfs_readdir,
};

// basic inodes

static unsigned root_child_count=0;

struct vfs_inode root_inode=
{
	.num=ROOT_NUM,
	.dev=RAMFS_DEVNUM,
	.mode=S_IFDIR|S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH,
	.inode_data=&root_child_count,
	.inode_funcs=&ramfs_rodir_functions,
};

static unsigned dev_child_count=0;

struct vfs_inode dev_inode=
{
	.num=DEV_NUM,
	.dev=RAMFS_DEVNUM,
	.mode=S_IFDIR|S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH,
	.inode_data=&dev_child_count,
	.inode_funcs=&ramfs_rodir_functions,
};

static unsigned tmp_child_count=0;

struct vfs_inode tmp_inode=
{
	.num=TMP_NUM,
	.dev=RAMFS_DEVNUM,
	.mode=S_IFDIR|S_IRWXU|S_IRWXG|S_IRWXO,
	.inode_data=&tmp_child_count,
	.inode_funcs=&ramfs_rodir_functions,
};

//
// /dev/null
//

static const ino_t NULL_DEVNUM=0x0001;

static ssize_t null_read(struct vfs_inode* inode,off_t pos,void* data,size_t len,int flags)
{
	return 0;
}

static ssize_t null_write(struct vfs_inode* inode,off_t pos,const void* data,size_t len,int flags)
{
	return len;
}

static const struct inode_functions null_functions=
{
	.free_inode=generic_free,
	.read=null_read,
	.write=null_write,
};

struct vfs_inode null_inode=
{
	.num=NULL_NUM,
	.dev=RAMFS_DEVNUM,
	.mode=S_IFCHR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH,
	.ndev=NULL_DEVNUM,
	.inode_funcs=&null_functions,
};

// ramfs functions

static ino_t ramfs_inode_num=6;

struct vfs_inode* ramfs_register_device(const char* name,mode_t mode,off_t size,dev_t ndev,void* data,const struct inode_functions* funcs)
{
	struct vfs_inode* device_inode=kmalloc(sizeof(struct vfs_inode));
	device_inode->num=ramfs_inode_num++;
	device_inode->dev=RAMFS_DEVNUM;
	device_inode->mode=mode;
	device_inode->uid=0;
	device_inode->gid=0;
	device_inode->size=size;
	device_inode->atime=(struct timespec){0,0};
	device_inode->mtime=(struct timespec){0,0};
	device_inode->ctime=(struct timespec){0,0};
	device_inode->links=1;
	device_inode->ndev=ndev;
	device_inode->inode_data=data;
	device_inode->inode_funcs=funcs;
	directory_entry_create(&dev_inode,name,dev_child_count++,device_inode);
	inode_cache_insert(device_inode);
	return device_inode;
}

static const kstringview kern_filename=kstringview_from_literal("sunflower.bin");

void ramfs_mount(struct vfs_inode* fs_root,const char* name)
{
	directory_entry_create(&root_inode,name,root_child_count++,fs_root);
	// does the kernel binary exist on the filesystem?
	struct vfs_inode* kernel_bin;
	int err=lookup(fs_root,kern_filename,&kernel_bin);
	if(err>=0)
	{
		directory_entry_create(&root_inode,"boot",root_child_count++,fs_root);
		klog("/%s is boot device",name);
	}
}

void ramfs_init(void)
{
	inode_cache_insert(&root_inode);
	inode_cache_insert(&dev_inode);
	inode_cache_insert(&tmp_inode);
	directory_entry_create(&root_inode,"dev",root_child_count++,&dev_inode);
	directory_entry_create(&root_inode,"tmp",root_child_count++,&tmp_inode);
	directory_entry_create(&root_inode,".",root_child_count++,&root_inode);
	directory_entry_create(&root_inode,"..",root_child_count++,&root_inode);
	directory_entry_create(&dev_inode,"null",dev_child_count++,&null_inode);
	//directory_entry_create(&dev_inode,"tty",dev_child_count++,NULL);
	directory_entry_create(&dev_inode,"console",dev_child_count++,&console_inode);
	directory_entry_create(&dev_inode,".",dev_child_count++,&dev_inode);
	directory_entry_create(&dev_inode,"..",dev_child_count++,&root_inode);
	directory_entry_create(&tmp_inode,".",tmp_child_count++,&tmp_inode);
	directory_entry_create(&tmp_inode,"..",tmp_child_count++,&root_inode);
}

// directory functions
static int ramfs_readdir(struct vfs_inode* dir,struct directory_entry* entry)
{
	const unsigned* n_childs=dir->inode_data;
	if(entry->index==*n_childs) return 0;
	else if(entry->index>*n_childs) return -ENOENT;
	else kpanic("ramfs: attempt to fetch direntry from nonexistent underlying");
}
