/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

int main()
{
	// open the standard streams
	open("/dev/tty00",O_RDONLY);
	open("/dev/tty00",O_WRONLY);
	open("/dev/tty00",O_WRONLY);

	int forkstatus=fork();
	if(forkstatus==0)
	{
		char* argv[]={"dash",NULL};
		char* env[]={NULL};
		execve("/boot/prog/bin/dash",argv,env);
		// this should not be reached
		write(STDERR_FILENO,"INIT ERROR: cannot launch shell",31);
	}
	else if(forkstatus==-1)
	{
		write(STDERR_FILENO,"INIT ERROR: failed to fork\n",27);
	}
	else
	{
		while(1)
		{
			int status=0;
			wait(&status);
		}
	}
	return -1;
}
