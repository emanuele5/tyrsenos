/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * This program is trivial and so is released under public domain
 */

#include <sunflower/syscall.h>
#include <unistd.h>

int main(int argc,char** argv)
{
	sync();
	syscall2(SYS_REBOOT,0x01031999,0);
}
