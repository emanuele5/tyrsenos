/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <stdint.h>

int abs(int x)
{
	if(x<0) return -x;
	else return x;
}

long int labs(long int x)
{
	if(x<0) return -x;
	else return x;
}

long long int llabs(long long int x)
{
	if(x<0) return -x;
	else return x;
}

float fabsf(float x)
{
	// type punning with union
	union
	{
		uint32_t i;
		float f;
	}u;
	u.f=x;
	// force unset sign bit
	u.i&=~(1u<<31);
	return u.f;
}

double fabs(double x)
{
	// type punning with union
	union
	{
		uint64_t i;
		double f;
	}u;
	u.f=x;
	// force unset sign bit
	u.i&=~(1ull<<63);
	return u.f;
}
