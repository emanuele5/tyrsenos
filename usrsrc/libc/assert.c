/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <stdio.h>
#include <stdlib.h>

void __assert(const char* file,int line,const char* func,const char* failedexpr)
{
	fprintf(stderr,"Assertion failed: %s in file %s, function %s, line %d",failedexpr,file,func,line);
	abort();
}
