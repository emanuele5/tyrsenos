.text
.globl _start

_start:
	# set up end of stack for backtrace
	movq $0,%rbp
	pushq %rbp
	pushq %rbp
	movq  %rsp,%rbp
	
	# save rsi and rdi, as they contain argc and argv
	pushq %rsi
	pushq %rdi
	
	call _libc_init
	call _init
	
	# call global constructors
	mov $__init_array_start,%rbx
	.1b:
	cmp $__init_array_end,%rbx
	je .1f
	mov (%rbx),%rax
	call *%rax
	add $8,%rbx
	jmp .1b
	
	.1f:

	# restore argc and argv
	popq %rdi
	popq %rsi
	call main

	# on return from main, call exit
	# exit will then call _fini
	mov %rax,%rdi
	call exit
