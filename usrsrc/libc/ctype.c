/*
 * Copyright 2020-2022,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <ctype.h>

int isalnum(int c)
{
	return (isalpha(c)||isdigit(c));
}

int isalpha(int c)
{
	return (isupper(c)||islower(c));
}

int isascii(int c)
{
	return (c>0 && c<=0x80);
}

int isblank(int c)
{
	switch(c)
	{
	case ' ':
	case '\t':
		return 1;
	default:
		return 0;
	}
}

int iscntrl(int c)
{
	switch(c)
	{
	case '\a':
	case '\b':
	case '\t':
	case '\n':
	case '\v':
	case '\f':
	case '\r':
	case 0:
		return 1;
	default:
		return 0;
	}
}

int isdigit(int c)
{
	if((c<'0')||(c>'9')) return 0;
	else return 1;
}

int isgraph(int c)
{
	if((c<'!')||(c>'~')) return 0;
	else return 1;
}

int islower(int c)
{
	if((c<'a')||(c>'z')) return 0;
	else return 1;
}

int isprint(int c)
{
	return (isgraph(c)||isspace(c));
}

int ispunct(int c)
{
	if(!isgraph(c)) return 0;
	if(isalnum(c)) return 0;
	return 1;
}

int isspace(int c)
{
	switch(c)
	{
	case '\f':
	case '\n':
	case '\r':
	case '\t':
	case '\v':
	case ' ':
		return 1;
	default:
		return 0;
	}
}

int isupper(int c)
{
	if((c<'A')||(c>'Z')) return 0;
	else return 1;
}

int isxdigit(int c)
{
	if(isdigit(c)) return 1;
	else if((c>='a')&&(c<='f')) return 1;
	else if((c>='A')&&(c<='F')) return 1;
	else return 0;
}

int toascii(int c)
{
	return c&0x7f;
}

int tolower(int c)
{
	if(isupper(c)) return c+'a'-'A';
	else return c;
}

int toupper(int c)
{
	if(islower(c)) return c-'a'+'A';
	else return c;
}
