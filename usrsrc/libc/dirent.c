/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>		// for open()
#include <stdlib.h>		// for malloc() and free()
#include <sys/stat.h>	// for fstat()
#include <unistd.h>		// for close() and lseek()
#include <sunflower/syscall.h>

DIR *opendir(const char *dirname)
{
	int fd=open(dirname,O_RDONLY|O_DIRECTORY);
	if(fd==-1) return NULL;

	DIR* ret=malloc(sizeof(DIR));
	ret->fd=fd;
	return ret;
}

DIR* fdopendir(int fd)
{
	// check that file descriptor is open and is a directory
	struct stat statbuf;
	if(fstat(fd,&statbuf)<0) return NULL;
	else if(!S_ISDIR(statbuf.st_mode))
	{
		errno=ENOTDIR;
		return NULL;
	}
	
	DIR* ret=malloc(sizeof(DIR));
	ret->fd=fd;
	return ret;
}

int dirfd(DIR* dirp)
{
	return dirp->fd;
}

struct dirent *readdir(DIR *dirp)
{
	static struct dirent direntbuf;
	int err=syscall2(SYS_READDIR,dirp->fd,(uintptr_t)&direntbuf);
	if(err==0) return NULL; // end of directory, don't set errno
	else if(err<0) // other errors, set errno
	{
		errno=-err;
		return NULL;
	}
	else return &direntbuf;
}

int readdir_r(DIR* restrict dirp,struct dirent* restrict entry,struct dirent** restrict result)
{
	int err=syscall2(SYS_READDIR,dirp->fd,(uintptr_t)entry);
	if(err<=0)
	{
		*result=NULL;
		return err;
	}
	else
	{
		*result=entry;
		return 0;
	}
}

void seekdir(DIR* dirp,long pos)
{
	lseek(dirp->fd,pos,SEEK_SET);
}

long telldir(DIR* dirp)
{
	return lseek(dirp->fd,0,SEEK_CUR);
}

void rewinddir(DIR* dirp)
{
	lseek(dirp->fd,0,SEEK_SET);
}

int closedir(DIR *dirp)
{
	int ret=close(dirp->fd);
	free(dirp);
	return ret;
}
