/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <stdlib.h>

div_t div(int numerator,int denumerator)
{
	return (div_t){numerator/denumerator,numerator%denumerator};
}

ldiv_t ldiv(long numerator,long denumerator)
{
	return (ldiv_t){numerator/denumerator,numerator%denumerator};
}

lldiv_t lldiv(long long numerator,long long denumerator)
{
	return (lldiv_t){numerator/denumerator,numerator%denumerator};
}
