/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <string.h>

char** environ;

char* getenv(const char* name)
{
	size_t len=strlen(name);
	for(size_t i=0;environ[i];i++)
	{
		if(strncmp(name,environ[i],len)==0 && environ[i][len]=='=')
		{
			return environ[i]+len+1;
		}
	}
	return NULL;
}
