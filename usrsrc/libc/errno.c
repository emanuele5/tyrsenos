/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include "errno.h"

#include <errno.h>
#include <stdio.h>	// for stderr and fprintf

#define ERR_MAX 65

static const char* errstr[ERR_MAX]=
{
	"Unknown error",
	"Functionality not supported",
	"No such file or directory",
	"Bad address",
	"Permission denied",
	"Bad file descriptor",
	"No such device or address",
	"Invalid argument",
	"Executable file format error",
	"Not a directory or a symbolic link to a directory",
	"Value too large to be stored in data type",
	"Interrupted function",
	"Operation would block",
	"File exists",
	"Result too large",
	"Not enough space",
	"Operation not permitted",
	"No such process",
	"Inappropriate I/O control operation",
	"Is a directory",
	"Too many levels of symbolic links",
	"Filename too long",
	"No space left on device",
	"Protocol not supported",
	"No such device",
	"Broken pipe",
	"Mathematics argument out of domain of function",
	"Argument list too long",
	"Cross-device link",
	"Device or resource busy",
	"Directory not empty",
	"File too large",
	"Invalid seek",
	"I/O error",
	"No child processes"
	"No buffer space available",
	"Not a socket",
	"Read-only file system",
	"File descriptor value too large",
	"Too many files open in system",
	"Resource deadlock would occur",
	"Network unreachable",
	"Network is down",
	"Too many links",
	"Connection timed out",
	"Protocol wrong type for socket",
	"Message too large",
	"Illegal byte sequence",
	"No locks available",
	"No message of the desired type",
	"The socket is not connected",
	"Operation in progress",
	"Connection aborted by network",
	"Host is unreachable",
	"Connection reset",
	"Connection refused",
	"Connection already in progress",
	"Connection aborted",
	"Address in use",
	"Destination address required",
	"Address family not supported",
	"Address not available",
	"Socket is connected",
	"Not supported",
	"Protocol not available",
};

static int __errno;
int* const __errno_location=&__errno;

long long __errno_update(long long e)
{
	if(e<0)
	{
		errno=-e;
		return -1;
	}
	else return e;
}

char* strerror(int errnum)
{
	if((errnum>=ERR_MAX)||(errnum<=0))
	{
		errno=EINVAL;
		return (char*)errstr[0];
	}
	return (char*)errstr[errnum];
}

void perror(const char* s)
{
	if(s!=NULL) fprintf(stderr,"%s: %s\n",s,strerror(errno));
	else fprintf(stderr,"%s\n",strerror(errno));
}
