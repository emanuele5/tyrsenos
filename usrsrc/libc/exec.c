/*
 * Copyright 2023-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#define _GNU_SOURCE
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sunflower/syscall.h>
#include "errno.h"

extern char** environ;

int execve(const char *path,char *const argv[],char *const envp[])
{
	int ret=syscall5(SYS_EXEC,AT_FDCWD,(uintptr_t)path,(uintptr_t)argv,(uintptr_t)envp,0);
	return __errno_update(ret);
}

int execv(const char* path,char *const argv[])
{
	return execve(path,argv,environ);
}

int fexecve(int fd,char *const argv[],char *const envp[])
{
	int ret=syscall5(SYS_EXEC,fd,(uintptr_t)NULL,(uintptr_t)argv,(uintptr_t)envp,AT_EMPTY_PATH);
	return __errno_update(ret);
}

int execvpe(const char *file,char *const argv[],char *const envp[])
{
	errno=ENOENT;
	// empty filename: error
	if (file[0]==0) return -1;
	// filename contains a path: normal execve
	if(strchr(file, '/')) return execve(file,argv,envp);

	// get environment path or default if it isn't set
	const char* path=getenv("PATH");
	if(path==NULL) path="/boot/prog/bin";

	size_t filelen=strnlen(file,NAME_MAX+1);
	if (filelen>NAME_MAX)
	{
		errno=ENAMETOOLONG;
		return -1;
	}

	// note: if we see a EACCESS it means a suitable executable file exists
	// it's useful to return the EACCESS to the user in this case
	bool seen_eacces=false;
	while(1)
	{
		char buffer[PATH_MAX];

		const char* pathend=strchrnul(path,':');
		size_t pathlen=pathend-path;
	
		if(pathlen+filelen+2>=PATH_MAX)
		{
			errno=ENAMETOOLONG;
			return -1;
		}
		
		// concatenate path and filename
		memcpy(buffer,path,pathlen);
		buffer[pathlen] = '/';
		memcpy(buffer+pathlen+1,file,filelen+1);
		// do exec
		execve(buffer,argv,envp);
		switch(errno)
		{
		case EACCES:
			seen_eacces=true;
		case ENOENT:
		case ENOTDIR:
			break;
		default:
			return -1;
		}
		if(path[pathlen]==0) break;
		else path+=pathlen+1;
	}
	if(seen_eacces) errno=EACCES;
	return -1;
}

int execvp(const char *path,char *const argv[])
{
	return execvpe(path,argv,environ);
}
