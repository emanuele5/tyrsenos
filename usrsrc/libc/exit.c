/*
 * Copyright 2020-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <sunflower/syscall.h>
#include <unistd.h>

//extern void _fini(void);

//extern void(*__fini_array_start)(void);
//extern void(*__fini_array_end)(void);

static struct
{
	void(*func)(void*);
	void* arg;
	void* dso_handle;
}fn_list[32];

static size_t fn_index=0;

// atexit gets a void(*)(void) while __cxa_atexit wants a void(*)(void*)
// so what do we do? make a void(*)(void*) that gets a void(*)(void) as an argument and calls it
static void call_c_func(void* arg)
{
	void(*func)(void)=arg;
	func();
}

int __cxa_atexit(void(*func)(void*),void* arg,void* dso_handle)
{
	if(fn_index>=32) return -1;
	fn_list[fn_index].func=func;
	fn_list[fn_index].arg=arg;
	fn_list[fn_index].dso_handle=dso_handle;
	fn_index++;
	return 0;
}

int atexit(void(*func)(void))
{
	return __cxa_atexit(call_c_func,func,NULL);
}

__attribute__((noreturn)) void exit(int status)
{
	// execute functions registered with atexit in reverse order
	for(int i=fn_index-1;i>=0;i--)
	{
		fn_list[i].func(fn_list[i].arg);
	}
	// call global destructors
	// NOTE: global constructors currently unsupported by the runtime linker
	/*void(**destructor)(void)=&__fini_array_start;
	for(;destructor<&__fini_array_end;destructor++)
	{
		(*destructor)();
	}*/
	//_fini();
	_exit(status);
}

__attribute__((noreturn)) void _exit(int status)
{
	syscall1(SYS_EXIT,status);
	__builtin_unreachable();
}

__attribute__((noreturn)) void _Exit(int status)
{
	syscall1(SYS_EXIT,status);
	__builtin_unreachable();
}
