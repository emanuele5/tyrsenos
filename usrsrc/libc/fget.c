/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <unistd.h>

#include "stdio-internal.h"

int fgetc(FILE* stream)
{
	unsigned char data=0;
	int err=read(stream->fd,&data,sizeof(char));
	if(err==-1)
	{
		stream->error=1;
		return EOF;
	}
	else if(err==0)
	{
		stream->eof=1;
		return EOF;
	}
	else return data;
}

int getc_unlocked(FILE* stream)
{
	return fgetc(stream);
}

int getc(FILE* stream)
{
	// TODO: add locking
	return getc_unlocked(stream);
}

int getchar(void)
{
	return getc(stdin);
}

char* fgets(char* restrict s,int n,FILE* restrict stream)
{
	int i;
	for(i=0;i<n;i++)
	{
		int ch=fgetc(stream);
		if(ch==EOF) return NULL;
		if(ch=='\n')
		{
			break;
		}
		s[i]=ch;
	}
	s[i+1]=0;
	return s;
}
