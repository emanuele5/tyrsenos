/*
 * Copyright 2021 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <sunflower/syscall.h>
#include <sys/types.h>
#include "errno.h"

pid_t fork(void)
{
	pid_t err=syscall0(SYS_FORK);
	return __errno_update(err);
}
