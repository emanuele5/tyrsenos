/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <string.h>	// for strlen()
#include <unistd.h>	// for write()

#include "stdio-internal.h"

int fputc(int c,FILE* stream)
{
	int err=write(stream->fd,&c,sizeof(char));
	if(err==-1) return -1;
	else return c;
}

int putc_unlocked(int c,FILE* stream)
{
	return fputc(c,stream);
}

int putc(int c,FILE* stream)
{
	// TODO: add locking
	return putc_unlocked(c,stream);
}

int putchar(int c)
{
	return putc(c,stdout);
}

int fputs(const char* restrict s,FILE* restrict stream)
{
	size_t size=strlen(s);
	int err=write(stream->fd,s,size);
	if(err==-1) return -1;
	else return 0;
}

int puts(const char* s)
{
	int err=fputs(s,stdout);
	if(err==-1) return err;
	err=fputc('\n',stdout);
	return err;
}
