/*
 * Copyright 2020, 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <unistd.h>	// for lseek()

#include "stdio-internal.h"

int fseek(FILE* stream,long offset,int whence)
{
	off_t res=lseek(stream->fd,offset,whence);
	if(res==-1) return -1;
	else return 0;
}

int fseeko(FILE* stream,off_t offset,int whence)
{
	off_t res=lseek(stream->fd,offset,whence);
	if(res==-1) return -1;
	return 0;
}

int fsetpos(FILE* stream,const fpos_t* pos)
{
	int err=lseek(stream->fd,*pos,SEEK_SET);
	if(err==-1) return -1;
	else return 0;
}

void rewind(FILE* stream)
{
	fseek(stream,0,SEEK_SET);
	stream->eof=false;
	stream->error=false;
}
