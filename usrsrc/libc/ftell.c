/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>	// for EOVERFOLOW
#include <limits.h>	// for LONG_MAX
#include <unistd.h>	// for lseek

#include "stdio-internal.h"

long ftell(FILE* stream)
{
	off_t res=lseek(stream->fd,0,SEEK_CUR);
	if(res>LONG_MAX)
	{
		errno=EOVERFLOW;
		return -1;
	}
	return res;
}

off_t ftello(FILE* stream)
{
	off_t res=lseek(stream->fd,0,SEEK_CUR);
	if(res==-1) return -1;
	return res;
}

int fgetpos(FILE* restrict stream,fpos_t* restrict pos)
{
	*pos=lseek(stream->fd,0,SEEK_CUR);
	if(*pos==-1) return -1;
	else return 0;
}
