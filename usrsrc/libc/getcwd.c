/*
 * Copyright 2021,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <dirent.h>
#include <errno.h>
#include <limits.h>		// for PATH_MAX
#include <string.h>
#include <sys/stat.h>

char* getcwd(char* buf,size_t size)
{
	// check buffer size
	if(size<2)
	{
		if(size==0) errno=EINVAL;
		else errno=ERANGE;
		return NULL;
	}

	size_t pos=size;
	buf[--pos]=0;

	// allocate .. buffer
	char dotbuf[PATH_MAX]=".";

	while(1)
	{
		// stat this directory
		struct stat statbuf;
		int err=stat(dotbuf,&statbuf);
		if(err==-1) return NULL;

		// open parent directory
		strcat(dotbuf,"/.."); // this can be optimized
		DIR* dir=opendir(dotbuf);
		if(dir==NULL) return NULL;

		const struct dirent* entry;
		while(1)
		{
			// search child in parent
			entry=readdir(dir);
			if(entry==NULL)
			{
				if(errno==0) errno=ENOENT; // this shouldn't happen, but just in case...
				closedir(dir);
				return NULL;
			}
			if(entry->d_ino==statbuf.st_ino && strcmp(entry->d_name,".")!=0)
			{
				closedir(dir);
				// if .. is the same as . then we have reached root
				if(strcmp(entry->d_name,"..")==0) goto root_reached;
				break;
			}
		}

		// add path component to the buffer
		size_t namelen=strlen(entry->d_name);
		if(namelen+1>=pos)
		{
			errno=ERANGE;
			return NULL;
		}
		pos-=namelen;
		memcpy(buf+pos,entry->d_name,namelen);
		buf[--pos]='/';
	}
	
	root_reached:
	// handle root
	if(pos==size-1)
	{
		buf="/";
	}
	else
	{
		// copy path to the beginning of the buffer
		memmove(buf,buf+pos,size-pos);
	}
	return buf;
}
