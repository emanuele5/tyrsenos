/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "stdio-internal.h"

ssize_t getdelim(char** restrict lineptr,size_t* restrict n,int delimiter,FILE* restrict stream)
{
	if(lineptr==NULL || n==NULL)
	{
		errno=EINVAL;
		stream->error=true;
		return -1;
	}

	size_t count=0;
	while(1)
	{
		int ch=fgetc(stream);
		if(ch==EOF)
		{
			// error or EOF flags already set by fgetc
			if(count==0) return -1;
			else break;
		}
		
		count++;
		if(count>*n)
		{
			*lineptr=realloc(*lineptr,count);
			(*lineptr)[count-1]=ch;
		}
		if(ch==delimiter) break;
	}
	// normal exit 
	// insert terminator
	*n=count+1;
	*lineptr=realloc(*lineptr,count+1);
	(*lineptr)[count]=0;
	return count;
}

ssize_t getline(char** restrict lineptr,size_t* restrict n,FILE* restrict stream)
{
	return getdelim(lineptr,n,'\n',stream);
}
