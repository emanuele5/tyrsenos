/*
 * Copyright 2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

char* inet_ntoa(struct in_addr in)
{
	static char buf[INET_ADDRSTRLEN];
	const uint8_t* ip4=(const uint8_t*)&in;
	snprintf(buf,INET_ADDRSTRLEN,"%d.%d.%d.%d",ip4[0],ip4[1],ip4[2],ip4[3]);
	return buf;
}

const char* inet_ntop(int af,const void* restrict src,char* restrict dst,socklen_t size)
{
	switch(af)
	{
	case AF_INET:;
		const uint8_t* ip4=src;
		if(snprintf(dst,INET_ADDRSTRLEN,"%d.%d.%d.%d",ip4[0],ip4[1],ip4[2],ip4[3])<size) return dst;
		break;
	case AF_INET6:;
		char buffer[INET6_ADDRSTRLEN];
		const uint16_t* ip6=src;
		if(memcmp("\0\0\0\0\0\0\0\0\0\0\xff\xff",src,12)==0)
		{
			// ipv4 compatible address
			snprintf(buffer,INET6_ADDRSTRLEN,"%x:%x:%x:%x:%x:%x:%d.%d.%d.%d",ip6[0],ip6[1],ip6[2],ip6[3],ip6[4],ip6[5],ip6[6]>>8,ip6[6]&0xff,ip6[7]>>8,ip6[7]&0xff);
		}
		else
		{
			snprintf(buffer,INET6_ADDRSTRLEN,"%x:%x:%x:%x:%x:%x:%x:%x",ip6[0],ip6[1],ip6[2],ip6[3],ip6[4],ip6[5],ip6[6],ip6[7]);
		}
		// TODO: compress the 0s in ::
		return dst;
	default:
		errno=EAFNOSUPPORT;
		return NULL;
	}
	errno=ENOSPC;
	return NULL;
}
