/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <stdio.h>
#include <unistd.h>

extern char** environ;

void _libc_init(int argc,char** argv,int envc,char** envp)
{
	// save environ pointer
	environ=envp;
	// open standard strems
	stdin=fdopen(STDIN_FILENO,"r");
	stdout=fdopen(STDOUT_FILENO,"w");
	stderr=fdopen(STDERR_FILENO,"w");
}
