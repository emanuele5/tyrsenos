/*
 * Copyright 2020, 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include "errno.h"
#include <sunflower/syscall.h>
#include <sunflower/termios.h>	// for TCGETATTR and TCSETATTR
#include <termios.h>			// for struct termios
#include <stdarg.h>

int ioctl(int fd,unsigned cmd,...)
{
	va_list ap;
	va_start(ap,cmd);
	uintptr_t data=va_arg(ap,uintptr_t);
	va_end(ap);

	int ret=syscall3(SYS_IOCTL,fd,cmd,data);
	return __errno_update(ret);
}

int tcgetattr(int fd,struct termios* tc_ptr)
{
	return ioctl(fd,TCGETATTR,tc_ptr);
}

int tcsetattr(int fd,int opt,const struct termios* tc_ptr)
{
	return ioctl(fd,TCSETATTR,tc_ptr);
}

speed_t cfgetispeed(const struct termios *tc_ptr)
{
	return tc_ptr->c_ispeed;
}

speed_t cfgetospeed(const struct termios *tc_ptr)
{
	return tc_ptr->c_ospeed;
}

int cfsetispeed(struct termios* tc_ptr,speed_t speed)
{
	tc_ptr->c_ispeed=speed;
	return 0;
}

int cfsetospeed(struct termios* tc_ptr,speed_t speed)
{
	tc_ptr->c_ospeed=speed;
	return 0;
}

int isatty(int fd)
{
	struct termios dummy_termios;
	int status=tcgetattr(fd,&dummy_termios);
	if(status==0) return 1;
	else return 0;
}
