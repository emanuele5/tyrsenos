/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <string.h>

char* basename(char* path)
{
	if(path==NULL || *path==0) return ".";
	
	size_t i=strlen(path)-1;
	// delete all the '/' at the end
	while(i && path[i]=='/')
	{
		path[i]=0;
		i--;
	}
	// skip until the '/' before or the start of the string
	while(i && path[i-1]!='/') i--;
	return path+i;
}

char* dirname(char* path)
{
	if(path==NULL || *path==0) return ".";
	
	size_t i=strlen(path)-1;
	// skip the end '/'
	while(path[i]=='/')
	{
		if(i==0) return "/"; // name is in form "/"
		i--;
	}
	// skip the file name
	while(path[i]!='/')
	{
		if(i==0) return "."; // name is in form "file/"
		i--;
	}
	// skip any further '/' separating file from directory
	while(path[i]=='/')
	{
		if(i==0) return "/"; // name is in form "/file/"
		i--;
	}
	path[i+1]=0; // name is in in form "dir/file/", truncate it as "dir"
	return path;
}
