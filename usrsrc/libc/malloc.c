/*
 * Copyright 2020-2021,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

// heavily inspired from liballoc but rewritten from scratch

#include <errno.h>		// for ENOMEM
#include <limits.h>		// for PAGE_SIZE
#include <string.h>		// for memcpy
#include <sys/mman.h>	// for mmap

struct block_t;

struct page_t
{
	struct page_t* next;
	struct page_t* prev;
	struct block_t* firstblock;
	size_t size;
	size_t used;
	size_t padding;
};

struct block_t
{
	struct block_t* next;
	struct block_t* prev;
	struct page_t* page;
	size_t size;
};

static union
{
	struct page_t firstpage;
	uint8_t empty[PAGE_SIZE];
}heapstart=
{
	.firstpage=
	{
		.prev=NULL,
		.next=NULL,
		.size=PAGE_SIZE,
		.firstblock=NULL,
		.used=sizeof(struct page_t),
	},
};

static inline size_t round_up(size_t size,unsigned n)
{
	return (size+n-1)&~(n-1);
}

static struct page_t* allocate_page(size_t size)
{
	size+=sizeof(struct page_t)+sizeof(struct block_t);
	size=round_up(size,PAGE_SIZE);

	// expand the heap
	struct page_t* newpage=mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_ANONYMOUS|MAP_PRIVATE,-1,0);
	newpage->prev=NULL;
	newpage->next=NULL;
	newpage->size=size;
	newpage->firstblock=NULL;
	newpage->used=sizeof(struct page_t);
	return newpage;
}

void* malloc(size_t size)
{
	if(size==0) return NULL;
	size=round_up(size,8);

	struct page_t* page=&heapstart.firstpage;
	while(1)
	{
		size_t delta=page->size-page->used;

		// page is big enough
		if(delta>=size+sizeof(struct block_t))
		{
			// page is empty
			if(page->firstblock==NULL)
			{
				struct block_t* block=(struct block_t*)(page+1);
				block->next=page->firstblock;
				block->prev=NULL;
				block->page=page;
				block->size=size;
				page->firstblock=block;
				page->used+=size+sizeof(struct block_t);
				return block+1;
			}

			// there is space at page start
			delta=(uintptr_t)(page->firstblock)-(uintptr_t)(page)-sizeof(struct page_t);
			if(delta>=size+sizeof(struct block_t))
			{
				struct block_t* block=(struct block_t*)(page+1);
				page->firstblock->prev=block;
				block->next=page->firstblock;
				block->prev=NULL;
				block->page=page;
				block->size=size;
				page->firstblock=block;
				page->used+=size+sizeof(struct block_t);
				return block+1;
			}

			// search contiguous space inside the page
			struct block_t* block=page->firstblock;
			while(block)
			{
				if(block->next==NULL) delta=(uintptr_t)(page)+page->size; // last block special case
				else delta=(uintptr_t)(block->next);
				delta-=(uintptr_t)(block)+block->size+sizeof(struct block_t);

				if(delta>=size+sizeof(struct block_t))
				{
					struct block_t* newblock=(struct block_t*)((uintptr_t)(block+1)+block->size);
					newblock->prev=block;
					newblock->next=block->next;
					newblock->size=size;
					newblock->page=page;
					if(block->next!=NULL) block->next->prev=newblock; // last block special case
					block->next=newblock;
					page->used+=size+sizeof(struct block_t);
					return newblock+1;
				}
				block=block->next;
			}
		}
		if(page->next==NULL)
		{
			page->next=allocate_page(size);
			if(page->next==NULL) return NULL;
			page->next->prev=page;
		}
		page=page->next;
	}
}

void free(void *ptr)
{
	if(!ptr) return;
	struct block_t* block=ptr;
	block--;
	struct page_t* page=block->page;
	page->used-=block->size+sizeof(struct block_t);
	if(block->next) block->next->prev=block->prev;
	if(block->prev) block->prev->next=block->next;
	else page->firstblock=block->next;
}

void* realloc(void* ptr,size_t size)
{
	if(size==0) return NULL;
	size=round_up(size,8);

	if(ptr==NULL) return malloc(size);

	struct block_t* block=ptr;
	block--;
	if(block->size>=size)
	{
		block->size=size;
		return ptr;
	}
	else
	{
		void* new=malloc(size);
		memcpy(new,ptr,block->size);
		free(ptr);
		return new;
	}
}

void* calloc(size_t nmemb,size_t size)
{
	if(size>(size_t)-1/nmemb)
	{
		errno=ENOMEM;
		return NULL;
	}

	void* p=malloc(nmemb*size);
	if(p==NULL) return NULL;

	memset(p,0,nmemb*size);
	return p;
}
