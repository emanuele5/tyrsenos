/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <sys/mman.h>
#include <sunflower/syscall.h>

#include "errno.h"

void* mmap(void* addr,size_t length,int prot,int flags,int fd,off_t offset)
{
	intptr_t result=syscall5(SYS_MMAP,(uintptr_t)addr,length,flags|prot,fd,offset);
	if(result<0)
	{
		errno=result;
		return NULL;
	}
	return (void*)result;
}
