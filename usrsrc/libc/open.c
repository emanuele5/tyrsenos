/*
 * Copyright 2023 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */
 
#include <fcntl.h>
#include <sunflower/syscall.h>
#include <stdarg.h>
#include <sys/types.h>
#include "errno.h"

static mode_t __umask=0;

mode_t umask(mode_t cmask)
{
	mode_t oldmask=__umask;
	__umask=cmask;
	return oldmask;
}

int openat(int dirfd, const char *pathname, int flags, ...)
{
	va_list v;
	va_start(v,flags);

	int ret=syscall4(SYS_OPEN,dirfd,(uintptr_t)pathname,flags,va_arg(v,int)&~__umask);
	va_end(v);

	return __errno_update(ret);
}

int open(const char *pathname, int flags, ...)
{
	va_list v;
	va_start(v,flags);
	int ret=openat(AT_FDCWD,pathname,flags,va_arg(v,int));
	va_end(v);
	return ret;
}

int creat(const char* pathname,mode_t mode)
{
	return open(pathname,O_WRONLY|O_CREAT|O_TRUNC,mode);
}

int close(int fd)
{
	int ret=syscall1(SYS_CLOSE,fd);
	return __errno_update(ret);
}
