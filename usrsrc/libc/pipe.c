/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <sunflower/syscall.h>

#include "errno.h"

int pipe(int pipefd[2])
{
	int ret=syscall1(SYS_PIPE,(uintptr_t)pipefd);
	return __errno_update(ret);
}
