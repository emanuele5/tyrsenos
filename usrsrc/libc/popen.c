/*
 * Copyright 2021 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>		// for strcmp()
#include <sys/wait.h>	// for wait()
#include <unistd.h>

FILE* popen(const char* cmd,const char* mode)
{
	// check mode
	bool write_mode;
	if(strcmp(mode,"r")==0) write_mode=false;
	else if(strcmp(mode,"w")==0) write_mode=true;
	else
	{
		errno=EINVAL;
		return NULL;
	}

	// create the pipe
	int pipefd[2];
	if(pipe(pipefd)==-1) return NULL;

	int status=fork();
	if(status==-1)
	{
		// fork failed, close the pipe
		close(pipefd[0]);
		close(pipefd[1]);
		return NULL;
	}

	if(status==0)
	{
		// child code
		// prepare file descriptors)
		if(write_mode)
		{
			close(STDIN_FILENO);
			dup2(pipefd[0],STDIN_FILENO);
			close(pipefd[0]);
		}
		else
		{
			close(STDOUT_FILENO);
			dup2(pipefd[1],STDOUT_FILENO);
			close(pipefd[1]);
		}
		// exec
		char* const args[]={"sh","-c",(char*)cmd,NULL};
		execv("/boot/prog/bin/dash",args);
		_exit(-1);
	}
	else
	{
		// parent code
		if(write_mode)
		{
			close(pipefd[1]);
			return fdopen(pipefd[1],"w");
		}
		else
		{
			close(pipefd[0]);
			return fdopen(pipefd[0],"r");
		}
	}
}

int pclose(FILE* stream)
{
	int status;
	fclose(stream);
	wait(&status);
	return status;
}

int system(const char* cmd)
{
	int status=fork();
	if(status==-1)
	{
		// fork failed
		return -1;
	}
	if(status==0)
	{
		// child code
		// exec
		char* const args[]={"sh","-c",(char*)cmd,NULL};
		execv("/boot/prog/bin/dash",args);
		_exit(-1);
	}
	else
	{
		// parent code
		waitpid(status,&status,0);
		return status;
	}
}
