/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <ctype.h>		// for isdigit
#include <stdbool.h>	// for bool
#include <string.h>		// for strlen()
#include <unistd.h>		// for write()

#include "stdio-internal.h"	// for FILE

enum printf_flags
{
	LEFT	=1,
	SIGNED	=2,
	PLUS	=4,
	SPACE	=8,
	ZEROPAD =16,
	UPPER	=32,
};

enum printf_arglen
{
	CHAR,
	SHORT,
	INT,
	LONG,
	LLONG,
	SIZE,
	PTRDIFF
};

union printf_out_data
{
	FILE* file;
	struct
	{
		char* str;
		size_t count;
	};
};

typedef bool(*printf_putc)(union printf_out_data*,char);

static long long int_get(enum printf_arglen len,va_list ap)
{
	switch(len)
	{
	case CHAR: return (char)va_arg(ap,int);
	case SHORT: return (short)va_arg(ap,int);
	default:
	case INT: return va_arg(ap,int);
	case LONG: return va_arg(ap,long);
	case LLONG: return va_arg(ap,long long);
	case SIZE: return va_arg(ap,size_t);
	case PTRDIFF: return va_arg(ap,ptrdiff_t);
	}
}

// main integer conversion routine
static int int_out(union printf_out_data* out,printf_putc put,enum printf_flags flags,
	unsigned long long n,unsigned base,unsigned width,unsigned precision)
{
	unsigned count=0;
	// select upper/lower case digits
	const char digits_lower[16]="0123456789abcdef";
	const char digits_upper[16]="0123456789ABCDEF";
	const char* digits=(flags&UPPER)?digits_upper:digits_lower;
	// convert signed to unsigned for conversion
	// set sign character if needed
	char sign=0;
	if(flags&SIGNED)
	{
		if(n>LONG_LONG_MAX)
		{
			n=-n;
			sign='-';
		}
		else if(flags&PLUS) sign='+';
		else if(flags&SPACE) sign=' ';
	}
	// main conversion cycle
	char buf[sizeof(long long)*CHAR_BIT]; // enough even for binary conversion
	unsigned ncount=0;
	do
	{
		buf[ncount]=digits[n%base];
		n/=base;
		ncount++;
	}while(n);
	
	unsigned nzero=0;
	if(precision>ncount) nzero=precision-ncount;
	else precision=ncount;

	unsigned npad=0;
	if(width>precision) npad=width-precision;
	
	// pad to width
	char padchar=(flags&ZEROPAD)?'0':' ';
	for(unsigned i=0;i<npad;i++) count+=put(out,padchar);
	// pad to precision with zeros
	for(unsigned i=0;i<nzero;i++) count+=put(out,'0');
	// write sign
	if(sign) count+=put(out,sign);
	// output number
	for(unsigned i=ncount-1;i<ncount;i--) count+=put(out,buf[i]);
	return count;
}

/*
 * main function for formatted output
 * all *printf functions converge here
 */
static int do_printf(union printf_out_data* restrict out,printf_putc put,const char* restrict format,va_list ap)
{
	unsigned count=0;
	while(1)
	{
		char ch=*format++;
		if(ch==0) break;
		// handle normal characters
		if(ch!='%')
		{
			count+=put(out,ch);
			continue;
		}
		ch=*format++;

		// parse flags
		enum printf_flags flags=0;
		while(1)
		{
			if(ch=='\''); // unsupported
			else if(ch=='-') flags|=LEFT;
			else if(ch=='+') flags|=PLUS;
			else if(ch==' ') flags|=SPACE;
			else if(ch=='#'); // unsupported
			else if(ch=='0') flags|=ZEROPAD;
			else break;
			ch=*format++;
		}
		// parse width
		int width=0;
		if(ch=='*')
		{
			width=va_arg(ap,int);
			ch=*format++;
		}
		else while(isdigit(ch))
		{
			width*=10;
			width+=ch-'0';
			ch=*format++;
		}
		if(width<0)
		{
			// negative width = '-' flag and positive width
			flags|=LEFT;
			width=-width;
		}
		// parse precision
		int precision=0;
		if(ch=='.')
		{
			ch=*format++;
			if(ch=='*')
			{
				precision=va_arg(ap,int);
				ch=*format++;
			}
			else while(isdigit(ch))
			{
				precision*=10;
				precision+=ch-'0';
				ch=*format++;
			}
		}
		if(precision<0) precision=0; // negative precision = 0 precision
		// parse length modifier (incomplete)
		enum printf_arglen len=INT;
		if(ch=='l')
		{
			len=LONG;
			ch=*format++;
		}
		else if(ch=='z')
		{
			len=SIZE;
			ch=*format++;
		}
		
		char char_arg;
		const char* str_arg;
		unsigned long long int_arg;
		// parse conversion type
		switch(ch)
		{
		case 'c':
			char_arg=(char)va_arg(ap,int);
			count+=put(out,char_arg);
			break;
		case 's':
			str_arg=va_arg(ap,const char*);
			size_t str_len=strlen(str_arg);
			// output padding if specified
			unsigned pad_len=0;
			if(width && width>str_len) pad_len=width-str_len;
			for(unsigned i=0;i<pad_len;i++) count+=put(out,' ');
			// output string
			for(unsigned i=0;i<str_len;i++) count+=put(out,str_arg[i]);
			break;
		case 'd':
		case 'i':
			flags|=SIGNED;
			// fall-through
		case 'u':
			int_arg=int_get(len,ap);
			count+=int_out(out,put,flags,int_arg,10,width,precision);
			break;
		case 'X':
			flags|=UPPER;
			// fall-through
		case 'x':
			int_arg=int_get(len,ap);
			count+=int_out(out,put,flags,int_arg,16,width,precision);
			break;
		case 'p':
			int_arg=int_get(PTRDIFF,ap);
			count+=int_out(out,put,ZEROPAD,int_arg,16,16,precision);
			break;
		case 'o':
			int_arg=int_get(len,ap);
			count+=int_out(out,put,flags,int_arg,8,width,precision);
			break;
		case '%':
			count+=put(out,'%');
			break;
		}
		if(ch==0) break;
	}
	return count;
}

int fprintf(FILE* restrict stream,const char* restrict format,...)
{
	va_list ap;
	va_start(ap,format);
	int ret=vfprintf(stream,format,ap);
	va_end(ap);
	return ret;
}

int printf(const char* restrict format,...)
{
	va_list ap;
	va_start(ap,format);
	int ret=vfprintf(stdout,format,ap);
	va_end(ap);
	return ret;
}

int snprintf(char* restrict s,size_t n,const char* restrict format,...)
{
	va_list ap;
	va_start(ap,format);
	int ret=vsnprintf(s,n,format,ap);
	va_end(ap);
	return ret;
}

int sprintf(char* restrict s,const char* restrict format,...)
{
	va_list ap;
	va_start(ap,format);
	int ret=vsnprintf(s,INT_MAX,format,ap);
	va_end(ap);
	return ret;
}

static bool file_out(union printf_out_data* out,char ch)
{
	int err=fputc(ch,out->file);
	if(err==EOF) return false;
	else return true;
}

int vfprintf(FILE* restrict stream,const char* restrict format,va_list ap)
{
	union printf_out_data out=
	{
		.file=stream,
	};
	return do_printf(&out,file_out,format,ap);
}

int vprintf(const char* restrict format,va_list ap)
{
	return vfprintf(stdout,format,ap);
}

static bool str_out(union printf_out_data* out,char ch)
{
	if(out->count)
	{
		*out->str=ch;
		out->str++;
		out->count--;
	}
	return true;
}

int vsnprintf(char* restrict s,size_t n,const char* restrict format,va_list ap)
{
	union printf_out_data out=
	{
		.str=s,
		.count=n,
	};
	int count=do_printf(&out,str_out,format,ap);
	if(count<n) s[count]=0;
	return count;
}

int vsprintf(char* restrict s,const char* restrict format,va_list ap)
{
	return vsnprintf(s,INT_MAX,format,ap);
}
