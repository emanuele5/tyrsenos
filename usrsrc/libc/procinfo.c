/*
 * Copyright 2023-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <sunflower/procinfo.h>
#include <sunflower/syscall.h>

#include "errno.h"

static void _get_pinfo(struct procinfo *pinfo)
{
	syscall2(SYS_GETPROCINFO,0,(uintptr_t)pinfo);
}

pid_t getpid(void)
{
	return syscall0(SYS_GETPID);
}

pid_t getppid(void)
{
	struct procinfo pinfo;
	_get_pinfo(&pinfo);
	return pinfo.ppid;
}

uid_t getuid(void)
{
	struct procinfo pinfo;
	_get_pinfo(&pinfo);
	return pinfo.uid;
}

gid_t getgid(void)
{
	struct procinfo pinfo;
	_get_pinfo(&pinfo);
	return pinfo.gid;
}

uid_t geteuid(void)
{
	struct procinfo pinfo;
	_get_pinfo(&pinfo);
	return pinfo.euid;
}

gid_t getegid(void)
{
	struct procinfo pinfo;
	_get_pinfo(&pinfo);
	return pinfo.egid;
}

int setuid(uid_t uid)
{
	int ret=syscall1(SYS_SETUID,uid);
	return __errno_update(ret);
}

int setgid(gid_t gid)
{
	int ret=syscall1(SYS_SETGID,gid);
	return __errno_update(ret);
}
