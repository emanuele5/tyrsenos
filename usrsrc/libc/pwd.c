/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static FILE* pwdfile=NULL;

static char* line=NULL;
static size_t line_len=0;

struct passwd* getpwent(void)
{
	static struct passwd pwent;
	// open users file if not open yet
	if(pwdfile==NULL)
	{
		pwdfile=fopen("/boot/conf/users","rbe");
		if(pwdfile==NULL) return NULL;
	}
	// read line containing user data
	int err=getline(&line,&line_len,pwdfile);
	if(err==-1) return NULL;

	char* ptr=line;
	// get user name
	pwent.pw_name=ptr;
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	*ptr++=0;
	// ignore password
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	ptr++;
	// get uid
	pwent.pw_uid=atoi(ptr);
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	ptr++;
	// get gid
	pwent.pw_gid=atoi(ptr);
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	ptr++;
	// ignore comment
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	ptr++;
	// get directory
	pwent.pw_dir=ptr;
	ptr=strchr(ptr,':');
	if(ptr==NULL) return NULL;
	*ptr++=0;
	// get shell
	pwent.pw_shell=ptr;
	ptr=strchr(ptr,'\n');
	if(ptr==NULL) return NULL;
	*ptr=0;
	return &pwent;
}

void setpwent(void)
{
	if(pwdfile) rewind(pwdfile);
}

void endpwent(void)
{
	if(pwdfile) fclose(pwdfile);
	pwdfile=NULL;

	free(line);
	line=NULL;
	line_len=0;
}

struct passwd* getpwuid(uid_t uid)
{
	setpwent();
	struct passwd* result;
	while((result=getpwent))
	{
		if(result->pw_uid==uid) return result;
	}
	return NULL;
}

struct passwd* getpwnam(const char* name)
{
	setpwent();
	struct passwd* result;
	while((result=getpwent))
	{
		if(strcmp(result->pw_name,name)==0) return result;
	}
	return NULL;
}
