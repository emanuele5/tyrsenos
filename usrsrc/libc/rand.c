/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

// random generator constants taken from the GNU C Library

static long long __randseed;

void srand(unsigned seed)
{
	__randseed=seed;
}

int rand(void)
{
	__randseed=(__randseed*1103515245)+12345;
	return __randseed;
}
