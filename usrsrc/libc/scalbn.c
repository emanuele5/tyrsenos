// code taken from the musl libc, see COPYRIGHT-musl.txt

#include <limits.h>
#include <stdint.h>

float scalbnf(float x, int n)
{
	union
	{
		float f;
		uint32_t i;
	}u;
	float y = x;

	if (n > 127)
	{
		y *= 0x1p127f;
		n -= 127;
		if (n > 127)
		{
			y *= 0x1p127f;
			n -= 127;
			if (n > 127) n = 127;
		}
	}else if (n < -126)
	{
		y *= 0x1p-126f * 0x1p24f;
		n += 126 - 24;
		if (n < -126)
		{
			y *= 0x1p-126f * 0x1p24f;
			n += 126 - 24;
			if (n < -126) n = -126;
		}
	}
	u.i = (uint32_t)(0x7f+n)<<23;
	x = y * u.f;
	return x;
}

double scalbn(double x, int n)
{
	union
	{
		double f;
		uint64_t i;
	}u;
	double y = x;

	if (n > 1023)
	{
		y *= 0x1p1023;
		n -= 1023;
		if (n > 1023)
		{
			y *= 0x1p1023;
			n -= 1023;
			if (n > 1023) n = 1023;
		}
	}else if (n < -1022)
	{
		/* make sure final n < -53 to avoid double
		   rounding in the subnormal range */
		y *= 0x1p-1022 * 0x1p53;
		n += 1022 - 53;
		if (n < -1022)
		{
			y *= 0x1p-1022 * 0x1p53;
			n += 1022 - 53;
			if (n < -1022) n = -1022;
		}
	}
	u.i = (uint64_t)(0x3ff+n)<<52;
	x = y * u.f;
	return x;
}

float scalblnf(float x, long n)
{
	if(n > INT_MAX) n = INT_MAX;
	else if (n < INT_MIN) n = INT_MIN;
	return scalbnf(x,n);
}

double scalbln(double x, long n)
{
	if(n > INT_MAX) n = INT_MAX;
	else if(n < INT_MIN) n = INT_MIN;
	return scalbn(x,n);
}

float ldexpf(float x, int n)
{
	return scalbnf(x,n);
}

double ldexp(double x, int n)
{
	return scalbn(x,n);
}
