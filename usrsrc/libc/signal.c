/*
 * Copyright 2022,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include "errno.h"
#include <signal.h>
#include <sunflower/syscall.h>
#include <unistd.h>				// for getpid()

int sigaction(int sig,const struct sigaction* sa,struct sigaction* old)
{
	int ret=syscall3(SYS_SIGACTION,sig,(uintptr_t)sa,(uintptr_t)old);
	return __errno_update(ret);
}

int signal(int sig,sighandler_t func)
{
	struct sigaction old;
	struct sigaction action=
	{
		.sa_handler=func,
	};
	return sigaction(sig,&action,&old);
}

int sigprocmask(int how,const sigset_t *restrict set,sigset_t *restrict oldset)
{
	int ret=syscall3(SYS_SIGMASK,how,(uintptr_t)set,(uintptr_t)oldset);
	return __errno_update(ret);
}

int sigwaitinfo(const sigset_t* restrict set,siginfo_t* restrict info)
{
	int ret=syscall2(SYS_SIGWAIT,(uintptr_t)set,(uintptr_t)info);
	return __errno_update(ret);
}

int sigpending(sigset_t* set)
{
	int ret=syscall1(SYS_SIGPENDING,(uintptr_t)set);
	return __errno_update(ret);
}

int sigwait(const sigset_t* restrict set,int* restrict sig)
{
	siginfo_t info;
	int err=sigwaitinfo(set,&info);
	if(err==-1) return -1;
	else return info.si_signo;
}

int kill(pid_t pid,int signum)
{
	int ret=syscall2(SYS_SIGSEND,pid,signum);
	return __errno_update(ret);
}

int raise(int signum)
{
	return kill(getpid(),signum);
}

void abort(void)
{
	raise(SIGABRT);
	// TODO: at this point we should disable the SIGABRT handler and retry
	while(1);
}

static const char* sigstr[NSIG+1]=
{
	"Invalid signal",
	"Process abort signal",
	"Alarm clock",
	"Access to an undefined portion of a memory object",
	"Child process terminated",
	"Continue executing",
	"Erroneous arithmetic operation",
	"Hangup",
	"Illegal instruction",
	"Terminal interrupt signal",
	"Kill",
	"Write on a pipe with no one to read it",
	"Terminal quit signal",
	"Invalid memory reference",
	"Stop executing",
	"Termination signal",
	"Terminal stop signal",
	"Background process attempting read",
	"Background process attempting write",
	"User-defined signal 1",
	"User-defined signal 2",
	"Pollable event",
	"Profiling timer expired",
	"Bad system call",
	"Trace breakpoint",
	"High bandwitdth data available on socket",
	"Virtual timer expired",
	"CPU time limit exceeded",
	"File size limit exceeded",
};

char* strsignal(int sig)
{
	if(sig<1||sig>NSIG) return (char*)sigstr[0];
	return (char*)sigstr[sig];
}
