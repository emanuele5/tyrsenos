/*
 * Copyright 2023 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <signal.h>
#include <errno.h>

int sigaddset(sigset_t *set, int signum)
{
	if((signum<1)||(signum>31))
	{
		errno=EINVAL;
		return -1;
	}
	*set|=(1u<<signum);
	return 0;
}

int sigdelset(sigset_t *set, int signum)
{
	if((signum<1)||(signum>31))
	{
		errno=EINVAL;
		return -1;
	}
	*set&=~(1u<<signum);
	return 0;
}

int sigemptyset(sigset_t *set)
{
	*set=0;
	return 0;
}

int sigfillset(sigset_t *set)
{
	*set=~0;
	return 0;
}

int sigismember(const sigset_t *set, int signum)
{
	if((signum<1)||(signum>31))
	{
		errno=EINVAL;
		return -1;
	}
	if(*set&((sigset_t)1<<signum)) return 1;
	else return 0;
}
