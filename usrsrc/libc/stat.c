/*
 * Copyright 2023-2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <fcntl.h>				// for AT_FDCWD and AT_EMPTY_PATH
#include <sunflower/syscall.h>
#include <sys/stat.h>
#include "errno.h"

int fstatat(int fd,const char* restrict pathname,struct stat* restrict statbuf,int flags)
{
	int ret=syscall4(SYS_STAT,fd,(uintptr_t)pathname,(uintptr_t)statbuf,flags);
	return __errno_update(ret);
}

int fstat(int fd, struct stat *buf)
{
	return fstatat(fd,NULL,buf,AT_EMPTY_PATH);
}

int stat(const char* restrict pathname,struct stat* restrict statbuf)
{
	return fstatat(AT_FDCWD,pathname,statbuf,0);
}

int lstat(const char* restrict pathname,struct stat* restrict statbuf)
{
	return fstatat(AT_FDCWD,pathname,statbuf,AT_SYMLINK_NOFOLLOW);
}
