#pragma once

#include <stdbool.h>

typedef struct _IO_FILE
{
	int fd;
	bool eof;
	bool error;
	int lock;
}FILE;
