/*
 * Copyright 2020 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "stdio-internal.h"

FILE* stdin;
FILE* stdout;
FILE* stderr;

static int create_flags(const char* mode)
{
	int flags=0;
	for(unsigned i=0;i<5;i++)
	{
		// standard says that anything except [r,w,a][b,+,b+,+b] is undefined
		// this simplifies the code a lot
		switch(mode[i])
		{
		case 'r':
			flags|=O_RDONLY;
			break;
		case 'w':
			flags|=O_WRONLY|O_TRUNC|O_CREAT;
			break;
		case 'a':
			flags|=O_WRONLY|O_CREAT|O_APPEND;
			break;
		case '+':
			flags|=O_RDWR;
			break;
		case 'b':
			break;
		case 'e':
			flags|=O_CLOEXEC;
		case 0:
			return flags;
		default:
			errno=EINVAL;
			return 0;
		}
	}
	return flags;
}

FILE* fopen(const char* restrict pathname,const char* restrict mode)
{
	int open_flags=create_flags(mode);
	if(!open_flags) return NULL;

	int fd=open(pathname,open_flags,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH |S_IWOTH);
	if(fd==-1) return NULL;

	return fdopen(fd,mode);
}

FILE* fdopen(int fd,const char* mode)
{
	// TODO: check that file flags are compatible with the mode
	FILE* f=malloc(sizeof(FILE));
	f->fd=fd;
	f->eof=false;
	f->error=false;
	return f;
}

int fclose(FILE* stream)
{
	int err=close(stream->fd);
	if(err==-1) return err;

	free(stream);
	return 0;
}

int fileno(FILE* stream)
{
	return stream->fd;
}

int ferror(FILE* stream)
{
	return stream->error;
}

int feof(FILE* stream)
{
	return stream->eof;
}

void clearerr(FILE* stream)
{
	stream->eof=false;
	stream->error=false;
}

size_t fread(void* restrict ptr,size_t size,size_t n,FILE* restrict stream)
{
	size_t total=n*size;
	if(total==0) return 0;

	ssize_t err=read(stream->fd,ptr,total);
	if(err==-1)
	{
		stream->error=true;
		return 0;
	}
	else if(err<total)
	{
		stream->eof=true;
		return err/size;
	}
	else return n;
}

size_t fwrite(const void* restrict ptr,size_t size,size_t n,FILE* restrict stream)
{
	size_t total=n*size;
	if(total==0) return 0;

	ssize_t err=write(stream->fd,ptr,total);
	if(err==-1)
	{
		stream->error=true;
		return 0;
	}
	else if(err!=total)
	{
		stream->eof=true;
		return err/size;
	}
	else return n;
}
