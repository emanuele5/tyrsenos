/*
 * Copyright 2020-2021,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <ctype.h>	// for islower and isupper in strncasecmp
#include <stdlib.h>	// for malloc in strdup and strndup
#include <string.h>

void* memchr(const void* s,int c,size_t n)
{
	unsigned char ch=c;
	const unsigned char* str=s;
	for(size_t i=0;i<n;i++)
	{
		if(str[i]==ch) return (void*)(str+i);
	}
	return NULL;
}

int memcmp(const void* s1,const void* s2,size_t n)
{
	if(n==0) return 0;
	const unsigned char* m1=s1;
	const unsigned char* m2=s2;
	for(size_t i=0;i<n;i++)
	{
		if(m1[i]!=m2[i]) return m1[i]-m2[i];
	}
	return 0;
}

void* memcpy(void* restrict dest,const void* restrict src,size_t n)
{
	const char* s=src;
	char* d=dest;
	for(size_t i=0;i<n;i++) d[i]=s[i];
	return dest;
}

void* memset(void* s, int c,size_t n)
{
	char* dest=s;
	size_t i;
	for(i=0;i<n;i++) dest[i]=c;
	return s;
}

char* stpcpy(char* restrict dest,const char* restrict src)
{
	while((*dest++ = *src++));
	return dest;
}

char* stpncpy(char* restrict dest,const char* restrict src,size_t n)
{
	for(;n;n--)
	{
		if(*src==0) break;
		*dest++=*src++;
	}
	memset(dest,0,n);
	return dest;
}

int strcasecmp(const char* s1,const char* s2)
{
	while(1)
	{
		char ch1=tolower(*s1++);
		char ch2=tolower(*s2++);
		if(ch1!=ch2) return ch1-ch2;
		if(ch1==0) return 0;
	}
}

char* strcat(char* restrict dest,const char* restrict src)
{
	char* res=dest;
	while(*dest) dest++;
	while(*src)
	{
		*dest=*src;
		dest++;
		src++;
	}
	*dest=0;
	return res;
}

char* strchr(const char* s,int c)
{
	for(;*s;s++)
	{
		if(*s==c) return (char*)s;
	}
	return NULL;
}

char* strchrnul(const char* s,int c)
{
	for(;*s;s++)
	{
		if(*s==c) break;
	}
	return (char*)s;
}

int strcmp(const char* s1,const char* s2)
{
	for(size_t i=0;;i++)
	{
		if(s1[i]!=s2[i]) return s1[i]-s2[i];
		if(s1[i]==0) break;
	}
	return 0;
}

char* strcpy(char* restrict dest,const char* restrict src)
{
	stpcpy(dest,src);
	return dest;
}

size_t strcspn(const char* s,const char* reject)
{
	size_t ret=0;
	while(!strchr(reject,*s))
	{
		if(*s++==0) break;
		ret++;
	}
	return ret;
}

char* strdup(const char* s)
{
	size_t len=strlen(s)+1;
	char* dst=malloc(len);
	if(dst==NULL) return NULL;
	return memcpy(dst,s,len);
}

size_t strlen(const char* s)
{
	size_t i=0;
	while(*s++) i++;
	return i;
}

int strncasecmp(const char* s1,const char* s2,size_t n)
{
	while(n)
	{
		char ch1=tolower(*s1++);
		char ch2=tolower(*s2++);
		if(ch1!=ch2) return ch1-ch2;
		if(ch1==0) return 0;
		n--;
	}
	return 0;
}

int strncmp(const char* s1,const char* s2,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		if(s1[i]!=s2[i]) return s1[i]-s2[i];
		if(s1[i]==0) break;
	}
	return 0;
}

char* strncpy(char* restrict dest,const char* restrict src,size_t n)
{
	stpncpy(dest,src,n);
	return dest;
}

char* strndup(const char* s,size_t n)
{
	size_t len=strnlen(s,n);
	char* dst=malloc(len+1);
	if(dst==NULL) return NULL;
	memcpy(dst,s,len);
	dst[len]=0;
	return dst;
}

size_t strnlen(const char* s,size_t n)
{
	size_t i=0;
	while(*s++)
	{
		i++;
		if(i==n) return n;
	}
	return i;
}

char* strpbrk(const char *s, const char *b)
{
	s+=strcspn(s,b);
	if(*s) return (char*)s;
	else return 0;
}

char* strrchr(const char* s,int c)
{
	const char* last=NULL;
	for(;*s;s++)
	{
		if(*s==c) last=s;
	}
	return (char*)last;
}

size_t strspn(const char* s,const char* accept)
{
	size_t ret=0;
	while(strchr(accept,*s))
	{
		if(*s++==0) break;
		ret++;
	}
	return ret;
}

char* strstr(const char* s1,const char* s2)
{
	size_t len=strlen(s2);
	while(1)
	{
		s1=strchr(s1,*s2);
		if(s1==NULL) return NULL;
		if(strncmp(s1,s2,len)==0) return (char*)s1;
		else s1++;
	}
}

char *strtok(char *restrict s, const char *restrict sep)
{
	static char *p;
	if (s==NULL)
	{
		s=p;
		if(s==NULL) return NULL;
	}

	s+=strspn(s,sep);
	if(*s==0)
	{
		p=NULL;
		return p;
	}

	p=s+strcspn(s,sep);
	if(*p) *p++=0;
	else p=NULL;
	return s;
}
