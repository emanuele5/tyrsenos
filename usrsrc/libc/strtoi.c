/*
 * Copyright 2020-2023 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <ctype.h>
#include <errno.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <strings.h>

int atoi(const char *str)
{
	return strtol(str,NULL,10);
}

long atol(const char* str)
{
	return strtol(str,NULL,10);
}

long long atoll(const char* str)
{
	return strtoll(str,NULL,10);
}

double atof(const char *str)
{
	return strtod(str,NULL);
}

long strtol(const char *restrict nptr, char **restrict endptr, int base)
{
	long long res=strtoll(nptr,endptr,base);
	if(res>LONG_MAX || res<LONG_MIN)
	{
		errno=ERANGE;
		return ULONG_MAX;
	}
	return res;
}

long long strtoll(const char *restrict nptr, char **restrict endptr, int base)
{
	int c;
	int neg=0;
	// skip initial spaces
	do
	{
		c=*nptr++;
	}while(isspace(c));

	// check sign
	if(c=='-')
	{
		neg=1;
		c=*nptr++;
	}
	else if(c=='+') c=*nptr++;

	if(base>36)
	{
		errno=EINVAL;
		return 0;
	}
	
	if(base==0)
	{
		if(c=='0')
		{
			if((*nptr=='x')||(*nptr=='X'))
			{
				base=16;
				nptr++;
			}
			else base=8;
		}
		else base=10;
	}

	long long res=0;
	for(;;c=*nptr++)
	{
		if(isdigit(c)) c-='0';
		else if (isupper(c)) c-=('A'-10);
		else if (islower(c)) c-=('a'-10);
		else break;

		if(c>=base) break;
		
		res*=base;
		res+=c;
	}

	if(neg) res=-res;
	if(endptr) *endptr=(char*)(nptr-1);
	return res;
}

unsigned long strtoul(const char *restrict nptr, char **restrict endptr, int base)
{
	unsigned long long res=strtoull(nptr,endptr,base);
	if(res>ULONG_MAX)
	{
		errno=ERANGE;
		return ULONG_MAX;
	}
	return res;
}

unsigned long long strtoull(const char *restrict nptr, char **restrict endptr, int base)
{
	int c;
	int neg=0;
	// skip initial spaces
	do
	{
		c=*nptr++;
	}while(isspace(c));

	// check sign
	if(c=='-')
	{
		neg=1;
		c=*nptr++;
	}
	else if(c=='+') c=*nptr++;

	if(base>36)
	{
		errno=EINVAL;
		return 0;
	}
	
	if(base==0)
	{
		if(c=='0')
		{
			if((*nptr=='x')||(*nptr=='X'))
			{
				base=16;
				nptr++;
			}
			else base=8;
		}
		else base=10;
	}

	unsigned long long res=0;
	for(;;c=*nptr++)
	{
		if(isdigit(c)) c-='0';
		else if (isupper(c)) c-=('A'-10);
		else if (islower(c)) c-=('a'-10);
		else break;

		if(c>=base) break;
		
		res*=base;
		res+=c;
	}

	if(neg) res=-res;
	if(endptr) *endptr=(char*)(nptr-1);
	return res;
}

float strtof(const char* nptr,char** endptr)
{
	long double res=strtold(nptr,endptr);
	if(res>FLT_MAX)
	{
		errno=ERANGE;
		return HUGE_VAL;
	}
	else if(res<FLT_MIN)
	{
		errno=ERANGE;
		return -HUGE_VAL;
	}
	return res;
}

double strtod(const char* nptr,char** endptr)
{
	long double res=strtold(nptr,endptr);
	if(res>DBL_MAX)
	{
		errno=ERANGE;
		return HUGE_VAL;
	}
	else if(res<DBL_MIN)
	{
		errno=ERANGE;
		return -HUGE_VAL;
	}
	return res;
}

long double strtold(const char* nptr,char** endptr)
{
	int c;
	int neg=0;
	// skip initial spaces
	do
	{
		c=*nptr++;
	}while(isspace(c));

	// check sign
	if(c=='-')
	{
		neg=1;
		c=*nptr++;
	}
	else if(c=='+') c=*nptr++;

	if(strncasecmp(nptr,"nan",3)==0)
	{
		if(endptr) *endptr=(char*)(nptr+3);
		return NAN;
	}

	long double res=0;
	for(;;c=*nptr++)
	{
		if(isdigit(c)) c-='0';
		else break;

		res*=10;
		res+=c;
	}

	if(neg) res=-res;
	if(endptr) *endptr=(char*)(nptr-1);
	return res;
}
