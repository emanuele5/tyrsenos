/*
 * Copyright 2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <sunflower/syscall.h>
#include <time.h>

#include "errno.h"

int daylight;
long timezone=0;
char* tzname[2];

// time manipulation functions

double difftime(time_t t1,time_t t2)
{
	return t1-t2;
}

time_t time(time_t* t_ptr)
{
	struct timespec ts;
	syscall2(SYS_GETTIME,CLOCK_REALTIME,(uintptr_t)&ts);
	if(t_ptr) *t_ptr=ts.tv_sec;
	return ts.tv_sec;
}

clock_t clock(void)
{
	struct timespec ts;
	// TODO: should actually use a per-process clock, but first I need to implement it
	syscall2(SYS_GETTIME,CLOCK_MONOTONIC,(uintptr_t)&ts);
	return ts.tv_sec*CLOCKS_PER_SEC + ts.tv_nsec/(1000000000/CLOCKS_PER_SEC);
}

// posix clock functions

int clock_gettime(clockid_t clockid,struct timespec* ts)
{
	int err=syscall2(SYS_GETTIME,CLOCK_REALTIME,(uintptr_t)ts);
	return __errno_update(err);
}

int clock_settime(clockid_t clockid,const struct timespec* ts)
{
	int err=syscall2(SYS_SETTIME,CLOCK_REALTIME,(uintptr_t)ts);
	return __errno_update(err);
}

// format conversion functions

char* asctime_r(const struct tm* restrict tm_ptr,char* restrict buf)
{
	static char wday_name[7][3]=
    {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static char mon_name[12][3]=
    {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    // TODO: use snprintf and warn instead of UB?
	sprintf(buf,"%.3s %.3s%3d %.2d:%.2d:%.2d %d\n",
		wday_name[tm_ptr->tm_wday],
		mon_name[tm_ptr->tm_mon],
		tm_ptr->tm_mday,tm_ptr->tm_hour,
		tm_ptr->tm_min,tm_ptr->tm_sec,1900+tm_ptr->tm_year);
	return buf;
}

char* asctime(const struct tm* tm_ptr)
{
	static char result[26];
	return asctime_r(tm_ptr,result);
}

char* ctime(const time_t* t_ptr)
{
	const struct tm* tm_ptr=localtime(t_ptr);
	if(tm_ptr==NULL) return NULL;
	
	return asctime(tm_ptr);
}

static const unsigned days_per_month[12]={31,28,31,30,31,30,31,31,30,31,30,31};

static const unsigned secs_per_minute=60;
static const unsigned secs_per_hour=60*secs_per_minute;
static const unsigned secs_per_day=24*secs_per_hour;

size_t strftime(char* restrict s,size_t maxsize,const char* restrict format,const struct tm* restrict tm_ptr)
{
	errno=ENOSYS;
	return 0;
}

static bool is_leap(unsigned year)
{
	return (year%4==0 && year%100!=0) || year%400==0;
}

static time_t secs_per_year(unsigned year)
{
	if(is_leap(year)) return secs_per_day*366;
	else return secs_per_day*365;
}

struct tm* gmtime(const time_t* t_ptr)
{
	static struct tm result;
	time_t seconds=*t_ptr;

	// calculate year
	int year=1970;
	while(seconds>=secs_per_year(year))
	{
		seconds-=secs_per_year(year);
		year++;
	}
	result.tm_year=year-1900;
	// calculate day in year
	unsigned day=seconds/secs_per_day;
	result.tm_yday=day;
	// calculate hour, minute and second
	seconds%=secs_per_day;
	result.tm_hour=seconds/secs_per_hour;
	seconds%=secs_per_hour;
	result.tm_min=seconds/secs_per_minute;
	result.tm_sec=seconds%secs_per_minute;
	// calculate month and day in month
	unsigned month=0;
	while(1)
	{
		unsigned days_in_month=days_per_month[month];
		// handle 29 days february
		if(month==1 && is_leap(year)) days_in_month++;
		
		if(day<days_in_month) break;
		day-=days_in_month;
		month++;
	}
	result.tm_mon=month+1;
	result.tm_mday=day+1;
	// calculate day of the week (1/1/1970 was a thursday)
	result.tm_wday=(4+*t_ptr/secs_per_day)%7;
	
	result.tm_isdst=0;
	return &result;
}

struct tm* localtime(const time_t* t_ptr)
{
	// this is buggy as f*ck, but will work for most common timezones
	// TODO: we don't actually support timezones anyways
	time_t t=*t_ptr-timezone;
	return gmtime(&t);
}
