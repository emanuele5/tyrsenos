/*
 * Copyright 2020, 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

char* tmpnam(char* s)
{
	const char validchars[]="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	static char name[L_tmpnam]="/tmp/tmp_";

	// try TMP_MAX times to generate an unique filename
	for(int t=0;t<TMP_MAX;t++)
	{
		// generate the random part of the name
		for(int i=9;i<15;i++)
		{
			unsigned r=rand();
			name[i]=validchars[r%62];
		}

		struct stat st;
		if(stat(name,&st)==-1)
		{
			if(errno==ENOENT)
			{
				// file doesn't exist, we can return it
				if(s)
				{
					strcpy(s,name);
					return s;
				}
				else return name;
			}
		}
	}
	return NULL;
}

FILE* tmpfile(void)
{
	static char name[L_tmpnam];
	if(tmpnam(name)==NULL) return NULL;

	return fopen(name,"wb+");
}
