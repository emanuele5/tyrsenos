/*
 * Copyright 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <fcntl.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sunflower/syscall.h>

#include "errno.h"

// read and write

ssize_t read(int fd,void *buf,size_t count)
{
	ssize_t ret=syscall3(SYS_READ,fd,(uintptr_t)buf,count);
	return __errno_update(ret);
}

ssize_t pread(int fd,void *buf,size_t count,off_t pos)
{
	ssize_t ret=syscall4(SYS_PREAD,fd,(uintptr_t)buf,count,pos);
	return __errno_update(ret);
}

ssize_t write(int fd,const void *buf,size_t count)
{
	ssize_t ret=syscall3(SYS_WRITE,fd,(uintptr_t)buf,count);
	return __errno_update(ret);
}

ssize_t pwrite(int fd,const void *buf,size_t count,off_t pos)
{
	ssize_t ret=syscall4(SYS_PWRITE,fd,(uintptr_t)buf,count,pos);
	return __errno_update(ret);
}

off_t lseek(int fd,off_t offset,int whence)
{
	off_t ret=syscall3(SYS_SEEK,fd,offset,whence);
	return __errno_update(ret);
}

// access variants

int faccesat(int fd,const char* path,int mode,int flags)
{
	int ret=syscall4(SYS_ACCESS,fd,(uintptr_t)path,mode,flags);
	return __errno_update(ret);
}

int access(const char* path, int mode)
{
	return faccesat(AT_FDCWD,path,mode,0);
}

// chown variants

int fchownat(int fd,const char* path,uid_t uid,gid_t gid,int flags)
{
	int ret=syscall5(SYS_CHOWN,AT_FDCWD,(uintptr_t)path,uid,gid,flags);
	return __errno_update(ret);
}

int chown(const char* path,uid_t uid,gid_t gid)
{
	return fchownat(AT_FDCWD,path,uid,gid,0);
}

int lchown(const char* path,uid_t uid,gid_t gid)
{
	return fchownat(AT_FDCWD,path,uid,gid,AT_SYMLINK_NOFOLLOW);
}

int fchown(int fd,uid_t uid,gid_t gid)
{
	return fchownat(fd,NULL,uid,gid,AT_EMPTY_PATH);
}

// chdir variants

int chdir(const char *path)
{
	int ret=syscall3(SYS_CHDIR,AT_FDCWD,(uintptr_t)path,0);
	return __errno_update(ret);
}

int fchdir(int fd)
{
	int ret=syscall3(SYS_CHDIR,fd,(uintptr_t)NULL,AT_EMPTY_PATH);
	return __errno_update(ret);
}

// dup and fcntl

int dup(int fd)
{
	return fcntl(fd,F_DUPFD,0);
}

int dup2(int oldfd,int newfd)
{
	int ret=syscall2(SYS_DUP,oldfd,newfd);
	return __errno_update(ret);
}

int fcntl(int fd,int cmd,...)
{
	va_list v;
	va_start(v,cmd);
	int ret=syscall3(SYS_FCNTL,fd,cmd,va_arg(v,int));
	va_end(v);
	return __errno_update(ret);
}

// sync

int sync(void)
{
	int ret=syscall0(SYS_SYNC);
	return __errno_update(ret);
}
