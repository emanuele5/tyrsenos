/*
 * Copyright 2020,2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include "errno.h"
#include <sunflower/syscall.h>
#include <sys/types.h>

pid_t waitpid(pid_t pid,int* wstatus,int options)
{
	pid_t ret=syscall3(SYS_WAITCHILD,pid,(uintptr_t)wstatus,options);
	return __errno_update(ret);
}

pid_t wait(int* wstatus)
{
	return waitpid(-1,wstatus,0);
}
