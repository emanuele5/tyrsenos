/*
 * Copyright 2020, 2024 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <errno.h>
#include <limits.h>	// for MB_LEN_MAX
#include <stdlib.h>
#include <stdio.h>	// for EOF
#include <wchar.h>

//
// MB TO WC
//

wint_t btowc(int ch)
{
	if(ch>=128 || ch<0) return EOF;
	else return ch;
}

int mblen(const char* s, size_t n)
{
	return mbtowc(NULL,s,n);
}

size_t mbrtowc(wchar_t* restrict pwc,const char* restrict s,size_t n,mbstate_t* restrict state)
{
	static mbstate_t static_state;
	if(state==NULL) state=&static_state;
	if(s==NULL)
	{
		s="";
		n=1;
	}

	size_t i;
	for(i=0;!(i && state->count==0);i++)
	{
		// characther too big for the buufer
		if(i>=n) return (size_t) -2;

		unsigned char ch=s[i];
		
		// handle first byte, which also gives us the length
		if(state->count==0)
		{
			// 0x00 to 0x7f: ASCII compatible region
			if(ch<0b10000000)
			{
				state->length=1;
				state->wch=ch;
			}
			// 0x80 to 0xbf: invalid in the first byte
			else if(ch<0b11000000) goto invalidseq;
			// 0xc0 to 0xdf: 5 bits from this byte +1 byte
			else if(ch<0b11100000)
			{
				state->count=1;
				state->length=2;
				state->wch=ch&0x1f;
			}
			// 0xe0 to 0xef: 4 bits from this byte +2 bytes
			else if(ch<0b11110000)
			{
				state->count=2;
				state->length=3;
				state->wch=ch&0x0f;
			}
			// 0xf0 t0 0xf7: 3 bits from this byte +3 bytes
			else if(ch<0b11111000)
			{
				state->count=3;
				state->length=4;
				state->wch=ch&0x07;
			}
			else goto invalidseq;
			// reject overlong encoding
			if(state->wch==0 && ch!=0) goto invalidseq;
		}
		// handle following bytes
		else
		{
			// following bytes must be in the form 0b10xxxxxx
			if((ch&0b11000000)!=0b10000000) goto invalidseq;
			ch&=0x3f;
			// reject overlong encoding
			if(ch==0) goto invalidseq;
			state->wch<<=6;
			state->wch|=ch;
			state->count--;
		}
	}

	wchar_t result=state->wch;
	if(pwc!=NULL) *pwc=result;

	state->length=0;
	state->wch=0;
	
	if(result!=0) return i;
	else return 0;
	
	invalidseq:
	errno=EILSEQ;
	return -1;
}

int mbtowc(wchar_t* restrict pwc,const char* restrict s,size_t n)
{
	if(s==NULL) return 0;
	unsigned byte_count=0;
	wchar_t wch=0;

	// read the first byte
	unsigned char ch=s[0];
	// 0x00 to 0x7f: ASCII compatible region
	if(ch<0b10000000) byte_count=1;
	// 0x80 to 0xbf: invalid in the first byte
	else if(ch<0b11000000) goto invalidseq;
	// 0xc0 to 0xdf: 5 bits from this byte +1 byte
	else if(ch<0b11100000)
	{
		byte_count=2;
		wch=ch&0x1f;
	}
	// 0xe0 to 0xef: 4 bits from this byte +2 bytes
	else if(ch<0b11110000)
	{
		byte_count=3;
		wch=ch&0x0f;
	}
	// 0xf0 t0 0xf7: 3 bits from this byte +3 bytes
	else if(ch<0b11111000)
	{
		byte_count=4;
		wch=ch&0x07;
	}
	else goto invalidseq;
	// reject overlong encoding
	if(wch==0 && ch!=0) goto invalidseq;

	if(byte_count>n) goto invalidseq;

	wch=ch;
	// now read the additional bytes
	for(unsigned i=1;i<byte_count;i++)
	{
		ch=s[i];
		// following bytes must be in the form 0b10xxxxxx
		if((ch&0b11000000)!=0b10000000) goto invalidseq;
		ch&=0x3f;
		// reject overlong encoding
		if(ch==0) goto invalidseq;
		wch<<=6;
		wch|=ch;
	}

	if(pwc!=NULL) *pwc=wch;
	return byte_count;

	invalidseq:
	errno=EILSEQ;
	return -1;
}

size_t mbsnrtowcs(wchar_t* restrict dst,const char** restrict src_ptr,size_t src_len,size_t dst_len,mbstate_t* restrict state)
{
	static mbstate_t static_state;
	if(state==NULL) state=&static_state;

	const char* src=*src_ptr;

	size_t dst_pos=0;
	size_t src_pos=0;
	while((dst==NULL || dst_pos<dst_len) && src_pos<src_len )
	{
		mbstate_t state_copy = *state;
		wchar_t wch;
		size_t len=mbrtowc(&wch,src+src_pos,src_len-src_pos,state);

		// decoding error
		if(len==(size_t)-1)
		{
			 *src_ptr=src+src_pos;
			 return -1;
		}
		// partial character
		if(len==(size_t) -2 )
		{
			*state=state_copy;
			break;
		}

		if(dst) dst[dst_pos]=wch;

		// null character, stop
		if(wch==0)
		{
			src=NULL;
			src_pos=0;
			break;
		}
		dst_pos++;
		src_pos+=len;
	}
	*src_ptr=src+src_pos;
	return dst_pos;
}

size_t mbsrtowcs(wchar_t* restrict dst,const char** restrict src_ptr,size_t dst_len,mbstate_t* restrict state)
{
	return mbsnrtowcs(dst,src_ptr,SIZE_MAX,dst_len,state);
}

size_t mbstowcs(wchar_t *restrict wcs,const char *restrict s,size_t n)
{
	return mbsrtowcs(wcs,(void*)&s,n,NULL);
}

//
// WC TO MB
//

int wctob(wint_t ch)
{
	if(ch>=128 || ch<0) return EOF;
	else return ch;
}

size_t wcrtomb(char* restrict s,wchar_t wch,mbstate_t* restrict state)
{
	if(s==NULL) return 1; // length of '\0'
	
	unsigned byte_count;
	if(wch<0x80)
	{
		s[0]=wch;
		byte_count=1;
	}
	else if(wch<0x800)
	{
		s[0]=0b11000000|(wch&0x1f);
		wch>>=5;
		byte_count=2;
	}
	else if(wch<0x10000)
	{
		s[0]=0b11100000|(wch&0x0f);
		wch>>=4;
		byte_count=3;
	}
	else if(wch<0x110000)
	{
		s[0]=0b11110000|(wch&0x07);
		wch>>=3;
		byte_count=4;
	}
	else
	{
		errno=EILSEQ;
		return -1;
	}

	for(unsigned i=1;i<byte_count;i++)
	{
		s[i]=0b10000000|(wch&0x3f);
		wch>>=6;
	}
	return byte_count;
}

int wctomb(char* s,wchar_t wch)
{
	if(s==NULL) return 0;
	else return wcrtomb(s,wch,NULL);
}

size_t wcsrtombs(char* restrict dest,const wchar_t** restrict src_ptr,size_t n,mbstate_t* restrict state)
{
	char buf[MB_LEN_MAX];
	if(dest==NULL)
	{
		n=0;
		// if dest is NULL just count the multibyte length
		for(const wchar_t* wcs=*src_ptr;*wcs!=0;wcs++)
		{
			int charlen=wcrtomb(buf,*wcs,NULL);
			if(charlen==-1) return -1;
			n+=charlen;
		}
		return n;
	}
	else
	{
		size_t max=n;
		while(n>=MB_LEN_MAX)
		{
			int len=wcrtomb(dest,**src_ptr,NULL);
			if(len==-1) return -1;
			if(**src_ptr==0)
			{
				*src_ptr=NULL;
				return max-n;
			}
			dest+=len;
			n-=len;
			(*src_ptr)++;
		}
		while(n)
		{
			// convert the wchar in a temporary buffer
			int len=wcrtomb(buf,**src_ptr,NULL);
			if(len==-1) return -1;
			// copy only if it can fit
			if(len>n) return max-n;
			wcrtomb(dest,**src_ptr,NULL);
			if(**src_ptr==0)
			{
				*src_ptr=NULL;
				return max-n;
			}
			dest+=len;
			n-=len;
			(*src_ptr)++;
		}
		return max;
	}
}

size_t wcstombs(char* restrict s,const wchar_t* restrict wcs,size_t n)
{
	const wchar_t** wcsptr=(const wchar_t**)&wcs;
	return wcsrtombs(s,wcsptr,n,NULL);
}
