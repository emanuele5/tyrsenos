/*
 * Copyright 2021,2024-2025 Emanuele Davalli <emanuele@davalli.it>
 * Released under the terms of the GNU General Public License version 3
 */

#include <stdlib.h>	// for malloc
#include <wchar.h>

wchar_t* wcpcpy(wchar_t* restrict dest,const wchar_t* restrict src)
{
	while((*dest++ = *src++));
	return dest;
}

wchar_t* wcpncpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n)
{
	for(;n;n--)
	{
		if((*dest++=*src++)==0) break;
	}
	wmemset(dest,0,n);
	return dest;
}

wchar_t* wcscat(wchar_t* restrict dest,const wchar_t* restrict src)
{
	wchar_t* res=dest;
	while(*dest) dest++;
	while(*src)
	{
		*dest=*src;
		dest++;
		src++;
	}
	*dest=0;
	return res;
}

wchar_t* wcschr(const wchar_t* wcs,wchar_t wc)
{
	for(;*wcs;wcs++)
	{
		if(*wcs==wc) return (wchar_t*)wcs;
	}
	return NULL;
}

int wcscmp(const wchar_t* s1,const wchar_t* s2)
{
	for(size_t i=0;;i++)
	{
		if(s1[i]!=s2[i]) return s1[i]-s2[i];
		if(s1[i]==0) break;
	}
	return 0;
}

wchar_t* wcscpy(wchar_t* restrict dest,const wchar_t* restrict src)
{
	wcpcpy(dest,src);
	return dest;
}

size_t wcscspn(const wchar_t* s,const wchar_t* reject)
{
	size_t ret=0;
	while(!wcschr(reject,*s))
	{
		if(*s++==0) break;
		ret++;
	}
	return ret;
}

wchar_t* wcsdup(const wchar_t* wcs)
{
	size_t len=wcslen(wcs)+1;
	wchar_t* dst=malloc(len*sizeof(wchar_t));
	if(dst==NULL) return NULL;
	return wmemcpy(dst,wcs,len);
}

size_t wcslen(const wchar_t* wcs)
{
	size_t i=0;
	while(*wcs++) i++;
	return i;
}

int wcsncmp(const wchar_t* s1,const wchar_t* s2,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		if(s1[i]!=s2[i]) return s1[i]-s2[i];
		if(s1[i]==0) break;
	}
	return 0;
}

wchar_t* wcsncpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n)
{
	wcpncpy(dest,src,n);
	return dest;
}

size_t wcsnlen(const wchar_t* wcs,size_t n)
{
	size_t i=0;
	while(*wcs++)
	{
		i++;
		if(i==n) return n;
	}
	return i;
}

wchar_t* wcsrchr(const wchar_t* s,wchar_t c)
{
	const wchar_t* last=NULL;
	for(;*s;s++)
	{
		if(*s==c) last=s;
	}
	return (wchar_t*)last;
}

size_t wcsspn(const wchar_t* s,const wchar_t* accept)
{
	size_t ret=0;
	while(wcschr(accept,*s))
	{
		if(*s++==0) break;
		ret++;
	}
	return ret;
}

wchar_t* wcsstr(const wchar_t* s1,const wchar_t* s2)
{
	size_t len=wcslen(s2);
	while(1)
	{
		s1=wcschr(s1,*s2);
		if(s1==NULL) return NULL;
		if(wcsncmp(s1,s2,len)==0) return (wchar_t*)s1;
		else s1++;
	}
}

wchar_t* wmemchr(const wchar_t* ws,wchar_t wc,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		if(ws[i]==wc) return (wchar_t*)(ws+i);
	}
	return NULL;
}

int wmemcmp(const wchar_t* ws1,const wchar_t* ws2,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		if(ws1[i]!=ws2[i]) return ws1[i]-ws2[i];
	}
	return 0;
}

wchar_t* wmemcpy(wchar_t* restrict dest,const wchar_t* restrict src,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		dest[i]=src[i];
	}
	return dest;
}

wchar_t* wmemset(wchar_t* ws,wchar_t wc,size_t n)
{
	for(size_t i=0;i<n;i++)
	{
		ws[i]=wc;
	}
	return ws;
}
